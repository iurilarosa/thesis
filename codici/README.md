# FrequencyHough on TensorFlow

The FrequencyHough analysis pipeline ([Astone, P. et al 2014](https://arxiv.org/abs/1407.8333)) has at its core an implementation of the so called Hough transform ([Hough, P. V. C., 1962](https://www.google.com/patents/US3069654), [Antonucci, F. et al., 2008](https://arxiv.org/abs/0807.5065)), which represents one of the heaviest part of the analysis both from the point of view of the computational effort and memory usage. The fresh first stable release of the TensorFlow framework, joint with the high computational power of the modern Graphic Processing Units, suggested to develop an implementation of the FrequencyHough algorithm using TensorFlow and able to run on GPUs. We present here the developed code and the results of the tests done so far, showing that the new code can reach a reduction of computation time of one or two order of magnitudes, depending on the devices used.

The code of the Tensorflow FrequencyHough is in the subfolder `./software/only hough`.
To run the code on a GPU you need a Nvidia GPU with CUDA support, otherwise you can run it also on CPU. You'll need the following prerequisite installed:

* `python` or `python3`
* `numpy`, `matplotlib`, `scipy`
* `tensorflow-gpu` (with CUDA GPU support) or `tensorflow` (without GPU support)

You can use `pip --user --upgrade packageName` (`pip3 --user --upgrade packageName`) to install the Python 2 (Python 3) packages.

To execute the code you can follow these steps:

1. download `main.py` and `inputfile.mat` from [./software/only hough](https://svn.ligo.caltech.edu/svn/cw/docs//MethodsPapers/FrequencyHoughTensorFlow/software/only hough)
2. with both files in the same folder, run in the terminal `python main.py` or `python3 main.py`
3. enjoy the plot

You can also check the jupyter notebook: it is setted up to do some comparisons between Hough maps generated by this code and maps generated by the original MATLAB code. In the `only hough` folder there is also the MATLAB code in a `.m` file.

The data input files are in the `./software/data` folder, to perform comparison you can run the code in the notebook or in the Matlab script simply changing the input file address. The last section of the notebook is dedicated to checks and comparisons.

To use the notebook you need to install the `jupyter` package, then run the `jupyter-notebook` command and navigate to the folder where the notebook is. Once opened the boxes with code are runnable pressing `CTRL+Enter` or `SHIFT+Enter` on the keyboard.

To run the Matlab code you need to download the SNAG toolbox from [this link](http://www.roma1.infn.it/~frasca/snag/default.htm) and to add the path to your Matlab.
