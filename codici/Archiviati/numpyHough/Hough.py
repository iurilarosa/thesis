import scipy.io
import pandas
import numpy
import os
from matplotlib import pyplot
from scipy import sparse
import multiprocessing
import timeit

#carico file dati
percorsoFile = "/home/protoss/Documenti/TESI/DATI/peaks.mat"

#print(picchi.shape)
#picchi[0]
#nb: picchi ha 0-tempi
#              1-frequenze
#              4-pesi
#ora popolo il dataframe

tabella = pandas.DataFrame(scipy.io.loadmat(percorsoFile)['PEAKS'])
tabella.drop(tabella.columns[[2, 3]], axis = 1, inplace=True)
tabella.columns = ["tempi", "frequenze","pesi"]

#fascia di sicurezza
securbelt = 4000

headerFreq= scipy.io.loadmat(percorsoFile)['hm_job'][0,0]['fr'][0]
headerSpindown = scipy.io.loadmat(percorsoFile)['hm_job'][0,0]['sd'][0]

#nb: headerFreq ha 0- freq minima,
#                  1- step frequenza, 
#                  2- enhancement in risoluzone freq, 
#                  3- freq massima, 
#headerSpindown ha 0- spin down iniziale di pulsar
#                  1- step spindown
#                  2- numero di step di spindown
#Definisco relative variabili per comodita' e chiarezza del codice

#frequenze
minFreq = headerFreq[0]
maxFreq = headerFreq[3]
enhancement = headerFreq[2]
stepFrequenza = headerFreq[1]
stepFreqRaffinato = stepFrequenza/enhancement

freqIniz = minFreq- stepFrequenza/2 - stepFreqRaffinato
freqFin = maxFreq + stepFrequenza/2 + stepFreqRaffinato
nstepFrequenze = numpy.ceil((freqFin-freqIniz)/stepFreqRaffinato)+securbelt

#spindown
spindownIniz = headerSpindown[0]
stepSpindown = headerSpindown[1]
nstepSpindown = headerSpindown[2].astype(int)


# riarrangio gli array in modo che abbia i dati 
# nel formato che voglio io
frequenze = tabella['frequenze'].values
frequenze = ((frequenze-freqIniz)/stepFreqRaffinato)-round(enhancement/2+0.001)

tempi = tabella['tempi'].values
tempi = ((tempi)*3600*24/stepFreqRaffinato)+1
tempi = tempi - numpy.amin(tempi)+1
#tempi = tempi.astype(int)

pesi = tabella['pesi'].values

#%reset_selective tabella
start_time = timeit.default_timer()

nstepSpindown = 300
spindowns = numpy.arange(1, nstepSpindown+1)
spindowns = numpy.multiply(spindowns,stepSpindown)
spindowns = numpy.add(spindowns, spindownIniz)
# cosi' ho i tre array delle tre grandezze


nRows = nstepSpindown
nColumns = nstepFrequenze.astype(int)
fakeRow = numpy.zeros(frequenze.size)

def itermatrix(stepIesimo):
    sdPerTempo = spindowns[stepIesimo]*tempi
    appoggio = numpy.round(frequenze-sdPerTempo+securbelt/2).astype(int)
   
    matrix = sparse.coo_matrix((pesi, (fakeRow, appoggio))).todense()
    matrix = numpy.ravel(matrix)
    missColumns = (nColumns-matrix.size)
    zeros = numpy.zeros(missColumns)
    matrix = numpy.concatenate((matrix, zeros))
    return matrix

pool = multiprocessing.Pool()
imageMapped = list(pool.map(itermatrix, range(nstepSpindown)))
pool.close()
imageMapped = numpy.array(imageMapped)
print(timeit.default_timer() - start_time)
ncolonne = nstepFrequenze.astype(int)
#pyplot.figure(figsize=(120, 36))
a = pyplot.imshow(imageMapped[:,3400:ncolonne-1500], aspect = 40)
pyplot.colorbar(shrink = 0.5,aspect = 10)
