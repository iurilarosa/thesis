freq1 = 10:128;
freq2 = 127:5:2048;

freq = horzcat(freq1, freq2);
numfreqs = length(freq);

grigliasizes = zeros(1,length(freq));

a= 0;
for i = 1:length(freq)
	
	freqmax = freq(i);
	
	if freqmax <= 128
		lenfft = 8192;
	end

	if freqmax > 128 && freqmax < 517
		lenfft = 4096;
	end

	if freqmax > 512 && freqmax < 1024
		lenfft = 2048;
	end

	if freqmax > 1024
		lenfft = 1024;
	end
	
	%dopplerNum = 0.0002*lenfft*freqmax; 
	dopplerNum = 0.000106*lenfft*freqmax; %DA MOTIVARE BENE QUESTA FORMULA E PERCHÉ 0.0001 E NON 0.0002
	griglia = pss_optmap(dopplerNum);
	
	[grigliasizes(i),a] = size(griglia);
end 

size(grigliasizes)

save /home/iuri.larosa/check/sizes grigliasizes
