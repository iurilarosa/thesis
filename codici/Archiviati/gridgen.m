tFft = 8192;
freqMax = 93;

doppnum = 0.000106*tFft*freqMax;


grigliaECL = pss_optmap(doppnum);
alpha = grigliaECL(:,1);
delta = grigliaECL(:,2);

a = size(grigliaECL)
sizeArray = a(1);

grigliaREC = zeros(sizeArray,3);
for i = 1:sizeArray 
	grigliaREC(i,:) = astro2rect([alpha(i),delta(i)],0); 
	end

griglia = struct;
griglia.ECL = grigliaECL;
griglia.REC = grigliaREC;
griglia

save /home/iuri.larosa/check/8192_MATgrid griglia
