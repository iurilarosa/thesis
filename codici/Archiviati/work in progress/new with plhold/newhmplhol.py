import tensorflow as tf
import numpy
import scipy.io
import time

sessione = tf.Session()

#---------------------------------------
# defining parameters                   |
#---------------------------------------
percorsoDati = "/home/protoss/Documenti/TESI/wn100bkp/data/dati9mesi52HWI.mat"

#times
PAR_tFft = 8192
PAR_tObs = 9 #mesi
PAR_tObs = PAR_tObs*30*24*60*60
PAR_epoch = (57722+57990)/2 

#frequencies
PAR_enhance = 10
PAR_stepFreq = 1/PAR_tFft
PAR_refinedStepFreq =  PAR_stepFreq/PAR_enhance

#spindowns
PAR_fdotMin = -1e-9
PAR_fdotMax = 1e-10
PAR_stepFdot = PAR_stepFreq/PAR_tObs
PAR_nstepFdot = numpy.round((PAR_fdotMax-PAR_fdotMin)/PAR_stepFdot).astype(numpy.int32)

#others
PAR_secbelt = 4000


#---------------------------------------
# loading and managing data             |
#---------------------------------------
struttura = scipy.io.loadmat(percorsoDati)['job_pack_0']

#times
tempi = struttura['peaks'][0,0][0]
tempi = tempi-PAR_epoch
tempi = ((tempi)*60*60*24/PAR_refinedStepFreq)

#frequencies
frequenze = struttura['peaks'][0,0][1]
freqMin = numpy.amin(frequenze)
freqMax = numpy.amax(frequenze)
freqIniz = freqMin- PAR_stepFreq/2 - PAR_refinedStepFreq
freqFin = freqMax + PAR_stepFreq/2 + PAR_refinedStepFreq
nstepFrequenze = numpy.ceil((freqFin-freqIniz)/PAR_refinedStepFreq)+PAR_secbelt
frequenze = frequenze-freqIniz
frequenze = (frequenze/PAR_refinedStepFreq)-round(PAR_enhance/2+0.001)
 
#spindowns
spindowns = numpy.arange(0, PAR_nstepFdot)
spindowns = numpy.multiply(spindowns,PAR_stepFdot)
spindowns = numpy.add(spindowns, PAR_fdotMin)

#others
pesi = (struttura['peaks'][0,0][4]+1)

peakmap = numpy.stack((tempi,frequenze,pesi),1).astype(numpy.float32)
print(peakmap.shape)
spindowns = spindowns.astype(numpy.float32)
nRows = numpy.int32(PAR_nstepFdot)
nColumns = numpy.int32(nstepFrequenze)

#---------------------------------------
# defining TensorFlow graph             |
#---------------------------------------

def mapnonVar(stepIesimo):
    sdTimed = tf.multiply(spindownsTF[stepIesimo], tempiTF, name = "Tdotpert")
    
    appoggio = tf.round(frequenzeTF-sdTimed+PAR_secbeltTF/2, name = "appoggioperindici")
    appoggio = tf.cast(appoggio, dtype=tf.int32)
    
    valori = tf.unsorted_segment_sum(pesiTF, appoggio, nColumns)
    return valori

PAR_secbeltTF = tf.constant(4000,dtype = tf.float32, name = 'secur')
tempiTF = tf.placeholder(tf.float32, name = 't')
frequenzeTF = tf.placeholder(tf.float32, name = 'f')
pesiTF = tf.placeholder(tf.float32, name = 'w')
spindownsTF = tf.placeholder(tf.float32, name = 'sd')


#pesiTF = tf.reshape(pesiTF,(1,tf.size(pesi)))
#pesiTF = pesiTF[0]


houghLeft = tf.map_fn(mapnonVar, tf.range(0, nRows), dtype=tf.float32, parallel_iterations=8)
houghRight = houghLeft[:,PAR_enhance:nColumns]-houghLeft[:,0:nColumns - PAR_enhance]
houghDiff = tf.concat([houghLeft[:,0:PAR_enhance],houghRight],1)
houghMap = tf.cumsum(houghDiff, axis = 1)

#----------------------------------------
# feeding and running                   |
#----------------------------------------

dizionario = { 
               tempiTF     : tempi,
               frequenzeTF : frequenze,
               pesiTF      : pesi,
               spindownsTF : spindowns
             }

start = time.time()
image = sessione.run(houghMap, feed_dict = dizionario)
end = time.time()
print(end-start)
from matplotlib import pyplot
a = pyplot.imshow(image, aspect = 200)
pyplot.show()
