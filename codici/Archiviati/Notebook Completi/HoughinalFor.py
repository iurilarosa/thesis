import tensorflow as tf
import numpy
import scipy.io
from tensorflow.python.client import timeline
import time

sessione = tf.Session()

# CARICO DATI

tFft = 8192
#tFft = 4096
tObs = 9 #mesi
tObs = tObs*30*24*60*60
nPunti = 2
cands = 100
primaFreq = 108

percorsoDati = "dati/dati9mesi108HWI.mat"
percorsoQuad = "quadHWI108.mat"
percorsoPatch = "quadHWI108Ecl.mat"
#percorsoDati = "dati/dati9mesi108HWI.mat"
#percorsoQuad = ("quad%dLIL.mat" % tFft)
#percorsoPatch = ("quad%dEclNew.mat" % tFft)





#carico file dati
quadrato = scipy.io.loadmat(percorsoQuad)['quad'].astype(numpy.float32)
patch = scipy.io.loadmat(percorsoPatch)['quadratoEclNew'].astype(numpy.float32)
struttura = scipy.io.loadmat(percorsoDati)['job_pack_0']
tempi = struttura['peaks'][0,0][0].astype(numpy.float32)
frequenze = struttura['peaks'][0,0][1].astype(numpy.float32)
pesi = (struttura['peaks'][0,0][4]+1).astype(numpy.float32)

print(tempi.size,frequenze.size)

#nb: picchi ha 0-tempi
#              1-frequenze
#              2-pesi

#headers vari
securbelt = 4000
#securbelt = 4000*3

#frequenze
stepFreq = 1/tFft
enhancement = 10
stepFreqRaffinato =  stepFreq/enhancement
nstepsFreq = securbelt+(numpy.amax(frequenze)-numpy.amin(frequenze) + stepFreq + 2*stepFreqRaffinato)/stepFreqRaffinato


#tempi
#epoca definita come mediana di tempi di tutto il run #WARNING da ridefinire con durata dati che prendo
epoca = (57722+57874)/2

#spindowns
spindownMin = -1e-9
spindownMax = 1e-10
stepSpindown = stepFreq/tObs 

nstepSpindown = numpy.round((spindownMax-spindownMin)/stepSpindown).astype(numpy.int32)
spindowns = numpy.arange(0, nstepSpindown)
spindowns = numpy.multiply(spindowns,stepSpindown)
spindowns = numpy.add(spindowns, spindownMin)

#LO STEP DI SPINDOWN LO SI DEFINISCE TRAMITE LA BANDA IN FREQUENZA DELLA PEAKMAP (in hz) 
#SU TEMPO DI OSSERVAZIONE (in sec):
#STEPFREQ/TOBS! DA QUI SCEGLIAMO QUALE SD MASSIMO E MINIMO TENERE

# COME VALORE MASSIMO TENERE SUI 1*-10^-9
# COME MASSIMO ANCHE CIRCA 1*+10^-10
# CONSIDERARE EVENTUALE SORGENTE NELLA SCELTA DI INTERVALLO SPINDOWN

#per doppler corr
veloc = struttura['basic_info'][0,0]['velpos'][0,0][0:3,:].astype(numpy.float32)
nTempi = struttura['basic_info'][0,0]['ntim'][0,0][0,0]
primoTempo = struttura['basic_info'][0,0]['tim0'][0,0][0,0]
indices = struttura['basic_info'][0,0]['index'][0,0][0]

#SPLITTO DOPP CORR E HOUGH TRANS

# PREPARO FUNZIONI

#TODO STUDIARE FATTIBILITÀ DI USARE TUTTO A 32BYTES

# calcola la correzione doppler per ogni punto del cielo
def doppcorr(i):
	quadratoNP = quadrato[i]
	indicesOpt = indices-1
	inizi = indicesOpt[:-1]
	fini = indicesOpt[1:]

	velocitas = numpy.zeros((3,frequenze.size))
	for i in numpy.arange(0,nTempi-1):
		velocitas[:,inizi[i]:fini[i]+1] = veloc[:,i:i+1]
		
	velPerPosIndex = numpy.dot(quadratoNP,velocitas)
	divisoreIndex = 1+velPerPosIndex
	freqCorr = frequenze / divisoreIndex
	print(freqCorr)
	
	#mi ricavo l'header per le frequenze
	freqMin = numpy.amin(freqCorr)
	#freqMax = tf.reduce_max(freqCorr)
	freqIniz = freqMin- stepFreq/2 - stepFreqRaffinato
	#freqFin = freqMax + stepFreq/2 + stepFreqRaffinato
	#nstepFrequenze = tf.ceil((freqFin-freqIniz)/stepFreqRaffinato)+securbelt

	# riarrangio gli array in modo che abbia i dati 
	# nel formato che voglio io
	freqCorr = freqCorr-freqIniz
	freqCorr = (freqCorr/stepFreqRaffinato)-round(enhancement/2+0.001)
		
	#freqs = tf.concat([[freqIniz], freqCorr], 0)
	
	return freqIniz, freqCorr#, nstepFrequenze

def noncorr():
	freqMin = numpy.amin(frequenze)
	freqIniz = freqMin - stepFreq/2 -stepFreqRaffinato
	freqNonCor = (frequenze -freqIniz)/stepFreqRaffinato-round(enhancement/2+0.001)
	freqNonCor = tf.constant(freqNonCor, dtype = tf.float32)
	return freqNonCor


# calcola la hough per ogni punto del cielo (per ogni spindown)
def inDaHough(i, freqHM):
	#calcola la hough per ogni step di spindown
	#WARNING per ora la metto qui perché non so come si passano variabili non iterabili con tf.map
	def houghizza(stepIesimo):
		sdTimed = tf.multiply(spindownsTF[stepIesimo], tempiHM, name = "Tdotpert")
		#sdTimed = tf.cast(sdTimed, dtype=tf.float32)
	
		appoggio = tf.round(freqHM-sdTimed+securbelt/2, name = "appoggioperindici")
		appoggio = tf.cast(appoggio, dtype=tf.int32)
		
		valorisx = tf.unsorted_segment_sum(pesiHM, appoggio, nColumns)
		return valorisx
	#un po' di manipolazioni per il calcolo della hough
	
	#freq = freqTF[i]
	#freqHM = freq[1:tf.size(freq)]
	#freqIn = freq[0]
	
	# faccio la hough (differenziale)
	houghDiff = tf.map_fn(houghizza, tf.range(0, nRows), dtype=tf.float32, parallel_iterations=8)

	def sliceInt():
		#faccio integrazione finale (vecchia versione senza conv)
		semiLarghezza = tf.round(enhancement/2+0.001)
		semiLarghezza = tf.cast(semiLarghezza, tf.int32)
		houghInt = houghDiff[:,enhancement:nColumns]-houghDiff[:,0:nColumns - enhancement]
		houghInt = tf.concat([houghDiff[:,0:enhancement],houghInt],1)
		return houghInt
	
	#def convInt():
	#    #faccio integrazione finale (nuova versione con conv)
	#    kernel = tf.concat(([-1.0],tf.zeros(enhancement-2,dtype=tf.float32),[1.0]),0)
	#
	#    houghConv = tf.reshape(houghDiff,(1,nRows,nColumns,1))
	#    kernel = tf.reshape(kernel, (1,enhancement,1,1))
	#
	#    houghInt = tf.nn.conv2d(input=houghConv,filter=kernel,strides=[1,1,1,1],padding ='VALID')
	#    houghInt = tf.reshape(houghInt, (nRows,nColumns-(enhancement-1)))
		#houghInt = tf.nn.conv2d(input=houghDiff,filter=kernel,strides=[1,1,1,1],padding ='SAME')
		#houghInt = tf.reshape(houghInt, (nRows,nColumns))
		#CONTROLLARE SE CI SONO DIFFERENZE
		#TESTARE SE CON MATRICI DI 8-9 MESI CI METTE DI MENO CHE CON SLICE!
	#    return houghInt
	
	
	
	hough = sliceInt()
	#hough = convInt()
	houghinal = tf.cumsum(hough, axis = 1)
	
	return houghinal
	#return houghDiff
    
    
def manchurian_candidates(numCand, freqIniz, image, coord):
	minDistance = enhancement*4

	candidati = numpy.zeros((9,numCand*2))

	primaFreq = freqIniz-(securbelt/2)*stepFreqRaffinato

	freqIniziale = struttura['basic_info'][0,0]['frin'][0,0][0,0]
	freqFinale = struttura['basic_info'][0,0]['frfi'][0,0][0,0]
	
	#QUI ANALOGO FUNZIONE CUT GD2
	#%time indexInizialewh = numpy.where(freqniu>freqIniziale)[0][0]
	#%time indexFinalewh = numpy.where(freqniu>freqFinale)[0][0]
	start = time.time()

	indexIniziale = ((freqIniziale-primaFreq)/stepFreqRaffinato).astype(numpy.int64)
	indexFinale = ((freqFinale-primaFreq)/stepFreqRaffinato+1).astype(numpy.int64)

	imageCand = image[:,indexIniziale:indexFinale]

	size = numpy.shape(imageCand)[1]
	freqniu = numpy.arange(0,size)*stepFreqRaffinato+freqIniziale

	maxPerColumn = numpy.amax(imageCand, axis = 0)
	rigaMax = numpy.argmax(imageCand, axis = 0)

	#######################

	stepFrequenzaNiu = maxPerColumn.size/numCand

	indiciFreq = numpy.arange(0,maxPerColumn.size,stepFrequenzaNiu)
	indiciFreq = numpy.append(indiciFreq, maxPerColumn.size)
	indiciFreq = numpy.round(indiciFreq).astype(numpy.int64)

	def statistics(ndArray):
		#ndArray = numpy.ravel(ndArray)
		mediana = numpy.median(ndArray)
		sigmana = numpy.median(numpy.absolute(ndArray-mediana))/0.6745
		return mediana, sigmana

	stats = statistics(imageCand)
	medianaTot = stats[0]

	iniziali = numpy.concatenate(([indiciFreq[0]],indiciFreq[0:numCand-2],[indiciFreq[indiciFreq.size-3]]),0)
	finali = numpy.concatenate(([indiciFreq[2]-1],indiciFreq[3:numCand+1]-1,[indiciFreq[indiciFreq.size-1]-1]),0)

	def statsPerCand(i):
		stat = statistics(maxPerColumn[iniziali[i]:finali[i]])#[0]
		return stat

	statPerCand = numpy.array(list(map(statsPerCand, numpy.arange(numCand))))
	medianaPerCand = statPerCand[:,0]
	sigmanaPerCand = statPerCand[:,1]
	
	filtro = numpy.where(medianaPerCand > 0)[0]
	#medCandFiltrata = medianaPerCand[filtro]
	counter = 0
	for i in filtro:
		inizio = indiciFreq[i]
		fine = indiciFreq[i+1]-1
		porzioneMaxPerColumn = maxPerColumn[inizio:fine]
		localMax = numpy.amax(porzioneMaxPerColumn)
		localInd = numpy.argmax(porzioneMaxPerColumn)
		if localMax > medianaPerCand[i] and localMax > medianaTot/2:
			counter = counter + 1
			index = indiciFreq[i] + localInd-1
			candidati[0,counter] = freqniu[index]
			candidati[1,counter] = coord[0]
			candidati[2,counter] = coord[1]
			riga = rigaMax[index]
			candidati[3,counter] = spindowns[riga]
			candidati[4,counter] = localMax
			candidati[5,counter] = (localMax-medianaPerCand[i])/sigmanaPerCand[i]
			candidati[6,counter] = coord[2]/2
			candidati[7,counter] = numpy.abs(coord[3]-coord[4])/4
			candidati[8,counter] = 1

			limite1 = numpy.amax([localInd-minDistance,1]).astype(numpy.int32)
			limite2 = numpy.amin([localInd+minDistance,porzioneMaxPerColumn.size]).astype(numpy.int32)
			porzioneMaxPerColumn[limite1:limite2] = 0
			secondLocMax = numpy.amax(porzioneMaxPerColumn)
			secondLocInd = numpy.argmax(porzioneMaxPerColumn)

			if numpy.absolute(secondLocInd-localInd) > 2 * minDistance and secondLocMax > medianaPerCand[i]:
				counter = counter + 1
				index = indiciFreq[i] + secondLocInd-1
				candidati[0,counter] = freqniu[index]
				candidati[1,counter] = coord[0]
				candidati[2,counter] = coord[1]
				riga = rigaMax[index]
				candidati[3,counter] = spindowns[riga]
				candidati[4,counter] = secondLocMax
				candidati[5,counter] = (secondLocMax-medianaPerCand[i])/sigmanaPerCand[i]
				candidati[6,counter] = coord[2]/2
				candidati[7,counter] = numpy.abs(coord[3]-coord[4])/4
				candidati[8,counter] = 2

	candidati[3,:]=numpy.round(candidati[3,:] / stepSpindown) * stepSpindown
	return candidati


    
    
#da qui si usa tensorflow
#definisco tutte le costanti necessarie
tempiTF = tf.constant(tempi, dtype=tf.float64)

pesiTF = tf.constant(pesi,dtype=tf.float64)

spindownsTF = tf.constant(spindowns, dtype=tf.float64) 

tempiHM = tempiTF-epoca
tempiHM = ((tempiHM)*3600*24/stepFreqRaffinato)
#tempiHM = tf.cast(tempiHM, tf.float32)

pesiHM = tf.reshape(pesiTF,(1,tf.size(pesiTF)))
pesiHM = pesiHM[0]

nRows = tf.constant(nstepSpindown, dtype=tf.int32)
#problema! num step freq cambia a seconda della correzione doppler
#perché freq min e freq max possono variare e lo step lo si lascia uguale
#posso andarci in 2 modi: uno è tagliando a 96000 tutto
#uno è mettendo un po' di zeri prima e dopo, cercando con la doppler corr quale è la max assoluta
#e quale è la min assoluta
nColumns = tf.cast(nstepsFreq, dtype=tf.int32)

#freqTF = noncorr()

#mettere un for
start = time.time()
for punto in numpy.arange(0,nPunti-1):
	freqInCorr,freqCorr = doppcorr(punto)
	freqTF = tf.constant(freqCorr, dtype = tf.float64)
	
	houghmap = inDaHough(punto,freqTF)
	hough = sessione.run(houghmap)
	
	candidati = manchurian_candidates(cands, freqInCorr, hough, patch[punto])
	nonzeri = numpy.nonzero(candidati[0])
	finalCand = candidati[:,nonzeri]
	print(finalCand[0])
stop = time.time()
print(stop-start)


from matplotlib import pyplot

pyplot.figure(figsize=(10, 8))

posxTick = numpy.arange(5551, 89454, round((89454-5551)/10))
labelxTick = numpy.arange(0,1.1,0.1)+primaFreq
pyplot.xticks(posxTick,labelxTick)


posyTick = numpy.arange(11)*20
labelyTick = numpy.arange(spindownMin, spindownMax, stepSpindown*20)
pyplot.yticks(posyTick,labelyTick)




a = pyplot.imshow(hough, aspect = 400)
pyplot.colorbar(shrink = 1,aspect = 10)
pyplot.show()
