import tensorflow as tf
import __main__

def frequencyHough(frequencies,times,weights,spindowns):
	"""
	Computes the frequency-Hough transform of a sparse peakmap.
	Parameters:
	frequencies, times : 1D tensors
	The coordinates of the peakmap in sparse format.
	weights :  1D tensor
	The values of the peaks in the sparse peakmap.
	spindowns : 1D tensor
	The spindowns values over which calculate the Hough transform.
	Size of the Hough map
	Returns:
	houghMap : 2D tensor
	The Hough transform matrix
	"""
	
	# there are some global parameters:
	# - Hough map size (nColumns, nRows)
	# - the frequency resolution enhancement in the map
	# - securbelt (a certain number of columns is added to avoid
	#   frequencies transformed outside the image
	#   they will be removed once the Hough map is computed)


	#this function computes the Hough transform histogram for a given spindown
	# WARNING the 64 bit precision slows the computation and could not be supported in many GPUs
	# TODO  remove any 64bit data
	def rowTransform(ithSD):
		sdTimed = tf.multiply(spindowns[ithSD], times)
		sdTimed = tf.cast(sdTimed, dtype = tf.float64)
		
		transform = tf.round(frequencies-sdTimed+__main__.securbelt/2)
		transform = tf.cast(transform, dtype=tf.int32)
		#the rounding operation brings a certain number of peaks in the same 
		#frequency-spindown bin in the Hough map
		#the left edge is then computed binning that peaks properly
		#(according to their values if the peakmap was adactive)
		#this is the core of the algoritm and brings the most computational effort
		values = tf.unsorted_segment_sum(weights, transform, __main__.nColumns)
		values = tf.cast(values, dtype=tf.float64)
		return values
	#to keep under control the memory usage, the map function is a 
	#very useful tool to apply the same function over a vector
	#in this way the vectorization is preserved
	houghLeft = tf.map_fn(rowTransform, tf.range(0, __main__.nRows), 
					dtype=tf.float64, parallel_iterations=8)
	#let's superimpose the right edge on the image
	houghRight = houghLeft[:,__main__.enhancement:__main__.nColumns]-houghLeft[:,0:__main__.nColumns - __main__.enhancement]
	houghDiff = tf.concat([houghLeft[:,0:__main__.enhancement],houghRight],1)
	#at last, the Hough map is computed integrating along the frequencies
	houghMap = tf.cumsum(houghDiff, axis = 1)
	return houghMap 
