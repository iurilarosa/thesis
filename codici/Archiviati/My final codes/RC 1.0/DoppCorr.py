import __main__
import numpy

#simple porting of the SNAG function hfdf_patch
def doppcorr(point, frequencies, indices,  velocities):
	"""
	Computes the Doppler-corrected peak table
	Parameters:
	point : integer scalar
	Index of the sky-position point in the chosen region
	frequencies : 1D array
	Array of the frequencies in the peakmap
	indices :  1D array
	Indices of the FFT labeled as "science" used to create the peakmap
	velocities : 3D array
	Average Earth velocity values for each T_FFT between the first and last FFT used to create the peakmap
	Returns:
	freqCorr : 1D array
	The array of doppler corrected frequencies for the sky point selected
	freqIn : scalar
	The lowest frequency at which the Hough map is computed from the corrected peakmap
	freqHM : 1D array
	The corrected frequencies with enhanced resolution for the Hough map computation
	"""
	
	# there are some global parameters:
	# - frequency step (stepFreq)
	# - resolution enhanced frequency step (refinedStepFreq)
	# - resolution enhancement factor (enhancement)

	# defining the arrays used for array slicing in the for loop
	starts = indices[:-1]
	ends = indices[1:]
	
	# selection of the velocities at the "science" times
	velForCorr = numpy.zeros((3,frequencies.size))
	for i in numpy.arange(0,__main__.numTimes-1):
		velForCorr[:,starts[i]:ends[i]+1] = velocities[:,i:i+1]

	# doppler correction
	velXpos = numpy.dot(point,velForCorr)
	freqCorr = frequencies / (1+velXpos)

	# computation of the minimum frequency of the Hough map
	freqMin = numpy.amin(freqCorr)
	freqIn = freqMin- __main__.stepFreq/2 - __main__.refinedStepFreq
	
	# computation of the corrected frequency array which will be passed to the FrequencyHough function
	freqHM = freqCorr-freqIn
	freqHM = (freqHM/__main__.refinedStepFreq)-round(__main__.enhancement/2+0.001)

	return freqCorr, freqIn, freqHM
