import numpy
import __main__

#simple porting of the SNAG function hfdf_peak
def select_candidates(numCand, freqIn, hmap, coord,spindowns):
	"""
	Selects the candidates to be stored, from the FrequencyHough map computed for a sky point
	Parameters:
	numCand : integer scalar
	The number of primary candidates to be selected in the Hough map
	freqIn : scalar
	The first frequency of the Hough map
	hmap : matrix array
	The Hough map matrix
	coord : 1D array
	Coordinates values of the selected sky point and their uncertainties
	spindowns : 1D array
	The spindown array used to compute che Hough map
	Returns:
	candidates : 9D array
	Array with all the needed information about the selected candidates
	"""
	
	# there are some global parameters:
	# - resolution enhanced frequency step (refinedStepFreq)
	# - resolution enhancement factor (enhancement)
	# - securbelt (a certain number of columns was added to avoid
	#  frequencies transformed outside the image
	#  they will be removed in this function)
	
	# candidates array initialization with 9 parameters for each selected candidate:
	# 1 - frequency
	# 2 - ecl lon
	# 3 - ecl lat
	# 4 - spindown
	# 5 - amplitude
	# 6 - critical ratio
	# 7 - ecl lon error
	# 8 - ecl lat error
	# 9 - primary or secondary
	candidates = numpy.zeros((numCand*2,9))

	# cutting out the securbelt finding the index of the first frequency of the Hough map
	minDistance = __main__.enhancement*4
	firstFreq = freqIn-(__main__.securbelt/2)*__main__.refinedStepFreq

	freqInitial = __main__.structure['basic_info'][0,0]['frin'][0,0][0,0]
	freqFinal = __main__.structure['basic_info'][0,0]['frfi'][0,0][0,0]

	# same as cut_gd2 SNAG function
	indexInitial = ((freqInitial-firstFreq)/__main__.refinedStepFreq).astype(numpy.int64)
	indexFinal = ((freqFinal-firstFreq)/__main__.refinedStepFreq+1).astype(numpy.int64)

	hmapCand = hmap[:,indexInitial:indexFinal].astype(numpy.int64)
	size = numpy.shape(hmapCand)[1]
	
	# labelling the Hough map pixels with the proper frequency
	freqNew = numpy.arange(0,size)*__main__.refinedStepFreq+freqInitial

	# finding the highest amplitudes in the map for each column
	maxPerColumn = numpy.amax(hmapCand, axis = 0)
	rowMax = numpy.argmax(hmapCand, axis = 0)

	# building the belts in the Hough map where a candidate has to be searched
	stepFreqNew = maxPerColumn.size/numCand
	indicesFreq = numpy.arange(0,maxPerColumn.size,stepFreqNew)
	indicesFreq = numpy.append(indicesFreq, maxPerColumn.size)
	indicesFreq = numpy.round(indicesFreq).astype(numpy.int64)
	
	# computing median and median absolute deviation for the whole map
	def statistics(ndArray):
		"""
		Computes useful statistic quantities (see numpy.median documentation)
		Parameters:
		ndArray : ND array
		A ND array
		Returns:
		median : scalar
		The median of the array elements
		sigman : scalar
		The median absolute deviation in standard deviation units
		"""
		median = numpy.median(ndArray)
		sigman = numpy.median(numpy.absolute(ndArray-median))/0.6745
		return median, sigman

	stats = statistics(hmapCand)
	medianTot = stats[0]
	sigmanTot = stats[1]

	# computing median and median absolute deviation for each selected belt in the map
	# preparing indices for array slicing
	initialIndices = numpy.concatenate(([indicesFreq[0]],
									    indicesFreq[0:numCand-2],
									    [indicesFreq[indicesFreq.size-3]]),0)
	finalIndices = numpy.concatenate(([indicesFreq[2]],
								      indicesFreq[3:numCand+1],
								      [indicesFreq[indicesFreq.size-1]]),0)

	def statsPerCand(i):
		stat = statistics(maxPerColumn[initialIndices[i]:finalIndices[i]])#[0]
		return stat

	statPerCand = numpy.array(list(map(statsPerCand, numpy.arange(numCand))))
	medianPerCand = statPerCand[:,0]
	sigmanPerCand = statPerCand[:,1]

	# begin of the main loop
	# first of all cut out every belt where the median is < 0
	filter = numpy.where(medianPerCand > 0)[0]
	counter = -1
	# then start the loop over every remaining belt
	for i in filter:
		# selecting every maximum amplitude pixel in the selected belt
		starts = indicesFreq[i]
		ends = indicesFreq[i+1]
		portionMaxPerColumn = maxPerColumn[starts:ends]

		# finding the highest amplitude in the selected belt
		localMax = numpy.amax(portionMaxPerColumn)
		localInd = numpy.argmax(portionMaxPerColumn)

		# primary candidate selection
		if localMax > medianPerCand[i] and localMax > medianTot/2:
			counter = counter + 1
			index = indicesFreq[i] + localInd
			row = rowMax[index]

			candidates[counter,0] = freqNew[index]
			candidates[counter,1] = coord[0]
			candidates[counter,2] = coord[1]
			candidates[counter,3] = spindowns[row]
			candidates[counter,4] = localMax
			candidates[counter,5] = (localMax-medianPerCand[i])/sigmanPerCand[i]
			candidates[counter,6] = coord[2]/2
			candidates[counter,7] = numpy.abs(coord[3]-coord[4])/4
			candidates[counter,8] = 1
			
			# secondary candidate selection
			inf = numpy.amax([localInd-minDistance,0]).astype(numpy.int64)
			sup = numpy.amin([localInd+minDistance+1,portionMaxPerColumn.size]).astype(numpy.int64)
			
			portionMaxPerColumn[inf:sup] = 0
			secondLocMax = numpy.amax(portionMaxPerColumn)
			secondLocInd = numpy.argmax(portionMaxPerColumn)
			
			if numpy.absolute(secondLocInd-localInd) > 2 * minDistance and secondLocMax > medianPerCand[i]:
				counter = counter + 1
				index = indicesFreq[i] + secondLocInd
				row = rowMax[index]

				candidates[counter,0] = freqNew[index]
				candidates[counter,1] = coord[0]
				candidates[counter,2] = coord[1]
				candidates[counter,3] = spindowns[row]
				candidates[counter,4] = secondLocMax
				candidates[counter,5] = (secondLocMax-medianPerCand[i])/sigmanPerCand[i]
				candidates[counter,6] = coord[2]/2
				candidates[counter,7] = numpy.abs(coord[3]-coord[4])/4
				candidates[counter,8] = 2

	candidates[3,:]=numpy.round(candidates[3,:] / __main__.stepSpindown) * __main__.stepSpindown
	return candidates
