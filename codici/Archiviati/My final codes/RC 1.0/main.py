import tensorflow as tf
import numpy
import scipy.io
import glob
from DoppCorr import doppcorr
from GPUHough import frequencyHough
from CandSel import select_candidates
import time


#first of all i define some job parameters:
#FFT length, security belt 
tFft = 8192
#time interval of data
tObs = 9 #months
tObs = tObs*30*24*60*60 #seconds
#number of frequency belts in the hough where candidates are selected
numCands = 100
#security belt to avoid negative numbers in frequency-hough transform
securbelt = 4000
#frequency steps
stepFreq = 1/tFft
enhancement = 10
refinedStepFreq = stepFreq/enhancement
#epoch, defined as average time fo the observation run
epoch = (57722+57990)/2
#spindown range
spindownMin = -1e-9
spindownMax = 1e-10
stepSpindown = stepFreq/tObs
numStepsSpindown = numpy.round((spindownMax-spindownMin)/stepSpindown).astype(numpy.int64)
#and spindowns values
spindowns = numpy.arange(0, numStepsSpindown)
spindowns = numpy.multiply(spindowns,stepSpindown)
spindowns = numpy.add(spindowns, spindownMin)


#Loading data
#file paths
pathSquare = ("PATH/TO/square%d.mat" % tFft)
pathPatch = ("PATH/TO/square%dEcl.mat" % tFft)
pathFolder = 'PATH/TO/DATAFOLDER'

# data for doppler correction: these depend on the sky region one wants to search
# 1 - coordinates of the points in the chosen region, in rectangular coordinates
square = scipy.io.loadmat(pathSquare)['square'].astype(numpy.float64)
numPoints = square.shape[0]
# 2 - coordinates of the points in the chosen region, in ecliptic coordinates
patch = scipy.io.loadmat(pathPatch)['squareEcl'].astype(numpy.float64)
# the sky patch is computed using the SNAG functions
# - pss_optmap (computes the sky grid in ecliptic coordinates)
# - astro_coord_fix (astronomical coordinate conversion)
# - astro2rect (conversion from astronomical to rectangular coordinates)
# and then stored

#list of input files (with peakmaps) of the chosen frequency band
fileslist = glob.glob(pathFolder)
fileslist = numpy.array(fileslist)
fileslist = numpy.sort(fileslist)
numFiles = fileslist.size

#initialization of candidates array
allCands = numpy.zeros((numFiles,numPoints,numCands*2,9))

#WARNING at the moment the hough transform is serialized between different input files
#AND between sky positions
#TODO improve parallelization

#for loop into every input file in the frequency band chosen (tFFT 8192/4096 s)
start = time.time()
print(start)
for ithFile in numpy.arange(numFiles):
	
	#loading the ith .mat input file and the useful data
	structure = scipy.io.loadmat(fileslist[ithFile])['job_pack_0']
	times = structure['peaks'][0,0][0]#.astype(numpy.float64)
	frequencies = structure['peaks'][0,0][1]#.astype(numpy.float64)
	weights = (structure['peaks'][0,0][4]+1)#.astype(numpy.float64)

	#per doppler corr
	velocities = structure['basic_info'][0,0]['velpos'][0,0][0:3,:].astype(numpy.float64)
	numTimes = structure['basic_info'][0,0]['ntim'][0,0][0,0]
	indices = structure['basic_info'][0,0]['index'][0,0][0]

	# for loop into every sky positions in the area chosen ([+-10 gal lon, +-10 gal lat])
	for point in numpy.arange(0,numPoints):
		# WARNING i didn't define a complete tensorflow input pipeline (this is a quite difficult problem and in fast developement), so i used only constants and i'm forced to reset the graph and the tf.Session at every iteration of the loop
		# TODO build a complete input pipeline, within the (unavoidable) iterations use only variables
		session = tf.Session()

		# doppler correction, simple numpy porting of the SNAG function hfdf_patch
		# TODO develop a fully vectorial GPU version of doppler correction
		pointRectangle = square[point]
		# the indices array is made to be used with MATLAB, where the first array index is 1 and not 0 
		indicesOpt = indices-1
		freqCorr, freqInCorr, freqForHough = doppcorr(pointRectangle, frequencies, indicesOpt, velocities)

		numStepsFreq = numpy.ceil(securbelt+(numpy.amax(freqCorr)-numpy.amin(freqCorr) + stepFreq + 2*refinedStepFreq)/refinedStepFreq)

		# now let's use TensorFlow
		# defining tf constants to the hough transform
		# WARNING i'm using 64bit data for times because 9 months with a step of 4096s exceed 32bit precision, this forces to use 64bit spindowns to run tf.matmul (tensorflow needs same data type tensors). This slows a bit the code with a GPU capable to run 64bit calculations (eg Tesla series), but can be a problem on other series (eg GeForce)
		# TODO remove any 64bit data
		timesTF = tf.constant(times,dtype=tf.float64)
		weightsTF = tf.constant(weights,dtype=tf.float64)
		spindownsTF = tf.constant(spindowns, dtype=tf.float64)

		timesHM = timesTF-epoch
		timesHM = ((timesHM)*3600*24/refinedStepFreq)
		timesHM = tf.cast(timesHM, tf.float64)

		weightsHM = tf.reshape(weightsTF,(1,tf.size(weightsTF)))
		weightsHM = weightsHM[0]

		freqHM = tf.constant(freqForHough, dtype = tf.float64)

		#calculating the Hough transofrm, with a fully vectorial tensorflow function with GPU parallelization

		nRows = tf.constant(numStepsSpindown, dtype=tf.int32)
		nColumns = tf.cast(numStepsFreq, dtype=tf.int32)

		houghmap = frequencyHough(freqHM,timesHM,weightsHM,spindownsTF)
		hough = session.run(houghmap)
		scipy.io.savemat("miahoughFINALcut.mat",{'miaH' : hough})
		
		#candidates selection, simple numpy porting of the SNAG function hfdf_peak
		#TODO develop a fully vectorial GPU version of candidates selection
		allCands[ithFile,point] = select_candidates(numCands, freqInCorr, hough, patch[point],spindowns)

		tf.reset_default_graph()
		session.close()
print(time.time()-start)

#rearranging the candidates selected to write them in a .mat file compatible with the 
#SNAG functions for coincidence selections and follow-up
numAllCands = numpy.int64(allCands.size/9)
allCands = allCands.reshape(numAllCands,9)
nonzeroCands = numpy.nonzero(allCands[:,0])
finallCands = allCands[nonzeroCands]
finallCands = numpy.transpose(finallCands)

#pathOut = 'PATH/TO/candidates.mat'

pathOut = 'candidates52.mat'

scipy.io.savemat(pathOut,{'cand' : finallCands})


 
