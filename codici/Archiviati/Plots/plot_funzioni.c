
void graficifit(const char* input)	//macro uguale alla precedente ma che popola da file un solo array di dati
{
	double appoggio;
	vector<double> ascisse;
	vector<double> ordinate;
	
//////////////////////////////////////////////////////////////////
// Acquisizione dati
/////////////////////////////////////////////////////////////////
	
	bool letturaeseguita = false; //lettura del file
	do
	{
		ifstream dati_in; //apro stream input
		dati_in.open (input, ios::in);
		
		if (dati_in)
		{
			while(!dati_in.eof()) //popolo dinamicamente i vettori fino a che raggiunge fine del file
			{
				dati_in >> appoggio;		//popolo vettore delle ascisse (bin del picco medio)
				ascisse.push_back(appoggio);
				
				dati_in >> appoggio;		//popolo il vettore delle energie nominali (no error)
				ordinate.push_back(appoggio);
			}
			letturaeseguita = true;
		}
		else
		{
			cout << "Impossibile leggere il file " << endl;
			letturaeseguita = false;
		}
		dati_in.close();
	} while(!letturaeseguita);
	
//////////////////////////////////////////////////////////////////////////
// Parte di programma in cui si usa root
//////////////////////////////////////////////////////////////////////////
	
	
	TCanvas *oggetto = new TCanvas(); //apro "tela"
 	TGraph *grafico = new TGraph(ascisse.size(), &ascisse[0], &ordinate[0]);
	TAxis *axis = grafico->GetXaxis();
		
	grafico->SetMarkerColor(1); //impostazioni stilistiche del grafico
	grafico->SetMarkerStyle(20);
	grafico->SetMarkerSize(0.01);
	
	grafico->SetTitle("Example periodic gravitational wave");
	grafico->GetXaxis()->SetTitle("time");
	grafico->GetYaxis()->SetTitle("GW signal amplitude");
	grafico->GetXaxis()->SetTitleOffset(0.5);
	grafico->GetYaxis()->SetTitleOffset(0.5);
	grafico->GetYaxis()->SetNdivisions(2,0,0,kFALSE);
	grafico->GetXaxis()->SetNdivisions(0,0,0,kTRUE);
	
	axis->SetLimits(0,4);
	grafico->GetHistogram()->SetMaximum(10.0);         
	grafico->GetHistogram()->SetMinimum(-10.0);
	
  	grafico->Draw("AP"); // traccia il grafico dei punti

// 	TF1 *retta = new TF1("retta", "x", -0.1, 2);	// definisco funzione di fitting: retta
// 	retta->SetLineColor(kGreen-6);
// 	retta->SetLineWidth(2.8);

// 	ampiezza = 1e-2;
// 	tcoal = 2.05;
// 	freqIniz = 20;
// 	TF1 *curva = new TF1("curva", "[0]*[2]*((1-(x+1)/[1])**(-2/8))*cos([2]*((1-(x+1)/[1])**(-3/8))*(x+1))", -0.1, 1);	// definisco funzione di fitting: retta
// 	curva -> SetParameters(ampiezza, tcoal,freqIniz);
	
	//cost = 5e-29;
	cost = 7;
	nu0 = 1;
	nudot = -5e-2;
	
	TF1 *curva = new TF1("curva", "[0]*([1]+[2]*x)**2*cos(6.283185307*([1]+[2]*x)*x)", -0.1, 10);	// definisco funzione di fitting: retta
	curva -> SetParameters(cost, nu0,nudot);
	
	
	curva -> SetNpx(100000);
// 	TF1 *curva = new TF1("curva", "0.4+0.6*(x**1.5)", -0.1, 2);	// definisco funzione di fitting: retta
	curva->SetLineColor(kBlue-7);
	curva->SetLineWidth(1.8);
	

	curva->Draw("SAME");
// 	retta->Draw("SAME");	
	grafico->Draw("SAME");		

	
// 	legenda = new TLegend(0.957,0.955,0.785,0.825);
// 	legenda->SetFillColor(0);
// 	legenda->AddEntry(retta,"C_{ER}(p)","L");
//  	legenda->AddEntry(curva, "C_{WS}(p)", "L");
// 	legenda->Draw("SAME");
}