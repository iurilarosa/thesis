import tensorflow as tf
import tensorflow as tf
from tensorflow.python.client import timeline
N = 100

run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
run_metadata = tf.RunMetadata()

sess = tf.Session()

matr1 = tf.random_normal((N,N), name = "matrix 1")
matr2 = tf.random_normal((N,N),name = "matrix 2")
		
prod = tf.matmul(matr1, matr2,name = "product")

sess.run(prod,options=run_options,run_metadata = run_metadata)



logs_path = 'logs'
#writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())


writer = tf.summary.FileWriter(logdir=logs_path,graph=sess.graph)

time_path = 'logs/timeline.json'
tl = timeline.Timeline(run_metadata.step_stats)
ctf = tl.generate_chrome_trace_format()
with open(time_path, 'w') as f:
    f.write(ctf)

writer.add_run_metadata(run_metadata,"mySess")
writer.close()
sess.close()