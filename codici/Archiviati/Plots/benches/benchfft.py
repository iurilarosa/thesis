 
import tensorflow as tf
import time
import numpy

maxmat = 10000

dims = numpy.arange(2000,maxmat,500)
print(dims.size)
#def genran(dim, dt):
	#mat1 = tf.random_normal((dim,dim), dtype = dt)
	#mat2 = tf.random_normal((dim,dim), dtype = dt)
	#return mat1, mat2



def fftCPU64(maxmat):
	tempo = numpy.zeros((dims.size))
	startone = time.time()
	for i in numpy.arange(dims.size):
		print("step", i)
		N = dims[i]
		
		start = time.time()
		
		
		#sessione = tf.Session()
		#tf1, tf2 = genran(N,tf.float64)
		
		#matr1 = sessione.run(tf1)
		#matr2 = sessione.run(tf2)
		
		#tf.reset_default_graph()
		#sessione.close()
		
		matr1 = numpy.random.rand(N*N)
		matr2 = numpy.random.rand(N*N)
		complexo = matr1+1j*matr2
		
		
		prod = numpy.fft.fft(complexo)
		stop = time.time()
		
		tempo[i] = stop-start    
		

		
	endone = time.time()
	print(endone-startone)

	numpy.save('fftCPU64.npy', tempo) 
	
	
	
	
def fftGPU32(maxmat):
	tempo = numpy.zeros((dims.size))

	startone = time.time()
	for i in numpy.arange(8):
		print("step", i)
		sessione = tf.Session()

		N = dims[i]
		
		matr1 = tf.random_normal((1,N*N))
		matr2 = tf.random_normal((1,N*N))
		
		complexo = tf.complex(matr1,matr2)
		
		prod = tf.fft(complexo)
		start = time.time()
		prodtf = sessione.run(prod)
		stop = time.time()
		tempo[i] = stop-start

		tf.reset_default_graph()
		sessione.close()
		
	endone = time.time()
	print(endone-startone)

	numpy.save('fftGPU32.npy', tempo) 
	
	
	
#fftCPU64(maxmat)
fftGPU32(maxmat)
