 
import tensorflow as tf
import time
import numpy

maxmat = 10000

dims = numpy.arange(2000,maxmat+1,500)
print(dims.size)
#def genran(dim, dt):
	#mat1 = tf.random_normal((dim,dim), dtype = dt)
	#mat2 = tf.random_normal((dim,dim), dtype = dt)
	#return mat1, mat2



def benchCPU64(maxmat):
	tempo = numpy.zeros((dims.size))
	startone = time.time()
	for i in numpy.arange(dims.size):
		print("step", i)
		N = dims[i]
		
		start = time.time()
		
		
		#sessione = tf.Session()
		#tf1, tf2 = genran(N,tf.float64)
		
		#matr1 = sessione.run(tf1)
		#matr2 = sessione.run(tf2)
		
		#tf.reset_default_graph()
		#sessione.close()
		
		matr1 = numpy.random.rand(N,N)
		matr2 = numpy.random.rand(N,N)
		
		
		prod = numpy.dot(matr1,matr2)
		stop = time.time()
		
		tempo[i] = stop-start    
		

		
	endone = time.time()
	print(endone-startone)

	numpy.save('benchCPU64.npy', tempo) 
	
	
	
	
def benchGPU32(maxmat):
	tempo = numpy.zeros((dims.size))

	startone = time.time()
	for i in numpy.arange(dims.size):
		print("step", i)
		sessione = tf.Session()

		N = dims[i]
		
		matr1 = tf.random_normal((N,N))
		matr2 = tf.random_normal((N,N))
		
		prod = tf.matmul(matr1, matr2)
		start = time.time()
		prodtf = sessione.run(prod)
		stop = time.time()
		tempo[i] = stop-start

		tf.reset_default_graph()
		sessione.close()
		
	endone = time.time()
	print(endone-startone)

	numpy.save('benchGPU32.npy', tempo) 
	
	
def benchGPU64(maxmat):
	tempo = numpy.zeros((dims.size))

	startone = time.time()
	for i in numpy.arange(dims.size):
		print("step", i)
		sessione = tf.Session()
		
		N = dims[i]
		

		matr1 = tf.random_normal((N,N), dtype = tf.float64)
		matr2 = tf.random_normal((N,N), dtype = tf.float64)
			
		prod = tf.matmul(matr1, matr2)
			
		start = time.time()
		prodtf = sessione.run(prod)
		stop = time.time()
		tempo[i] = stop-start


		tf.reset_default_graph()
		sessione.close()
		
	endone = time.time()
	print(endone-startone)

	numpy.save('benchGPU64.npy', tempo)
	
	
	
benchCPU64(maxmat)
benchGPU32(maxmat)
benchGPU64(maxmat)



