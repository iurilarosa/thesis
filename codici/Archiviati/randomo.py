import tensorflow as tf
import numpy
import time

#for esponenti in numpy.arange(1,14):
#rc = numpy.power(2, esponenti)
#print(rc)
#sess = tf.Session()
#atf = tf.random_uniform(shape=(rc,rc))
#btf = tf.random_uniform(shape=(rc,rc))
#ctf = tf.matmul(atf, btf)
#
#start = time.time()
#c = sess.run(ctf)
#stop = time.time()
#sess.close()
#x = numpy.array([rc,stop-start]).reshape((1,2))
#f_handle = open('timestats.txt', 'a')
#numpy.savetxt(f_handle, x,delimiter=',')
#f_handle.close()

#test Numpy
def calcolaNP(rc):
    start = time.time()
    a = numpy.random.rand(rc,rc)
    b = numpy.random.rand(rc,rc)
    c = numpy.matmul(a,b)
    stop = time.time()
    return stop-start
        
#test TensorFlow
def calcolaTF(rc):
    sess = tf.Session()
    atf = tf.random_uniform(shape=(rc,rc), dtype = tf.float64)
    btf = tf.random_uniform(shape=(rc,rc), dtype = tf.float64)
    ctf = tf.matmul(atf, btf)

    start = time.time()
    c = sess.run(ctf)
    stop = time.time()
    sess.close()
    tf.reset_default_graph()
    return stop-start
        
        
import tensorflow as tf
import numpy
import time

#test comparativo TensorFlow-Numpy
# genera due matrici con valori random, ne fa il prodotto matriciale
for righe in numpy.arange(200,14201,500):
        print(righe)
        
        tempoTF = calcolaTF(righe)
        x = numpy.array([righe,tempoTF]).reshape((1,2))
        f_handle = open('timestatsTF64.txt', 'a')
        numpy.savetxt(f_handle, x,delimiter=',')
        f_handle.close()
        
        tempoNP = calcolaNP(righe)
        x = numpy.array([righe,tempoNP]).reshape((1,2))
        f_handle = open('timestatsNP.txt', 'a')
        numpy.savetxt(f_handle, x,delimiter=',')
        f_handle.close()
        