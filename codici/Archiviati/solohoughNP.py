import tensorflow as tf
import numpy
import scipy.io
#from tensorflow.python.client import timeline
import time

#percorsoDati = "/home/protoss/Documenti/TESI/wn100bkp/data/dati9mesi108HWI.mat"
percorsoDati = "/home/iurilarosa/dati/dati9mesi108HWI.mat"

tFft = 8192
#tFft = 4096
tObs = 9 #mesi
tObs = tObs*30*24*60*60

#carico file dati
struttura = scipy.io.loadmat(percorsoDati)['job_pack_0']

#peakmap = struttura['peaks'][0,0]
#print(peakmap)
#tempiIndiciSorted = numpy.argsort(peakmap[0,:])
#tempi = peakmap[0, tempiIndiciSorted]
#frequenze = peakmap[1, tempiIndiciSorted]
#pesi =peakmap[4, tempiIndiciSorted] + 1 #numpy.ones(peakmap[4].size)#

#print(tempi)
#print(frequenze)
#print(pesi)

tempi = struttura['peaks'][0,0][0]#.astype(numpy.float32)
frequenze = struttura['peaks'][0,0][1]#.astype(numpy.float32)
pesi = (struttura['peaks'][0,0][4]+1)#.astype(numpy.float32)


primoTempo = struttura['basic_info'][0,0]['tim0'][0,0][0,0]

tempi = (tempi-primoTempo)

filtro = numpy.where(tempi[0:tempi.size-1]-tempi[1:tempi.size] != 0)
tempiUnici = numpy.append(tempi[filtro], tempi[tempi.size-1])
print(tempiUnici[tempiUnici.size-1])
#nb: picchi ha 0-tempi
#              1-frequenze
#              2-pesi

#headers vari
securbelt = 4000


#frequenze
#frequenze
stepFrequenza = 1/tFft
enhancement = 10
stepFreqRaffinato =  stepFrequenza/enhancement

freqMin = numpy.amin(frequenze)
freqMax = numpy.amax(frequenze)
freqIniz = freqMin- stepFrequenza/2 - stepFreqRaffinato
freqFin = freqMax + stepFrequenza/2 + stepFreqRaffinato
nstepFrequenze = numpy.ceil((freqFin-freqIniz)/stepFreqRaffinato)+securbelt

#tempi
#epoca definita come mediana di tempi di tutto il run
#epoca = (57722+57990)/2 #0
epoca = 0

#spindowns
spindownMin = -1e-9
spindownMax = 2e-10
stepSpindown = stepFrequenza/tObs 

nstepSpindown = numpy.round((spindownMax-spindownMin)/stepSpindown).astype(numpy.int32)

print(nstepSpindown)
# riarrangio gli array in modo che abbia i dati 
# nel formato che voglio io
frequenze = frequenze-freqIniz
frequenze = (frequenze/stepFreqRaffinato)-round(enhancement/2+0.001)

tempi = tempi-epoca
tempi = ((tempi)*3600*24/stepFreqRaffinato)
#tempi = numpy.round(tempi/1e8)*1e8

spindowns = numpy.arange(0, nstepSpindown)
spindowns = numpy.multiply(spindowns,stepSpindown)
spindowns = numpy.add(spindowns, spindownMin)
# così ho i tre array delle tre grandezze, 
#più i pesi e la fascia di sicurezza
indice0 = numpy.where(spindowns>0)[0][0]-1
print(indice0)
spindowns = spindowns-spindowns[indice0]
 
nRows = nstepSpindown
nColumns = nstepFrequenze.astype(int)
fakeRow = numpy.zeros(frequenze.size)

def itermatrix(stepIesimo):
    sdPerTempo = spindowns[stepIesimo]*tempi
    appoggio = numpy.round(frequenze-sdPerTempo+securbelt/2).astype(int)
    
    valori = numpy.bincount(appoggio,pesi)
    
    missColumns = (nColumns-valori.size)
    zeros = numpy.zeros(missColumns)
    matrix = numpy.concatenate((valori, zeros))
    return matrix

#pool = multiprocessing.Pool()
#%time imageMapped = list(pool.map(itermatrix, range(nstepSpindown)))
#pool.close

start = time.time()
imageMapped = list(map(itermatrix, range(nRows)))
imageMapped = numpy.array(imageMapped)

semiLarghezza = numpy.round(enhancement/2+0.001).astype(int)
imageMapped[:,semiLarghezza*2:nColumns]=imageMapped[:,semiLarghezza*2:nColumns]-imageMapped[:,0:nColumns - semiLarghezza*2]
imageMapped = numpy.cumsum(imageMapped, axis = 1)
end = time.time()

print(end-start)


start = time.time()
rigasingola = itermatrix(1)

semiLarghezza = numpy.round(enhancement/2+0.001).astype(int)
rigasingola[semiLarghezza*2:nColumns]=rigasingola[semiLarghezza*2:nColumns]-rigasingola[0:nColumns - semiLarghezza*2]
rigasingola = numpy.cumsum(rigasingola)
end = time.time()

print(end-start)
