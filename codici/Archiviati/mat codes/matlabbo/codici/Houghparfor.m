% carico file .mat
load /home/protoss/Documenti/TESI/DATI/peaks.mat;

securbelt = 4000;

% definisco i tre array di dati
tempi = PEAKS(:,1);
frequenze = PEAKS(:,2);
pesi = PEAKS(:,5);
%pesi = transpose(pesi);

% definisco i valori che vengono dall'header
% e altri valori iniziali per la frequenza
minFreq = hm_job.fr(1);
maxFreq = hm_job.fr(4);
enhancement = hm_job.fr(3);
stepFrequenza = hm_job.fr(2);

stepFreqRaffinato = stepFrequenza/enhancement;

freqIniz = minFreq - stepFrequenza/2 - stepFreqRaffinato;
freqFin = maxFreq + stepFrequenza/2 + stepFreqRaffinato;

nstepFrequenze = ceil((freqFin-freqIniz)/stepFreqRaffinato)+securbelt;

% definisco i valori che vengono dall'header
% e altri valori iniziali per lo spindown
spindownIniz = hm_job.sd(1);
stepSpindown = hm_job.sd(2);
nstepSpindown = hm_job.sd(3);

% rimaneggio i due array t e freq per rappresentare ciò che voglio
frequenze = ((frequenze-freqIniz)/stepFreqRaffinato)-round(enhancement/2+0.001);
%frequenze = round(frequenze);
%frequenze = int64(frequenze);

epoch=basic_info.epoch;
tempi = tempi - epoch;
tempi = (tempi)*3600*24/stepFreqRaffinato;
tempi = round(tempi);
%tempi = int64(tempi);

% ora definisco gli spindowns
spindowns = 0:(nstepSpindown-1);
spindowns = spindowns*stepSpindown + spindownIniz;

nRows = nstepSpindown;
nColumns = nstepFrequenze;
hough = zeros(nRows,nColumns);

%spindowns = gpuArray(spindowns);
%tempi = gpuArray(tempi);
%frequenze = gpuArray(frequenze);
%pesi = gpuArray(pesi);
%hough = gpuArray(hough);

tic;
parfor(i = 1:nstepSpindown,2)
    sdPerTempo = spindowns(i)*tempi;
    appoggio = 1+round(frequenze-sdPerTempo+securbelt/2);
    
    valori = accumarray(appoggio,pesi);
    valori = transpose(valori);
    
    nValori = size(valori);
    zeriDopo = zeros(1,(nColumns-nValori(2)));
    
    row = horzcat(valori,zeriDopo);
    hough(i,:) = row(:);
end
toc;

% provare con arrayfun che dovrebbe funzionare
% oppure con calcolo simbolico

semiLarghezza = round(enhancement/2+0.001);
hough(:,semiLarghezza*2+1:nColumns)= hough(:,semiLarghezza*2+1:nColumns)-hough(:,1:nColumns-semiLarghezza*2); % half to full diff. map - Carl Sabottke idea
hough=cumsum(hough,2);
hough = gather(hough);
%imshow(hough);

save matlabbo/miaimgconcumsum.mat hough
%save matlabbo/miaimgnoncumsum.mat hough