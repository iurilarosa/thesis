
load /home/protoss/wn100bkp/dati/peakmap0fed.mat
tFft = 8192;
peaks = zeros(5,size(freq,2));
peaks(1,:) = tempi;
peaks(2,:) = freq;

[n1,n2]=size(peaks);

I1000=2000;  % security belt (even)
I500=I1000/2;
Day_inSeconds=86400;

% oper=' adaptive';
%%hm_job.oper='noadapt';
peaks(5,:)=ones(1,n2);


epoch=60; % Hough epoch (time barycenter) in mjd
peaks(1,:)=peaks(1,:)-epoch;




minf0=min(freq);
maxf0=max(freq);
df=1/tFft;   % raw frequency resolution
enh=10;  % frequency enhancement factor (typically 10)
df2=df/2;
ddf=df/enh;  % refined frequency step
inifr=minf0-df2-ddf;
finfr=maxf0+df2+ddf;
nbin_f0=ceil((finfr-inifr)/ddf)+I1000;
deltaf2=round(enh/2+0.001); % semi-width of the strip (in ddf)

n_of_peaks=length(peaks);
ii=find(diff(peaks(1,:)));  % find the different times
ii=[ii n_of_peaks]; 
nTimeSteps=length(ii); % number of times of the peakmap

dmin1=-10e-10;
dmax = 9e-10;
nbin_d=100;
deltad=(dmax-dmin1)/nbin_d;  % spin-down step
  % spin-down number of steps
d=dmin1+(0:nbin_d-1)*deltad;
ii0=1; %nbin_d,nbin_f0
nbin_f0
binh_df0=zeros(nbin_d,nbin_f0);  %  HM matrix container

for it = 1:nTimeSteps
    kf=(peaks(2,ii0:ii(it))-inifr)/ddf;  % normalized frequencies
    w=peaks(5,ii0:ii(it));               % wiener weights
    t=peaks(1,ii0)*Day_inSeconds; % time conversion days to s
    tddf=t/ddf;
    f0_a=kf-deltaf2; 
    
    for id = 1:nbin_d   % loop for the creation of half-differential map
        td=d(id)*tddf;
        a=1+round(f0_a-td+I500); 



        binh_df0(id,a)=binh_df0(id,a)+w; % left edge of the strips
    end
    ii0=ii(it)+1;
end



binh_df0(:,deltaf2*2+1:nbin_f0)=...
    binh_df0(:,deltaf2*2+1:nbin_f0)-binh_df0(:,1:nbin_f0-deltaf2*2); % half to full diff. map - Carl Sabottke idea
binh_df0=cumsum(binh_df0,2);   % creation of the Hough map


hfdf=gd2(binh_df0.');
hfdf=edit_gd2(hfdf,'dx',ddf,'ini',inifr-I500*ddf,'dx2',deltad,'ini2',dmin1,'capt','Histogram of spin-f0');


