%load peaks.mat

%HFDF_HOUGH  creates a f/df Hough map (based on 2011 version)
%
%     [hfdf,job_info]=hfdf_hough(peaks,basic_info,job_info,hm_job)
%
%
%    peaks(5,n)      peaks of the peakmap as [t,fr,amp,wnoise,wien] (corrected for the Doppler effect)
%                      time is MJD, sd in Hz/s
%    basic_info
%    job_info
%    hm_job          hough map structure 
%        .oper       'adapt' (def), 'noiseadapt' or 'noadapt'
%        .fr         sel  mode : [minf df enh maxf] min fr, enhanced frequency step, maxfr
%                    full mode or enhanced full mode: doesn't exist
%        .sd         sel mode or enhanced full mode: [minsd dsd nsd] min sd, step, number of sd
%                    full mode : doesn't exist
%        .mimaf      used for refined, otherwise nat_range is used
%        .mimaf0       "         "        
%
%    hfdf            hough map (gd2)
%    job_info        job info structure
%    checkE          service structure for test and debug

% Version 2.0 - October 2013 
% Part of Snag toolbox - Signal and Noise for Gravitational Antennas
% by Sergio Frasca - sergio.frasca@roma1.infn.it
% Department of Physics - Universit? "La Sapienza" - Rome
%%FORCED TO BE onlysigadapt********
tic

%checkE=struct();

%job_info.proc.E_hfdf_hough.vers='140904';
%job_info.proc.E_hfdf_hough.hm_job=hm_job;
%job_info.proc.E_hfdf_hough.tim=datestr(now);

%job_info.error_hough=0;
peaks = nonzeros(H);

peaks = job_pack_0.peaks;
%peaks(2,:) = freqniu;
[n1,n2]=size(peaks);

I1000=4000;  % security belt (even)
I500=I1000/2;
Day_inSeconds=86400;
tObs = 9;
tObs = tObs*30*24*60*60;
% oper=' adaptive';
hm_job.oper='noadapt';
%%display('Function forced to do onlysigadapt !!')
peaks(5,:) = 1;

epoch=(57722+57990)/2; % Hough epoch (time barycenter) in mjd
peaks(1,:)=peaks(1,:)-epoch;

%mind1=basic_info.run.sd(2);
tFft = 8192;
minf0=min(peaks(2,:));
maxf0=max(peaks(2,:));
df=1/tFft;   % raw frequency resolution
enh=10;  % frequency enhancement factor (typically 10)
df2=df/2;
ddf=df/enh;  % refined frequency step
inifr=minf0-df2-ddf;
finfr=maxf0+df2+ddf;
nbin_f0=ceil((finfr-inifr)/ddf)+I1000;
deltaf2=round(enh/2+0.001); % semi-width of the strip (in ddf)

n_of_peaks=length(peaks);
ii=find(diff(peaks(1,:)));  % find the different times
ii=[ii n_of_peaks]; 
nTimeSteps=length(ii); % number of times of the peakmap

dmin1=-1e-9;
dmax = 1e-10;% spin-down initial value
deltad=df/tObs;  % spin-down step
nbin_d=round((dmax-dmin1)/deltad);  % spin-down number of steps
%nbin_d=200;
d=dmin1+(0:nbin_d-1)*deltad;  
ii0=1; %nbin_d,nbin_f0
save spindown52.mat d
binh_df0ORIG=zeros(nbin_d,nbin_f0);  %  HM matrix container
nbin_f0
for it = 1:nTimeSteps
    kf=(peaks(2,ii0:ii(it))-inifr)/ddf;  % normalized frequencies
    w=peaks(5,ii0:ii(it));               % wiener weights
    t=peaks(1,ii0)*Day_inSeconds; % time conversion days to s
    tddf=t/ddf;
    f0_a=kf-deltaf2; 
    
    for id = 1:nbin_d   % loop for the creation of half-differential map
        td=d(id)*tddf;
        
        a=1+round(f0_a-td+I500); 
        if a > nbin_f0
            warning = "ATENTO"
            a
        end
        binh_df0ORIG(id,a)=binh_df0ORIG(id,a)+w; % left edge of the strips
    end
    ii0=ii(it)+1;
end

binh_df0ORIG(:,deltaf2*2+1:nbin_f0)=...
    binh_df0ORIG(:,deltaf2*2+1:nbin_f0)-binh_df0ORIG(:,1:nbin_f0-deltaf2*2); % half to full diff. map - Carl Sabottke idea
binh_df0ORIG=cumsum(binh_df0ORIG,2);   % creation of the Hough map

save hough52HWINONCOR.mat binh_df0ORIG

hfdf=gd2(binh_df0ORIG.');
hfdf=edit_gd2(hfdf,'dx',ddf,'ini',inifr-I500*ddf,'dx2',deltad,'ini2',dmin1,'capt','Histogram of spin-f0');
toc;

%job_info.hm_job=hm_job;
%job_info.hm_job.fr(1)=inifr;
%job_info.hm_job.fr(4)=finfr;

%job_info.proc.E_hfdf_hough.duration=toc;