
format long
%display('Apply the veto and then adaptive Hough')

folder= "/home/iuri.larosa/programmi/matlabLibs";
addpath(genpath(folder));

detector = "LL";
fileS = "/home/iuri.larosa/master/inputs/O3_" + detector+ "_C01_segments_science_withCAT1veto.txt";
%nomefile = "_01_0079_";
%nomefile = "_02_0322_";
%nomefile = "_03_0764_";
nomefile = "_04_1533_";

path = "/home/iuri.larosa/master/data/in_O3" + detector + nomefile + ".mat"
load(path)
peaks = job_pack_0.peaks;
basic_info = job_pack_0.basic_info;

tic

PERC = 0.99;
excl=2/3;

GPScience=tdfread(fileS,' ');
TFFT=1/basic_info.dfr;
TFFT2=TFFT/2;
ffttim=basic_info.tim; %these are the m1 MIDDLE  times in that file

Nt=basic_info.ntim; %number of times
gpsFFTTIM=mjd2gps(ffttim);
kkCE=[];
for iS=1:length(GPScience.INI)
    kkmI=find(gpsFFTTIM>=GPScience.INI(iS));
    kkmF=find(gpsFFTTIM<=GPScience.FIN(iS));
    kkmIF=intersect(kkmI,kkmF);
    for iSS=1:length(kkmIF)
        A=(gpsFFTTIM(kkmIF(iSS))-GPScience.INI(iS))/TFFT2;
        if (A>0.5)
            A=0.5;
        end
        B=(-gpsFFTTIM(kkmIF(iSS))+GPScience.FIN(iS))/TFFT2;
        if (B>0.5)
            B=0.5;
        end
        if (A+B) >=PERC
            kkCE=[kkCE kkmIF(iSS)];
        end
    end
end

%shapekk = size(kkCE)
%kkcheck = kkCE(1:10)
%kkcheck = kkCE(end-10:end)

gpsFFTTIM=gpsFFTTIM-TFFT2; %beg time
clear kkmI kkmF kkmIF
for iS=1:length(GPScience.INI)
    kkmI=find(gpsFFTTIM<GPScience.INI(iS));
    kkmF=find(gpsFFTTIM+TFFT<=GPScience.FIN(iS));
    kkmIF=intersect(kkmI,kkmF);
    for iSS=1:length(kkmIF)
        A=(gpsFFTTIM(kkmIF(iSS))+TFFT-GPScience.INI(iS))/TFFT;
        if (A) >=PERC
            kkCE=[kkCE kkmIF(iSS)];
        end
    end
end

%a = "second loop a"
%kkmIFCHECK = kkmIF(1:10)
%kkmIFCHECK = kkmIF(end-10:end)
%shapekk = size(kkCE)
%kkcheck = kkCE(1:10)
%kkcheck = kkCE(end-10:end)

clear kkmI kkmF kkmIF
for iS=1:length(GPScience.INI)
    kkmI=find(gpsFFTTIM<GPScience.INI(iS));
    kkmF=find(gpsFFTTIM+TFFT>GPScience.FIN(iS));
    kkmIF=intersect(kkmI,kkmF);
    for iSS=1:length(kkmIF)
        A=(GPScience.INI(iS)-GPScience.FIN(iS))/TFFT;
        if (A) >=PERC
            kkCE=[kkCE kkmIF(iSS)];
        end
    end
end

%a = "second loop b"
%kkmIFCHECK = kkmIF(1:10)
%kkmIFCHECK = kkmIF(end-10:end)
%shapekkMIF = size(kkmIF)
%shapekk = size(kkCE)
%kkcheck = kkCE(1:10)
%kkcheck = kkCE(end-10:end)

%kkCE=sort(kkCE);
%kkOK=unique(kkCE);
%size(kkOK)

kkCE=sort(kkCE);
kkOK=unique(kkCE);

%shapekk = size(kkOK)
%kkcheck = kkOK(1:10)
%kkcheck = kkOK(end-10:end)

kkk=zeros(1,Nt);  %vector with 0 for each time index
kkk(kkOK)=1; % 1 in the indexes corresponding to good FFTs
%display('Percentage of Vetoed spectra' )
Fraction_vetoed=1-length(kkOK)/Nt;
%Fraction_vetoed
index=basic_info.index;
gsp=basic_info.gsp;
sp=y_gd2(gsp);
inisp=ini2_gd2(gsp);
dfsp=dx2_gd2(gsp);
[m1,m2]=size(sp); % m1 times num, m2 freqs num

%wien=sp*0;
%spNEW=sp*0;
BASE=zeros(size(sp));  %TO avoid NAN !!!!!Modifica luglio 2019
wien=BASE;
spNEW=BASE;

spNEW(kkOK,:)=sp(kkOK,:);

%shapesp = size(spNEW)
%spCHECK = spNEW(1:3,:)


for i = 1:m2
    sp1=spNEW(:,i);
    m=median(sp1(kkOK));
    ii=find(sp1 < excl*m);
    sp1(ii)=0;
    iiT=find(sp1 > 10*m);
    sp1(iiT)=0;   
    jj=find(sp1);
    sp1=1./sp1(jj);
    wien(jj,i)=sp1/mean(sp1);
end

%shapewien = size(wien)
%wienCHECK = wien(1:3,:)

%%Use  spNEW, che non ha i NaN !!! luglio 2019
%sp=sp.*wien;
clear sp
sp=spNEW.*wien;
% sp  fatto da spNEW, wien (sp1,mean sp1)


wsp=zeros(1,m2);
for i = 1:m2
    wsp(i)=sum(sp(:,i))/sum(wien(:,i));
end

for i = 1:Nt
        fr=peaks(2,index(i):index(i+1)-1);
        convfr=round((fr-inisp)/dfsp)+1;
        peaks(4,index(i):index(i+1)-1)=peaks(4,index(i):index(i+1)-1)*0+median(wien(i,convfr));
end

weights = peaks(4,:);
toc
save("/home/iuri.larosa/master/outputs/tests/in_O3" + detector + nomefile + "MATweights.mat", "weights")
