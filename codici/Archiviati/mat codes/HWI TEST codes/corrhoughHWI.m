


load /home/protoss/Documenti/TESI/DATI/dati9mesi187HWI.mat
load /home/protoss/Documenti/TESI/thesis/codici/matlabbo/HWI/quadHWI187.mat
load /home/protoss/Documenti/TESI/thesis/codici/matlabbo/HWI/quadHWI187Ecl.mat

%load /home/protoss/Documenti/TESI/DATI/dati9mesi108HWI.mat
%load /home/protoss/Documenti/TESI/thesis/codici/matlabbo/HWI/quadHWI108.mat
%load /home/protoss/Documenti/TESI/thesis/codici/matlabbo/HWI/quadHWI108Ecl.mat



quadr = quad;
tic
pin = job_pack_0.peaks;
pout=pin;

index=job_pack_0.basic_info.index;
Nt= job_pack_0.basic_info.ntim;

vel_orig=job_pack_0.basic_info.velpos;


r=quadr(1,:);%astro2rect([alpha,delta],0);
v=vel_orig(1:3,:);


v1=r*v;  % DA BOLOGNA


vel1=zeros(1,index(Nt+1)-1);
for i=1:Nt
    for j=index(i):index(i+1)-1
        vel1(:,j)=v1(:,i);
    end
end


freqniu=pout(2,:)./(1+vel1);

peaks = job_pack_0.peaks;
peaks(2,:) = freqniu;
[n1,n2]=size(peaks);

I1000=4000*2;  % security belt (even)
%I1000=4000;
I500=I1000/2;
Day_inSeconds=86400;
tObs = 9;
tObs = tObs*30*24*60*60;
% oper=' adaptive';
hm_job.oper='noadapt';
%%display('Function forced to do onlysigadapt !!')
peaks(5,:) = 1;

epoch= (57722+57990)/2; % Hough epoch (time barycenter) in mjd
peaks(1,:)=peaks(1,:)-epoch;

%mind1=basic_info.run.sd(2);
%tFft = 8192;
tFft = 4096;
minf0=min(peaks(2,:));
maxf0=max(peaks(2,:));
df=1/tFft;   % raw frequency resolution
enh=10;  % frequency enhancement factor (typically 10)
df2=df/2;
ddf=df/enh;  % refined frequency step
inifr=minf0-df2-ddf;
finfr=maxf0+df2+ddf;
nbin_f0=ceil((finfr-inifr)/ddf)+I1000;
deltaf2=round(enh/2+0.001); % semi-width of the strip (in ddf)

n_of_peaks=length(peaks);
ii=find(diff(peaks(1,:)));  % find the different times
ii=[ii n_of_peaks]; 
nTimeSteps=length(ii); % number of times of the peakmap

dmin1=-1e-9;
dmax =1e-10;% spin-down initial value
%dmin1=-9e-9;
%dmax =-8e-9;
deltad=df/tObs;  % spin-down step
nbin_d=round((dmax-dmin1)/deltad);  % spin-down number of steps
%nbin_d=200;
d=dmin1+(0:nbin_d-1)*deltad;  
ii0=1; %nbin_d,nbin_f0

binh_df0=zeros(nbin_d,nbin_f0);  %  HM matrix container
for it = 1:nTimeSteps
    kf=(peaks(2,ii0:ii(it))-inifr)/ddf;  % normalized frequencies
    w=peaks(5,ii0:ii(it));               % wiener weights
    t=peaks(1,ii0)*Day_inSeconds; % time conversion days to s
    tddf=t/ddf;
    f0_a=kf-deltaf2; 
    
    for id = 1:nbin_d   % loop for the creation of half-differential map
        td=d(id)*tddf;
        
        a=1+round(f0_a-td+I500); 
        %if a > nbin_f0
        %    warning = "ATENTO"
        %    a
        %end
        binh_df0(id,a)=binh_df0(id,a)+w; % left edge of the strips
    end
    ii0=ii(it)+1;
end

binh_df0(:,deltaf2*2+1:nbin_f0)=...
    binh_df0(:,deltaf2*2+1:nbin_f0)-binh_df0(:,1:nbin_f0-deltaf2*2); % half to full diff. map - Carl Sabottke idea
binh_df0=cumsum(binh_df0,2);   % creation of the Hough map

hfdf=gd2(binh_df0.');
hfdf=edit_gd2(hfdf,'dx',ddf,'ini',inifr-I500*ddf,'dx2',deltad,'ini2',dmin1,'capt','Histogram of spin-f0');

toc