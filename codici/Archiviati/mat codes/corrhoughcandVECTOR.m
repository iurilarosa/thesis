tic;
addpath(genpath('/home/iuri.larosa/programmi/matlabLibs/'))
detector = "LL";
nomefile = "_01_0079_";
%nomefile = "_02_0322_";
%nomefile = "_03_0764_";
%nomefile = "_04_1533_";


file = "in_O3" + detector + nomefile
display("load")
load("/home/iuri.larosa/master/data/" + file + ".mat")
load("/home/iuri.larosa/master/outputs/tests/tf/" + file + "grid.mat")
load("/home/iuri.larosa/master/outputs/tests/" + file + "MATweights.mat")

df=job_pack_0.basic_info.dfr ;
runstr = job_pack_0.basic_info.run;
tFft = 1/df;

I1000=4000*2;
if tFft == 8192
    I1000 = 4000*8;
end
if tFft == 4096
    I1000 = 4000*3;
end
I500=I1000/2;

kcand = 100;
if tFft == 8192
    kcand = 20;
end

%size(rectgrid)
point = 1;
grigliaREC = rectgrid(point,:);
grigliaECL = eclgrid(point,:);
pin = job_pack_0.peaks;
pout=pin;
sizepeaks = size(pout);

index=job_pack_0.basic_info.index;
Nt= job_pack_0.basic_info.ntim;

vel_orig=job_pack_0.basic_info.velpos;

toc;
display("startloop")
display("weights+dopp")
tic;
griglia = grigliaECL;
[alpha,delta]=astro_coord('ecl','equ',grigliaECL(1),grigliaECL(2));
r=grigliaREC;  %astro2rect([alpha,delta],0);
v=vel_orig(1:3,:);


v1=r*v;  % DA BOLOGNA


vel1=zeros(1,index(Nt+1)-1);
for i=1:Nt
    for j=index(i):index(i+1)-1
        vel1(:,j)=v1(:,i);
    end
end


for i=1:10
    for j=index(i):index(i+1)-1
        vel1(:,j)=v1(:,i);
    end
end

for i=(Nt-10):Nt
    for j=index(i):index(i+1)-1
        vel1(:,j)=v1(:,i);
    end
end

times = pin(1,:);
sidh=gmst(pin(1,:));
sour.a=alpha;
sour.d=delta;
sour.eta=1;
sour.psi=0;

sidpat=pss_sidpat_psi(sour,runstr.ant,120,0);

pat = y_gd(sidpat);

radpat=gd_interp(sidpat,sidh);

radpat=radpat/mean(radpat);

freqniu=pout(2,:)./(1+vel1);

peaks = job_pack_0.peaks;
minf0=min(peaks(2,:));
maxf0=max(peaks(2,:));
peaks(2,:) = freqniu;
peaks(4,:) = weights;
peaks(5,:)=radpat;

peaks(5,:)=peaks(5,:).*peaks(4,:);
%unw = unique(peaks(5,:)); 
%size(unw)
%w = unw(1:10);
%w = unw(end-10:end);

peaks(5,:)=peaks(5,:)/mean(peaks(5,:));
%unw = unique(peaks(5,:)); 
%size(unw);
%w = unw(1:10)
%w = unw(end-10:end)

wpeaks = peaks(5,:);

[n1,n2]=size(peaks);
toc;
display("prehough")
tic;

Day_inSeconds=86400;
tObs = tFft*Nt/(2*60*60*24*30);
tObs = round(tObs*30*24*60*60);

epoch=job_pack_0.basic_info.epoch;
peaks(1,:)=peaks(1,:)-epoch;

%mind1=basic_info.run.sd(2);
minf0=min(peaks(2,:));
maxf0=max(peaks(2,:));
enh=10;  
df2=df/2;
ddf=df/enh;  % refined frequency step
inifr=minf0-df2-ddf;
finfr=maxf0+df2+ddf;
nbin_f0=ceil((finfr-inifr)/ddf)+I1000;
deltaf2=round(enh/2+0.001); % semi-width of the strip (in ddf)

n_of_peaks=length(peaks);
ii=find(diff(peaks(1,:)));  % find the different times
ii=[ii n_of_peaks]; 
nTimeSteps=length(ii); % number of times of the peakmap

dmin1=-1e-8;
%dmin1 = -7.253835385771362e-09;
dmax =1e-9;
%dmax =  -1.761506157314088e-09;
deltad=df/tObs  % spin-down step
nbin_d=round(((dmax-dmin1)/deltad));  % spin-down number of steps
d=dmin1+(0:nbin_d-1)*deltad;
%d = d(320:320+640);
%d = d(1:640)
size(d)
format long
%fprintf("%f", d);
ii0=1; %nbin_d,nbin_f0
toc;
display("hough")
tic;

#ridefinizione di variabili per comodità
nstepFdot = nbin_d;
nstepsFreqs = nbin_f0;
enhance = 10;
secbelt = I1000;
times = peaks(0,:)
frequencies = peaks(1,:)
weights = peaks(2,:)
spindowns = d;

#funzione nuova
function [houghmap] = vectorHough(times, frequencies, weights, spindowns, nstepFdot, nstepsFreqs, enhance, secbelt)
    nRows = nstepFdot;
    nColumns = nstepFreqs;
    
    values = zeros(nColumns)
    function [values] = rowTransform(ithStep)
        sdTimed = spindowns(ithStep)*times
        transform = round(frequencies-sdTimed+secbelt/2)
        %WARNING
        values = sum(weights(transform))???
        %WARNING
    end
    houghLeft = zeros((nRows,nColumns))
    for i=1:nRows
        houghLeft(i,:) = rowTransform(i)
    end
end

#qui la funzione viene eseguita e il codice prosegue come prima
binh_df0 = vectorHough(times, frequencies, weights, spindowns, nstepFdot, nstepsFreqs, enhance, secbelt);

binh_df0(:,deltaf2*2+1:nbin_f0)=...
    binh_df0(:,deltaf2*2+1:nbin_f0)-binh_df0(:,1:nbin_f0-deltaf2*2); % half to full diff. map - Carl Sabottke idea
binh_df0=cumsum(binh_df0,2);   % creation of the Hough map

hough = binh_df0;

hfdf=gd2(binh_df0.');
hfdf=edit_gd2(hfdf,'dx',ddf,'ini',inifr-I500*ddf,'dx2',deltad,'ini2',dmin1,'capt','Histogram of spin-f0');
toc;
display("cand")
tic;
mode=2;
%job_info.proc.F_hfdf_peak.mode=mode;
if mode == 2
    mno=job_pack_0.basic_info.mode.hm_job.frenh*4;
 %   job_info.proc.F_hfdf_peak.mno=mno;
end

frini=job_pack_0.basic_info.frin;
frfin=job_pack_0.basic_info.frfi;
cand=zeros(9,kcand);

g=cut_gd2(hfdf,[frini,frfin],[-100,dmin1+nbin_d*deltad/4],1);
y=y_gd2(g);
houghcand = y;
fr=x_gd2(g);
sd=x2_gd2(g);
[ym,im]=max(y');
N=length(ym);
df=N/kcand;
ix=round(1:df:N);
ix=[ix N+1];
robst=robstat(y(:),0.01);
%job_info.robst=robst;
robmedtot=robst(1);
robstdtot=robst(2);


robmed=zeros(1,kcand);
robstd=robmed;

for i = 2:kcand-1
    robst=robstat(ym(ix(i-1):ix(i+2)-1),0.01);
    robmed(i)=robst(1);
    robstd(i)=robst(2);
end

robst=robstat(ym(ix(1):ix(3)-1),0.01);
robmed(1)=robst(1);
robstd(1)=robst(2);
ii=length(ix);
robst=robstat(ym(ix(ii-2):ix(ii)-1),0.01);
robmed(kcand)=robst(1);
robstd(kcand)=robst(2);

jj=0;
for i = 1:kcand
    if robmed(i) > 0
        ii=ix(i);
        yy=ym(ix(i):ix(i+1)-1);
        [ma,ima]=max(yy);
        
        if ma > robmed(i) && ma > robmedtot/2
            jj=jj+1;
            iii=ii+ima-1;
            cand(1,jj)=ma;
            cand(2,jj)=(ma-robmed(i))/robstd(i);
            cand(3,jj)=fr(iii);
            cand(4,jj)=sd(im(iii));
            cand(5,jj)=grigliaECL(1);
            cand(6,jj)=grigliaECL(2);
            cand(7,jj)=grigliaECL(3)/2;
            cand(8,jj)=abs(grigliaECL(4)-grigliaECL(5))/4;
            cand(9,jj)=1;
            %QUI C'È LA RICERCA DELL'ALTRO MASSIMO
            mode = 1;
        end
    end
end

cand(4,:)=correct_sampling(cand(4,:),0, job_pack_0.basic_info.run.sd.dnat);
toc;
display("endcand, enditer, endcode")
%save("/home/iuri.larosa/master/outputs/" + nomefile + "MATsidpat.mat", "pat")
%save("/home/iuri.larosa/master/outputs/" + nomefile + "MATinTime.mat", "times")
%save("/home/iuri.larosa/master/outputs/" + nomefile + "MATgmstimes.mat", "sidh")
%save("/home/iuri.larosa/master/outputs/" + nomefile + "MATradpat.mat", "radpat")
%save("/home/iuri.larosa/master/outputs/" + nomefile + "MATwpeaks.mat", "wpeaks")
%save("/home/iuri.larosa/master/outputs/tests/" + nomefile + "MATCands.mat", "cand")
%save("/home/iuri.larosa/master/outputs/" + nomefile + "MATFreqCorr.mat", "freqniu")
%save("/home/iuri.larosa/master/outputs/tests/" + nomefile + "MATHough.mat", "hough")
%save("/home/iuri.larosa/master/outputs/tests/" + nomefile + "MATHoughcand.mat", "houghcand")
