load in_O2LL_02_0327_.mat
%in_O2LL_02_0327_.mat %in_O2LL_03_0744_.mat  %in_O2LL_01_0093_.mat in_O2LL_04_1473_.mat


tic





enh=10.0;  % frequency enhancement factor (typically 10)
df=job_pack_0.basic_info.dfr   % raw frequency resolution
tFft = 1/df
df2=df/2;
ddf=df/enh;  % refined frequency steps

Nt= job_pack_0.basic_info.ntim;
tObs = tFft*Nt/(2*60*60*24*30);
tObs = round(tObs*30*24*60*60);
epoch=(57722+57990)/2;

dmin1=-1e-8;
dmax =1e-9;% spin-down initial value
deltad=df/tObs;  % spin-down steps
nbin_d=round((dmax-dmin1)/(deltad))  % spin-down number of steps

I1000=4000;  % security belt (even)
I500=I1000/2;
Day_inSeconds=86400;




peaks = job_pack_0.peaks;
[n1,n2]=size(peaks);

peaks(1,:)=peaks(1,:)-epoch;

minf0=min(peaks(2,:));
maxf0=max(peaks(2,:));
inifr=minf0-df2-ddf;
finfr=maxf0+df2+ddf;
nbin_f0=ceil((finfr-inifr)/ddf)+I1000;
deltaf2=round(enh/2+0.001); % semi-width of the strip (in ddf)

n_of_peaks=length(peaks);
ii=find(diff(peaks(1,:)));  % find the different times
ii=[ii n_of_peaks]; 
nTimeSteps=length(ii); % number of times of the peakmap

d=dmin1+(0:nbin_d-1)*deltad;

peaks(5,:) = 1;

ii0=1; %nbin_d,nbin_f0

binh_df0 = coreHoughDynLoop_mex(peaks,inifr,ddf,Day_inSeconds,deltaf2,ii0,ii,nbin_d,d,I500,nTimeSteps,nbin_f0);

binh_df0(:,deltaf2*2+1:nbin_f0)=...
    binh_df0(:,deltaf2*2+1:nbin_f0)-binh_df0(:,1:nbin_f0-deltaf2*2); % half to full diff. map - Carl Sabottke idea
binh_df0=cumsum(binh_df0,2);   % creation of the Hough map
    
hfdf=gd2(binh_df0.');
hfdf=edit_gd2(hfdf,'dx',ddf,'ini',inifr-I500*ddf,'dx2',deltad,'ini2',dmin1,'capt','Histogram of spin-f0');
toc

