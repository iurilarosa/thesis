
tic;
addpath(genpath('/home/iuri.larosa/programmi/matlabLibs/'))
detector = "LL";
%nomefile = "_01_0079_";
nomefile = "_02_0322_";
%nomefile = "_03_0764_";
%nomefile = "_04_1533_";


file = "in_O3" + detector + nomefile
display("load")
load("/home/iuri.larosa/master/data/" + file + ".mat")
load("/home/iuri.larosa/master/outputs/tests/tf/" + file + "grid.mat")
load("/home/iuri.larosa/master/outputs/tests/" + file + "MATweights.mat")

df=job_pack_0.basic_info.dfr ;
runstr = job_pack_0.basic_info.run;
tFft = 1/df;

I1000=4000*2;
if tFft == 8192
    I1000 = 4000*8;
end
if tFft == 4096
    I1000 = 4000;
end
I500=I1000/2;

kcand = 100;
if tFft == 8192
    kcand = 20;
end

%size(rectgrid)
point = 1;
grigliaREC = rectgrid(point,:);
grigliaECL = eclgrid(point,:);
pin = job_pack_0.peaks;
pout=pin;
sizepeaks = size(pout);

index=job_pack_0.basic_info.index;
Nt= job_pack_0.basic_info.ntim;

vel_orig=job_pack_0.basic_info.velpos;

toc;
display("startloop")
display("weights+dopp")
tic;
griglia = grigliaECL;
[alpha,delta]=astro_coord('ecl','equ',grigliaECL(1),grigliaECL(2));
r=grigliaREC;  %astro2rect([alpha,delta],0);
v=vel_orig(1:3,:);


v1=r*v;  % DA BOLOGNA


vel1=zeros(1,index(Nt+1)-1);
for i=1:Nt
    for j=index(i):index(i+1)-1
        vel1(:,j)=v1(:,i);
    end
end


for i=1:10
    for j=index(i):index(i+1)-1
        vel1(:,j)=v1(:,i);
    end
end

for i=(Nt-10):Nt
    for j=index(i):index(i+1)-1
        vel1(:,j)=v1(:,i);
    end
end

times = pin(1,:);
sidh=gmst(pin(1,:));
sour.a=alpha;
sour.d=delta;
sour.eta=1;
sour.psi=0;

sidpat=pss_sidpat_psi(sour,runstr.ant,120,0);

pat = y_gd(sidpat);

radpat=gd_interp(sidpat,sidh);

radpat=radpat/mean(radpat);

freqniu=pout(2,:)./(1+vel1);

peaks = job_pack_0.peaks;
minf0=min(peaks(2,:));
maxf0=max(peaks(2,:));
peaks(2,:) = freqniu;
peaks(4,:) = weights;
peaks(5,:)=radpat;

peaks(5,:)=peaks(5,:).*peaks(4,:);
%unw = unique(peaks(5,:)); 
%size(unw)
%w = unw(1:10);
%w = unw(end-10:end);

peaks(5,:)=peaks(5,:)/mean(peaks(5,:));
%unw = unique(peaks(5,:)); 
%size(unw);
%w = unw(1:10)
%w = unw(end-10:end)

wpeaks = peaks(5,:);

[n1,n2]=size(peaks);
toc;
display("prehough")
tic;

Day_inSeconds=86400;
tObs = tFft*Nt/(2*60*60*24*30);
tObs = round(tObs*30*24*60*60);

epoch=job_pack_0.basic_info.epoch;
peaks(1,:)=peaks(1,:)-epoch;

%mind1=basic_info.run.sd(2);
minf0=min(peaks(2,:));
maxf0=max(peaks(2,:));
enh=10;  
df2=df/2;
ddf=df/enh;  % refined frequency step
inifr=minf0-df2-ddf;
finfr=maxf0+df2+ddf;
nbin_f0=ceil((finfr-inifr)/ddf)+I1000;
deltaf2=round(enh/2+0.001); % semi-width of the strip (in ddf)

n_of_peaks=length(peaks);
ii=find(diff(peaks(1,:)));  % find the different times
ii=[ii n_of_peaks]; 
nTimeSteps=length(ii); % number of times of the peakmap

dmin1=-1e-8;
dmax =1e-9;
deltad=df/tObs;  % spin-down step
nbin_d=round(((dmax-dmin1)/deltad));  % spin-down number of steps
d=dmin1+(0:nbin_d-1)*deltad;
d(321)
d(321+640)
size(d)