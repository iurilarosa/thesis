load /home/iuri.larosa/ifil/o
in_O2LL_02_0327_.mat %in_O2LL_02_0327_.mat %in_O2LL_03_0744_.mat  %in_O2LL_01_0093_.mat in_O2LL_04_1473_.mat
load /home/iuri.larosa/check/MATfullgrids/4096_MATgrid.mat %8192_MATgrid.mat 4096_MATgrid.mat  1024_MATgrid.mat
tFft = 4096;

pin = job_pack_0.peaks;
pout=pin;

index=job_pack_0.basic_info.index;
Nt= job_pack_0.basic_info.ntim;

peaks = job_pack_0.peaks;
[n1,n2]=size(peaks);

I1000=4000;
I500=I1000/2;
Day_inSeconds=86400;
tObs = tFft*Nt/(2*60*60*24*30);
tObs = round(tObs*30*24*60*60);
peaks(5,:) = 1;

epoch= (57722+57990)/2; % Hough epoch (time barycenter) in mjd
peaks(1,:)=peaks(1,:)-epoch;

minf0=min(peaks(2,:));
maxf0=max(peaks(2,:));
df=1/tFft;   % raw frequency resolution
enh=10;  % frequency enhancement factor (typically 10)
df2=df/2;
ddf=df/enh;  % refined frequency step
inifr=minf0-df2-ddf;
finfr=maxf0+df2+ddf;
nbin_f0=ceil((finfr-inifr)/ddf)+I1000;
deltaf2=round(enh/2+0.001); % semi-width of the strip (in ddf)

n_of_peaks=length(peaks);
ii=find(diff(peaks(1,:)));  % find the different times
ii=[ii n_of_peaks]; 
nTimeSteps=length(ii); % number of times of the peakmap

dmin1=-1e-8;
dmax =1e-9;% spin-down initial value
deltad=df/tObs;  % spin-down step
nbin_d=round((dmax-dmin1)/(deltad))  % spin-down number of steps

d=dmin1+(0:nbin_d-1)*deltad;  
ii0=1; %nbin_d,nbin_f0

tic;
binh_df0 = coreHoughDynLoop_mex(peaks,inifr,ddf,Day_inSeconds,deltaf2,ii0,ii,nbin_d,d,I500,nTimeSteps,nbin_f0);
%  binh_df0=zeros(nbin_d,nbin_f0);  %  HM matrix container
%  for it = 1:nTimeSteps
%      kf=(peaks(2,ii0:ii(it))-inifr)/ddf;  % normalized frequencies
%      w=peaks(5,ii0:ii(it));               % wiener weights
%      t=peaks(1,ii0)*Day_inSeconds; % time conversion days to s
%      tddf=t/ddf;
%      f0_a=kf-deltaf2; 
%      
%      for id = 1:nbin_d   % loop for the creation of half-differential map
%          td=d(id)*tddf;
%          
%          a=1+round(f0_a-td+I500); 
%          %if a > nbin_f0
%          %    warning = "ATENTO"
%          %    a
%          %end
%          binh_df0(id,a)=binh_df0(id,a)+w; % left edge of the strips
%      end
%      ii0=ii(it)+1;
%  end

binh_df0(:,deltaf2*2+1:nbin_f0)=...
    binh_df0(:,deltaf2*2+1:nbin_f0)-binh_df0(:,1:nbin_f0-deltaf2*2); % half to full diff. map - Carl Sabottke idea
binh_df0=cumsum(binh_df0,2);   % creation of the Hough map

hough = binh_df0;
save /home/iuri.larosa/master/out/2048_MATHough.mat hough

hfdf=gd2(binh_df0.');
hfdf=edit_gd2(hfdf,'dx',ddf,'ini',inifr-I500*ddf,'dx2',deltad,'ini2',dmin1,'capt','Histogram of spin-f0');
toc

mode=1;
%job_info.proc.F_hfdf_peak.mode=mode;
if mode == 2
    mno=job_pack_0.basic_info.mode.hm_job.frenh*4;
 %   job_info.proc.F_hfdf_peak.mno=mno;
end
kcand = 100;
frini=job_pack_0.basic_info.frin;
frfin=job_pack_0.basic_info.frfi;
cand=zeros(9,kcand);

g=cut_gd2(hfdf,[frini,frfin],[-100,100],1);
y=y_gd2(g);


save /home/iuri.larosa/master/out/2048_MATHoughSliced.mat y 

size(y)
fr=x_gd2(g);
sd=x2_gd2(g);
[ym,im]=max(y');
N=length(ym);
df=N/kcand;
ix=round(1:df:N);
ix=[ix N+1];
robst=robstat(y(:),0.01);
%job_info.robst=robst;
robmedtot=robst(1);
robstdtot=robst(2);


robmed=zeros(1,kcand);
robstd=robmed;

for i = 2:kcand-1
%     disp(sprintf('%d %d %d %d',i,ix(i-1),ix(i+2)-1,length(ym)))
    robst=robstat(ym(ix(i-1):ix(i+2)-1),0.01);
    robmed(i)=robst(1);
    robstd(i)=robst(2);
end
robst=robstat(ym(ix(1):ix(3)-1),0.01);
robmed(1)=robst(1);
robstd(1)=robst(2);
ii=length(ix);
robst=robstat(ym(ix(ii-2):ix(ii)-1),0.01);
robmed(kcand)=robst(1);
robstd(kcand)=robst(2);
%job_info.robmed=robmed;
%job_info.robstd=robstd;

%stat = struct();
%stat.ymax = ym;
%stat.imax = im; 
%stat.matstat = [robmed', robstd'];
%stat.imageCand= y;

%save /home/protoss/Documenti/TESI/HWITest/robcontrol187.mat stat

jj=0;
for i = 1:kcand
    if robmed(i) > 0
        ii=ix(i);
        yy=ym(ix(i):ix(i+1)-1);
        [ma,ima]=max(yy);
        
        %if i==2
        %    ii=ix(i)
        %    iff = ix(i+1)-1
        %    yy
        %    [ma,ima]=max(yy)
        %    robmed(i)
        %    robmedtot/2
        %end
        if ma > robmed(i) && ma > robmedtot/2
            jj=jj+1;
            iii=ii+ima-1;
            cand(1,jj)=iii;%fr(iii);
            cand(2,jj)=0;
            cand(3,jj)=0;
            cand(4,jj)=im(iii);%sd(im(iii));
            cand(5,jj)=ma;
            cand(6,jj)=(ma-robmed(i))/robstd(i);
            cand(7,jj)=0;
            cand(8,jj)=0;
            cand(9,jj)=1;

        end
    end
end

%cand(4,:)=correct_sampling(cand(4,:),0, job_pack_0.basic_info.run.sd.dnat);
save /home/iuri.larosa/check/2048_MATCands.mat cand 

