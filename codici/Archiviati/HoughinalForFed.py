import tensorflow as tf
import numpy
import scipy.io
from tensorflow.python.client import timeline
import time



# CARICO DATI

#tFft = 8192

percorsoDati = "/storage/users/Muciaccia/data/validation/dense_peakmaps/amplitude_1.mat"


#index = 9002 #signal
index = 4 #signal meno bello
#index = 5001
#index = 0

struttura1 = scipy.io.loadmat(percorsoDati)['H']
#peakmap1 = struttura1[index].copy()
#del struttura1

struttura2 = scipy.io.loadmat(percorsoDati)['L']
#peakmap2 = struttura2[index].copy()
#del struttura2

struttura3 = scipy.io.loadmat(percorsoDati)['V']
#peakmap3 = struttura3[index].copy()
#del struttura3


strutturaTOT = struttura1 + struttura2 + struttura3


###############################


def noncorr():
	freqMin = numpy.amin(frequenze)
	freqIniz = freqMin - stepFreq/2 -stepFreqRaffinato
	freqNonCor = (frequenze -freqIniz)/stepFreqRaffinato-round(enhancement/2+0.001)
	#freqNonCor = tf.constant(freqNonCor, dtype = tf.float32)
	return freqNonCor, freqIniz


# calcola la hough per ogni punto del cielo (per ogni spindown)
def inDaHough(freqHM):
	#calcola la hough per ogni step di spindown
	#WARNING per ora la metto qui perché non so come si passano variabili non iterabili con tf.map
	def houghizza(stepIesimo):
		sdTimed = tf.multiply(spindownsTF[stepIesimo], tempiHM, name = "Tdotpert")
		#sdTimed = tf.cast(sdTimed, dtype=tf.float32)
	
		appoggio = tf.round(freqHM-sdTimed+securbelt/2, name = "appoggioperindici")
		appoggio = tf.cast(appoggio, dtype=tf.int32)
		
		valorisx = tf.unsorted_segment_sum(pesiHM, appoggio, nColumns)
		return valorisx

	houghDiff = tf.map_fn(houghizza, tf.range(0, nRows), dtype=tf.float32, parallel_iterations=8)

	def sliceInt():
		#faccio integrazione finale (vecchia versione senza conv)
		semiLarghezza = tf.round(enhancement/2+0.001)
		semiLarghezza = tf.cast(semiLarghezza, tf.int32)
		houghInt = houghDiff[:,enhancement:nColumns]-houghDiff[:,0:nColumns - enhancement]
		houghInt = tf.concat([houghDiff[:,0:enhancement],houghInt],1)
		return houghInt

	
	hough = sliceInt()
	#hough = convInt()
	houghinal = tf.cumsum(hough, axis = 1)
	
	return houghinal
	#return houghDiff
    
    
def manchurian_candidates(numCand, freqIniz, image):
	minDistance = enhancement*4
	
	
	candidati = numpy.zeros((numCand*2,5))
	#candidati = numpy.zeros((5,numCand*2))

	primaFreq = freqIniz-(securbelt/2)*stepFreqRaffinato

	freqIniziale = numpy.amin(frequenze)
	freqFinale = numpy.amax(frequenze)
	

	indexIniziale = ((freqIniziale-primaFreq)/stepFreqRaffinato).astype(numpy.int64)
	indexFinale = ((freqFinale-primaFreq)/stepFreqRaffinato+1).astype(numpy.int64)

	imageCand = image[:,indexIniziale:indexFinale]

	size = numpy.shape(imageCand)[1]
	freqniu = numpy.arange(0,size)*stepFreqRaffinato+freqIniziale

	maxPerColumn = numpy.amax(imageCand, axis = 0)
	rigaMax = numpy.argmax(imageCand, axis = 0)

	#######################

	stepFrequenzaNiu = maxPerColumn.size/numCand

	indiciFreq = numpy.arange(0,maxPerColumn.size,stepFrequenzaNiu)
	indiciFreq = numpy.append(indiciFreq, maxPerColumn.size)
	indiciFreq = numpy.round(indiciFreq).astype(numpy.int64)

	def statistics(ndArray):
		#ndArray = numpy.ravel(ndArray)
		mediana = numpy.median(ndArray)
		sigmana = numpy.median(numpy.absolute(ndArray-mediana))/0.6745
		return mediana, sigmana

	stats = statistics(imageCand)
	medianaTot = stats[0]

	iniziali = numpy.concatenate(([indiciFreq[0]],indiciFreq[0:numCand-2],[indiciFreq[indiciFreq.size-3]]),0)
	finali = numpy.concatenate(([indiciFreq[2]-1],indiciFreq[3:numCand+1]-1,[indiciFreq[indiciFreq.size-1]-1]),0)

	def statsPerCand(i):
		stat = statistics(maxPerColumn[iniziali[i]:finali[i]])#[0]
		return stat

	statPerCand = numpy.array(list(map(statsPerCand, numpy.arange(numCand))))
	medianaPerCand = statPerCand[:,0]
	sigmanaPerCand = statPerCand[:,1]
	
	filtro = numpy.where(medianaPerCand > 0)[0]
	#medCandFiltrata = medianaPerCand[filtro]
	counter = 0
	for i in filtro:
		inizio = indiciFreq[i]
		fine = indiciFreq[i+1]-1
		porzioneMaxPerColumn = maxPerColumn[inizio:fine]
		localMax = numpy.amax(porzioneMaxPerColumn)
		localInd = numpy.argmax(porzioneMaxPerColumn)
		if localMax > medianaPerCand[i] and localMax > medianaTot/2:
			counter = counter + 1
			index = indiciFreq[i] + localInd-1
			candidati[counter,0] = freqniu[index]
			riga = rigaMax[index]
			candidati[counter,1] = spindowns[riga]
			candidati[counter,2] = localMax
			candidati[counter,3] = (localMax-medianaPerCand[i])/sigmanaPerCand[i]
			candidati[counter,4] = 1

			limite1 = numpy.amax([localInd-minDistance,1]).astype(numpy.int32)
			limite2 = numpy.amin([localInd+minDistance,porzioneMaxPerColumn.size]).astype(numpy.int32)
			porzioneMaxPerColumn[limite1:limite2] = 0
			secondLocMax = numpy.amax(porzioneMaxPerColumn)
			secondLocInd = numpy.argmax(porzioneMaxPerColumn)

			if numpy.absolute(secondLocInd-localInd) > 2 * minDistance and secondLocMax > medianaPerCand[i]:
				counter = counter + 1
				index = indiciFreq[i] + secondLocInd-1
				candidati[counter,0] = freqniu[index]
				riga = rigaMax[index]
				candidati[counter,1] = spindowns[riga]
				candidati[counter,2] = secondLocMax
				candidati[counter,3] = (secondLocMax-medianaPerCand[i])/sigmanaPerCand[i]
				candidati[counter,4] = 2

	candidati[1,:]=numpy.round(candidati[1,:] / stepSpindown) * stepSpindown
	
	
	return candidati


start = time.time()

tFft = 4096
tObs = 1/5 #mesi
tObs = tObs*30*24*60*60
#nPunti = 2
cands = 32
primaFreq = 1/tFft

#maximumCand = numpy.array()

#nIndexes = 10
nIndexes = strutturaTOT.shape[0]
print(nIndexes)
allMaxCands = numpy.zeros((nIndexes,4,5))
#print(allMaxCands.shape, allMaxCands)

#nIndexes = 100
for index in numpy.arange(nIndexes):
	sessione = tf.Session()
	#print(index)
	#startino = time.time()
	peakmapTOT = strutturaTOT[index]

	#del struttura1
	#del struttura2
	#del struttura3

	sparsa = numpy.nonzero(peakmapTOT)

	frequenze,tempi = sparsa
	tempi = tempi+1
	frequenze = frequenze / tFft + 1
	pesi = numpy.ones(sparsa[0].size)




	#nb: picchi ha 0-tempi
	#              1-frequenze
	#              2-pesi

	#headers vari
	securbelt = 2000
	#securbelt = 4000*3

	#frequenze
	stepFreq = 1/tFft
	enhancement = 10
	#enhancement = 1
	stepFreqRaffinato =  stepFreq/enhancement
	nstepsFreq = securbelt+(numpy.amax(frequenze)-numpy.amin(frequenze) + stepFreq + 2*stepFreqRaffinato)/stepFreqRaffinato


	epoca = 55

	#spindowns
	spindownMin = -10e-10
	spindownMax = 9e-10
	nstepSpindown = 100
	stepSpindown = (spindownMax-spindownMin)/nstepSpindown

	spindowns = numpy.arange(0, nstepSpindown)
	spindowns = numpy.multiply(spindowns,stepSpindown)
	spindowns = numpy.add(spindowns, spindownMin)
	
	
	#da qui si usa tensorflow
	#definisco tutte le costanti necessarie
	tempiTF = tf.constant(tempi, dtype=tf.float64)

	pesiTF = tf.constant(pesi,dtype=tf.float32)

	spindownsTF = tf.constant(spindowns, dtype=tf.float32) 

	tempiHM = tempiTF-epoca
	tempiHM = ((tempiHM)*3600*24/stepFreqRaffinato)
	tempiHM = tf.cast(tempiHM, tf.float32)

	pesiHM = tf.reshape(pesiTF,(1,tf.size(pesiTF)))
	pesiHM = pesiHM[0]

	nRows = tf.constant(nstepSpindown, dtype=tf.int32)
	#problema! num step freq cambia a seconda della correzione doppler
	#perché freq min e freq max possono variare e lo step lo si lascia uguale
	#posso andarci in 2 modi: uno è tagliando a 96000 tutto
	#uno è mettendo un po' di zeri prima e dopo, cercando con la doppler corr quale è la max assoluta
	#e quale è la min assoluta
	nColumns = tf.cast(nstepsFreq, dtype=tf.int32)

	#freqTF = noncorr()

	#for punto in numpy.arange(0,nPunti-1):
	freq, freqIn = noncorr()

	freqTF = tf.constant(freq, dtype = tf.float32)
	houghmap = inDaHough(freqTF)
	
	#startino = time.time()
	hough = sessione.run(houghmap)
	#endino = time.time()
	#print(endino-startino)
	
	candidati = manchurian_candidates(cands, freqIn, hough)

	maximumCand =candidati[numpy.argsort(candidati[:,3])]
	maximumCand = maximumCand[-4:]
	allMaxCands[index] = maximumCand
	#endino = time.time()
	#print(endino-startino)
	tf.reset_default_graph()
	sessione.close()

print(time.time()-start)

#numAllCands = 4 * nIndexes
#allCands = allCands.reshape(numAllCands,5)
#nonzeri = numpy.nonzero(allCands[:,0])
#finallCands = allCands[nonzeri]
#finallCands = numpy.transpose(allMaxCands)
#print(allMaxCands)
	
	
#finalCand = finalCand[:,0,:]

numpy.save("topCandsFedAmp1.npy", allMaxCands)
#percorsoCand = "provaCandFed.mat"
#scipy.io.savemat(percorsoCand,{"topcands": allMaxCands})
						     #"maxs": maxPerColumn,
						     #"imax": rigaMax,
						     #"imageCand": imageCand})
	#print(finalCand[0])


#from matplotlib import pyplot

#pyplot.figure(figsize=(12, 8))

#a = pyplot.imshow(hough, cmap = 'gray_r', origin = 'lower', interpolation = 'none', aspect = 30)
#pyplot.colorbar(shrink = 0.5,aspect = 15)
#pyplot.show()
