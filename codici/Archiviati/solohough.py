import tensorflow as tf
import numpy
import scipy.io
#from tensorflow.python.client import timeline
import time

percorsoDati = "/home/protoss/Documenti/TESI/wn100bkp/data/dati9mesi108HWI.mat"
#percorsoDati = "/home/iurilarosa/dati/dati9mesi108HWI.mat"

tFft = 8192
#tFft = 4096
tObs = 9 #mesi
tObs = tObs*30*24*60*60

#carico file dati
struttura = scipy.io.loadmat(percorsoDati)['job_pack_0']

#peakmap = struttura['peaks'][0,0]
#print(peakmap)
#tempiIndiciSorted = numpy.argsort(peakmap[0,:])
#tempi = peakmap[0, tempiIndiciSorted]
#frequenze = peakmap[1, tempiIndiciSorted]
#pesi =peakmap[4, tempiIndiciSorted] + 1 #numpy.ones(peakmap[4].size)#

#print(tempi)
#print(frequenze)
#print(pesi)

tempi = struttura['peaks'][0,0][0]#.astype(numpy.float32)
frequenze = struttura['peaks'][0,0][1]#.astype(numpy.float32)
pesi = (struttura['peaks'][0,0][4]+1)#.astype(numpy.float32)


primoTempo = struttura['basic_info'][0,0]['tim0'][0,0][0,0]

tempi = (tempi-primoTempo)

filtro = numpy.where(tempi[0:tempi.size-1]-tempi[1:tempi.size] != 0)
tempiUnici = numpy.append(tempi[filtro], tempi[tempi.size-1])
print(tempiUnici[tempiUnici.size-1])
#nb: picchi ha 0-tempi
#              1-frequenze
#              2-pesi

#headers vari
securbelt = 4000


#frequenze
#frequenze
stepFrequenza = 1/tFft
enhancement = 10
stepFreqRaffinato =  stepFrequenza/enhancement

freqMin = numpy.amin(frequenze)
freqMax = numpy.amax(frequenze)
freqIniz = freqMin- stepFrequenza/2 - stepFreqRaffinato
freqFin = freqMax + stepFrequenza/2 + stepFreqRaffinato
nstepFrequenze = numpy.ceil((freqFin-freqIniz)/stepFreqRaffinato)+securbelt

#tempi
#epoca definita come mediana di tempi di tutto il run
#epoca = (57722+57990)/2 #0
epoca = 0

#spindowns
spindownMin = -1e-9
spindownMax = 2e-10
stepSpindown = stepFrequenza/tObs 

nstepSpindown = numpy.round((spindownMax-spindownMin)/stepSpindown).astype(numpy.int32)

print(nstepSpindown)
# riarrangio gli array in modo che abbia i dati 
# nel formato che voglio io
frequenze = frequenze-freqIniz
frequenze = (frequenze/stepFreqRaffinato)-round(enhancement/2+0.001)

tempi = tempi-epoca
tempi = ((tempi)*3600*24/stepFreqRaffinato)
#tempi = numpy.round(tempi/1e8)*1e8

spindowns = numpy.arange(0, nstepSpindown)
spindowns = numpy.multiply(spindowns,stepSpindown)
spindowns = numpy.add(spindowns, spindownMin)
# così ho i tre array delle tre grandezze, 
#più i pesi e la fascia di sicurezza
indice0 = numpy.where(spindowns>0)[0][0]-1
print(indice0)
spindowns = spindowns-spindowns[indice0]
 


def mapnonVar(stepIesimo):
    sdTimed = tf.multiply(spindownsTF[stepIesimo], tempiTF, name = "Tdotpert")
    #sdTimed = tf.cast(sdTimed, dtype=tf.float32)
    
    appoggio = tf.round(frequenzeTF-sdTimed+securbeltTF/2, name = "appoggioperindici")
    appoggio = tf.cast(appoggio, dtype=tf.int32)
    
    valori = tf.unsorted_segment_sum(pesiTF, appoggio, nColumns)

#    zeriDopo = tf.zeros([nColumns - tf.size(valori)], dtype=tf.float32)    
#    riga = tf.concat([valori,zeriDopo],0, name = "rigadihough")
    return valori


#ora uso Tensorflow
securbeltTF = tf.constant(securbelt,dtype=tf.float32)
tempiTF = tf.constant(tempi,dtype=tf.float32)
pesiTF = tf.constant(pesi,dtype=tf.float32)
spindownsTF = tf.constant(spindowns, dtype=tf.float32)
frequenzeTF = tf.constant(frequenze, dtype=tf.float32)

nRows = numpy.int32(nstepSpindown)
nColumns = numpy.int32(nstepFrequenze)

pesiTF = tf.reshape(pesiTF,(1,tf.size(pesi)))
pesiTF = pesiTF[0]

houghLeft = tf.map_fn(mapnonVar, tf.range(0, nRows), dtype=tf.float32, parallel_iterations=8)
#let's superimpose the right edge on the image
houghRight = houghLeft[:,enhancement:nColumns]-houghLeft[:,0:nColumns - enhancement]
houghDiff = tf.concat([houghLeft[:,0:enhancement],houghRight],1)
#at last, the Hough map is computed integrating along the frequencies
houghMap = tf.cumsum(houghDiff, axis = 1)


#sessione = tf.Session(config=tf.ConfigProto(log_device_placement=True))
sessione = tf.Session()

#run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
#run_metadata = tf.RunMetadata()

start = time.time()
#image = sessione.run(imagenonVar, options=run_options, run_metadata=run_metadata)
image = sessione.run(houghMap)
print(image.shape, image.size, image.nbytes)

#nColumns = nColumns.astype(int)
#semiLarghezza = numpy.round(enhancement/2+0.001).astype(int)
#image[:,semiLarghezza*2:nColumns]=image[:,semiLarghezza*2:nColumns]-image[:,0:nColumns - semiLarghezza*2]
#image = numpy.cumsum(image, axis = 1)

stop = time.time()
print(stop-start)


rigaLeft = mapnonVar(1)
#let's superimpose the right edge on the image
rigaRight = rigaLeft[enhancement:nColumns]-rigaLeft[0:nColumns - enhancement]
rigaDiff = tf.concat([rigaLeft[0:enhancement],rigaRight], 0)
#at last, the Hough map is computed integrating along the frequencies
rigaMap = tf.cumsum(rigaDiff)


#sessione = tf.Session(config=tf.ConfigProto(log_device_placement=True))
sessione = tf.Session()

#run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
#run_metadata = tf.RunMetadata()

start = time.time()
#image = sessione.run(imagenonVar, options=run_options, run_metadata=run_metadata)
riga = sessione.run(rigaMap)


#nColumns = nColumns.astype(int)
#semiLarghezza = numpy.round(enhancement/2+0.001).astype(int)
#image[:,semiLarghezza*2:nColumns]=image[:,semiLarghezza*2:nColumns]-image[:,0:nColumns - semiLarghezza*2]
#image = numpy.cumsum(image, axis = 1)

stop = time.time()
print(stop-start)


