%notebook per analisi HWI
%inputs

load ../data/in_O2LL_01_0069_.mat
%load ../data/in_O2LL_02_0367_.mat
%load ../data/in_O2LL_03_0794_.mat
%load ../data/in_O2LL_04_1768_.mat




pin = job_pack_0.peaks;
pout=pin;

Nt= job_pack_0.basic_info.ntim;

peaks = job_pack_0.peaks;
[n1,n2]=size(peaks);

%I1000=4000*2;  % security belt (even)
I1000=4000;
I500=I1000/2;
Day_inSeconds=86400;
tObs = 9;
tObs = tObs*30*24*60*60;
% oper=' adaptive';
hm_job.oper='noadapt';
%'Function forced to do onlysigadapt !!'
peaks(5,:) = 1;

epoch= (57722+57990)/2; % Hough epoch (time barycenter) in mjd
peaks(1,:)=peaks(1,:)-epoch;

firstFreq = job_pack_0.basic_info.frin
if  firstFreq < 2048
    tFft = 1024;
end
if firstFreq < 1024
    tFft = 2048;
end
if firstFreq < 512
    tFft = 4096;
end
if firstFreq < 128
    tFft = 8192;
end
tFft
minf0=min(peaks(2,:));
maxf0=max(peaks(2,:));
df=1/tFft;   % raw frequency resolution
enh=10;  % frequency enhancement factor (typically 10)
df2=df/2;
ddf=df/enh;  % refined frequency step
inifr=minf0-df2-ddf;
finfr=maxf0+df2+ddf;
nbin_f0=ceil((finfr-inifr)/ddf)+I1000;
deltaf2=round(enh/2+0.001); % semi-width of the strip (in ddf)

n_of_peaks=length(peaks);
ii=find(diff(peaks(1,:)));  % find the different times
ii=[ii n_of_peaks]; 
nTimeSteps=length(ii); % number of times of the peakmap

dmin1=-1e-9;
dmax =1e-10;% spin-down initial value
deltad=df/tObs;  % spin-down step
nbin_d=round((dmax-dmin1)/deltad);  % spin-down number of steps
%nbin_d=200;
d=dmin1+(0:nbin_d-1)*deltad;  
ii0=1; %nbin_d,nbin_f0

tic;
binh_df0 = coreHoughDynLoop_mex(peaks,inifr,ddf,Day_inSeconds,deltaf2,ii0,ii,nbin_d,d,I500,nTimeSteps,nbin_f0);

binh_df0(:,deltaf2*2+1:nbin_f0)=...
    binh_df0(:,deltaf2*2+1:nbin_f0)-binh_df0(:,1:nbin_f0-deltaf2*2); % half to full diff. map - Carl Sabottke idea
binh_df0=cumsum(binh_df0,2);   % creation of the Hough map
toc
hough = binh_df0;

filename = sprintf("check/mapMAT%d.mat",tFft)
save(filename,"hough")


hfdf=gd2(binh_df0.');
hfdf=edit_gd2(hfdf,'dx',ddf,'ini',inifr-I500*ddf,'dx2',deltad,'ini2',dmin1,'capt','Histogram of spin-f0');
