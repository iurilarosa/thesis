import tensorflow as tf
import numpy
import time

rc = 1000

for rc in numpy.arange(1000,6000,500):
	print(rc)
	a = numpy.random.rand(rc,rc).astype(numpy.float32)
	b = numpy.random.rand(rc,rc).astype(numpy.float32)

	#print(a,b)

	sess = tf.Session()

	atf = tf.placeholder(dtype = tf.float32)

	btf = tf.placeholder(dtype = tf.float32) 

	ctf = tf.matmul(atf, btf)

	vocab = {
			atf : a,
			btf: b
			}

	start = time.time()
	c = sess.run(ctf, feed_dict = vocab)
	stop = time.time()
	
	inputmem = (a.nbytes+b.nbytes)/1e6
	occtot = sess.run(tf.contrib.memory_stats.MaxBytesInUse())/1e6
	#print("memory used " + str(occupazione) + " MB")
	sess.close()
	x = numpy.array([rc,stop-start,inputmem,occtot]).reshape((1,4))
	f_handle = open('stats.txt', 'a')
	numpy.savetxt(f_handle, x,delimiter=',')#,newline"")
	f_handle.close()
