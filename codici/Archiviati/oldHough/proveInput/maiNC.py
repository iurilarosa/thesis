import tensorflow as tf
import numpy
import xarray
import time

# open a tf session
sess = tf.Session()

# input file path
dataPath = "inputfile1.mat"#"../data/in_O2LL_01_0069_.mat"#"inputfile.mat"

# loading the input file (with the peakmap inside a matlab structure)
#WARNING at the moment the hough transform is parallelized only on a single peakmap
#TODO improve parallelization on multiple peakmaps (or use bigger peakmaps)
struct = scipy.io.loadmat(dataPath)['job_pack_0']

#---------------------------------------
# defining parameters                   |
#---------------------------------------
# times
PAR_tFft = 8192.0
PAR_Ntimes = struct["basic_info"][0,0]["ntim"][0,0][0,0]
PAR_tObs = numpy.round(PAR_tFft*PAR_Ntimes/(2*60*60*24*30)) # mesi
print(PAR_tObs)
PAR_tObs = PAR_tObs*30*24*60*60
PAR_epoch = (57722+57990)/2 

# frequencies
PAR_enhance = 10
PAR_stepFreq = 1/PAR_tFft
PAR_refinedStepFreq =  PAR_stepFreq/PAR_enhance

# spindowns
PAR_fdotMin = -1e-9
PAR_fdotMax = 1e-10
PAR_stepFdot = PAR_stepFreq/PAR_tObs
PAR_nstepFdot = numpy.round((PAR_fdotMax-PAR_fdotMin)/PAR_stepFdot).astype(numpy.int32)

# others
PAR_secbelt = 4000

#---------------------------------------
# loading and managing data             |
#---------------------------------------
# times
times = struct['peaks'][0,0][0]
times = times-PAR_epoch
times = ((times)*60*60*24/PAR_refinedStepFreq)

# frequencies
freqs = struct['peaks'][0,0][1]
freqMin = numpy.amin(freqs)
freqMax = numpy.amax(freqs)
freqStart = freqMin- PAR_stepFreq/2 - PAR_refinedStepFreq
freqEnd = freqMax + PAR_stepFreq/2 + PAR_refinedStepFreq
nstepFreqs = numpy.ceil((freqEnd-freqStart)/PAR_refinedStepFreq)+PAR_secbelt
freqs = freqs-freqStart
freqs = (freqs/PAR_refinedStepFreq)-round(PAR_enhance/2+0.001)
 
# spindowns
spindowns = numpy.arange(0, PAR_nstepFdot)
spindowns = numpy.multiply(spindowns,PAR_stepFdot)
spindowns = numpy.add(spindowns, PAR_fdotMin)

# others
weights = (struct['peaks'][0,0][4]+1)

# once the data of the peakmap are rearranged as above, it's better to rejoin them in an unique array
peakmap = numpy.stack((times,freqs,weights),1)
spindowns = spindowns

# these two variables are only redifinition of old variable to clarify better the Hough transorm code
PAR_nRows = numpy.int32(PAR_nstepFdot)
PAR_nColumns = numpy.int32(nstepFreqs)

#---------------------------------------
# defining TensorFlow graph             |
#---------------------------------------
PAR_secbeltTF = tf.constant(4000,dtype = tf.float32, name = 'secur')
peakmapTF = tf.placeholder(tf.float32, name = 'inputPM')
spindownsTF = tf.placeholder(tf.float32, name = 'inputSD')

# calculating the Hough transofrm, with a fully vectorial tensorflow function with GPU parallelization
def frequencyHough(peakmapHM,spindownsHM):
	def mapnonVar(ithStep):
		sdTimed = tf.multiply(spindownsHM[ithStep], timesHM, name = "Tdotpert")

		transform = tf.round(freqsHM-sdTimed+PAR_secbeltTF/2, name = "trasfFreq")
		transform = tf.cast(transform, dtype=tf.int32)

		values = tf.unsorted_segment_sum(weightsHM, transform, PAR_nColumns, name = "cumsum")
		return values


	timesHM = peakmapHM[:,0]
	freqsHM = peakmapHM[:,1]
	weightsHM = peakmapHM[:,2]

	houghLeft = tf.Variable(tf.zeros([PAR_nRows,PAR_nColumns]),name = "var")
	houghLeft = houghLeft.assign(tf.map_fn(mapnonVar, tf.range(0, PAR_nRows), dtype=tf.float32, parallel_iterations=8))

	# let's superimpose the right edge on the image
	leftStripe = tf.slice(houghLeft, [0,0], [houghLeft.get_shape()[0],10], name = "left_stripe")
	houghRight = tf.subtract(tf.slice(houghLeft, [0,10],[houghLeft.get_shape()[0], houghLeft.get_shape()[1]-10]),
							 tf.slice(houghLeft, [0,0],[houghLeft.get_shape()[0], houghLeft.get_shape()[1]-10]), name = "right_stripe")
	# now we have the so called differential hough map
	houghDiff = tf.concat([leftStripe,houghRight],1, name = "diff_hough")
	# and at last we can cumulative sum along the rows to have the integral hough map
	houghMap = tf.cumsum(houghDiff, axis = 1, name = "output")
	return houghMap

FHMap = frequencyHough(peakmapTF, spindownsTF)

#----------------------------------------
# feeding and running                   |
#----------------------------------------
# this is the dictionary with the data to be fed to the graph using placeholders
dataDict = { 
               peakmapTF: peakmap,
               spindownsTF : spindowns
             }
# initializing the variable

sess.run(tf.global_variables_initializer())
# running the graph
start = time.time()
image = sess.run(FHMap, feed_dict = dataDict)
stop = time.time()
print("execution time " + str(stop-start) + " s")

#from matplotlib import pyplot
#a = pyplot.imshow(image, aspect = 200)
#pyplot.show()
 
 
sess.close()

 
 
 
 
