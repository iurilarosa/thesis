import tensorflow as tf
import numpy
import scipy.io
import time
from tensorflow.python.client import timeline

maxFiles = 5
maxFiles = numpy.concatenate(([1], numpy.arange(35,45)))  
# input file path
PAR_tFft = 4096
dataPath = "/home/protoss/Documenti/TESI/data/in_O2LL_02_0317_.mat" #"input" + str(PAR_tFft) + ".mat"

# loading the input file (with the peakmap inside a matlab structure)
#WARNING at the moment the hough transform is parallelized only on a single peakmap
#TODO improve parallelization on multiple peakmaps (or use bigger peakmaps)
struct = scipy.io.loadmat(dataPath)['job_pack_0']

#---------------------------------------
# defining parameters                   |
#---------------------------------------
# times
PAR_Ntimes = struct["basic_info"][0,0]["ntim"][0,0][0,0]
PAR_tObs = numpy.round(PAR_tFft*PAR_Ntimes/(2*60*60*24*30)) # mesi
print(PAR_tObs)
PAR_tObs = PAR_tObs*30*24*60*60

# frequencies
PAR_enhance = 10
PAR_stepFreq = 1/PAR_tFft
PAR_refinedStepFreq =  PAR_stepFreq/PAR_enhance

# spindowns
PAR_fdotMin = -1e-9
PAR_fdotMax = 1e-10
PAR_stepFdot = PAR_stepFreq/PAR_tObs
PAR_nstepFdot = numpy.round((PAR_fdotMax-PAR_fdotMin)/PAR_stepFdot).astype(numpy.int32)

# others
PAR_secbelt = 4000

#---------------------------------------
# loading and managing data             |
#---------------------------------------

# the address of the peakmap in the input file structure is job_pack_0.peaks
# the peakmap is espressed as a sparse matrix with 3 arrays, since the most part is 0: 
# row coordinates (times), column coordinates (frequencies), values (weights)

for nFiles in numpy.arange(1,maxFiles): 
    # times
    print(nFiles)
    times = struct['peaks'][0,0][0]
    PAR_epoch = (numpy.amax(times)+numpy.amin(times))/2 
    freqs = struct['peaks'][0,0][1]
    weights = (struct['peaks'][0,0][4]+1)

    primatimes = struct['peaks'][0,0][0]
    primafreqs = struct['peaks'][0,0][1]
    primaweights = (struct['peaks'][0,0][4]+1)
    primasize = primatimes.size
    for i in numpy.arange(1, nFiles):

        times = numpy.concatenate((times,primatimes))

        print(times.size/primasize)

        freqs = numpy.concatenate((freqs,primafreqs+i*5))

        weights = numpy.concatenate((weights,primaweights))

    times = times-PAR_epoch
    times = ((times)*60*60*24/PAR_refinedStepFreq)

    # frequencies
    freqMin = numpy.amin(freqs)
    freqMax = numpy.amax(freqs)
    freqStart = freqMin- PAR_stepFreq/2 - PAR_refinedStepFreq
    freqEnd = freqMax + PAR_stepFreq/2 + PAR_refinedStepFreq
    nstepFreqs = numpy.ceil((freqEnd-freqStart)/PAR_refinedStepFreq)+PAR_secbelt
    freqs = freqs-freqStart
    freqs = (freqs/PAR_refinedStepFreq)-round(PAR_enhance/2+0.001)

    # spindowns
    spindowns = numpy.arange(0, PAR_nstepFdot)
    spindowns = numpy.multiply(spindowns,PAR_stepFdot)
    spindowns = numpy.add(spindowns, PAR_fdotMin)

    # others

    # once the data of the peakmap are rearranged as above, it's better to rejoin them in an unique array
    peakmap = numpy.stack((times,freqs,weights),1)
    spindowns = spindowns

    # these two variables are only redifinition of old variable to clarify better the Hough transorm code
    PAR_nRows = numpy.int32(PAR_nstepFdot)
    PAR_nColumns = numpy.int32(nstepFreqs)

    #---------------------------------------
    # defining TensorFlow graph             |
    #---------------------------------------
    
    # open a tf session
    sess = tf.Session()
    # now let's use TensorFlow
    # defining tf constants to the hough transform
    # WARNING i'm using 64bit data for times because 9 months with a step of 4096s exceed 32bit precision, this forces to use 64bit spindowns to run tf.matmul (tensorflow needs same data type tensors). This slows a bit the code with a GPU capable to run 64bit calculations (eg Tesla series), but can be a problem on other series (eg GeForce)
    # TODO remove any 64bit data
    # WARNING uncomment this for 64bit data
    #PAR_secbeltTF = tf.constant(4000,dtype = tf.float64, name = 'secur')
    #peakmapTF = tf.placeholder(tf.float64, name = 'inputPM')
    #spindownsTF = tf.placeholder(tf.float64, name = 'inputSD')
    # WARNING comment this for 64bit data
    PAR_secbeltTF = tf.constant(4000,dtype = tf.float32, name = 'secur')
    peakmapTF = tf.placeholder(tf.float32, name = 'inputPM')
    spindownsTF = tf.placeholder(tf.float32, name = 'inputSD')

    # calculating the Hough transofrm, with a fully vectorial tensorflow function with GPU parallelization
    def frequencyHough(peakmapHM,spindownsHM):
        """
        Computes the frequency-Hough transform of a sparse peakmap.
        Parameters:
        frequencies, times : 1D tensors
        The coordinates of the peakmap in sparse format.
        weights :  1D tensor
        The values of the peaks in the sparse peakmap.
        spindowns : 1D tensor
        The spindowns values over which calculate the Hough transform.
        Size of the Hough map
        Returns:
        houghMap : 2D tensor
        The Hough transform matrix
        """

        def mapnonVar(ithStep):
            # this function computes the Hough transform histogram for a given spindown
            # WARNING the 64 bit precision slows the computation and could not be supported in many GPUs
            # TODO  remove any 64bit data
            sdTimed = tf.multiply(spindownsHM[ithStep], timesHM, name = "Tdotpert")
            # WARNING uncomment this for 64bit data (perhaps unnecessary cast)
            #sdTimed = tf.cast(sdTimed, dtype = tf.float64)

            transform = tf.round(freqsHM-sdTimed+PAR_secbeltTF/2, name = "trasfFreq")
            transform = tf.cast(transform, dtype=tf.int32)
            # the rounding operation brings a some peaks in the same frequency-spindown bin in the Hough map
            # the left edge is then computed binning that peaks properly
            # (according to their values, if the peakmap was adactive)
            # the following is the core of the algoritm and brings the most computational effort
            values = tf.unsorted_segment_sum(weightsHM, transform, PAR_nColumns, name = "cumsum")
            # WARNING uncomment this for 64bit data
            #values = tf.cast(values, dtype=tf.float32)
            return values


        timesHM = peakmapHM[:,0]
        freqsHM = peakmapHM[:,1]
        weightsHM = peakmapHM[:,2]

        # in order to save a graph and then loading it in another code,
        # here the hough map tensor is defined as a variable
        houghLeft = tf.Variable(tf.zeros([PAR_nRows,PAR_nColumns]),name = "var")
        # to keep under control the memory usage, the map function is a 
        # very useful tool to apply the same function over a vector
        # in this way the vectorization is preserved
        houghLeft = houghLeft.assign(tf.map_fn(mapnonVar, tf.range(0, PAR_nRows), dtype=tf.float32, parallel_iterations=8))

        # let's superimpose the right edge on the image
        leftStripe = tf.slice(houghLeft, [0,0], [houghLeft.get_shape()[0],10], name = "left_stripe")
        houghRight = tf.subtract(tf.slice(houghLeft, [0,10],[houghLeft.get_shape()[0], houghLeft.get_shape()[1]-10]),
                                 tf.slice(houghLeft, [0,0],[houghLeft.get_shape()[0], houghLeft.get_shape()[1]-10]), name = "right_stripe")
        # now we have the so called differential hough map
        houghDiff = tf.concat([leftStripe,houghRight],1, name = "diff_hough")
        # and at last we can cumulative sum along the rows to have the integral hough map
        houghMap = tf.cumsum(houghDiff, axis = 1, name = "output")
        return houghMap

    FHMap = frequencyHough(peakmapTF, spindownsTF)

    #----------------------------------------
    # feeding and running                   |
    #----------------------------------------
    # this is the dictionary with the data to be fed to the graph using placeholders
    dataDict = { 
                   peakmapTF: peakmap,
                   spindownsTF : spindowns
                 }

    sess.run(tf.global_variables_initializer())
    # running the graph
    start = time.time()
    image = sess.run(FHMap, feed_dict = dataDict)
    stop = time.time()

    occupazione = sess.run(tf.contrib.memory_stats.BytesInUse())/1e6#MaxBytesInUse())/1e6
    inputmem = (peakmap.nbytes+spindowns.nbytes)/1e6
    occtot = sess.run(tf.contrib.memory_stats.MaxBytesInUse())/1e6
    #print("memory used " + str(occupazione) + " MB")
    nHertz = nFiles*5
    x = numpy.array([nHertz, freqs.size, PAR_nRows, stop-start, inputmem, occtot]).reshape((1,6))
    f_handle = open('stats.txt', 'a')
    numpy.savetxt(f_handle, x,delimiter=',')#,newline"")
    f_handle.close()
    sess.close()


#from matplotlib import pyplot
#a = pyplot.imshow(image, aspect = 1000, origin = "lower")
#pyplot.show()