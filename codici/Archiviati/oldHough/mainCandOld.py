import tensorflow as tf
import numpy
import scipy.io
import time

# open a tf session
sess = tf.Session()

# input file path
#dataPath ="/home/iuri.larosa/ifil/in_O2LL_04_1473_.mat"
#dataPath ="/home/iuri.larosa/ifil/in_O2LL_03_0744_.mat"
dataPath ="/home/protoss/Documenti/TESI/ifil/in_O2LL_03_0744_.mat"
#dataPath = "/home/protoss/Documenti/TESI/ifil/in_O2LL_02_0327_.mat"
#dataPath ="/home/iurilarosa/data/ifil/in_O2LL_03_0744_.mat"


# loading the input file (with the peakmap inside a matlab structure)
#WARNING at the moment the hough transform is parallelized only on a single peakmap
#TODO improve parallelization on multiple peakmaps (or use bigger peakmaps)
struct = scipy.io.loadmat(dataPath)['job_pack_0']

#---------------------------------------
# defining parameters                   |
#---------------------------------------
# frequencies
PAR_enhance = 10
PAR_stepFreq = 1/PAR_tFft
PAR_refinedStepFreq =  PAR_stepFreq/PAR_enhance


# times
PAR_Ntimes = struct["basic_info"][0,0]["ntim"][0,0][0,0]
PAR_tObs = PAR_tFft*PAR_Ntimes/(2*60*60*24*30) # mesi
print(PAR_tObs)
PAR_tObs = numpy.round(PAR_tObs*30*24*60*60)
PAR_epoch = (57722+57990)/2 



# spindowns
PAR_fdotMin = -1e-8
PAR_fdotMax = 1e-9
PAR_stepFdot = PAR_stepFreq/PAR_tObs
PAR_nstepFdot = numpy.round((PAR_fdotMax-PAR_fdotMin)/PAR_stepFdot).astype(numpy.int32)

# others
PAR_secbelt = 4000

#---------------------------------------
# loading and managing data             |
#---------------------------------------

# the address of the peakmap in the input file structure is job_pack_0.peaks
# the peakmap is espressed as a sparse matrix with 3 arrays, since the most part is 0: 
# row coordinates (times), column coordinates (frequencies), values (weights)

# times
times = struct['peaks'][0,0][0]
times = times-PAR_epoch
times = ((times)*60*60*24/PAR_refinedStepFreq)

# frequencies
freqs = struct['peaks'][0,0][1]
freqMin = numpy.amin(freqs)
freqMax = numpy.amax(freqs)
freqStart = freqMin- PAR_stepFreq/2 - PAR_refinedStepFreq
freqEnd = freqMax + PAR_stepFreq/2 + PAR_refinedStepFreq
nstepFreqs = numpy.ceil((freqEnd-freqStart)/PAR_refinedStepFreq)+PAR_secbelt
freqs = freqs-freqStart
freqs = (freqs/PAR_refinedStepFreq)-round(PAR_enhance/2+0.001)
 
# spindowns
spindowns = numpy.arange(0, PAR_nstepFdot)
spindowns = numpy.multiply(spindowns,PAR_stepFdot)
spindowns = numpy.add(spindowns, PAR_fdotMin)

# others
weights = (struct['peaks'][0,0][4]+1)

# once the data of the peakmap are rearranged as above, it's better to rejoin them in an unique array
peakmap = numpy.stack((times,freqs,weights),1)
spindowns = spindowns

# these two variables are only redifinition of old variable to clarify better the Hough transorm code
PAR_nRows = numpy.int32(PAR_nstepFdot)
PAR_nColumns = numpy.int32(nstepFreqs)

#---------------------------------------
# defining TensorFlow graph             |
#---------------------------------------
# now let's use TensorFlow
# defining tf constants to the hough transform
# WARNING i'm using 64bit data for times because 9 months with a step of 4096s exceed 32bit precision, this forces to use 64bit spindowns to run tf.matmul (tensorflow needs same data type tensors). This slows a bit the code with a GPU capable to run 64bit calculations (eg Tesla series), but can be a problem on other series (eg GeForce)
# TODO remove any 64bit data
# WARNING uncomment this for 64bit data
PAR_secbeltTF = tf.constant(4000,dtype = tf.float64, name = 'secur')
peakmapTF = tf.placeholder(tf.float64, name = 'inputPM')
spindownsTF = tf.placeholder(tf.float64, name = 'inputSD')
# WARNING comment this for 64bit data
#PAR_secbeltTF = tf.constant(4000,dtype = tf.float32, name = 'secur')
#peakmapTF = tf.placeholder(tf.float32, name = 'inputPM')
#spindownsTF = tf.placeholder(tf.float32, name = 'inputSD')

# calculating the Hough transofrm, with a fully vectorial tensorflow function with GPU parallelization
def frequencyHough(peakmapHM,spindownsHM):
	"""
	Computes the frequency-Hough transform of a sparse peakmap.
	Parameters:
	frequencies, times : 1D tensors
	The coordinates of the peakmap in sparse format.
	weights :  1D tensor
	The values of the peaks in the sparse peakmap.
	spindowns : 1D tensor
	The spindowns values over which calculate the Hough transform.
	Size of the Hough map
	Returns:
	houghMap : 2D tensor
	The Hough transform matrix
	"""

	def mapnonVar(ithStep):
		# this function computes the Hough transform histogram for a given spindown
		# WARNING the 64 bit precision slows the computation and could not be supported in many GPUs
		# TODO  remove any 64bit data
		sdTimed = tf.multiply(spindownsHM[ithStep], timesHM, name = "Tdotpert")
		# WARNING uncomment this for 64bit data (perhaps unnecessary cast)
		sdTimed = tf.cast(sdTimed, dtype = tf.float64)
		
		transform = tf.round(freqsHM-sdTimed+PAR_secbeltTF/2, name = "trasfFreq")
		transform = tf.cast(transform, dtype=tf.int32)
		# the rounding operation brings a some peaks in the same frequency-spindown bin in the Hough map
		# the left edge is then computed binning that peaks properly
		# (according to their values, if the peakmap was adactive)
		# the following is the core of the algoritm and brings the most computational effort
		values = tf.unsorted_segment_sum(weightsHM, transform, PAR_nColumns, name = "cumsum")
		# WARNING uncomment this for 64bit data
		values = tf.cast(values, dtype=tf.float32)
		return values


	timesHM = peakmapHM[:,0]
	freqsHM = peakmapHM[:,1]
	weightsHM = peakmapHM[:,2]

	# in order to save a graph and then loading it in another code,
	# here the hough map tensor is defined as a variable
	houghLeft = tf.Variable(tf.zeros([PAR_nRows,PAR_nColumns]),name = "var")
	# to keep under control the memory usage, the map function is a 
	# very useful tool to apply the same function over a vector
	# in this way the vectorization is preserved
	houghLeft = houghLeft.assign(tf.map_fn(mapnonVar, tf.range(0, PAR_nRows), dtype=tf.float32, parallel_iterations=8))

	# let's superimpose the right edge on the image
	leftStripe = tf.slice(houghLeft, [0,0], [houghLeft.get_shape()[0],10], name = "left_stripe")
	houghRight = tf.subtract(tf.slice(houghLeft, [0,10],[houghLeft.get_shape()[0], houghLeft.get_shape()[1]-10]),
							 tf.slice(houghLeft, [0,0],[houghLeft.get_shape()[0], houghLeft.get_shape()[1]-10]), name = "right_stripe")
	# now we have the so called differential hough map
	houghDiff = tf.concat([leftStripe,houghRight],1, name = "diff_hough")
	# and at last we can cumulative sum along the rows to have the integral hough map
	houghMap = tf.cumsum(houghDiff, axis = 1, name = "output")
	return houghMap

FHMap = frequencyHough(peakmapTF, spindownsTF)


#----------------------------------------
# feeding and running                   |
#----------------------------------------
# this is the dictionary with the data to be fed to the graph using placeholders
dataDict = { 
               peakmapTF: peakmap,
               spindownsTF : spindowns
             }
# initializing the variable
sess.run(tf.global_variables_initializer())
# running the graph
start = time.time()
hough = sess.run(FHMap, feed_dict = dataDict)
stop = time.time()
print(hough.shape)
print("execution time " + str(stop-start) + " s")

scipy.io.savemat("/home/iuri.larosa/check/2048_REFHough.mat", {"hough": hough})

#from matplotlib import pyplot
#a = pyplot.imshow(image, aspect = 200)
#pyplot.show()
sess.close()


### CAND SEL
#PAR defs
PAR_coord = [1,2,0.1,0.1, 0.1]
PAR_numCands = 100




def select_candidates(numCand, freqIn, hmap, coord,spindowns):
	candidates = numpy.zeros((numCand,9))

	# cutting out the securbelt finding the index of the first frequency of the Hough map
	minDistance =PAR_enhance*4
	
	firstFreq = freqStart
	print(firstFreq)
	#firstFreq = freqIn-(PAR_secbelt/2)*PAR_refinedStepFreq

	freqInitial = struct['basic_info'][0,0]['frin'][0,0][0,0]
	freqFinal = struct['basic_info'][0,0]['frfi'][0,0][0,0]

	#print(freqInitial, freqFinal, firstFreq)

	# same as cut_gd2 SNAG function
	indexInitial = ((freqInitial-firstFreq)/PAR_refinedStepFreq+PAR_secbelt).astype(numpy.int64)
	indexFinal = ((freqFinal-firstFreq)/PAR_refinedStepFreq+PAR_secbelt+1).astype(numpy.int64)
	
	print(indexInitial, indexFinal)

	hmapCand = hmap[:,indexInitial:indexFinal-1].astype(numpy.int64)

	scipy.io.savemat("/home/iuri.larosa/check/2048_REFHoughSliced.mat", {"houghCand" : hmapCand})
	size = numpy.shape(hmapCand)[1]
	
	print(hmapCand.shape)
	
	# labelling the Hough map pixels with the proper frequency
	freqNew = numpy.arange(0,size)*PAR_refinedStepFreq+freqInitial

	# finding the highest amplitudes in the map for each column
	maxPerColumn = numpy.amax(hmapCand, axis = 0)
	rowMax = numpy.argmax(hmapCand, axis = 0)

	# building the belts in the Hough map where a candidate has to be searched
	stepFreqNew = maxPerColumn.size/numCand
	#print(maxPerColumn, stepFreqNew)
	
	
	indicesFreq = numpy.arange(0,maxPerColumn.size,stepFreqNew)
	indicesFreq = numpy.append(indicesFreq, maxPerColumn.size)
	indicesFreq = numpy.round(indicesFreq).astype(numpy.int64)
	
	
	
	
	# computing median and median absolute deviation for the whole map
	def statistics(ndArray):
		median = numpy.median(ndArray)
		sigman = numpy.median(numpy.absolute(ndArray-median))/0.6745
		return median, sigman

	stats = statistics(hmapCand)
	medianTot = stats[0]
	sigmanTot = stats[1]

	# computing median and median absolute deviation for each selected belt in the map
	# preparing indices for array slicing
	initialIndices = numpy.concatenate(([indicesFreq[0]],
									    indicesFreq[0:numCand-2],
									    [indicesFreq[indicesFreq.size-3]]),0)
	finalIndices = numpy.concatenate(([indicesFreq[2]],
								      indicesFreq[3:numCand+1],
								      [indicesFreq[indicesFreq.size-1]]),0)

	def statsPerCand(i):
		stat = statistics(maxPerColumn[initialIndices[i]:finalIndices[i]])#[0]
		return stat

	statPerCand = numpy.array(list(map(statsPerCand, numpy.arange(numCand))))
	medianPerCand = statPerCand[:,0]
	sigmanPerCand = statPerCand[:,1]

	# begin of the main loop
	# first of all cut out every belt where the median is < 0
	filter = numpy.where(medianPerCand > 0)[0]
	counter = -1
	# then start the loop over every remaining belt
	
	
	
	
	for i in filter:
		# selecting every maximum amplitude pixel in the selected belt
		starts = indicesFreq[i]
		ends = indicesFreq[i+1]
		portionMaxPerColumn = maxPerColumn[starts:ends]

		# finding the highest amplitude in the selected belt
		localMax = numpy.amax(portionMaxPerColumn)
		localInd = numpy.argmax(portionMaxPerColumn)

		# primary candidate selection
		if localMax > medianPerCand[i] and localMax > medianTot/2:
			counter = counter + 1
			index = indicesFreq[i] + localInd
			row = rowMax[index]

			candidates[counter,0] = localMax
			candidates[counter,1] = index #freqNew[index]
			candidates[counter,2] = row #spindowns[row]
			candidates[counter,3] = (localMax-medianPerCand[i])/sigmanPerCand[i]
			candidates[counter,4] = coord[0]
			candidates[counter,5] = coord[1]
			candidates[counter,6] = coord[2]/2
			candidates[counter,7] = numpy.abs(coord[3]-coord[4])/4
			candidates[counter,8] = 1
			
			## secondary candidate selection
			#inf = numpy.amax([localInd-minDistance,0]).astype(numpy.int64)
			#sup = numpy.amin([localInd+minDistance+1,portionMaxPerColumn.size]).astype(numpy.int64)
			
			#portionMaxPerColumn[inf:sup] = 0
			#secondLocMax = numpy.amax(portionMaxPerColumn)
			#secondLocInd = numpy.argmax(portionMaxPerColumn)
			
			#if numpy.absolute(secondLocInd-localInd) > 2 * minDistance and secondLocMax > medianPerCand[i]:
				#counter = counter + 1
				#index = indicesFreq[i] + secondLocInd
				#row = rowMax[index]

				#candidates[counter,0] = freqNew[index]
				#candidates[counter,1] = coord[0]
				#candidates[counter,2] = coord[1]
				#candidates[counter,3] = spindowns[row]
				#candidates[counter,4] = secondLocMax
				#candidates[counter,5] = (secondLocMax-medianPerCand[i])/sigmanPerCand[i]
				#candidates[counter,6] = coord[2]/2
				#candidates[counter,7] = numpy.abs(coord[3]-coord[4])/4
				#candidates[counter,8] = 2

#	candidates[: , 2]=numpy.round(candidates[2,:] / PAR_stepFdot) * PAR_stepFdot
	return candidates



allCands = select_candidates(PAR_numCands, numpy.amin(freqs), hough, PAR_coord,spindowns)

scipy.io.savemat("/home/iuri.larosa/check/2048_REFCands.mat", {"cands" : allCands})

stop = time.time()

print(stop-start, allCands[-5:,:])

#image = sess.run(FHMap, feed_dict = dataDict)#, options=run_options,run_metadata = run_metadata)
#from matplotlib import pyplot
#a = pyplot.imshow(image, aspect = 200)
#pyplot.show()

