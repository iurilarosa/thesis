import tensorflow as tf
import numpy
import scipy.io
import time
from tensorflow.python.client import timeline

# open a tf session
sess = tf.Session()

# input file path
#dataPath ="/home/iuri.larosa/ifil/in_O2LL_03_0744_.mat"
dataPath ="/home/iurilarosa/data/ifil/in_O2LL_03_0744_.mat"


#dataPath ="/home/iuri.larosa/ifil/in_O2LL_04_1473_.mat"

#dataPath = "/home/protoss/Documenti/TESI/ifil/in_O2LL_02_0327_.mat"

# loading the input file (with the peakmap inside a matlab structure)
#WARNING at the moment the hough transform is parallelized only on a single peakmap
#TODO improve parallelization on multiple peakmaps (or use bigger peakmaps)
struct = scipy.io.loadmat(dataPath)['job_pack_0']

#---------------------------------------
# defining parameters                   |
#---------------------------------------
# times
PAR_tFft = 2048
PAR_Ntimes = struct["basic_info"][0,0]["ntim"][0,0][0,0]
PAR_tObs = numpy.round(PAR_tFft*PAR_Ntimes/(2*60*60*24*30)) # mesi
#print(PAR_tObs)
PAR_tObs = PAR_tObs*30*24*60*60
PAR_epoch = (57722+57990)/2 

# frequencies
PAR_enhance = 10
PAR_stepFreq = 1/PAR_tFft
PAR_refinedStepFreq =  PAR_stepFreq/PAR_enhance

# spindowns
PAR_fdotMin = -1e-8
PAR_fdotMax = 1e-9
PAR_stepFdot = PAR_stepFreq/PAR_tObs
PAR_nstepFdot = numpy.round((PAR_fdotMax-PAR_fdotMin)/PAR_stepFdot).astype(numpy.int32)

# others
PAR_secbelt = 4000

#---------------------------------------
# loading and managing data             |
#---------------------------------------

# the address of the peakmap in the input file structure is job_pack_0.peaks
# the peakmap is espressed as a sparse matrix with 3 arrays, since the most part is 0: 
# row coordinates (times), column coordinates (frequencies), values (weights)

# times
times = struct['peaks'][0,0][0]
times = times-PAR_epoch
times = ((times)*60*60*24/PAR_refinedStepFreq)

# frequencies
freqs = struct['peaks'][0,0][1]
freqMin = numpy.amin(freqs)
freqMax = numpy.amax(freqs)
freqStart = freqMin- PAR_stepFreq/2 - PAR_refinedStepFreq
freqEnd = freqMax + PAR_stepFreq/2 + PAR_refinedStepFreq
nstepFreqs = numpy.ceil((freqEnd-freqStart)/PAR_refinedStepFreq)+PAR_secbelt
freqs = freqs-freqStart
freqs = (freqs/PAR_refinedStepFreq)-round(PAR_enhance/2+0.001)
 
# spindowns
spindowns = numpy.arange(0, PAR_nstepFdot)
spindowns = numpy.multiply(spindowns,PAR_stepFdot)
spindowns = numpy.add(spindowns, PAR_fdotMin)

# others
weights = (struct['peaks'][0,0][4]+1)

# once the data of the peakmap are rearranged as above, it's better to rejoin them in an unique array
peakmap = numpy.stack((times,freqs,weights),1)
spindowns = spindowns

# these two variables are only redifinition of old variable to clarify better the Hough transorm code
PAR_nRows = numpy.int32(PAR_nstepFdot)
PAR_nColumns = numpy.int32(nstepFreqs)

#---------------------------------------
# defining TensorFlow graph             |
#---------------------------------------
PAR_secbeltTF = tf.constant(4000,dtype = tf.float32, name = 'secur')


timesTF = tf.placeholder(tf.float32, name = 'inputT')
freqsTF = tf.placeholder(tf.float32, name = 'inputF')
weightsTF = tf.placeholder(tf.float32, name = 'inputW')
spindownsTF = tf.placeholder(tf.float32, name = 'inputSD')

# calculating the Hough transofrm, with a fully vectorial tensorflow function with GPU parallelization
def frequencyHough(timesHM,freqsHM,weightsHM,spindownsHM):


	def mapnonVar(ithStep):
		sdTimed = tf.multiply(spindownsHM[ithStep], timesHM, name = "Tdotpert")

		transform = tf.round(freqsHM-sdTimed+PAR_secbeltTF/2, name = "trasfFreq")
		transform = tf.cast(transform, dtype=tf.int32)

		values = tf.unsorted_segment_sum(weightsHM, transform, PAR_nColumns, name = "cumsum")

		return values

	houghLeft = tf.Variable(tf.zeros([PAR_nRows,PAR_nColumns]),name = "var")

	houghLeft = houghLeft.assign(tf.map_fn(mapnonVar, tf.range(0, PAR_nRows), dtype=tf.float32, parallel_iterations=8))

	# let's superimpose the right edge on the image
	leftStripe = tf.slice(houghLeft, [0,0], [houghLeft.get_shape()[0],10], name = "left_stripe")
	houghRight = tf.subtract(tf.slice(houghLeft, [0,10],[houghLeft.get_shape()[0], houghLeft.get_shape()[1]-10]),
							 tf.slice(houghLeft, [0,0],[houghLeft.get_shape()[0], houghLeft.get_shape()[1]-10]), name = "right_stripe")
	# now we have the so called differential hough map
	houghDiff = tf.concat([leftStripe,houghRight],1, name = "diff_hough")
	# and at last we can cumulative sum along the rows to have the integral hough map
	houghMap = tf.cumsum(houghDiff, axis = 1, name = "output")
	return houghMap


dataDict = { 
               timesTF : peakmap[:,0],
               freqsTF : peakmap[:,1],
               weightsTF : peakmap[:,2],
               spindownsTF : spindowns
             }



### CAND SEL
#PAR defs
PAR_coord = numpy.array([1,2,0.1,0.1,0.15])
NPcoord = numpy.array([PAR_coord[0], PAR_coord[1], PAR_coord[2]/2, numpy.abs(PAR_coord[3]-PAR_coord[4])/4])
PAR_numCand = 100
coords = tf.constant(numpy.zeros((PAR_numCand,4))+NPcoord, dtype = tf.float32)

PAR_freqInCorr = freqMin

# cutting out the securbelt finding the index of the first frequency of the Hough map
PAR_minDistance = PAR_enhance*4
PAR_firstFreq = PAR_freqInCorr-(PAR_secbelt/2)*PAR_refinedStepFreq

PAR_freqInitial = struct['basic_info'][0,0]['frin'][0,0][0,0]
PAR_freqFinal = struct['basic_info'][0,0]['frfi'][0,0][0,0]

# same as cut_gd2 SNAG function
PAR_indexInitial = ((PAR_freqInitial-PAR_firstFreq)/PAR_refinedStepFreq).astype(numpy.int32)
PAR_indexFinal = ((PAR_freqFinal-PAR_firstFreq)/PAR_refinedStepFreq+1).astype(numpy.int32)


#FH con fasce laterali già tolte
FHMapCand = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF)[:,PAR_indexInitial:PAR_indexFinal-1]


PAR_totWidth = tf.shape(FHMapCand)[1]
PAR_totHeight = tf.constant(PAR_nstepFdot)

PAR_beltWidth = tf.round(PAR_totWidth/PAR_numCand)
PAR_beltWidth = tf.cast(PAR_beltWidth, dtype = tf.int32)
belts = tf.range(0, PAR_totWidth, PAR_beltWidth)

#splitting the map in belts
houghBelts = tf.zeros([PAR_numCand, PAR_totHeight,PAR_beltWidth])
houghBelts = tf.split(FHMapCand, num_or_size_splits=PAR_numCand, axis=1)
houghBelts = tf.cast(houghBelts, dtype = tf.int32)

medianTOT = tf.contrib.distributions.percentile(FHMapCand, 50)
#sigmanTOT = tf.contrib.distributions.percentile(tf.abs(FHMapCand-medianTOT),50)
#sigmanTOT = tf.cast(sigmanTOT, tf.float32)/0.6745



#-------------------SOLO PRIMARI!!!!!-----------------------------
#PAR_numCand = 100*2

def cands():
	def find_maxs(tensor, numColumns):
		local_top = tf.reduce_max(tensor,axis = [1,2])
		
		x = tf.reshape(tensor, [tf.shape(tensor)[0], -1])
		x = tf.cast(x, tf.int32)
		indices = tf.argmax(x, axis=1)
		
		indices = tf.cast(indices, tf.int32)
		
		rows = tf.cast(indices / numColumns, tf.float32)
		cols = tf.cast(indices % numColumns, tf.float32)
		
		rows = tf.cast(rows, tf.int32)
		cols = tf.cast(cols, tf.int32)
		
		local_maxs = tf.transpose(tf.stack([local_top, cols, rows]))
		
		return local_maxs

	#NB [ampiezza max, riga => spindown, colonna => freq]
	primaryMaxs = find_maxs(houghBelts, PAR_beltWidth)
	

	localMedians = tf.contrib.distributions.percentile(houghBelts, 50, axis = [1,2])
	localSigmans = tf.contrib.distributions.percentile(tf.abs(houghBelts-tf.reshape(localMedians,[PAR_numCand,1,1])), 50, axis = [1,2])
	localSigmans = tf.cast(localSigmans, tf.float32)/0.6745

	primaryMaxs = tf.cast(primaryMaxs, tf.float32)
	localMedians = tf.cast(localMedians, tf.float32)
	localSigmans = tf.cast(localSigmans, tf.float32)
	semimedianTOT = tf.cast(medianTOT/2, tf.float32)

	def find_cands(medians, sigmans, maxs, semimedian):
		CR = tf.reshape((maxs[:,0]-medians)/sigmans, [PAR_numCand, 1])

		preliminaryCands = tf.concat([maxs, CR, coords], axis = 1)
		
		primaryCond0 = (medians > 0)
		primaryCond1 = tf.logical_and(maxs[:,0] > medians, maxs[:,0] > semimedian)
		primaryCond = tf.equal(primaryCond0,primaryCond1)

		primaryCands = tf.gather(preliminaryCands,tf.where(primaryCond))
		
		
		#cands = tf.concat([primaryCands], axis = 1)
		
		return primaryCands


	return find_cands(localMedians, localSigmans, primaryMaxs, semimedianTOT)


candycandy = cands()


#----------------------------------------
# feeding and running                   |
#----------------------------------------
# this is the dictionary with the data to be fed to the graph using placeholders
#dataDict = { 
               #timesTF : peakmap[:,0],
               #freqsTF : peakmap[:,1],
               #weightsTF : peakmap[:,2],
               #spindownsTF : spindowns
             #}


sess.run(tf.global_variables_initializer())

start = time.time()
#image = sess.run(FHMapCand, feed_dict = dataDict)

#maxconSec = sess.run(find_maxs(houghBelts, PAR_numCand, PAR_beltWidth), feed_dict = dataDict)
#maxsoloPrim = sess.run(find_maxs(houghBelts, PAR_beltWidth), feed_dict = dataDict)

#[cands1, cands2] = sess.run(candycandy, feed_dict = dataDict)
cands = sess.run(candycandy, feed_dict = dataDict)



#cands = numpy.concatenate([cands1,cands2], axis = 0)
#cands = numpy.reshape(cands, [cands.shape[0], cands.shape[2]])


#cands[:,2] = cands[:,2]+(numpy.arange(PAR_numCand)*1024)
##cands[:,2] = cands[:,2]*PAR_refinedStepFreq+PAR_freqInitial

stop = time.time()

print(stop-start)


sess.close()


#image = sess.run(FHMap, feed_dict = dataDict)#, options=run_options,run_metadata = run_metadata)
#from matplotlib import pyplot
#a = pyplot.imshow(image, aspect = 200)
#pyplot.show()







##-------------------CON SECONDARI!!-----------------------------

#def find_maxs(tensor, numMatrices, numColumns):
	#tensor = tf.matrix_transpose(tensor)

	#local_top = tf.nn.top_k(tensor,k=1, sorted = True)

	#ones = tf.ones(numMatrices, dtype = tf.int32)  
	#columns = tf.tensordot(ones,tf.range(numColumns),axes = 0)
	#columns = tf.reshape(columns,[numMatrices,numColumns,1])

	#column_tops = tf.concat([tf.cast(local_top.values, tf.int32), local_top.indices, columns], axis = 2)

	#idx = tf.reshape(tf.range(numMatrices), [-1, 1])
	#idx_flat = tf.reshape(tf.tile(idx, [1, numColumns]), [-1])
	#top_k_flat = tf.reshape(tf.nn.top_k(column_tops[:,:,0], k=numColumns).indices, [-1])
	#final_idx = tf.reshape(tf.stack([idx_flat, top_k_flat], 1),[numMatrices, numColumns, 2])

	#column_maxs = tf.gather_nd(column_tops, final_idx)

	#localMaxs = column_maxs[:,0]
	#second_localMaxs = column_maxs[:,1]
	#return localMaxs, second_localMaxs


#[primaryMaxs, secondaryMaxs] = find_maxs(houghBelts, PAR_numCand, beltWidth)


#localMedians = tf.contrib.distributions.percentile(houghBelts, 50, axis = [1,2])
#localSigmans = tf.contrib.distributions.percentile(tf.abs(houghBelts-tf.reshape(localMedians,[PAR_numCand,1,1])), 50, axis = [1,2])
#localSigmans = tf.cast(localSigmans, tf.float32)/0.6745


#primaryMaxs = tf.cast(primaryMaxs, tf.float32)
#secondaryMaxs = tf.cast(secondaryMaxs, tf.float32)
#localMedians = tf.cast(localMedians, tf.float32)
#localSigmans = tf.cast(localSigmans, tf.float32)
#semimedianTOT = tf.cast(medianTOT/2, tf.float32)

#def find_cands(localMedians, localMaxs, semimedianTOT, second_localMaxs):
	#primaryCond0 = (localMedians > 0)
	#primaryCond1 = tf.logical_and(localMaxs[:,0] > localMedians, localMaxs[:,0] > semimedianTOT)
	#primaryCond = tf.equal(primaryCond0,primaryCond1)

	#primaryCands = tf.gather(localMaxs,tf.where(primaryCond))

	#localIds = localMaxs[:,2]
	#second_localIds = second_localMaxs[:,2]

	#secondaryCond0 = tf.logical_and(tf.abs(second_localIds-localIds) > 2 * PAR_minDistance, second_localMaxs[:,0] > localMedians)
	#secondaryCond = tf.equal(primaryCond, secondaryCond0)

	#secondaryCands = tf.gather(second_localMaxs, tf.where(secondaryCond))
	#return primaryCands, secondaryCands


#candycandy = find_cands(localMedians, primaryMaxs, semimedianTOT, secondaryMaxs)
