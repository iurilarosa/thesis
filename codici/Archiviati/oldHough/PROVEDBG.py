import numpy
import tensorflow as tf

#my purpose is to build an image of displacements starting from position-time-speed data

# building some fake data (usually i load them from a file)
# data are in the shape of a sparse matrix of x-y positions in different times
matrix = numpy.random.randint(2, size = 100).astype(float).reshape(10,10)
x = numpy.nonzero(matrix)[0]
y = numpy.nonzero(matrix)[1]
time = numpy.random.rand(x.size)

# then i build a range of speed values i can consider
PAR_nStepsSpeed = 5
speed = numpy.arange(1,PAR_nStepsSpeed+1)

# shape of the image
PAR_blackBelt = 50
PAR_nRows = PAR_nStepsSpeed
PAR_nColumns = time.size + PAR_blackBelt

# ----- defining the graph -----
#opening session and defining tensors
sess = tf.Session()
xTF = tf.placeholder(dtype = tf.float32, name = 'inputX')
yTF = tf.placeholder(dtype = tf.float32, name = 'inputY')
timeTF = tf.placeholder(dtype = tf.float32, name = 'inputT')
speedTF = tf.placeholder(dtype = tf.float32, name = 'inputV')

# this is the function which builds the image
def image_building(xImg, yImg, timeImg, speedImg):
	# computing the displacements for each speed value
	def displacements(ithStep):
		displacement = tf.multiply(speedImg[ithStep],xImg)
		
		positions = tf.round((yImg - displacement) + PAR_blackBelt)
		positions = tf.cast(positions, dtype=tf.int32)

		values = tf.unsorted_segment_sum(timeImg, positions, PAR_nColumns)
		return values

	# with tf.map_fn I parallelize over the speed values
	cumDisplacement = tf.Variable(tf.zeros([PAR_nRows,PAR_nColumns]))
	cumDisplacement = cumDisplacement.assign(tf.map_fn(displacements, 
									 tf.range(0, PAR_nRows), 
									 dtype=tf.float32))
	return cumDisplacement

# running the function
imageTF = image_building(xTF, yTF, timeTF, speedTF)

# ----- feeding and running the graph -----
dataDict = {	xTF : x,
			yTF : y,
			timeTF : time,
			speedTF : speed}

image = sess.run(imageTF, feed_dict = dataDict)


from matplotlib import pyplot
pyplot.imshow(image, aspect = 10)
pyplot.colorbar(shrink = 0.75,aspect = 10)
pyplot.show()
