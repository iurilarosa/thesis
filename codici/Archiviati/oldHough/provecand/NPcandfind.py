import time
import numpy

medianTOT = 60
localMedians = numpy.array([50,50, 0, 50]).astype(numpy.int32)
minDistance = 0.1

localMaxs = numpy.array([
						[49, 2, 1],
						[65, 5, 3],
						[60, 3, 12],
						[23, 1, 3],
					]).astype(numpy.int32)

second_localMaxs = numpy.array([
						[39, 5, 2],
						[55, 4, 1],
						[50, 2, 4],
						[13, 1, 5],
					]).astype(numpy.int32)

#print(localMaxs[:,0])

#primaryCands = localMaxs[numpy.all([localMedians >0,localMaxs[:,0]>localMedians], axis = 0)]
#print(primaryCands)

filtering = numpy.concatenate((localMaxs, second_localMaxs, localMedians.reshape([4,1])), axis = 1)

start = time.time()
filtering = filtering[filtering[:,6]>0]
filtering = filtering[filtering[:,0]>filtering[:,6]]
filtering = filtering[filtering[:,0]>medianTOT/2]

#stop = time.time()
print(filtering)

primTosecDist = numpy.abs(filtering[:,5]-filtering[:,2])
print(primTosecDist)
secondFiltering = filtering[primTosecDist > 2 * minDistance, 3:]
secondFiltering = secondFiltering[secondFiltering[:,0]>secondFiltering[:,3]]

cands = numpy.stack((filtering[:,0:3], secondFiltering[:,0:3]))

print(cands)
