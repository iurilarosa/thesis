import tensorflow as tf
import numpy
import scipy.io
import time
from tensorflow.python.client import timeline

# open a tf session
sess = tf.Session()

# input file path
dataPath ="/home/iuri.larosa/ifil/in_O2LL_03_0744_.mat"
#dataPath ="/home/iuri.larosa/ifil/in_O2LL_04_1473_.mat"

#dataPath = "/home/protoss/Documenti/TESI/ifil/in_O2LL_02_0327_.mat"

# loading the input file (with the peakmap inside a matlab structure)
#WARNING at the moment the hough transform is parallelized only on a single peakmap
#TODO improve parallelization on multiple peakmaps (or use bigger peakmaps)
struct = scipy.io.loadmat(dataPath)['job_pack_0']

#---------------------------------------
# defining parameters                   |
#---------------------------------------
# times
PAR_tFft = 2048
PAR_Ntimes = struct["basic_info"][0,0]["ntim"][0,0][0,0]
PAR_tObs = numpy.round(PAR_tFft*PAR_Ntimes/(2*60*60*24*30)) # mesi
#print(PAR_tObs)
PAR_tObs = PAR_tObs*30*24*60*60
PAR_epoch = (57722+57990)/2 

# frequencies
PAR_enhance = 10
PAR_stepFreq = 1/PAR_tFft
PAR_refinedStepFreq =  PAR_stepFreq/PAR_enhance

# spindowns
PAR_fdotMin = -1e-8
PAR_fdotMax = 1e-9
PAR_stepFdot = PAR_stepFreq/PAR_tObs
PAR_nstepFdot = numpy.round((PAR_fdotMax-PAR_fdotMin)/PAR_stepFdot).astype(numpy.int32)

# others
PAR_secbelt = 4000

#---------------------------------------
# loading and managing data             |
#---------------------------------------

# the address of the peakmap in the input file structure is job_pack_0.peaks
# the peakmap is espressed as a sparse matrix with 3 arrays, since the most part is 0: 
# row coordinates (times), column coordinates (frequencies), values (weights)

# times
times = struct['peaks'][0,0][0]
times = times-PAR_epoch
times = ((times)*60*60*24/PAR_refinedStepFreq)

# frequencies
freqs = struct['peaks'][0,0][1]
freqMin = numpy.amin(freqs)
freqMax = numpy.amax(freqs)
freqStart = freqMin- PAR_stepFreq/2 - PAR_refinedStepFreq
freqEnd = freqMax + PAR_stepFreq/2 + PAR_refinedStepFreq
nstepFreqs = numpy.ceil((freqEnd-freqStart)/PAR_refinedStepFreq)+PAR_secbelt
freqs = freqs-freqStart
freqs = (freqs/PAR_refinedStepFreq)-round(PAR_enhance/2+0.001)
 
# spindowns
spindowns = numpy.arange(0, PAR_nstepFdot)
spindowns = numpy.multiply(spindowns,PAR_stepFdot)
spindowns = numpy.add(spindowns, PAR_fdotMin)

# others
weights = (struct['peaks'][0,0][4]+1)

# once the data of the peakmap are rearranged as above, it's better to rejoin them in an unique array
peakmap = numpy.stack((times,freqs,weights),1)
spindowns = spindowns

# these two variables are only redifinition of old variable to clarify better the Hough transorm code
PAR_nRows = numpy.int32(PAR_nstepFdot)
PAR_nColumns = numpy.int32(nstepFreqs)

#---------------------------------------
# defining TensorFlow graph             |
#---------------------------------------
PAR_secbeltTF = tf.constant(4000,dtype = tf.float32, name = 'secur')


timesTF = tf.placeholder(tf.float32, name = 'inputT')
freqsTF = tf.placeholder(tf.float32, name = 'inputF')
weightsTF = tf.placeholder(tf.float32, name = 'inputW')
spindownsTF = tf.placeholder(tf.float32, name = 'inputSD')

# calculating the Hough transofrm, with a fully vectorial tensorflow function with GPU parallelization
def frequencyHough(timesHM,freqsHM,weightsHM,spindownsHM):


	def mapnonVar(ithStep):
		sdTimed = tf.multiply(spindownsHM[ithStep], timesHM, name = "Tdotpert")

		transform = tf.round(freqsHM-sdTimed+PAR_secbeltTF/2, name = "trasfFreq")
		transform = tf.cast(transform, dtype=tf.int32)

		values = tf.unsorted_segment_sum(weightsHM, transform, PAR_nColumns, name = "cumsum")

		return values

	houghLeft = tf.Variable(tf.zeros([PAR_nRows,PAR_nColumns]),name = "var")

	houghLeft = houghLeft.assign(tf.map_fn(mapnonVar, tf.range(0, PAR_nRows), dtype=tf.float32, parallel_iterations=8))

	# let's superimpose the right edge on the image
	leftStripe = tf.slice(houghLeft, [0,0], [houghLeft.get_shape()[0],10], name = "left_stripe")
	houghRight = tf.subtract(tf.slice(houghLeft, [0,10],[houghLeft.get_shape()[0], houghLeft.get_shape()[1]-10]),
							 tf.slice(houghLeft, [0,0],[houghLeft.get_shape()[0], houghLeft.get_shape()[1]-10]), name = "right_stripe")
	# now we have the so called differential hough map
	houghDiff = tf.concat([leftStripe,houghRight],1, name = "diff_hough")
	# and at last we can cumulative sum along the rows to have the integral hough map
	houghMap = tf.cumsum(houghDiff, axis = 1, name = "output")
	return houghMap



### CAND SEL
#PAR defs
#PAR_coord = [1,2,0.1,0.1]
PAR_numCand = 100

PAR_freqInCorr = freqMin

# cutting out the securbelt finding the index of the first frequency of the Hough map
PAR_minDistance = PAR_enhance*4
PAR_firstFreq = PAR_freqInCorr-(PAR_secbelt/2)*PAR_refinedStepFreq

PAR_freqInitial = struct['basic_info'][0,0]['frin'][0,0][0,0]
PAR_freqFinal = struct['basic_info'][0,0]['frfi'][0,0][0,0]

# same as cut_gd2 SNAG function
PAR_indexInitial = ((PAR_freqInitial-PAR_firstFreq)/PAR_refinedStepFreq).astype(numpy.int32)
PAR_indexFinal = ((PAR_freqFinal-PAR_firstFreq)/PAR_refinedStepFreq+1).astype(numpy.int32)


FHMapCand = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF)[:,PAR_indexInitial:PAR_indexFinal-1]

# TODO SISTEMARE QUESTO RITAGLIO DELLE SECURBELTS
#FHMapCand = FHMap[:,PAR_indexInitial:PAR_indexFinal-1]
#FHMapCand = tf.cast(FHMapCand, dtype = tf.int32)



totWidth = tf.shape(FHMapCand)[1]
totHeight = tf.constant(PAR_nstepFdot)
beltWidth = tf.round(totWidth/PAR_numCand)
beltWidth = tf.cast(beltWidth, dtype = tf.int32)
#print(sess.run(beltWidth))


##splitting the map in belts

#houghBelts = tf.zeros([PAR_numCand, totHeight,beltWidth])
houghBelts = tf.split(FHMapCand, num_or_size_splits=PAR_numCand, axis=1)
#houghBelts = tf.cast(houghBelts, dtype = tf.int32)
#print(sess.run(tf.shape(houghBelts), feed_dict = dataDict))

medianTOT = tf.contrib.distributions.percentile(FHMapCand, 50)
sigmanTOT = tf.contrib.distributions.percentile(tf.abs(FHMapCand-medianTOT),50)
sigmanTOT = tf.cast(sigmanTOT, tf.float32)/0.6745



#-------------------CON SECONDARI!!-----------------------------
def find_maxs(tensor, numMatrices, numColumns):
	tensor = tf.matrix_transpose(tensor)

	local_top = tf.nn.top_k(tensor,k=1, sorted = True)

	ones = tf.ones(numMatrices, dtype = tf.int32)  
	columns = tf.tensordot(ones,tf.range(numColumns, dtype = tf.int32),axes = 0)
	columns = tf.reshape(columns,[numMatrices,numColumns,1])

	column_tops = tf.concat([tf.cast(local_top.values, tf.int32), local_top.indices, columns], axis = 2)

	idx = tf.reshape(tf.range(numMatrices), [-1, 1])
	idx_flat = tf.reshape(tf.tile(idx, [1, numColumns]), [-1])
	top_k_flat = tf.reshape(tf.nn.top_k(column_tops[:,:,0], k=numColumns).indices, [-1])
	final_idx = tf.reshape(tf.stack([idx_flat, top_k_flat], 1),[numMatrices, numColumns, 2])

	column_maxs = tf.gather_nd(column_tops, final_idx)

	localMaxs = column_maxs[:,0]
	second_localMaxs = column_maxs[:,1]
	return localMaxs, second_localMaxs


[primaryMaxs, secondaryMaxs] = find_maxs(houghBelts, PAR_numCand, beltWidth)

primaryMaxs = tf.cast(primaryMaxs, tf.float32)
secondaryMaxs = tf.cast(secondaryMaxs, tf.float32)

localMedians = tf.contrib.distributions.percentile(houghBelts, 50, axis = [1,2])
localSigmans = tf.contrib.distributions.percentile(tf.abs(houghBelts-tf.reshape(localMedians,[PAR_numCand,1,1])), 50, axis = [1,2])
localSigmans = tf.cast(localSigmans, tf.float32)/0.6745


maxSet = tf.concat([primaryMaxs, secondaryMaxs, tf.reshape(localMedians, [PAR_numCand, 1]), tf.reshape(localSigmans, [PAR_numCand, 1])], axis = 1)



#----------------------------------------
# feeding and running                   |
#----------------------------------------
# this is the dictionary with the data to be fed to the graph using placeholders
dataDict = { 
               timesTF : peakmap[:,0],
               freqsTF : peakmap[:,1],
               weightsTF : peakmap[:,2],
               spindownsTF : spindowns
             }

sess.run(tf.global_variables_initializer())

start = time.time()
[filtering, medianTOT, sigmanTOT] = sess.run([maxSet, medianTOT, sigmanTOT], feed_dict = dataDict)


filtering = filtering[filtering[:,6]>0]
filtering = filtering[filtering[:,0]>filtering[:,6]]
filtering = filtering[filtering[:,0]>medianTOT/2]


primTosecDist = numpy.abs(filtering[:,5]-filtering[:,2])
#print(primTosecDist)
secondFiltering = filtering[primTosecDist > 2 * PAR_minDistance, 3:]
secondFiltering = secondFiltering[secondFiltering[:,0]>secondFiltering[:,3]]


cands = numpy.concatenate((filtering[:,0:3], secondFiltering[:,0:3]), axis = 0)

stop = time.time()

print(stop-start)


#image = sess.run(FHMap, feed_dict = dataDict)#, options=run_options,run_metadata = run_metadata)
#from matplotlib import pyplot
#a = pyplot.imshow(image, aspect = 200)
#pyplot.show()
