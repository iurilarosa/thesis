 
import tensorflow as tf
import numpy



sess = tf.Session()
listofmat = [
			[
				[10,11,12,25],
				[11,3,14,2],
				[12,19,13,11]
			],
			[
				[1,6,1,5],
				[1,200,2,2],
				[2,10,1,4]
			]
		]


numMatrices = 2
numColumns = 4
tensorBelts = tf.constant(listofmat, dtype = tf.int32)

def mapmax(i):
	tensor = tf.transpose(tensorBelts[i])

	column_top = tf.nn.top_k(tensor,k=1)
	local_top = tf.reshape(column_top.values, [numColumns])
	local_top = tf.nn.top_k(local_top, k=2)
	
	localValue = tf.reshape(local_top.values, [2, 1])
	localRow = tf.gather(column_top.indices, local_top.indices)
	localColumn = tf.reshape(local_top.indices, [2,1])
	
	local_max = tf.concat([localValue, localRow, localColumn], axis = 1)
	
	#primary_max = local_max[0]
	#secondary_max = local_max[1]
	return local_max

#localmaxs = mapmax(1)
localmaxs = tf.map_fn(mapmax, tf.range(numMatrices), parallel_iterations = 2)

primaryMaxs = localmaxs[:,0]
secondaryMaxs = localmaxs[:,1]



print(sess.run([primaryMaxs,secondaryMaxs]))






#print(sess.run(localMaxs))
#print(sess.run([primaryCond, primaryCands]))

