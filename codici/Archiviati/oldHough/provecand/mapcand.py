
import tensorflow as tf

medianTOT = tf.constant(60.0)
localMedians = tf.constant([50,50, 0, 50], dtype = tf.float32)

localMaxs = tf.constant([
						[49, 2, 1],
						[65, 5, 3],
						[60, 3, 12],
						[23, 1, 3],
					], dtype = tf.float32)

second_localMaxs = tf.constant([
						[39, 5, 2],
						[55, 4, 1],
						[50, 2, 4],
						[13, 1, 5],
					], dtype = tf.float32)



primaryCond0 = (localMedians > 0)
primaryCond1 = tf.logical_and(localMaxs[:,0] > localMedians, localMaxs[:,0] > medianTOT/2)
primaryCond = tf.equal(primaryCond0,primaryCond1)

primaryCands = tf.gather(localMaxs,tf.where(primaryCond))



localIds = localMaxs[:,2]
second_localIds = second_localMaxs[:,2]

secondaryCond0 = tf.logical_and(tf.absolute(second_localIds-localIds) > 2 * minDistance, second_localMaxs > localMedians)

secondaryCond = tf.equal(primaryCond, secondaryCond0)


secondaryCands = tf.gather(second_localMaxs, tf.where(secondaryCond))

 
sess = tf.Session()
print(sess.run(primaryCands))
