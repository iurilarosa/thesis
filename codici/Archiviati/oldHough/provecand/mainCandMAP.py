import tensorflow as tf
import numpy
import scipy.io
import time
from tensorflow.python.client import timeline

# open a tf session
sess = tf.Session()

# input file path
dataPath ="/home/iuri.larosa/ifil/in_O2LL_03_0744_.mat"
#dataPath ="/home/iuri.larosa/ifil/in_O2LL_04_1473_.mat"

#dataPath = "/home/protoss/Documenti/TESI/ifil/in_O2LL_02_0327_.mat"

# loading the input file (with the peakmap inside a matlab structure)
#WARNING at the moment the hough transform is parallelized only on a single peakmap
#TODO improve parallelization on multiple peakmaps (or use bigger peakmaps)
struct = scipy.io.loadmat(dataPath)['job_pack_0']

#---------------------------------------
# defining parameters                   |
#---------------------------------------
# times
PAR_tFft = 2048
PAR_Ntimes = struct["basic_info"][0,0]["ntim"][0,0][0,0]
PAR_tObs = numpy.round(PAR_tFft*PAR_Ntimes/(2*60*60*24*30)) # mesi
#print(PAR_tObs)
PAR_tObs = PAR_tObs*30*24*60*60
PAR_epoch = (57722+57990)/2 

# frequencies
PAR_enhance = 10
PAR_stepFreq = 1/PAR_tFft
PAR_refinedStepFreq =  PAR_stepFreq/PAR_enhance

# spindowns
PAR_fdotMin = -1e-8
PAR_fdotMax = 1e-9
PAR_stepFdot = PAR_stepFreq/PAR_tObs
PAR_nstepFdot = numpy.round((PAR_fdotMax-PAR_fdotMin)/PAR_stepFdot).astype(numpy.int32)

# others
PAR_secbelt = 4000

#---------------------------------------
# loading and managing data             |
#---------------------------------------

# the address of the peakmap in the input file structure is job_pack_0.peaks
# the peakmap is espressed as a sparse matrix with 3 arrays, since the most part is 0: 
# row coordinates (times), column coordinates (frequencies), values (weights)

# times
times = struct['peaks'][0,0][0]
times = times-PAR_epoch
times = ((times)*60*60*24/PAR_refinedStepFreq)

# frequencies
freqs = struct['peaks'][0,0][1]
freqMin = numpy.amin(freqs)
freqMax = numpy.amax(freqs)
freqStart = freqMin- PAR_stepFreq/2 - PAR_refinedStepFreq
freqEnd = freqMax + PAR_stepFreq/2 + PAR_refinedStepFreq
nstepFreqs = numpy.ceil((freqEnd-freqStart)/PAR_refinedStepFreq)+PAR_secbelt
freqs = freqs-freqStart
freqs = (freqs/PAR_refinedStepFreq)-round(PAR_enhance/2+0.001)
 
# spindowns
spindowns = numpy.arange(0, PAR_nstepFdot)
spindowns = numpy.multiply(spindowns,PAR_stepFdot)
spindowns = numpy.add(spindowns, PAR_fdotMin)

# others
weights = (struct['peaks'][0,0][4]+1)

# once the data of the peakmap are rearranged as above, it's better to rejoin them in an unique array
peakmap = numpy.stack((times,freqs,weights),1)
spindowns = spindowns

# these two variables are only redifinition of old variable to clarify better the Hough transorm code
PAR_nRows = numpy.int32(PAR_nstepFdot)
PAR_nColumns = numpy.int32(nstepFreqs)

#---------------------------------------
# defining TensorFlow graph             |
#---------------------------------------
PAR_secbeltTF = tf.constant(4000,dtype = tf.float32, name = 'secur')


timesTF = tf.placeholder(tf.float32, name = 'inputT')
freqsTF = tf.placeholder(tf.float32, name = 'inputF')
weightsTF = tf.placeholder(tf.float32, name = 'inputW')
spindownsTF = tf.placeholder(tf.float32, name = 'inputSD')

# calculating the Hough transofrm, with a fully vectorial tensorflow function with GPU parallelization
def frequencyHough(timesHM,freqsHM,weightsHM,spindownsHM):


	def mapnonVar(ithStep):
		sdTimed = tf.multiply(spindownsHM[ithStep], timesHM, name = "Tdotpert")

		transform = tf.round(freqsHM-sdTimed+PAR_secbeltTF/2, name = "trasfFreq")
		transform = tf.cast(transform, dtype=tf.int32)

		values = tf.unsorted_segment_sum(weightsHM, transform, PAR_nColumns, name = "cumsum")

		return values

	houghLeft = tf.Variable(tf.zeros([PAR_nRows,PAR_nColumns]),name = "var")

	houghLeft = houghLeft.assign(tf.map_fn(mapnonVar, tf.range(0, PAR_nRows), dtype=tf.float32, parallel_iterations=8))

	# let's superimpose the right edge on the image
	leftStripe = tf.slice(houghLeft, [0,0], [houghLeft.get_shape()[0],10], name = "left_stripe")
	houghRight = tf.subtract(tf.slice(houghLeft, [0,10],[houghLeft.get_shape()[0], houghLeft.get_shape()[1]-10]),
							 tf.slice(houghLeft, [0,0],[houghLeft.get_shape()[0], houghLeft.get_shape()[1]-10]), name = "right_stripe")
	# now we have the so called differential hough map
	houghDiff = tf.concat([leftStripe,houghRight],1, name = "diff_hough")
	# and at last we can cumulative sum along the rows to have the integral hough map
	houghMap = tf.cumsum(houghDiff, axis = 1, name = "output")
	return houghMap



### CAND SEL
#PAR defs
#PAR_coord = [1,2,0.1,0.1]
PAR_numCand = 100

PAR_freqInCorr = freqMin

# cutting out the securbelt finding the index of the first frequency of the Hough map
PAR_minDistance = PAR_enhance*4
PAR_firstFreq = PAR_freqInCorr-(PAR_secbelt/2)*PAR_refinedStepFreq

PAR_freqInitial = struct['basic_info'][0,0]['frin'][0,0][0,0]
PAR_freqFinal = struct['basic_info'][0,0]['frfi'][0,0][0,0]

# same as cut_gd2 SNAG function
PAR_indexInitial = ((PAR_freqInitial-PAR_firstFreq)/PAR_refinedStepFreq).astype(numpy.int32)
PAR_indexFinal = ((PAR_freqFinal-PAR_firstFreq)/PAR_refinedStepFreq+1).astype(numpy.int32)




FHMapCand = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF)[:,PAR_indexInitial:PAR_indexFinal-1]

#----------------------------------------
# feeding and running                   |
#----------------------------------------
# this is the dictionary with the data to be fed to the graph using placeholders
dataDict = { 
               timesTF : peakmap[:,0],
               freqsTF : peakmap[:,1],
               weightsTF : peakmap[:,2],
               spindownsTF : spindowns
             }


# TODO SISTEMARE QUESTO RITAGLIO DELLE SECURBELTS
#FHMapCand = FHMap[:,PAR_indexInitial:PAR_indexFinal-1]
#FHMapCand = tf.cast(FHMapCand, dtype = tf.int32)



totWidth = tf.shape(FHMapCand)[1]
totHeight = tf.constant(PAR_nstepFdot)
beltWidth = tf.round(totWidth/PAR_numCand)
beltWidth = tf.cast(beltWidth, dtype = tf.int32)
belts = tf.range(0, totWidth, beltWidth)


medianTOT = tf.contrib.distributions.percentile(FHMapCand, 50)
sigmanTOT = tf.contrib.distributions.percentile(tf.abs(FHMapCand-medianTOT),50)
sigmanTOT = tf.cast(sigmanTOT, tf.float32)/0.6745


def mapstats(i):
	
	belt = FHMapCand[:,belts[i]:belts[i+1]]
	belt = tf.transpose(belt)
	
	numColumns = beltWidth

	column_top = tf.nn.top_k(belt,k=1)
	local_top = tf.reshape(column_top.values, [numColumns])
	local_top = tf.nn.top_k(local_top, k=2)
	
	localValue = tf.reshape(local_top.values, [2, 1])
	localRow = tf.cast(tf.gather(column_top.indices, local_top.indices), tf.float32)
	localColumn = tf.cast(tf.reshape(local_top.indices, [2,1]), tf.float32)
	
	local_max = tf.concat([localValue, localRow, localColumn], axis = 1)
	

	local_median = tf.contrib.distributions.percentile(belt, 50, axis = [0,1])
	local_sigman = tf.contrib.distributions.percentile(tf.abs(belt-local_median), 50, axis = [0,1])
	local_sigman = tf.cast(local_sigman, tf.float32)/0.6745
	
	
	local_max = tf.cast(local_max, dtype = tf.float32)

	local_stats = tf.concat([
						local_max[0],
						local_max[1], 
						[local_median], 
						[local_sigman]
						], 
	axis = 0)
	
	return local_stats

def laststats():
	belt = FHMapCand[:,belts[-1]:]
	numColumns = tf.shape(belt)[1]
	
	belt = tf.transpose(belt)
	
	column_top = tf.nn.top_k(belt,k=1)
	local_top = tf.reshape(column_top.values, [numColumns])
	local_top = tf.nn.top_k(local_top, k=2)
	
	localValue = tf.reshape(local_top.values, [2, 1])
	localRow = tf.cast(tf.gather(column_top.indices, local_top.indices), tf.float32)
	localColumn = tf.cast(tf.reshape(local_top.indices, [2,1]), tf.float32)
	
	local_max = tf.concat([localValue, localRow, localColumn], axis = 1)
	

	local_median = tf.contrib.distributions.percentile(belt, 50, axis = [0,1])
	local_sigman = tf.contrib.distributions.percentile(tf.abs(belt-local_median), 50, axis = [0,1])
	local_sigman = tf.cast(local_sigman, tf.float32)/0.6745
	
	
	last_max = tf.cast(local_max, dtype = tf.float32)

	last_stats = tf.concat([
						local_max[0],
						local_max[1], 
						[local_median], 
						[local_sigman]
						], 
	axis = 0)
	
	return last_stats


#localStats = mapstats(1)

localStats = tf.map_fn(mapstats, tf.range(0,PAR_numCand-1), dtype = tf.float32, parallel_iterations = 8)

lastStats = tf.reshape(laststats(), [1,8])

localStats = tf.concat([localStats, lastStats ], axis = 0)



#print(sess.run(lastStats, feed_dict = dataDict))

primaryMaxs = tf.cast(localStats[:,0:3], tf.float32)
secondaryMaxs = tf.cast(localStats[:,3:6], tf.float32)
localMedians = tf.cast(localStats[:,6], tf.float32)
localSigmans = tf.cast(localStats[:,7], tf.float32)
semimedianTOT = tf.cast(medianTOT/2, tf.float32)


#print(sess.run(primaryMaxs, feed_dict = dataDict))

def find_cands(localMedians, localMaxs, semimedianTOT, second_localMaxs):
	primaryCond0 = (localMedians > 0)
	primaryCond1 = tf.logical_and(localMaxs[:,0] > localMedians, localMaxs[:,0] > semimedianTOT)
	primaryCond = tf.equal(primaryCond0,primaryCond1)

	primaryCands = tf.gather(localMaxs,tf.where(primaryCond))

	localIds = localMaxs[:,2]
	second_localIds = second_localMaxs[:,2]

	secondaryCond0 = tf.logical_and(tf.abs(second_localIds-localIds) > 2 * PAR_minDistance, second_localMaxs[:,0] > localMedians)
	secondaryCond = tf.equal(primaryCond, secondaryCond0)

	secondaryCands = tf.gather(second_localMaxs, tf.where(secondaryCond))
	return primaryCands, secondaryCands


candycandy = find_cands(localMedians, primaryMaxs, semimedianTOT, secondaryMaxs)

start = time.time()
[cands1, cands2] = sess.run(candycandy, feed_dict = dataDict)
stop = time.time()

print(stop-start, cands2.shape)


sess.close()
