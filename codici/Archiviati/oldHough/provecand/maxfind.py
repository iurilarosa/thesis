import tensorflow as tf
import numpy




listofmat = [
			[
				[10,11,12,25],
				[11,3,14,2],
				[12,19,13,11]
			],
			[
				[1,6,1,5],
				[1,200,2,2],
				[2,10,1,4]
			]
		]


numMatrices = 2
numColumns = 4
tensorBelts = tf.constant(listofmat, dtype = tf.int32)


#def find_2maxs(tensor, numMatrices, numColumns):
	#tensor = tf.matrix_transpose(tensor)

	#local_top = tf.nn.top_k(tensor,k=1, sorted = True)

	#ones = tf.ones(numMatrices, dtype = tf.int32)  
	#columns = tf.tensordot(ones,tf.range(numColumns),axes = 0)
	#columns = tf.reshape(columns,[numMatrices,numColumns,1])

	#column_tops = tf.concat([local_top.values, local_top.indices, columns], axis = 2)

	#idx = tf.reshape(tf.range(numMatrices), [-1, 1])
	#idx_flat = tf.reshape(tf.tile(idx, [1, numColumns]), [-1])
	#top_k_flat = tf.reshape(tf.nn.top_k(column_tops[:,:,0], k=numColumns).indices, [-1])
	#final_idx = tf.reshape(tf.stack([idx_flat, top_k_flat], 1),[numMatrices, numColumns, 2])

	#column_maxs = tf.gather_nd(column_tops, final_idx)

	#localMaxs = column_maxs[:,0]
	#second_localMaxs = column_maxs[:,1]
	#return localMaxs, second_localMaxs


#[primaxs, secmaxs] = find_maxs(tensorBelts, numMatrices, numColumns)



def find_1max(tensor, numColumns):
	local_top = tf.reduce_max(tensor,axis = [1,2])
	
	x = tf.reshape(tensor, [tf.shape(tensor)[0], -1])
	indices = tf.argmax(x, axis=1)
	rows = indices / numColumns
	cols = indices % numColumns
	
	cols = tf.cast(cols, tf.int32)
	rows = tf.cast(rows, tf.int32)
	
	
	local_maxs = tf.transpose(tf.stack([local_top, rows,cols]))
	
	
	return local_maxs

primaxs = find_1max(tensorBelts, numColumns)



sess = tf.Session()
print(sess.run(primaxs))
#print(sess.run([primaryCond, primaryCands]))

