import tensorflow as tf
import numpy
import scipy.io
import time
#from tensorflow.python.client import timeline

# open a tf session
sess = tf.Session()

# input file path
#dataPath ="/home/iuri.larosa/ifil/in_O2LL_01_0093_.mat"
#dataPath ="/home/iuri.larosa/ifil/in_O2LL_02_0327_.mat"
#dataPath ="/home/iuri.larosa/ifil/in_O2LL_03_0744_.mat"
#dataPath ="/home/iuri.larosa/ifil/in_O2LL_04_1473_.mat"


#dataPath ="/home/iurilarosa/data/ifil/in_O2LL_01_0093_.mat"
#dataPath ="/home/iurilarosa/data/ifil/in_O2LL_02_0327_.mat"
dataPath ="/home/iurilarosa/data/ifil/in_O2LL_03_0744_.mat"
#dataPath ="/home/iurilarosa/data/ifil/in_O2LL_04_1473_.mat"

#dataPath = "/home/protoss/Documenti/TESI/ifil/in_O2LL_01_0093_.mat"
#dataPath = "/home/protoss/Documenti/TESI/ifil/in_O2LL_02_0327_.mat"
#dataPath = "/home/protoss/Documenti/TESI/ifil/in_O2LL_03_0744_.mat"
#dataPath = "/home/protoss/Documenti/TESI/ifil/in_O2LL_04_1473_.mat"

# loading the input file (with the peakmap inside a matlab structure)
#WARNING at the moment the hough transform is parallelized only on a single peakmap
#TODO improve parallelization on multiple peakmaps (or use bigger peakmaps)
struct = scipy.io.loadmat(dataPath)['job_pack_0']

#---------------------------------------
# defining parameters                   |
#---------------------------------------
# frequencies
PAR_enhance = 10
PAR_stepFreq = struct["basic_info"][0,0]["dfr"][0,0][0,0]
PAR_refinedStepFreq =  PAR_stepFreq/PAR_enhance

# times
# TODO PROVARE PEAKMAP DA 8000S
PAR_tFft = numpy.int32(1/PAR_stepFreq)
print(PAR_tFft)
PAR_Ntimes = struct["basic_info"][0,0]["ntim"][0,0][0,0]
PAR_tObs = numpy.round(PAR_tFft*PAR_Ntimes/(2*60*60*24*30)) # mesi
#print(PAR_tObs)
PAR_tObs = PAR_tObs*30*24*60*60
PAR_epoch = (57722+57990)/2 



# spindowns
PAR_fdotMin = -1e-8
PAR_fdotMax = 1e-9
PAR_stepFdot = PAR_stepFreq/PAR_tObs
PAR_nstepFdot = numpy.round((PAR_fdotMax-PAR_fdotMin)/PAR_stepFdot).astype(numpy.int32)

# others
PAR_secbelt = 4000

#---------------------------------------
# loading and managing data             |
#---------------------------------------

# the address of the peakmap in the input file structure is job_pack_0.peaks
# the peakmap is espressed as a sparse matrix with 3 arrays, since the most part is 0: 
# row coordinates (times), column coordinates (frequencies), values (weights)

# times
times = struct['peaks'][0,0][0]
times = times-PAR_epoch
times = ((times)*60*60*24/PAR_refinedStepFreq)

# frequencies
freqs = struct['peaks'][0,0][1]
freqMin = numpy.amin(freqs)
freqMax = numpy.amax(freqs)
freqStart = freqMin- PAR_stepFreq/2 - PAR_refinedStepFreq
freqEnd = freqMax + PAR_stepFreq/2 + PAR_refinedStepFreq
nstepFreqs = numpy.ceil((freqEnd-freqStart)/PAR_refinedStepFreq)+PAR_secbelt
freqs = freqs-freqStart
freqs = (freqs/PAR_refinedStepFreq)-round(PAR_enhance/2+0.001)
freqs = freqs

# spindowns
spindowns = numpy.arange(0, PAR_nstepFdot)
spindowns = numpy.multiply(spindowns,PAR_stepFdot)
spindowns = numpy.add(spindowns, PAR_fdotMin)
print(spindowns.size)

# others
weights = (struct['peaks'][0,0][4]+1)

# once the data of the peakmap are rearranged as above, it's better to rejoin them in an unique array
peakmap = numpy.stack((times,freqs,weights),1).astype(numpy.float32)

# these two variables are only redifinition of old variable to clarify better the Hough transorm code
PAR_nRows = numpy.int32(PAR_nstepFdot)
PAR_nColumns = numpy.int32(nstepFreqs)

#---------------------------------------
# defining TensorFlow graph             |
#---------------------------------------
PAR_secbeltTF = tf.constant(4000,dtype = tf.float32, name = 'secur')


timesTF = tf.placeholder(tf.float32, name = 'inputT')
freqsTF = tf.placeholder(tf.float32, name = 'inputF')
weightsTF = tf.placeholder(tf.float32, name = 'inputW')
spindownsTF = tf.placeholder(tf.float32, name = 'inputSD')

# calculating the Hough transofrm, with a fully vectorial tensorflow function with GPU parallelization
def frequencyHough(timesHM,freqsHM,weightsHM,spindownsHM):
	def mapnonVar(ithStep):
		sdTimed = tf.multiply(spindownsHM[ithStep], timesHM, name = "Tdotpert")

		transform = tf.round(freqsHM-sdTimed+PAR_secbeltTF/2, name = "trasfFreq")
		transform = tf.cast(transform, dtype=tf.int32)

		values = tf.unsorted_segment_sum(weightsHM, transform, PAR_nColumns, name = "cumsum")

		return values

	houghLeft = tf.Variable(tf.zeros([PAR_nRows,PAR_nColumns]),name = "var")

	houghLeft = houghLeft.assign(tf.map_fn(mapnonVar, tf.range(0, PAR_nRows), dtype=tf.float32, parallel_iterations=8))

	# let's superimpose the right edge on the image
	leftStripe = tf.slice(houghLeft, [0,0], [houghLeft.get_shape()[0],10], name = "left_stripe")
	houghRight = tf.subtract(
						tf.slice(houghLeft, 
								[0,10],
								[houghLeft.get_shape()[0],houghLeft.get_shape()[1]-10]),
						tf.slice(houghLeft,
								[0,0],
								[houghLeft.get_shape()[0], houghLeft.get_shape()[1]-10]),
								name = "right_stripe"
						)
						
	# now we have the so called differential hough map
	houghDiff = tf.concat([leftStripe,houghRight],1, name = "diff_hough")
	# and at last we can cumulative sum along the rows to have the integral hough map
	houghMap = tf.cumsum(houghDiff, axis = 1, name = "output")
	return houghMap

####
#ONLY HOUGH CHECK TODO UNCOMMENT IF NEEDED
#dataDict = { 
               #timesTF : peakmap[:,0],
               #freqsTF : peakmap[:,1],
               #weightsTF : peakmap[:,2],
               #spindownsTF : spindowns
             #}

#hough = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF)
#sess.run(tf.global_variables_initializer())

#image = sess.run(hough, feed_dict = dataDict)
#print(image.shape)
####


### CAND SEL
#PAR defs
PAR_coord = numpy.array([1,2,0.1,0.1,0.15])
NPcoord = numpy.array([PAR_coord[0], PAR_coord[1], PAR_coord[2]/2, numpy.abs(PAR_coord[3]-PAR_coord[4])/4])
PAR_numCand = 10
PAR_numstripes = 1
coords = tf.constant(numpy.zeros((PAR_numCand,4))+NPcoord, dtype = tf.float32)

PAR_freqInCorr = freqMin

# cutting out the securbelt finding the index of the first frequency of the Hough map
PAR_minDistance = PAR_enhance*4
#PAR_firstFreq = PAR_freqInCorr-(PAR_secbelt/2)*PAR_refinedStepFreq
PAR_firstFreq = freqStart
PAR_freqInitial = struct['basic_info'][0,0]['frin'][0,0][0,0]
PAR_freqFinal = struct['basic_info'][0,0]['frfi'][0,0][0,0]

print(PAR_freqInitial,PAR_freqFinal,PAR_firstFreq)

# same as cut_gd2 SNAG function
PAR_indexInitial = ((PAR_freqInitial-PAR_firstFreq)/PAR_refinedStepFreq).astype(numpy.int32)
PAR_indexFinal = ((PAR_freqFinal-PAR_firstFreq)/PAR_refinedStepFreq+1).astype(numpy.int32)


#FH con fasce laterali già tolte
FHMapCand = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF)[:,PAR_indexInitial:PAR_indexFinal-1]
dataDict = { 
               timesTF : peakmap[:,0],
               freqsTF : peakmap[:,1],
               weightsTF : peakmap[:,2],
               spindownsTF : spindowns
             }

sess.run(tf.global_variables_initializer())

image = sess.run(FHMapCand, feed_dict = dataDict)
print(image.shape, "AAAAA")




PAR_totWidth = tf.shape(FHMapCand)[1]
PAR_totHeight = tf.constant(PAR_nstepFdot)

PAR_beltWidth = tf.floor(PAR_totWidth/PAR_numCand)
PAR_beltWidth = tf.cast(PAR_beltWidth, dtype = tf.int32)
belts = tf.range(0, PAR_totWidth, PAR_beltWidth)
#print(sess.run([PAR_totWidth, PAR_beltWidth], feed_dict = dataDict))

PAR_stripeHeight = tf.floor(PAR_totHeight/PAR_numstripes)
PAR_stripeHeight = tf.cast(PAR_stripeHeight, dtype = tf.int32)

#splitting the map in belts
houghBelts = tf.zeros([PAR_numCand, PAR_totHeight,PAR_beltWidth])
houghBelts = tf.split(FHMapCand, num_or_size_splits=PAR_numCand, axis=1)
houghBelts = tf.cast(houghBelts, dtype = tf.int32)
#print(sess.run([PAR_stripeHeight, tf.shape(houghBelts)], feed_dict = dataDict))

medianTOT = tf.contrib.distributions.percentile(FHMapCand, 50)
#sigmanTOT = tf.contrib.distributions.percentile(tf.abs(FHMapCand-medianTOT),50)
#sigmanTOT = tf.cast(sigmanTOT, tf.float32)/0.6745



#-------------------SOLO PRIMARI!!!!!-----------------------------
def cands(i):
	houghSquares = houghBelts[:,i*PAR_stripeHeight:(i+1)*PAR_stripeHeight,:]
	#print(sess.run([i*PAR_stripeHeight, (i+1)*PAR_stripeHeight, tf.shape(houghSquares)], feed_dict = dataDict ))
	def find_maxs(tensor, numColumns):
		local_top = tf.reduce_max(tensor,axis = [1,2])
		
		x = tf.reshape(tensor, [tf.shape(tensor)[0], -1])
		x = tf.cast(x, tf.int32)
		indices = tf.argmax(x, axis=1)
		
		indices = tf.cast(indices, tf.int32)
		
		rows = tf.cast(indices / numColumns, tf.float32)
		cols = tf.cast(indices % numColumns, tf.float32)
		
		rows = tf.cast(rows, tf.int32)
		cols = tf.cast(cols, tf.int32)
		
		local_maxs = tf.transpose(tf.stack([local_top, rows, cols]))
		
		return local_maxs

	#NB [ampiezza max, riga => spindown, colonna => freq]
	primaryMaxs = find_maxs(houghSquares, PAR_beltWidth)
	

	localMedians = tf.contrib.distributions.percentile(houghSquares, 50, axis = [1,2])
	localSigmans = tf.contrib.distributions.percentile(tf.abs(houghSquares-tf.reshape(localMedians,[PAR_numCand,1,1])), 50, axis = [1,2])
	localSigmans = tf.cast(localSigmans, tf.float32)/0.6745

	primaryMaxs = tf.cast(primaryMaxs, tf.float32)
	localMedians = tf.cast(localMedians, tf.float32)
	localSigmans = tf.cast(localSigmans, tf.float32)
	semimedianTOT = tf.cast(medianTOT/2, tf.float32)

	def find_cands(medians, sigmans, maxs, semimedian):
		CR = tf.reshape((maxs[:,0]-medians)/sigmans, [PAR_numCand, 1])

		preliminaryCands = tf.concat([maxs, CR, coords], axis = 1)
		
		primaryCond0 = (medians > 0)
		primaryCond1 = tf.logical_and(maxs[:,0] > medians, maxs[:,0] > semimedian)
		primaryCond = tf.equal(primaryCond0,primaryCond1)

		primaryCands = tf.gather(preliminaryCands,tf.where(primaryCond))
		
		
		#cands = tf.concat([primaryCands], axis = 1)
		
		return primaryCands


	return find_cands(localMedians, localSigmans, primaryMaxs, semimedianTOT)


#def nonMapCands():
	#houghSquares = houghBelts
	##print(sess.run([i*PAR_stripeHeight, (i+1)*PAR_stripeHeight, tf.shape(houghSquares)], feed_dict = dataDict ))
	#def find_maxs(tensor, numColumns):
		#local_top = tf.reduce_max(tensor,axis = [1,2])
		
		#x = tf.reshape(tensor, [tf.shape(tensor)[0], -1])
		#x = tf.cast(x, tf.int32)
		#indices = tf.argmax(x, axis=1)
		
		#indices = tf.cast(indices, tf.int32)
		
		#rows = tf.cast(indices / numColumns, tf.float32)
		#cols = tf.cast(indices % numColumns, tf.float32)
		
		#rows = tf.cast(rows, tf.int32)
		#cols = tf.cast(cols, tf.int32)
		
		#local_maxs = tf.transpose(tf.stack([local_top, rows, cols]))
		
		#return local_maxs

	##NB [ampiezza max, riga => spindown, colonna => freq]
	#primaryMaxs = find_maxs(houghSquares, PAR_beltWidth)
	

	#localMedians = tf.contrib.distributions.percentile(houghSquares, 50, axis = [1,2])
	#localSigmans = tf.contrib.distributions.percentile(tf.abs(houghSquares-tf.reshape(localMedians,[PAR_numCand,1,1])), 50, axis = [1,2])
	#localSigmans = tf.cast(localSigmans, tf.float32)/0.6745

	#primaryMaxs = tf.cast(primaryMaxs, tf.float32)
	#localMedians = tf.cast(localMedians, tf.float32)
	#localSigmans = tf.cast(localSigmans, tf.float32)
	#semimedianTOT = tf.cast(medianTOT/2, tf.float32)

	#def find_cands(medians, sigmans, maxs, semimedian):
		#CR = tf.reshape((maxs[:,0]-medians)/sigmans, [PAR_numCand, 1])

		#preliminaryCands = tf.concat([maxs, CR, coords], axis = 1)
		
		#primaryCond0 = (medians > 0)
		#primaryCond1 = tf.logical_and(maxs[:,0] > medians, maxs[:,0] > semimedian)
		#primaryCond = tf.equal(primaryCond0,primaryCond1)

		#primaryCands = tf.gather(preliminaryCands,tf.where(primaryCond))
		
		
		##cands = tf.concat([primaryCands], axis = 1)
		
		#return primaryCands


	#return find_cands(localMedians, localSigmans, primaryMaxs, semimedianTOT)




#candycandy = tf.map_fn(cands, tf.range(0, PAR_numstripes), dtype=tf.float32, parallel_iterations=1)
candycandy = cands(0)
#candycandy = nonMapCands()

#----------------------------------------
# feeding and running                   |
#----------------------------------------
# this is the dictionary with the data to be fed to the graph using placeholders
dataDict = { 
               timesTF : peakmap[:,0],
               freqsTF : peakmap[:,1],
               weightsTF : peakmap[:,2],
               spindownsTF : spindowns
             }


sess.run(tf.global_variables_initializer())

#t1 = time.time()
#image = sess.run(houghBelts, feed_dict = dataDict)
t2 = time.time()
#maxconSec = sess.run(find_maxs(houghBelts, PAR_numCand, PAR_beltWidth), feed_dict = dataDict)
#maxsoloPrim = sess.run(find_maxs(houghBelts, PAR_beltWidth), feed_dict = dataDict)

cands = sess.run(candycandy, feed_dict = dataDict)
#cands = sess.run([FHMapCand, PAR_beltWidth, PAR_stripeHeight, candycandy], feed_dict = dataDict)
sess.close()

#image = cands[0]
#PAR_beltwidth = cands[1]
#PAR_stripeHeight = cands[2]
#cands = cands[3]

#cands = numpy.reshape(cands, [cands.shape[0],cands.shape[1], cands.shape[3]])

#for i in numpy.arange(PAR_numstripes-1):
	#cands[i,:,1] = cands[i,:,1]+(i*PAR_stripeHeight)

#for i in numpy.arange(PAR_numCand-1):
	#cands[:,i,2] = cands[:,i,2]+(i*PAR_beltwidth)
	
#cands = numpy.reshape(cands, [cands.shape[0]*cands.shape[1], cands.shape[2]])


#cands[:,2] = cands[:,2]*PAR_refinedStepFreq+PAR_freqInitial

stop = time.time()
#print(t2-t1)

cands = numpy.reshape(cands, [cands.shape[0], cands.shape[2]])
for i in numpy.arange(PAR_numCand-1):
	cands[i,2] = cands[i,2]+(i*1024)


print(stop-t2, cands.shape, "\n",cands.astype(numpy.int32))




#image = sess.run(FHMap, feed_dict = dataDict)#, options=run_options,run_metadata = run_metadata)
#from matplotlib import pyplot
#a = pyplot.imshow(image, aspect=200)
##b = pyplot.scatter(x = cands[:,2], y = cands[:,1], s= 2 )
#pyplot.show()


