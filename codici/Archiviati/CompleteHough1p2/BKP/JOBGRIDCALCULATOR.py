import glob
import os
import numpy
import scipy.io
from PAR import parDefs
from GridGen import make_skygrid

band = "2048"
pathslist = glob.glob("/m100_work/INF20_virgo_1/ilarosa0/data/"+band+"Hz/LL/*")
pathslist = numpy.sort(numpy.array(pathslist))
fileslist = pathslist

#time_per_iter = 5.5
time_per_iter = 6.8
npoints = numpy.zeros(fileslist.size)

for i in numpy.arange(fileslist.size):
    print(i)
    dataPath = fileslist[i]
    struct = scipy.io.loadmat(dataPath)['job_pack_0']
    PAR = parDefs(struct)
    # sky grid generation
    [eclCoord, rectCoord, eqCoord] = make_skygrid(PAR)
    npoints[i] = eclCoord.shape[0]
print(npoints.shape, npoints)
numpy.savetxt('/m100/home/userexternal/ilarosa0/2048gridsizes.txt',npoints)

#npoints = numpy.loadtxt('/m100/home/userexternal/ilarosa0/'+band+'gridsizes.txt').astype(int)
totpoints = numpy.sum(npoints)
timetot = time_per_iter*totpoints
print(totpoints,timetot)



print(timetot/(60*60), 8*2*timetot/(60*60))
#ndetectors = 2
#jobtime = 12 #ore per detector (2 detectors)
#totjobtimeH = jobtime*ndetectors
#jobtime = jobtime*3600

#njobs = numpy.ceil(time_per_file/jobtime).astype(int)

#pointsPerJob = numpy.ceil(npoints/njobs).astype(int)


#print(npoints, njobs, pointsPerJob)
