#Passing arguments to main function
import sys, getopt

def usage():
	msg = """\
	-h, --help       display this message
	-j, --job-id     job ID
	-n, --filename   name of the input filex
    -d, --detector   code of the detector (LH, LL, AV) """
	print(msg)

# Initialise command line argument variables
if sys.argv[1] == ("--help"):
  usage()
  sys.exit(1)

job_id = -1.0
filename = -1.0
detector = -1.0
path = "./"

job_id = int(sys.argv[2])
filename = sys.argv[4]
detector = sys.argv[6]
#path = sys.argv[4]

if job_id == -1.0:
  print(sys.stderr, "No job number specified.")
  print(sys.stderr, "Use --job-id to specify it.")
  usage()
  sys.exit(1)

if filename == -1.0:
  print(sys.stderr, "No file name specified.")
  print(sys.stderr, "Use --filename to specify it.")
  usage()
  sys.exit(1)
    
if detector == -1.0:
  print(sys.stderr, "No detector specified.")
  print(sys.stderr, "Use --detector to specify it.")
  usage()
  sys.exit(1)

#-----------
# MAIN CODE |
#-----------
import time
START = time.time()
import tensorflow as tf
import numpy
import scipy.io
from PAR import parDefs
from NoiseAdapt import compute_weights
from Inputs import loadData
from GridGen import make_skygrid
from DoppCorr import doppcorr
from RadPat import compute_radpat
from GPUHough import frequencyHough
from GPUCandSel import select_candidates
from Write_out import finalize

#----------------
# Job definition | 
# ---------------
#for the moment working on only one file
run = "O3"

#pathin = "/home/iuri.larosa/master/inputs/ifil/"
#pathout = "/home/iuri.larosa/master/outputs/"

pathin = "/m100/home/userexternal/ilarosa0/inputs/"
pathout = "/m100/home/userexternal/ilarosa0/outputs/"

dataPath = pathin + "in_" + run + detector + filename + ".mat"
sciencePath = pathin + run + "_" + detector + "_C01_segments_science_withCAT1veto.txt"

pointsPerJob = 3
points = numpy.arange(job_id*pointsPerJob, job_id*pointsPerJob+pointsPerJob)

#---------------------------
# Data loading and managing |
#---------------------------
startLOAD = time.time()
struct = scipy.io.loadmat(dataPath)['job_pack_0']

PAR = parDefs(struct)

# manage data
[firstFreq, peakmap, spindowns, velocities, indices] = loadData(struct, PAR)
endLOAD = time.time()

print("NUM SPINDOWN VALUES ", spindowns.size, spindowns[-2:])
startWEIGHTS = time.time()
# sky grid generation
[eclCoord, rectCoord, eqCoord] = make_skygrid(PAR)

#radiation pattern and weighted peakmap computation
scienceWeights = compute_weights(struct, PAR, sciencePath)

radiationPattern = compute_radpat(struct, PAR, eqCoord[i], filename, detector)
weights = radiationPattern*scienceWeights
weights = weights/numpy.mean(weights)
endWEIGHTS = time.time()
# OUTPUT CHECK WEIGHTS

#--------------------
# Doppler correction |
#--------------------
startDOPP = time.time()

pointECL = eclCoord[i]

# dopp corr
pointRECT = rectCoord[0]
[freqNew1, freqHM1] = doppcorr(pointRECT, peakmap[:,1], indices, velocities, PAR)
pointRECT = rectCoord[1]
[freqNew2, freqHM2] = doppcorr(pointRECT, peakmap[:,1], indices, velocities, PAR)
pointRECT = rectCoord[2]
[freqNew3, freqHM3] = doppcorr(pointRECT, peakmap[:,1], indices, velocities, PAR)
pointRECT = rectCoord[3]
[freqNew4, freqHM4] = doppcorr(pointRECT, peakmap[:,1], indices, velocities, PAR)
endDOPP = time.time()

freqNew = numpy.stack([freqNew1, freqNew2, freqNew3, freqNew4])
freqHM = numpy.stack([freqHM1, freqHM2, freqHM3, freqHM4])

# OUTPUT CHECK DOPPLER CORRECTION

#-----------
# Hough Map |
#-----------
# open a tf session
startGRAPH = time.time()
sess = tf.Session(config=tf.ConfigProto(log_device_placement=True, allow_soft_placement=True))

timesTF = tf.placeholder(tf.float64, name = 'inputT')
freqsTF = tf.placeholder(tf.float64, name = 'inputF')
weightsTF = tf.placeholder(tf.float64, name = 'inputW')
spindownsTF = tf.placeholder(tf.float64, name = 'inputSD')

for i, d in enumerate(['/gpu:0', '/gpu:1', '/gpu:2', '/gpu:3']):
    with tf.device(d):
        #FH con fasce laterali già tolte
        # same as cut_gd2 SNAG function
        freqStart = numpy.amin(freqNew[i]) - PAR['stepFreq']/2 - PAR['refinedStepFreq']
        indexInitial = ((PAR['freqInitial']-freqStart)/PAR['refinedStepFreq']+PAR['secbelt']/2).astype(numpy.int32)
        indexFinal = numpy.int(indexInitial + PAR["tFft"]/2*PAR['numCand'])
        print(indexInitial,indexFinal)
        #((PAR['freqFinalCorr']-firstFreq)/PAR['refinedStepFreq']+PAR['secbelt']/2+1).astype(numpy.int32)

        FHMap = frequencyHough(timesTF, freqsTF[i], weightsTF, spindownsTF, PAR)
        FHMapCand = FHMap[:,indexInitial:indexFinal]


        totWidth = tf.cast(tf.shape(FHMapCand)[1], dtype= tf.int32)
        totHeight = tf.cast(tf.shape(FHMapCand)[0], dtype = tf.int32)

        #---------------------
        # Candidate selection |
        #---------------------
        [stripeHeight, beltWidth, candidates] = select_candidates(FHMapCand, totWidth, totHeight, pointECL, PAR)

#----------------------------------------
# Feeding and running                   |
#----------------------------------------
# this is the dictionary with the data to be fed to the graph using placeholders
dataDict = {
                        timesTF : peakmap[:,0],
                        freqsTF : freqHM,
                        weightsTF : weights,
                        spindownsTF : spindowns
                        }

sess.run(tf.global_variables_initializer())
endGRAPH = time.time()

startHOUGHCAND = time.time()
houghcand = sess.run(FHMapCand, feed_dict = dataDict)
endHOUGHCAND = time.time()
print(houghcand.shape)

[PAR_stripeHeight, PAR_beltWidth, cands] = sess.run([stripeHeight, beltWidth, candidates], feed_dict = dataDict)
sess.close()


candidates = finalize(cands, PAR_stripeHeight, PAR_beltWidth, spindowns, PAR)
scipy.io.savemat(pathout + "in_" + run + detector + filename + "cands.mat", {"cands" : candidates})


times = numpy.array([endLOAD - startLOAD,
endWEIGHTS - startWEIGHTS,
endDOPP - startDOPP,
endGRAPH - startGRAPH,
endHOUGH - startHOUGH,
endHOUGHCAND - startHOUGHCAND,
endWRITE - startWRITE,
endWRITE - START
])
numpy.savetxt(pathout + "in_" + run + detector + filename + "benches.txt", times)


