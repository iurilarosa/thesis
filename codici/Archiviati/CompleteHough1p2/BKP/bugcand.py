#Passing arguments to main function
import sys, getopt

def usage():
	msg = """\
	-h, --help       display this message
	-j, --job-id     job ID
	-n, --filename   name of the input filex
    -d, --detector   code of the detector (LH, LL, AV) """
	print(msg)

# Initialise command line argument variables
if sys.argv[1] == ("--help"):
  usage()
  sys.exit(1)

job_id = -1.0
filename = -1.0
detector = -1.0
path = "./"

job_id = int(sys.argv[2])
filename = sys.argv[4]
detector = sys.argv[6]
#path = sys.argv[4]

if job_id == -1.0:
  print(sys.stderr, "No job number specified.")
  print(sys.stderr, "Use --job-id to specify it.")
  usage()
  sys.exit(1)

if filename == -1.0:
  print(sys.stderr, "No file name specified.")
  print(sys.stderr, "Use --filename to specify it.")
  usage()
  sys.exit(1)
    
if detector == -1.0:
  print(sys.stderr, "No detector specified.")
  print(sys.stderr, "Use --detector to specify it.")
  usage()
  sys.exit(1)

#-----------
# MAIN CODE |
#-----------
import time
START = time.time()
import tensorflow as tf
import numpy
import scipy.io
from PAR import parDefs
from NoiseAdapt import compute_weights
from Inputs import loadData
from GridGen import make_skygrid
from DoppCorr import doppcorr
from RadPat import compute_radpat
from GPUHough import frequencyHough
from Write_out import finalize
print(tf.__version__)
#----------------
# Job definition | 
# ---------------
#for the moment working on only one file
run = "O3"
band = "4096Hz"

#pathin = "/home/iuri.larosa/master/inputs/ifil/"
#pathout = "/home/iuri.larosa/master/outputs/"

pathin = "/m100_work/INF20_virgo_1/ilarosa0/data/" + band + "/" + detector + "/"
pathscience = "/m100/home/userexternal/ilarosa0/inputs/"
pathout = "/m100/home/userexternal/ilarosa0/outputs/"

dataPath = pathin + "in_" + run + detector + filename + ".mat"
sciencePath = pathscience + run + "_" + detector + "_C01_segments_science_withCAT1veto.txt"

pointsPerJob = 3
points = numpy.arange(job_id*pointsPerJob, job_id*pointsPerJob+pointsPerJob)
i=0

#---------------------------
# Data loading and managing |
#---------------------------
startLOAD = time.time()
struct = scipy.io.loadmat(dataPath)['job_pack_0']

PAR = parDefs(struct)

# manage data
[firstFreq, peakmap, spindowns, velocities, indices] = loadData(struct, PAR)
endLOAD = time.time()

print("NUM SPINDOWN VALUES ", spindowns.size, spindowns[-2:])
startWEIGHTS = time.time()
# sky grid generation
[eclCoord, rectCoord, eqCoord] = make_skygrid(PAR)
#scipy.io.savemat(pathout + "in_" + run + detector + filename + "grid.mat", {"eclgrid" : eclCoord, "rectgrid" : rectCoord})

#radiation pattern and weighted peakmap computation
scienceWeights = compute_weights(struct, PAR, sciencePath)
#scipy.io.savemat(pathout + "in_" + run + detector + filename + "weights.mat", {"weights" : scienceWeights})

print("PEEESIIII")
radiationPattern = compute_radpat(struct, PAR, eqCoord[i])
#scipy.io.savemat(pathout + "in_" + run + detector + filename + "radpat.mat", {"radpat" : radiationPattern})
weights = radiationPattern*scienceWeights
print("mul ", numpy.unique(weights), numpy.unique(weights).shape)
weights = weights/numpy.mean(weights)
endWEIGHTS = time.time()
# OUTPUT CHECK WEIGHTS
#scipy.io.savemat(pathout + "in_" + run + detector + filename + "wpeaks.mat", {"wpeaks" : weights})

#--------------------
# Doppler correction |
#--------------------
startDOPP = time.time()
pointECL = eclCoord[i]
pointRECT = rectCoord[i]

# dopp corr
[freqNew, freqHM] = doppcorr(pointRECT, peakmap[:,1], indices, velocities, PAR)
endDOPP = time.time()
# OUTPUT CHECK DOPPLER CORRECTION
#scipy.io.savemat(pathout + "in_" + run + detector + filename + "doppcorr.mat", {"freqCorr" : freqNew})

#-----------
# Hough Map |
#-----------
# open a tf session
startGRAPH = time.time()
sess = tf.Session()

timesTF = tf.placeholder(tf.float64, name = 'inputT')
freqsTF = tf.placeholder(tf.float64, name = 'inputF')
weightsTF = tf.placeholder(tf.float64, name = 'inputW')
spindownsTF = tf.placeholder(tf.float64, name = 'inputSD')

#FH con fasce laterali già tolte
# same as cut_gd2 SNAG function
freqStart = numpy.amin(freqNew) - PAR['stepFreq']/2 - PAR['refinedStepFreq']
indexInitial = ((PAR['freqInitial']-freqStart)/PAR['refinedStepFreq']+PAR['secbelt']/2).astype(numpy.int32)
indexFinal = numpy.int(indexInitial + PAR["tFft"]/2*PAR['numCand'])
print(indexInitial,indexFinal)
#((PAR['freqFinalCorr']-firstFreq)/PAR['refinedStepFreq']+PAR['secbelt']/2+1).astype(numpy.int32)

FHMap = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF, PAR)
FHMapCand = FHMap[:,indexInitial:indexFinal]


totWidth = tf.cast(tf.shape(FHMapCand)[1], dtype= tf.int32)
totHeight = tf.cast(tf.shape(FHMapCand)[0], dtype = tf.int32)

#---------------------
# Candidate selection |
#---------------------
# These two variables are only redifinition of old variable to clarify better the code
numCand = numpy.int32(PAR['numCand'])
numStripes = numpy.int32(PAR['numStripes'])
print(numStripes,numCand)
NPcoords = numpy.array([pointECL[0], pointECL[1], pointECL[2]/2, numpy.abs(pointECL[3]-pointECL[4])/4])
coords = tf.constant(numpy.zeros((numCand,4)) + NPcoords, dtype = tf.float32)

# Defining the horizontal-vertical portions of the Frequency Hough map
beltWidth = tf.floor(totWidth/numCand)
beltWidth = tf.cast(beltWidth, dtype = tf.int32)
belts = tf.range(0, totWidth, beltWidth)
#print(sess.run([__PAR__totWidth, __PAR__beltWidth], feed_dict = dataDict))

stripeHeight = tf.floor(totHeight/numStripes)
stripeHeight = tf.cast(stripeHeight, dtype = tf.int32)
sess = tf.Session()    
print(sess.run([stripeHeight, beltWidth]))
# Splitting the map in vertical belts
houghBelts = tf.zeros([numCand, totHeight,beltWidth])
houghBelts = tf.split(FHMapCand, num_or_size_splits=numCand, axis=1)
houghBelts = tf.cast(houghBelts, dtype = tf.int32)
# Computing the median of the amplitude values over the whole map
medianTOT = tf.contrib.distributions.percentile(FHMapCand, 50)

# Function which selects the candidates both in horizontal and in vertical portions of the map
def cands(i):
    # Spitting the ith belt in horizontal stripes
    houghSquares = houghBelts[:,i*stripeHeight:(i+1)*stripeHeight,:]

    # Finding the local maximum values in each belt of the ith stripe
    def find_maxs(tensor, numColumns):
        local_top = tf.reduce_max(tensor,axis = [1,2])

        x = tf.reshape(tensor, [tf.shape(tensor)[0], -1])
        x = tf.cast(x, tf.int32)
        indices = tf.argmax(x, axis=1)

        indices = tf.cast(indices, tf.int32)

        rows = tf.cast(indices / numColumns, tf.float32)
        cols = tf.cast(indices % numColumns, tf.float32)

        rows = tf.cast(rows, tf.int32)
        cols = tf.cast(cols, tf.int32)

        local_maxs = tf.transpose(tf.stack([local_top, rows, cols]))

        return local_maxs

    # Executing the function vectorially in each belt of the ith stripe
    primaryMaxs = find_maxs(houghSquares, beltWidth)
    #NB structure of the array: [max value, row => spindown, column => freq]

    # Finding the local median and dev std for the median, in each belt of the ith stripe
    localMedians = tf.contrib.distributions.percentile(houghSquares, 50, axis = [1,2])
    localSigmans = tf.contrib.distributions.percentile(tf.abs(houghSquares-tf.reshape(localMedians,[numCand,1,1])), 50, axis = [1,2])
    localSigmans = tf.cast(localSigmans, tf.float32)/0.6745

    # Data conversion for tensorflow 
    # TODO perhaps unnecessary TODO remove it to save memory usage
    primaryMaxs = tf.cast(primaryMaxs, tf.float32)      
    localMedians = tf.cast(localMedians, tf.float32)
    localSigmans = tf.cast(localSigmans, tf.float32)
    semimedianTOT = tf.cast(medianTOT/2, tf.float32)

    # Function that checks which of the local maxima is a candidate
    def find_cands(medians, sigmans, maxs, semimedian):
        # Critical Ratio CR_i = (amplitude_i - median)/sigman
        CR = tf.reshape((maxs[:,0]-medians)/sigmans, [numCand, 1])
        preliminaryCands = tf.concat([maxs, CR, coords], axis = 1)

        # Conditions to select the candidates
        primaryCond0 = (medians > 0)
        primaryCond1 = tf.logical_and(maxs[:,0] > medians, maxs[:,0] > semimedian)
        primaryCond = tf.equal(primaryCond0,primaryCond1)

        # Selection
        cands = tf.gather(preliminaryCands,tf.where(primaryCond))
        primaryCands = cands 
        # WARNING cands = tf.concat([primaryCands], axis = 1)

        return primaryCands
    return find_cands(localMedians, localSigmans, primaryMaxs, semimedianTOT)

# With tf.map it's possible to search the candidates over all the stripes in parallel
candidates = tf.map_fn(cands, tf.range(0, numStripes), dtype=tf.float32, parallel_iterations=1)



#----------------------------------------
# Feeding and running                   |
#----------------------------------------
# this is the dictionary with the data to be fed to the graph using placeholders
dataDict = {
                        timesTF : peakmap[:,0],
                        freqsTF : freqHM,
                        weightsTF : weights,
                        spindownsTF : spindowns
                        }

sess.run(tf.global_variables_initializer())
endGRAPH = time.time()

startHOUGH = time.time()
hough = sess.run(FHMap, feed_dict = dataDict)
endHOUGH = time.time()
# OUTPUT CHECK HOUGH
scipy.io.savemat(pathout + "in_" + run + detector + filename + "hough.mat", {"hough": hough} )
print(hough.shape)
startHOUGHCAND = time.time()
houghcand = sess.run(FHMapCand, feed_dict = dataDict)
endHOUGHCAND = time.time()
# OUTPUT CHECK CUT HOUGH
scipy.io.savemat(pathout + "in_" + run + detector + filename + "houghcand.mat", {"houghcand": houghcand} )
print(houghcand.shape)

[PAR_stripeHeight, PAR_beltWidth, cands] = sess.run([stripeHeight, beltWidth, candidates], feed_dict = dataDict)
sess.close()

startWRITE = time.time()

candidates = finalize(cands, PAR_stripeHeight, PAR_beltWidth, spindowns, PAR)
#scipy.io.savemat("/m100_work/INF20_virgo_1/ilarosa0/in_" + run + detector + filename + "cands.mat", {"cands" : candidates})
scipy.io.savemat(pathout + "in_" + run + detector + filename + "cands.mat", {"cands" : candidates})


endWRITE = time.time()

times = numpy.array([endLOAD - startLOAD,
endWEIGHTS - startWEIGHTS,
endDOPP - startDOPP,
endGRAPH - startGRAPH,
endHOUGH - startHOUGH,
endHOUGHCAND - startHOUGHCAND,
endWRITE - startWRITE,
endWRITE - START
])
#numpy.savetxt(pathout + "in_" + run + detector + filename + "benches.txt", times)


