import tensorflow as tf
import numpy
import time
import scipy.io
from inputs import loadData
from PAR import parDefs
from DoppCorr import doppcorr
from GPUHough import frequencyHough
from CandSel import select_candidates

# input file path
#dataPath ="/home/iuri.larosa/ifil/in_O2LL_01_0093_.mat"
#dataPath ="/home/iuri.larosa/ifil/in_O2LL_02_0327_.mat"
dataPath ="/home/iuri.larosa/ifil/in_O2LL_03_0744_.mat"
#dataPath ="/home/iuri.larosa/ifil/in_O2LL_04_1473_.mat"

#dataPath ="/home/iurilarosa/data/ifil/in_O2LL_01_0093_.mat"
#dataPath ="/home/iurilarosa/data/ifil/in_O2LL_02_0327_.mat"
#dataPath ="/home/iurilarosa/data/ifil/in_O2LL_03_0744_.mat"
#dataPath ="/home/iurilarosa/data/ifil/in_O2LL_04_1473_.mat"

#dataPath = "/home/protoss/Documenti/TESI/ifil/in_O2LL_01_0093_.mat"
#dataPath = "/home/protoss/Documenti/TESI/ifil/in_O2LL_02_0327_.mat"
#dataPath = "/home/protoss/Documenti/TESI/ifil/in_O2LL_03_0744_.mat"
#dataPath = "/home/protoss/Documenti/TESI/ifil/in_O2LL_04_1473_.mat"

# loading the input file (with the peakmap inside a matlab structure)
#---------------------------
# Data loading and managing |
#---------------------------
struct = scipy.io.loadmat(dataPath)['job_pack_0']

PAR = parDefs(struct)

# manage data
[firstFreq, nstepFreqs, peakmap, spindowns] = loadData(struct, PAR)

PAR.update({'nstepFreqs': nstepFreqs})

# dopp corr
#TODO newFirstFreq after correction

#-----------
# Hough Map |
#-----------
# open a tf session
sess = tf.Session()

#defining TensorFlow graph
timesTF = tf.placeholder(tf.float32, name = 'inputT')
freqsTF = tf.placeholder(tf.float32, name = 'inputF')
weightsTF = tf.placeholder(tf.float32, name = 'inputW')
spindownsTF = tf.placeholder(tf.float32, name = 'inputSD')

#dataDict = { 
#               timesTF : peakmap[:,0],
#               freqsTF : peakmap[:,1],
#               weightsTF : peakmap[:,2],
#               spindownsTF : spindowns
#             }

#FH con fasce laterali già tolte
# same as cut_gd2 SNAG function
indexInitial = ((PAR['freqInitial']-firstFreq)/PAR['refinedStepFreq']).astype(numpy.int32)
indexFinal = ((PAR['freqFinal']-firstFreq)/PAR['refinedStepFreq']+1).astype(numpy.int32)

FHMapCand = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF, PAR)[:,indexInitial:indexFinal-1]
#print(sess.run(tf.shape(FHMapCand), feed_dict = dataDict), " dim fhmap")
totWidth = tf.cast(tf.shape(FHMapCand)[1], dtype= tf.int32)
totHeight = tf.cast(tf.shape(FHMapCand)[0], dtype = tf.int32)
#print(sess.run([totHeight, totWidth], feed_dict = dataDict), " dim fhmap check")

#---------------------
# Candidate selection |
#---------------------
PAR_coord = numpy.array([1,2,0.1,0.1,0.15])
NPcoord = numpy.array([PAR_coord[0], PAR_coord[1], PAR_coord[2]/2, numpy.abs(PAR_coord[3]-PAR_coord[4])/4])
coords = tf.constant(numpy.zeros((PAR['numCand'],4)) + NPcoord, dtype = tf.float32)

[stripeHeight, beltWidth, candidates] = select_candidates(FHMapCand, totWidth, totHeight, coords, PAR)


#----------------------------------------
# feeding and running                   |
#----------------------------------------
# this is the dictionary with the data to be fed to the graph using placeholders
dataDict = { 
               timesTF : peakmap[:,0],
               freqsTF : peakmap[:,1],
               weightsTF : peakmap[:,2],
               spindownsTF : spindowns
             }


sess.run(tf.global_variables_initializer())

t2 = time.time()
[PAR_stripeHeight, PAR_beltWidth, cands] = sess.run([stripeHeight, beltWidth, candidates], feed_dict = dataDict)
sess.close()

cands = numpy.reshape(cands, [cands.shape[0],cands.shape[1], cands.shape[3]])
#cands = [amplitude, row = spindown, column = freq, CR, coord1, coord2, errcoord1, errcoord2]

for i in numpy.arange(PAR['numStripes']-1):
	cands[i,:,1] = cands[i,:,1]+(i*PAR_stripeHeight)

for i in numpy.arange(PAR['numCand']-1):
	cands[:,i,2] = cands[:,i,2]+(i*PAR_beltWidth)

cands = numpy.reshape(cands, [cands.shape[0]*cands.shape[1], cands.shape[2]])

stop = time.time()
print(stop-t2, cands.shape, "\n")


#image = sess.run(FHMap, feed_dict = dataDict)#, options=run_options,run_metadata = run_metadata)
#from matplotlib import pyplot
#a = pyplot.imshow(image, aspect=200)
##b = pyplot.scatter(x = cands[:,2], y = cands[:,1], s= 2 )
#pyplot.show()


