#Passing arguments to main function
import warnings
warnings.filterwarnings("ignore")
import sys, getopt
import os

def usage():
	msg = """\
	-h, --help       display this message
	-n, --filename   name of the input filex
	-d, --detector   code of the detector (LH, LL, AV)
	-p, --points     number of points per job"""
	print(msg)

# Initialise command line argument variables
if sys.argv[1] == ("--help"):
    usage()
    sys.exit(1)

job_id = 0
filename = -1.0
detector = -1.0
pointsPerJob = -1.0
path = "./"

filename = sys.argv[2]
detector = sys.argv[4]
pointsPerJob = sys.argv[6]

if filename == -1.0:
    print(sys.stderr, "No file name specified.")
    print(sys.stderr, "Use --filename to specify it.")
    usage()
    sys.exit(1)
    
if detector == -1.0:
    print(sys.stderr, "No detector specified.")
    print(sys.stderr, "Use --detector to specify it.")
    usage()
    sys.exit(1)
    
if pointsPerJob == -1.0:
    print(sys.stderr, "No points amount specified.")
    print(sys.stderr, "Use --detector to specify it.")
    usage()
    sys.exit(1)

#-----------
# MAIN CODE |
#-----------
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' # tf log errors only
import tensorflow as tf
import logging
logging.getLogger('tensorflow').setLevel(logging.ERROR)

import numpy
import scipy.io
from PAR import parDefs
from NoiseAdapt import compute_weights
from Inputs import loadData
from DoppCorr import doppcorr
from RadPat import compute_radpat
from GPUHough import frequencyHough
from GPUCandSel import select_candidates
from Write_out import finalize

import astropy
from astropy.coordinates import SkyCoord
from astropy.coordinates import BarycentricTrueEcliptic
print(tf.__version__)

#----------------
# Job definition | 
# ---------------
#for the moment working on only one file
run = "O2"
band = "2048Hz"

#pathin = "/home/iuri.larosa/master/inputs/ifil/"
#pathout = "/home/iuri.larosa/master/outputs/"

folderin = "/m100_work/INF20_virgo_1/ilarosa0/test8pulsars/" + detector + "/"
folderscience = "/m100/home/userexternal/ilarosa0/inputs/"


folderout = "/m100_work/INF20_virgo_1/ilarosa0/output/in_O2" + detector + filename + "/"
os.system('mkdir ' + folderout)
pathout = folderout + "out_" + str(job_id)

dataPath = folderin + "in_O2" + detector + filename + ".mat"
sciencePath = folderscience + "O2_" + detector + "_C01_segments_science_withCAT1veto.txt"

pointsPerJob = numpy.int32(pointsPerJob)
points = numpy.arange(job_id*pointsPerJob, job_id*pointsPerJob+pointsPerJob)
print("Computing input file" + folderin + "in_O2" + detector + filename + ".mat")
print("Processing points from " + str(points[0]) + " to " + str(points[-1]))

#---------------------------
# Data loading and managing |
#---------------------------
struct = scipy.io.loadmat(dataPath)['job_pack_0']
PAR = parDefs(struct)

# manage data
[firstFreq, peakmap, spindowns, velocities, indices] = loadData(struct, PAR)



# sky grid generation
def make_skygrid():
    coords = numpy.array([[ 4.22020613, -0.57246799],
 [ 0.78539816,  0.3843215 ],
 [ 3.59974158, -0.94841192],
 [ 4.62599518, -1.10741141],
 [ 0.78539816, -0.78522363],
 [ 1.27758101, -0.29600784],
 [ 2.35619449, -1.31493106],
 [ 3.87724893, -0.33981561],
 [ 5.39830338, -0.67509335],
 [ 0.35604717, -1.00181899],
 [ 4.02385659, -0.47612582],
 [ 6.22297145, -0.89465577],
 [ 4.46629756, -0.16737708],
 [ 2.98451302,  1.31493106],
 [ 1.27758101,  1.15959676]])

    coordToConv = SkyCoord(ra = coords[:,0] * astropy.units.rad , 
                           dec = coords[:,1] * astropy.units.rad,
                           unit = "rad", frame = 'icrs') 
    eqRa = numpy.array(coordToConv.ra)
    eqDec = numpy.array(coordToConv.dec)
    eqGrid = numpy.stack([eqRa, eqDec], axis = 1)
    eqRad = eqGrid[:,0:2]*numpy.pi/180

    eclCoord = coordToConv.transform_to('barycentrictrueecliptic')
    eclLat = numpy.array(eclCoord.lat)
    eclLon = numpy.array(eclCoord.lon)
    delta = numpy.ones(eclLat.size)
    eclGrid = numpy.stack([eclLat, eclLon, delta,delta,delta], axis = 1)

    rectGrid = numpy.zeros((eclGrid.shape[0], 3))
    rectGrid[:,0] = numpy.cos(eqRad[:,0])*numpy.cos(eqRad[:,1])
    rectGrid[:,1] = numpy.sin(eqRad[:,0])*numpy.cos(eqRad[:,1])
    rectGrid[:,2] = numpy.sin(eqRad[:,1])
    return eclGrid, rectGrid, eqGrid


[eclCoord, rectCoord, eqCoord] = make_skygrid()

#radiation pattern and weighted peakmap computation
#scienceWeights = compute_weights(struct, PAR, sciencePath)

final_candidates = numpy.zeros((PAR['numCand']*PAR['numStripes']*pointsPerJob, 8))

j = 0
for i in points:
	print(j/pointsPerJob)    
	if i > eclCoord.shape[0]:
		print("Input File " + filename + "  analyzed")
		sys.exit()
	
	weights = numpy.zeros(peakmap[:,1].size)+1.0

	#--------------------
	# Doppler correction |
	#--------------------
	pointECL = eclCoord[i]
	pointRECT = rectCoord[i]

	# dopp corr
	[freqNew, freqHM] = doppcorr(pointRECT, peakmap[:,1], indices, velocities, PAR)

	#-----------
	# Hough Map |
	#-----------
	# open a tf session
	sess = tf.Session()

	timesTF = tf.placeholder(tf.float64, name = 'inputT')
	freqsTF = tf.placeholder(tf.float64, name = 'inputF')
	weightsTF = tf.placeholder(tf.float64, name = 'inputW')
	spindownsTF = tf.placeholder(tf.float64, name = 'inputSD')

	#FH con fasce laterali già tolte
	# same as cut_gd2 SNAG function
	freqStart = numpy.amin(freqNew) - PAR['stepFreq']/2 - PAR['refinedStepFreq']
	indexInitial = ((PAR['freqInitial']-freqStart)/PAR['refinedStepFreq']+PAR['secbelt']/2).astype(numpy.int32)
	indexFinal = numpy.int(indexInitial + PAR["tFft"]/2*PAR['numCand'])

	FHMapCand = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF, PAR)[:,indexInitial:indexFinal]

	totWidth = tf.cast(tf.shape(FHMapCand)[1], dtype= tf.int32)
	totHeight = tf.cast(tf.shape(FHMapCand)[0], dtype = tf.int32)

	#---------------------
	# Candidate selection |
	#---------------------
#	[stripeHeight, beltWidth, candidates] = select_candidates(FHMapCand, totWidth, totHeight, pointECL, PAR)

	#----------------------------------------
	# Feeding and running                   |
	#----------------------------------------
	# this is the dictionary with the data to be fed to the graph using placeholders
	dataDict = { 
				timesTF : peakmap[:,0],
				freqsTF : freqHM,
				weightsTF : weights,
				spindownsTF : spindowns
				}

	sess.run(tf.global_variables_initializer())

	houghcand = sess.run(FHMapCand, feed_dict = dataDict)
	scipy.io.savemat(folderout + "in_O2" + detector + filename + "houghcand"+str(i)+".mat", {"houghcand": houghcand} )
	print(houghcand.shape)
#	[PAR_stripeHeight, PAR_beltWidth, cands] = sess.run([stripeHeight, beltWidth, candidates], feed_dict = dataDict)
	parperplot = numpy.array([PAR['freqInitial'],PAR['freqFinal'],PAR["refinedStepFreq"],PAR['fdotMin'],PAR['fdotMax'],PAR['stepFdot']])
	print("hough ", i, ":\n", parperplot)    
	sess.close()
	tf.reset_default_graph()

#	cands = finalize(cands, PAR_stripeHeight, PAR_beltWidth, spindowns, PAR)
#	top = PAR['numCand']*PAR['numStripes']*j
#	bottom = PAR['numCand']*PAR['numStripes']*(j+1)
#	j = j + 1
#	final_candidates[top:bottom,:] = cands
    
#scipy.io.savemat(pathout + ".mat", {"cands" : final_candidates})

