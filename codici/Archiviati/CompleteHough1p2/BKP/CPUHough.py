import tensorflow as tf
import numpy

def frequencyHough(times, frequencies, weights, spindowns, PAR):
	"""
	Computes the frequency-Hough transform of a sparse peakmap.
	Parameters:
	times, frequencies : 1D tensors
		The coordinates of the peakmap in sparse format.
	weights :  1D tensor
		The values of the peaks in the sparse peakmap.
	spindowns : 1D tensor
		The spindowns values over which calculate the Hough transform.
	PAR : dict
		All the parameters necessary for the computation of the Hough map
	Returns:
	houghMap : 2D tensor
		The Hough transform matrix
	"""
	# these variables are only redifinition of old variable to clarify better the following code
	nRows = numpy.int32(PAR['nstepFdot'])
	nColumns = numpy.int32(PAR['nstepFreqs'])
	enhance = numpy.int32(PAR['enhance'])

	#WARNING uncomment the following line for 64bit data
	PAR_secbelt = PAR['secbelt']
	#WARNING uncomment the following line for 32bit data
	#PAR_secbeltTF = tf.constant(PAR['secbelt'],dtype = tf.float32, name = 'secur')	

	# this function computes the Hough transform histogram for a given spindown
	def rowTransform(ithStep):
		sdTimed = spindowns[ithStep]*times
		transform = (numpy.round(frequencies-sdTimed+PAR_secbelt/2)).astype(int)
#		print(transform.shape, numpy.where(transform<0), transform[:10])        
#		values = numpy.zeros(nColumns)
		values = numpy.bincount(transform,weights)
		zeriDopo = numpy.zeros([nColumns - values.size])
		row = numpy.concatenate([values,zeriDopo],0)
		return row
#		return values

	# definition of the TensorFlow variable and values assignment with tf.map_fn
	houghLeft = numpy.zeros([nRows, nColumns])
	for i in numpy.arange(nRows):
		houghLeft[i] = rowTransform(i)

	# let's superimpose the right edge on the image
	semiLarghezza = numpy.round(enhance/2+0.001).astype(int)
	houghLeft[:,semiLarghezza*2:nColumns]=houghLeft[:,semiLarghezza*2:nColumns]-houghLeft[:,0:nColumns - semiLarghezza*2]
	houghMap = numpy.cumsum(houghLeft, axis = 1)

	return houghMap
