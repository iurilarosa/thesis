#Passing arguments to main function
import sys, getopt

def usage():
	msg = """\
	-h, --help               display this message
	-j, --job-id             job ID
	-d, --dir		 directory path of the input file
	-n, --filename		 name of the input file"""
	print(msg)

# Initialise command line argument variables
if sys.argv[1] == ("--help"):
  usage()
  sys.exit(1)

job_id = -1.0
filename = -1.0
path = "./"

job_id = int(sys.argv[2])
filename = sys.argv[4]
#path = sys.argv[4]

if job_id == -1.0:
  print(sys.stderr, "No job number specified.")
  print(sys.stderr, "Use --job-id to specify it.")
  usage()
  sys.exit(1)

if filename == -1.0:
  print(sys.stderr, "No file name specified.")
  print(sys.stderr, "Use --filename to specify it.")
  usage()
  sys.exit(1)
    
#-----------
# MAIN CODE |
#-----------
import tensorflow as tf
import numpy
import time
import scipy.io
from inputs import loadData
from PAR import parDefs
from GridGen import make_skygrid
from DoppCorr import doppcorr
from GPUHough import frequencyHough
from CandSel import select_candidates

#----------------
# Job definition | 
# ---------------
#for the moment working on only one file
pathin = "/home/iuri.larosa/master/ifil/"
pathout = "/home/iuri.larosa/master/out/" 
dataPath = pathin + filename + ".mat"
outPath = pathout + filename + "Cands" + str(job_id)  + ".mat"

pointsPerJob = 3
points = numpy.arange(job_id*pointsPerJob, job_id*pointsPerJob+pointsPerJob)
i=0
#---------------------------
# Data loading and managing |
#---------------------------
startLOADMANAGE = time.time()
struct = scipy.io.loadmat(dataPath)['job_pack_0']

PAR = parDefs(struct)

# manage data
[firstFreq, peakmap, spindowns, velocities, indices] = loadData(struct, PAR)
endLOADMANAGE = time.time()

print(peakmap.shape)

# sky grid generation
startGRID = time.time()
[eclCoord, rectCoord] = make_skygrid(PAR)
endGRID = time.time()

for i in points:
	#--------------------
	# Doppler correction |
	#--------------------
	startDOPP = time.time()
	pointECL = eclCoord[i]
	pointRECT = rectCoord[i]

	# dopp corr
	[freqNew, freqHM] = doppcorr(pointRECT, peakmap[:,1], indices, velocities, PAR)
	endDOPP = time.time()
	print("doppcorr ",endDOPP-startDOPP)
	#-----------
	# Hough Map |
	#-----------
	startTFSETUP = time.time()
	# open a tf session
	sess = tf.Session()

	timesTF = tf.placeholder(tf.float64, name = 'inputT')
	freqsTF = tf.placeholder(tf.float64, name = 'inputF')
	weightsTF = tf.placeholder(tf.float64, name = 'inputW')
	spindownsTF = tf.placeholder(tf.float64, name = 'inputSD')

	#FH con fasce laterali già tolte
	# same as cut_gd2 SNAG function
	freqStart = numpy.amin(freqNew) - PAR['stepFreq']/2 - PAR['refinedStepFreq']
	indexInitial = ((PAR['freqInitial']-freqStart)/PAR['refinedStepFreq']+PAR['secbelt']/2).astype(numpy.int32)
	indexFinal = numpy.int(indexInitial + PAR["tFft"]/2*PAR['numCand'])
	#((PAR['freqFinalCorr']-firstFreq)/PAR['refinedStepFreq']+PAR['secbelt']/2+1).astype(numpy.int32)
		
	FHMapCand = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF, PAR)[:,indexInitial:indexFinal]

	totWidth = tf.cast(tf.shape(FHMapCand)[1], dtype= tf.int32)
	totHeight = tf.cast(tf.shape(FHMapCand)[0], dtype = tf.int32)

	#---------------------
	# Candidate selection |
	#---------------------
	[stripeHeight, beltWidth, candidates] = select_candidates(FHMapCand, totWidth, totHeight, pointECL, PAR)

	#----------------------------------------
	# Feeding and running                   |
	#----------------------------------------
	# this is the dictionary with the data to be fed to the graph using placeholders
	weights = numpy.zeros(peakmap[:,0].size) + 0.55 #numpy.random.uniform(0,1,peakmap[:,0].size)
	dataDict = { 
				timesTF : peakmap[:,0],
				freqsTF : freqHM,
				weightsTF : weights,
				spindownsTF : spindowns
				}

	endTFSETUP = time.time()
	print(i, filename[i], "\n\n", "tfsetupnoinit ",endTFSETUP-startTFSETUP)
	sess.run(tf.global_variables_initializer())
	endTFSETUP = time.time()
	print("tfsetup ",endTFSETUP-startTFSETUP)
#	print(sess.run(tf.contrib.memory_stats.MaxBytesInUse()),sess.run(tf.contrib.memory_stats.BytesInUse()))	
	startTFRUN = time.time()	
	[PAR_stripeHeight, PAR_beltWidth, cands] = sess.run([stripeHeight, beltWidth, candidates], feed_dict = dataDict)
	sess.close()
	tf.reset_default_graph()

	endTFRUN = time.time()
	print("tfrun(hough+cand) ",endTFRUN-startTFRUN)	

	startWRITE = time.time()
	cands = numpy.reshape(cands, [cands.shape[0],cands.shape[1], cands.shape[3]])
	# NB cands = [amplitude, row = spindown, column = freq, CR, coord1, coord2, errcoord1, errcoord2]

	for i in numpy.arange(PAR['numStripes']):
		# recovering spindown value
		cands[i,:,1] = cands[i,:,1]+(i*PAR_stripeHeight)
		row = cands[i,:,1].astype(numpy.int32)
		cands[i,:,1] = spindowns[row]

	for i in numpy.arange(PAR['numCand']):
		cands[:,i,2] = cands[:,i,2]+(i*PAR_beltWidth)
		column = cands[:,i,2].astype(numpy.int32)
		cands[:,i,2] = 1024 + column*PAR["refinedStepFreq"]

#	print(cands.shape)
	cands = numpy.reshape(cands, [cands.shape[0]*cands.shape[1], cands.shape[2]])
#	scipy.io.savemat("../check/2048_2p0Cands.mat", {"cands" : cands})
	endWRITE = time.time()
	print("finalise+write ",endWRITE-startWRITE , "\n")



#--------------------
# Printing benchmark |
#--------------------
print(dataPath)
print("load+manage ", endLOADMANAGE-startLOADMANAGE)
print("grid ",endGRID-startGRID)
print("doppcorr ",endDOPP-startDOPP)
print("tfsetup ",endTFSETUP-startTFSETUP)
print("tfrun(hough+cand) ",endTFRUN-startTFRUN)
print("finalise+write ",endWRITE-startWRITE)


