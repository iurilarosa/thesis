import numpy
import math
import astropy
from astropy.coordinates import SkyCoord
from astropy.coordinates import BarycentricTrueEcliptic

def make_skygrid(PAR):
	"""
	Computes the sky grid for the input file analyzed.
	Parameters:
	PAR : dict
		All the parameters necessary for the computation of the Hough map
	Returns:
	eclGrid, eqGrid, rectGrid : 5D tensor
		Arrays of the positions and sizes of the elements of the sky grid, in ecliptic (for candidate listing), equatorial (for radiation pattern) and rectangular (for Doppler correction) coordinates. Dimensions are: lon, lat, deltalon, semideltalat up, semideltalat down
	"""	
	
	
	freqMax = PAR['freqFinal']#749
	tFft = PAR['tFft']#2048
	pi = math.pi	
	# Porting of the SNAG function pss_optmatp with some vectorization
	def ecl_skygrid():
		dopplerNum = 0.000106*tFft*freqMax

		indBet = 1 + 0.01/dopplerNum
		betErr = 1
		bet = -0.999

		i = 0
		while betErr > -bet:
			i = i + 1
			beterr = -bet
			bet = pi/2

			mid = 1
			indBet = indBet - 0.01/dopplerNum
			while bet > 0:
				mid = mid+1
				bet = bet - indBet/(dopplerNum*math.sin(bet))

		mid = mid - 1
		nLats = mid*2-1

		eclLat = numpy.zeros(nLats);
		eclLat[0] = pi/2
		for j in numpy.arange(1,mid-1):
			eclLat[j] = eclLat[j-1] - indBet/(dopplerNum*math.sin(eclLat[j-1]))
			
		secondHalf = -eclLat[:mid-1]
		secondHalf = secondHalf[::-1]
		eclLat[mid:] = secondHalf

		numLon = numpy.ceil( 2*pi * dopplerNum * numpy.cos(eclLat) )
		deltaLon = 360.0 / numLon
		nPoints = numpy.sum(numLon).astype(numpy.int32)

		eclCoord = numpy.zeros((nPoints,5))

		js = numpy.arange(1,nLats-1)
		fase = (js+1 - 2*numpy.floor((js+1)/2))*deltaLon[1:-1]/2

		start = 0
		for j in numpy.arange(1,nLats-1):
			start = numpy.int(start + numLon[j-1])
			end = numpy.int(start + numLon[j])
			k = numpy.arange(0,numLon[j])
			
			eclCoord[start:end,0] = fase[j-1] + k*deltaLon[j]
			eclCoord[start:end,1] = eclLat[j]*180/pi
			eclCoord[start:end,2] = deltaLon[j]
			eclCoord[start:end,3] = eclLat[j-1]*180/pi
			eclCoord[start:end,4] = eclLat[j+1]*180/pi


		eclCoord[0,0] = 180
		eclCoord[0,1] = 90
		eclCoord[0,2] = 360
		eclCoord[0,3] = 90
		eclCoord[0,4] = eclCoord[1,1]

		eclCoord[-1,0] = 180
		eclCoord[-1,1] = -90
		eclCoord[-1,2] = 360
		eclCoord[-1,3] = eclCoord[-2,1]
		eclCoord[-1,4] = -90
		
		return eclCoord
	
	eclGrid = ecl_skygrid()
	
	# Conversion from ecliptic to equatorial
	# NB there is a slight difference (~1%) between the equatorial coordinates system used in the SNAG function and the icrs system used in astropy.coordinates
	coordToConv = SkyCoord(lon = eclGrid[:,0]*astropy.units.degree, lat = eclGrid[:,1]*astropy.units.degree, frame = 'barycentrictrueecliptic') 
	eqCoord = coordToConv.transform_to('icrs')
	eqRa = numpy.array(eqCoord.ra)
	eqDec = numpy.array(eqCoord.dec)
	eqGrid = numpy.stack([eqRa, eqDec], axis = 1)
	eqRad = eqGrid[:,0:2]*pi/180

	# Porting of the SNAG function astro2rect
	rectGrid = numpy.zeros((eclGrid.shape[0], 3))
	rectGrid[:,0] = numpy.cos(eqRad[:,0])*numpy.cos(eqRad[:,1])
	rectGrid[:,1] = numpy.sin(eqRad[:,0])*numpy.cos(eqRad[:,1])
	rectGrid[:,2] = numpy.sin(eqRad[:,1])
	
	return eclGrid, rectGrid, eqGrid



