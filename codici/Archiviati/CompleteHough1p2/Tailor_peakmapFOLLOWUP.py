import numpy

def join_peakmaps(peakmap, weights, dataPath_2, point):
    struct_2 = scipy.io.loadmat(dataPath_2)['job_pack_0']
    PAR_2 = parDefs(struct_2)
    # manage data
    [firstFreq_2, peakmap_2, spindowns_2, velocities_2, indices_2] = loadData(struct, PAR)
    
    scienceWeights_2 = compute_weights(struct_2, PAR_2, sciencePath)
    
    radiationPattern = compute_radpat(struct_2, PAR_2, eqCoord[i])
    weights = radiationPattern*scienceWeights
    weights_2 = weights/numpy.mean(weights)
    [freqNew_2, freqHM_2] = doppcorr(pointRECT, peakmap[:,1], indices, velocities, PAR)

    
    
    

def cut_peakmap(peakmap, weights, freqOutlier, offset):
    frequencies = peakmap[:,1]
    top_cond = numpy.where(frequencies < freqOutlier + offset)
    sel_frequencies = frequencies[top_cond]
    bottom_cond = numpy.where(sel_frequencies > freqOutlier-offset)
    fin_frequencies = sel_frequencies[bottom_cond]
    
    times = peakmap[:,0]
    sel_times = times[top_cond]
    fin_times = sel_times[bottom_cond]
    
    sel_weights = weights[top_cond]
    fin_weights = weights[bottom_cond]
    
    return fin_frequencies, fin_times, fin_weights
    
    
