import numpy
import tensorflow as tf

def select_candidates(FHMap, totWidth, totHeight, pointCoords, PAR):
	"""
	Selects the candidates in the Frequency Hough map
	Parameters:
	FHMap : 2D tensor
		The Frequency Hough matrix.
	totWidth, totHeight :  int
		The shape of the Frequency Hough matrix
	coords : 1D array
		The coordinates of the sky point
	PAR : dict
		All the parameters necessary for the computation of the Frequency Hough map
	Returns:
	stripeHeight, beltWidth : int 
		The size of the horizontal-vertical portions of the Frequency Hough map
	candidates : 8D array
		The array of the candidates with values: i(spindown), j(frequency), amplitude, CR, lon, lat, deltalon, semideltalat up, semideltalat down
	"""
	
	# These two variables are only redifinition of old variable to clarify better the code
	numCand = numpy.int32(PAR['numCand'])
	numStripes = numpy.int32(PAR['numStripes'])
	
	NPcoords = numpy.array([pointCoords[0], pointCoords[1], pointCoords[2]/2, numpy.abs(pointCoords[3]-pointCoords[4])/4])
	coords = tf.constant(numpy.zeros((numCand,4)) + NPcoords, dtype = tf.float32)
	
	# Defining the horizontal-vertical portions of the Frequency Hough map
	beltWidth = tf.floor(totWidth/numCand)
	beltWidth = tf.cast(beltWidth, dtype = tf.int32)
	belts = tf.range(0, totWidth, beltWidth)
	#print(sess.run([__PAR__totWidth, __PAR__beltWidth], feed_dict = dataDict))

	stripeHeight = tf.floor(totHeight/numStripes)
	stripeHeight = tf.cast(stripeHeight, dtype = tf.int32)
	# Splitting the map in vertical belts
	houghBelts = tf.zeros([numCand, totHeight,beltWidth])
	houghBelts = tf.split(FHMap, num_or_size_splits=numCand, axis=1)
	houghBelts = tf.cast(houghBelts, dtype = tf.int32)
	# Computing the median of the amplitude values over the whole map
	medianTOT = tf.contrib.distributions.percentile(FHMap, 50)

	# Function which selects the candidates both in horizontal and in vertical portions of the map
	def cands(i):
		# Spitting the ith belt in horizontal stripes
		houghSquares = houghBelts[:,i*stripeHeight:(i+1)*stripeHeight,:]
		
		# Finding the local maximum values in each belt of the ith stripe
		def find_maxs(tensor, numColumns):
			local_top = tf.reduce_max(tensor,axis = [1,2])
			
			x = tf.reshape(tensor, [tf.shape(tensor)[0], -1])
			x = tf.cast(x, tf.int32)
			indices = tf.argmax(x, axis=1)
			
			indices = tf.cast(indices, tf.int32)
			
			rows = tf.cast(indices / numColumns, tf.float32)
			cols = tf.cast(indices % numColumns, tf.float32)
			
			rows = tf.cast(rows, tf.int32)
			cols = tf.cast(cols, tf.int32)
			
			local_maxs = tf.transpose(tf.stack([local_top, rows, cols]))
			
			return local_maxs

		# Executing the function vectorially in each belt of the ith stripe
		primaryMaxs = find_maxs(houghSquares, beltWidth)
		#NB structure of the array: [max value, row => spindown, column => freq]

		# Finding the local median and dev std for the median, in each belt of the ith stripe
		localMedians = tf.contrib.distributions.percentile(houghSquares, 50, axis = [1,2])
		localSigmans = tf.contrib.distributions.percentile(tf.abs(houghSquares-tf.reshape(localMedians,[numCand,1,1])), 50, axis = [1,2])
		localSigmans = tf.cast(localSigmans, tf.float32)/0.6745

		# Data conversion for tensorflow 
		# TODO perhaps unnecessary TODO remove it to save memory usage
		primaryMaxs = tf.cast(primaryMaxs, tf.float32)      
		localMedians = tf.cast(localMedians, tf.float32)
		localSigmans = tf.cast(localSigmans, tf.float32)
		semimedianTOT = tf.cast(medianTOT/2, tf.float32)

		# Function that checks which of the local maxima is a candidate
		def find_cands(medians, sigmans, maxs, semimedian):
			# Critical Ratio CR_i = (amplitude_i - median)/sigman
			CR = tf.reshape((maxs[:,0]-medians)/sigmans, [numCand, 1])
			preliminaryCands = tf.concat([maxs, CR, coords], axis = 1)
			
			# Conditions to select the candidates
			primaryCond0 = (medians > 0)
			primaryCond1 = tf.logical_and(maxs[:,0] > medians, maxs[:,0] > semimedian)
			primaryCond = tf.equal(primaryCond0,primaryCond1)
			
			# Selection
			cands = tf.gather(preliminaryCands,tf.where(primaryCond))
			shapesel = tf.shape(cands)[0]
			void = tf.cast((shapesel < 100), tf.bool)
			primaryCands = tf.cond(void, lambda: tf.concat([cands,tf.zeros([numCand-shapesel,1,8])],axis = 0), lambda: (tf.identity(cands)))
			#primaryCands = tf.gather(preliminaryCands,tf.where(primaryCond))
			
			return primaryCands
		return find_cands(localMedians, localSigmans, primaryMaxs, semimedianTOT)
	
	# With tf.map it's possible to search the candidates over all the stripes in parallel
	candidates = tf.map_fn(cands, tf.range(0, numStripes), dtype=tf.float32, parallel_iterations=1)
	return [stripeHeight, beltWidth, candidates]
