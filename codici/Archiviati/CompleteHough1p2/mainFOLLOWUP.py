#Passing arguments to main function
import warnings
warnings.filterwarnings("ignore")
import sys, getopt
import os

def usage():
	msg = """\
	-h, --help       display this message
	-j, --job-id     job number
	-d, --detector   code of the detector (LH, LL, AV)"""
	print(msg)

# Initialise command line argument variables
if sys.argv[1] == ("--help"):
    usage()
    sys.exit(1)

job_id = 0
detector = -1.0
path = "./"

job_id = int(sys.argv[2])
detector = sys.argv[6]

if detector == -1.0:
    print(sys.stderr, "No detector specified.")
    print(sys.stderr, "Use --detector to specify it.")
    usage()
    sys.exit(1)

#-----------
# MAIN CODE |
#-----------
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' # tf log errors only
import tensorflow as tf
import logging
from tensorflow.python.client import timeline
#logging.getLogger('tensorflow').setLevel(logging.ERROR)

import numpy
import scipy.io
from PAR import parDefs
from GridFolloUp import load_skygrid
from NoiseAdapt import compute_weights
from Inputs import loadData
from DoppCorr import doppcorr
from RadPat import compute_radpat
#from GPUHough import frequencyHough
#from GPUCandSel import select_candidates
from Write_out import finalize
from tailor_peakmap import cutpeak

import astropy
from astropy.coordinates import SkyCoord
from astropy.coordinates import BarycentricTrueEcliptic
print(tf.__version__)

#----------------
# Job definition | 
# ---------------
#for the moment working on only one file
run = "O3"
followUp_offset = 0.5 #Hz
#pathin = "/home/iuri.larosa/master/inputs/ifil/"
#pathout = "/home/iuri.larosa/master/outputs/"

folderin = "/m100_work/INF21_virgo_0/ilarosa0/data/outliers/" + detector + "/"
folderscience = "/m100/home/userexternal/ilarosa0/inputs/"

outliers = numpy.genfromtxt('outliers_example.csv', delimiter=',')
freqOutlier = outliers[job_id,0]
grid = scipy.io.loadmat("gridtest"+str(job_id+1) + ".mat")["skymap"]

filelist = glob.glob(folderin)
fileslist = numpy.sort(fileslist)

freqFiles = numpy.zeros(fileslist.size).astype(int)
for i in numpy.arange(fileslist.size):
    freqFile[i] = numpy.int(fileslist[i,45+11:45+4+11])
    
id_file = numpy.where(freqFile < freqOutlier)[0][-1]
dataPath = fileslist[id_file]

if(freqOutlier + 1 > freqFile + 5):
    dataPath_2 = fileslist[id_file + 1]
if(freqOutlier -1 < freqFile):
    dataPath_2 = fileslits[id_file - 1]

folderout = "/m100_work/INF21_virgo_0/ilarosa0/output/outliers/"
os.system('mkdir ' + folderout)
pathout = folderout + "outcands_" + str(job_id)


sciencePath = folderscience + run + "_" + detector + "_C01_segments_science_withCAT1veto.txt"

#---------------------------
# Data loading and managing |
#---------------------------
struct = scipy.io.loadmat(dataPath)['job_pack_0']
PAR = parDefs(struct)

# manage data
[firstFreq, peakmap, spindowns, velocities, indices] = loadData(struct, PAR)

# sky grid generated with matlab
eclGrid = grid

# Conversion from ecliptic to equatorial
# NB there is a slight difference (~1%) between the equatorial coordinates system used in the SNAG function and the icrs system used in astropy.coordinates
coordToConv = SkyCoord(lon = eclGrid[:,0]*astropy.units.degree, lat = eclGrid[:,1]*astropy.units.degree, frame = 'barycentrictrueecliptic') 
eqCoord = coordToConv.transform_to('icrs')
eqRa = numpy.array(eqCoord.ra)
eqDec = numpy.array(eqCoord.dec)
eqGrid = numpy.stack([eqRa, eqDec], axis = 1)
eqRad = eqGrid[:,0:2]*pi/180

# Porting of the SNAG function astro2rect
rectGrid = numpy.zeros((eclGrid.shape[0], 3))
rectGrid[:,0] = numpy.cos(eqRad[:,0])*numpy.cos(eqRad[:,1])
rectGrid[:,1] = numpy.sin(eqRad[:,0])*numpy.cos(eqRad[:,1])
rectGrid[:,2] = numpy.sin(eqRad[:,1])

numPoints = grid.shape[0]
print(numPoints)
#--------------------------------------------------
#radiation pattern and weighted peakmap computation|
#--------------------------------------------------
scienceWeights = compute_weights(struct, PAR, sciencePath)


final_candidates = numpy.zeros((PAR['numCand']*PAR['numStripes']*numPoints, 8))

#main loop starts: for each sky point the weighted map is computed, frequencies are Doppler corrected,
#                  FH map is computed and candidates are selected and appended to the final output
j = 0
for i in points:
	print(j/pointsPerJob)    
	if i > eclCoord.shape[0]:
		print("Input File " + filename + "  analyzed")
		sys.exit()
	
	radiationPattern = compute_radpat(struct, PAR, eqCoord[i])
	weights = radiationPattern*scienceWeights
	weights = weights/numpy.mean(weights)
    
	#--------------------
	# Doppler correction |
	#--------------------
	pointECL = eclCoord[i]
	pointRECT = rectCoord[i]

	# dopp corr
	[freqNew, freqHM] = doppcorr(pointRECT, peakmap[:,1], indices, velocities, PAR)

	#-----------
	# Hough Map |
	#-----------
	# open a tf session
	#sess = tf.Session()

	#timesTF = tf.placeholder(tf.float64, name = 'inputT')
	#freqsTF = tf.placeholder(tf.float64, name = 'inputF')
	#weightsTF = tf.placeholder(tf.float64, name = 'inputW')
	#spindownsTF = tf.placeholder(tf.float64, name = 'inputSD')

	#FH lateral stripes already removed
	# same as cut_gd2 SNAG function
	#freqStart = numpy.amin(freqNew) - PAR['stepFreq']/2 - PAR['refinedStepFreq']
	#indexInitial = ((PAR['freqInitial']-freqStart)/PAR['refinedStepFreq']+PAR['secbelt']/2).astype(numpy.int32)
	#indexFinal = numpy.int(indexInitial + PAR["tFft"]/2*PAR['numCand'])

	#FHMapCand = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF, PAR)[:,indexInitial:indexFinal]

	#totWidth = tf.cast(tf.shape(FHMapCand)[1], dtype= tf.int32)
	#totHeight = tf.cast(tf.shape(FHMapCand)[0], dtype = tf.int32)

	#---------------------
	# Candidate selection |
	#---------------------
	#[stripeHeight, beltWidth, candidates] = select_candidates(FHMapCand, totWidth, totHeight, pointECL, PAR)

	#-----------------------------------------------
	# Feeding and running the TF graph defined above |
	#-----------------------------------------------
	# this is the dictionary with the data to be fed to the graph using placeholders
	#dataDict = { 
	#			timesTF : peakmap[:,0],
	#			freqsTF : freqHM,
	#			weightsTF : weights,
	#			spindownsTF : spindowns
	#			}

	#sess.run(tf.global_variables_initializer())

	#[PAR_stripeHeight, PAR_beltWidth, cands] = sess.run([stripeHeight, beltWidth, candidates], feed_dict = dataDict)
	#sess.close()
	#tf.reset_default_graph()
	
	#the candidates array coming from the TensorFlow code are rearranged and appended to a unique array    
	#cands = finalize(cands, PAR_stripeHeight, PAR_beltWidth, spindowns, PAR)
	#top = PAR['numCand']*PAR['numStripes']*j
	#bottom = PAR['numCand']*PAR['numStripes']*(j+1)
	#j = j + 1
	#final_candidates[top:bottom,:] = cands
    
#the candidates array of the submitted job is saved
#scipy.io.savemat(pathout + ".mat", {"cands" : final_candidates})


