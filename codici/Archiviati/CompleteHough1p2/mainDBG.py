#Passing arguments to main function
import warnings
warnings.filterwarnings("ignore")
import sys, getopt
import os

def usage():
	msg = """\
	-h, --help       display this message
	-j, --job-id     job ID
	-n, --filename   name of the input filex
	-d, --detector   code of the detector (LH, LL, AV)
	-p, --points     number of points per job"""
	print(msg)

# Initialise command line argument variables
if sys.argv[1] == ("--help"):
    usage()
    sys.exit(1)

job_id = -1.0
filename = -1.0
detector = -1.0
pointsPerJob = -1.0
path = "./"

job_id = int(sys.argv[2])
filename = sys.argv[4]
detector = sys.argv[6]
pointsPerJob = sys.argv[8]

if job_id == -1.0:
    print(sys.stderr, "No job number specified.")
    print(sys.stderr, "Use --job-id to specify it.")
    usage()
    sys.exit(1)

if filename == -1.0:
    print(sys.stderr, "No file name specified.")
    print(sys.stderr, "Use --filename to specify it.")
    usage()
    sys.exit(1)
    
if detector == -1.0:
    print(sys.stderr, "No detector specified.")
    print(sys.stderr, "Use --detector to specify it.")
    usage()
    sys.exit(1)
    
if pointsPerJob == -1.0:
    print(sys.stderr, "No detector specified.")
    print(sys.stderr, "Use --detector to specify it.")
    usage()
    sys.exit(1)

#-----------
# MAIN CODE |
#-----------
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' # tf log errors only
import tensorflow as tf
import logging
logging.getLogger('tensorflow').setLevel(logging.ERROR)
import time
import numpy
import scipy.io
from PAR import parDefs
from NoiseAdapt import compute_weights
from Inputs import loadData
from GridGen import make_skygrid
from DoppCorr import doppcorr
from RadPat import compute_radpat
from GPUHough import frequencyHough
from GPUCandSel import select_candidates
from Write_out import finalize
from Outliers import load_skygrid

print(tf.__version__)

#----------------
# Job definition | 
# ---------------
#for the moment working on only one file
run = "O3"
#band = "2048Hz"
interval = 0.1
freqOutlier = 126.875#663.96875 
#pathin = "/home/iuri.larosa/master/inputs/ifil/"
#pathout = "/home/iuri.larosa/master/outputs/"

folderin = "/m100_work/INF23_virgo_0/ilarosa0/data/outliers/" + run + "/" + detector[1] + "/"
folderscience = "/m100/home/userexternal/ilarosa0/inputs/"
gridfolder = "/m100/home/userexternal/ilarosa0/phdproject/folloup/datas/"

folderout = "/m100_work/INF23_virgo_0/ilarosa0/output/outliers/joinpeaks/" + detector[1] + "/"
os.system('mkdir ' + folderout)
pathout = folderout + "out_" + str(job_id) + ".mat"

dataPath = folderin + "in_" + run + detector + filename + ".mat"
sciencePath = folderscience + run + "_" + detector + "_C01_segments_science_withCAT1veto.txt"

pointsPerJob = numpy.int32(pointsPerJob)
#points = numpy.arange(job_id*pointsPerJob, job_id*pointsPerJob+pointsPerJob)
points= numpy.arange(2)
print("Computing input file" + folderin + "in_" + run + detector + filename + ".mat")
print("Processing points from " + str(points[0]) + " to " + str(points[-1]))

#---------------------------
# Data loading and managing |
#---------------------------
struct = scipy.io.loadmat(dataPath)['job_pack_0']
PAR = parDefs(struct)
PAR.update({'freqInitial': freqOutlier-interval})
PAR.update({'freqFinal': freqOutlier+interval})
PAR.update({'numCand' : 10})
# manage data
[firstFreq, peakmap, spindowns, velocities, indices] = loadData(struct, PAR)

# sky grid generation
#[eclCoord, rectCoord, eqCoord] = make_skygrid(PAR)
[eclCoord, rectCoord, eqCoord] = load_skygrid(job_id, gridfolder + "gridtest")
#--------------------------------------------------
#radiation pattern and weighted peakmap computation|
#--------------------------------------------------
#scienceWeights = compute_weights(struct, PAR, sciencePath)

final_candidates = numpy.zeros((PAR['numCand']*PAR['numStripes']*pointsPerJob, 8))

#main loop starts: for each sky point the weighted map is computed, frequencies are Doppler corrected,
#                  FH map is computed and candidates are selected and appended to the final output
j = 0
start = time.time()
for i in points:
	print(j/pointsPerJob) 
	if i > eclCoord.shape[0]:
		print("Input File " + filename + "  analyzed")
		sys.exit()
	
#	radiationPattern = compute_radpat(struct, PAR, eqCoord[i])
#	weights = radiationPattern*scienceWeights
#	weights = weights/numpy.mean(weights)

	#--------------------
	# Doppler correction |
	#--------------------
	pointECL = eclCoord[i]
	pointRECT = rectCoord[i]
	print("\n coordinates", pointECL, "\n")
	# dopp corr
	[freqNew, freqHM] = doppcorr(pointRECT, peakmap[:,1], indices, velocities, PAR)
	#folder = "/m100_work/INF23_virgo_0/ilarosa0/output/outliers/joinpeaks/"
	#peakpath = folder+detector[1]+"/tot_peak_"+str(job_id)+".mat"    
	#peaks = scipy.io.loadmat(peakpath)["tot_peak"]
	frequencies = peakmap[:,1]
	times = peakmap[:,0]
	weights = numpy.ones(peakmap[:,1].size)
    

	# Computation of the minimum frequency of the Hough map
	freqMin = numpy.amin(frequencies)
	freqStart = freqMin- PAR['stepFreq']/2 - PAR['refinedStepFreq']
	freqMax = numpy.amax(frequencies)
	freqEnd = freqMax + PAR['stepFreq']/2 + PAR['refinedStepFreq']
	PAR.update({'freqInitialCorr': freqStart})
	PAR.update({'freqFinalCorr': freqEnd})

	nstepFreqs = numpy.ceil((freqEnd-freqStart)/PAR['refinedStepFreq'])+PAR['secbelt']
	PAR.update({'nstepFreqs': nstepFreqs})

	#Effective start and end of Hough freq dimension (removing security sidebands)
	indexInitial = ((PAR['freqInitial']-freqStart)/PAR['refinedStepFreq']+PAR['secbelt']/2).astype(numpy.int32)
	indexFinal = ((PAR['freqFinal']-freqStart)/PAR['refinedStepFreq']+PAR['secbelt']/2).astype(numpy.int32)
	print("\n\n")
	print("CHECK CUTS")
	print(PAR['freqInitial'],PAR['freqFinal'])
	print(PAR["freqInitialCorr"], PAR["freqFinalCorr"])
	print(indexInitial, indexFinal)

	# "Normalization" of the frequency array which will be passed to the FrequencyHough function
	freqHM = frequencies-freqStart
	freqHM = (freqHM/PAR['refinedStepFreq'])-numpy.round(PAR['enhance']/2+0.001)

	#-----------
	# Hough Map |
	#-----------
	# open a tf session
	sess = tf.Session()

	timesTF = tf.placeholder(tf.float64, name = 'inputT')
	freqsTF = tf.placeholder(tf.float64, name = 'inputF')
	weightsTF = tf.placeholder(tf.float64, name = 'inputW')
	spindownsTF = tf.placeholder(tf.float64, name = 'inputSD')

	FHMapCand = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF, PAR)[:,indexInitial:indexFinal]
	FHMap = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF, PAR)

	totWidth = tf.cast(tf.shape(FHMapCand)[1], dtype= tf.int32)
	totHeight = tf.cast(tf.shape(FHMapCand)[0], dtype = tf.int32)

	#---------------------
	# Candidate selection |
	#---------------------
	#[stripeHeight, beltWidth, candidates] = select_candidates(FHMapCand, totWidth, totHeight, pointECL, PAR)

	#-----------------------------------------------
	# Feeding and running the TF graph defined above |
	#-----------------------------------------------
	# this is the dictionary with the data to be fed to the graph using placeholders
#	dataDict = { 
#				timesTF : peakmap[:,0],
#				freqsTF : freqHM,
#				weightsTF : weights,
#				spindownsTF : spindowns
#				}
	dataDict = { 
				timesTF : times,
				freqsTF : freqHM,
				weightsTF : weights,
				spindownsTF : spindowns
				}
	peakmap_final = numpy.stack([times,freqHM,weights],axis = 1)
	scipy.io.savemat("/m100_work/INF23_virgo_0/ilarosa0/output/test/tot_peakORIG_" + str(job_id) + ".mat", {"tot_peak" : peakmap_final})
	
	sess.run(tf.global_variables_initializer())
	imagecut = sess.run(FHMapCand, feed_dict = dataDict)
#scipy.io.savemat( "/m100_work/INF23_virgo_0/ilarosa0/output/outliers/joinpeaks/L/hmapcut_11.mat" ,{"hmap" : imagecut})
	scipy.io.savemat( "/m100_work/INF23_virgo_0/ilarosa0/output/test/hmapcutFULLORIG_" + str(job_id) + ".mat" ,{"hmap" : imagecut})

	image = sess.run(FHMap, feed_dict = dataDict)
	print(image.shape)
#scipy.io.savemat( "/m100_work/INF23_virgo_0/ilarosa0/output/outliers/joinpeaks/L/hmap_11.mat" ,{"hmap" : image})
	scipy.io.savemat( "/m100_work/INF23_virgo_0/ilarosa0/output/test/hmapFULLORIG_" + str(job_id) + ".mat" ,{"hmap" : image})

    
	#[PAR_stripeHeight, PAR_beltWidth, cands] = sess.run([stripeHeight, beltWidth, candidates], feed_dict = dataDict)
	sess.close()
	tf.reset_default_graph()

	#the candidates array coming from the TensorFlow code are rearranged and appended to a unique array    
	#cands = finalize(cands, PAR_stripeHeight, PAR_beltWidth, spindowns, PAR)
	#top = PAR['numCand']*PAR['numStripes']*j
	#bottom = PAR['numCand']*PAR['numStripes']*(j+1)
	#j = j + 1
	#final_candidates[top:bottom,:] = cands
print(time.time()-start)

#the candidates array of the submitted job is saved
scipy.io.savemat(pathout + ".mat", {"cands" : final_candidates})

