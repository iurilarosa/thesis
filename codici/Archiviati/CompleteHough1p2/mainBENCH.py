#Passing arguments to main function
import warnings
warnings.filterwarnings("ignore")
import sys, getopt
import os

job_id = -1.0
filename = -1.0
detector = -1.0
pointsPerJob = -1.0
path = "./"

job_id = int(sys.argv[2])
filename = sys.argv[4]
detector = sys.argv[6]
pointsPerJob = int(sys.argv[8])

#-----------
# MAIN CODE |
#-----------
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' # tf log errors only
import tensorflow as tf
import logging
logging.getLogger('tensorflow').setLevel(logging.ERROR)
import time
import numpy
import scipy.io
from PAR import parDefs
from NoiseAdapt import compute_weights
from Inputs import loadData
from GridGen import make_skygrid
from DoppCorr import doppcorr
from RadPat import compute_radpat
from GPUHough import frequencyHough
from GPUCandSel import select_candidates
from Write_out import finalize

print(tf.__version__)

#----------------
# Job definition | 
# ---------------
#for the moment working on only one file
run = "O3"
band = "2048Hz"

#pathin = "/home/iuri.larosa/master/inputs/ifil/"
#pathout = "/home/iuri.larosa/master/outputs/"

folderin = "/m100_work/INF22_virgo_0/ilarosa0/data/bench/O3/"
folderscience = "/m100/home/userexternal/ilarosa0/inputs/"


folderout = "/m100_work/INF22_virgo_0/ilarosa0/output/bench/in_" + run + detector + filename + "/"
os.system('mkdir ' + folderout)
pathout = folderout + "out_" + str(job_id)

dataPath = folderin + "in_" + run + detector + filename + ".mat"
sciencePath = folderscience + run + "_" + detector + "_C01_segments_science_withCAT1veto.txt"

points = numpy.arange(job_id*pointsPerJob, job_id*pointsPerJob+pointsPerJob)
print("Computing input file" + folderin + "in_" + run + detector + filename + ".mat")
print("Processing points from " + str(points[0]) + " to " + str(points[-1]))

#---------------------------
# Data loading and managing |
#---------------------------
struct = scipy.io.loadmat(dataPath)['job_pack_0']
PAR = parDefs(struct)

# manage data
[firstFreq, peakmap, spindowns, velocities, indices] = loadData(struct, PAR)

# sky grid generation
[eclCoord, rectCoord, eqCoord] = make_skygrid(PAR)

#--------------------------------------------------
#radiation pattern and weighted peakmap computation|
#--------------------------------------------------
scienceWeights = compute_weights(struct, PAR, sciencePath)

final_candidates = numpy.zeros((PAR['numCand']*PAR['numStripes']*pointsPerJob, 8))

#main loop starts: for each sky point the weighted map is computed, frequencies are Doppler corrected,
#                  FH map is computed and candidates are selected and appended to the final output
j = 0
startLOOP = time.time()
for i in points:
	startITER = time.time()    
	print(j/pointsPerJob)    
	if i > eclCoord.shape[0]:
		print("Input File " + filename + "  analyzed")
		sys.exit()
	
	radiationPattern = compute_radpat(struct, PAR, eqCoord[i])
	weights = radiationPattern*scienceWeights
	weights = weights/numpy.mean(weights)

	#--------------------
	# Doppler correction |
	#--------------------
	pointECL = eclCoord[i]
	pointRECT = rectCoord[i]

	# dopp corr
	[freqNew, freqHM] = doppcorr(pointRECT, peakmap[:,1], indices, velocities, PAR)

	#-----------
	# Hough Map |
	#-----------
	# open a tf session
	sess = tf.Session()

	timesTF = tf.placeholder(tf.float64, name = 'inputT')
	freqsTF = tf.placeholder(tf.float64, name = 'inputF')
	weightsTF = tf.placeholder(tf.float64, name = 'inputW')
	spindownsTF = tf.placeholder(tf.float64, name = 'inputSD')

	#FH lateral stripes already removed
	# same as cut_gd2 SNAG function
	freqStart = numpy.amin(freqNew) - PAR['stepFreq']/2 - PAR['refinedStepFreq']
	indexInitial = ((PAR['freqInitial']-freqStart)/PAR['refinedStepFreq']+PAR['secbelt']/2).astype(numpy.int32)
	indexFinal = numpy.int(indexInitial + PAR["tFft"]/2*PAR['numCand'])

	FHMapCand = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF, PAR)[:,indexInitial:indexFinal]

	totWidth = tf.cast(tf.shape(FHMapCand)[1], dtype= tf.int32)
	totHeight = tf.cast(tf.shape(FHMapCand)[0], dtype = tf.int32)

	#---------------------
	# Candidate selection |
	#---------------------
	[stripeHeight, beltWidth, candidates] = select_candidates(FHMapCand, totWidth, totHeight, pointECL, PAR)
	print("Box size", stripeHeight, beltWidth)
	#-----------------------------------------------
	# Feeding and running the TF graph defined above |
	#-----------------------------------------------
	# this is the dictionary with the data to be fed to the graph using placeholders
	dataDict = { 
				timesTF : peakmap[:,0],
				freqsTF : freqHM,
				weightsTF : weights,
				spindownsTF : spindowns
				}

	sess.run(tf.global_variables_initializer())

	[PAR_stripeHeight, PAR_beltWidth, cands] = sess.run([stripeHeight, beltWidth, candidates], feed_dict = dataDict)
	sess.close()
	tf.reset_default_graph()
	
	#the candidates array coming from the TensorFlow code are rearranged and appended to a unique array    
	cands = finalize(cands, PAR_stripeHeight, PAR_beltWidth, spindowns, PAR)
	top = PAR['numCand']*PAR['numStripes']*j
	bottom = PAR['numCand']*PAR['numStripes']*(j+1)
	j = j + 1
	final_candidates[top:bottom,:] = cands
	endITER = time.time()
endLOOP = time.time()

print(endITER-startITER)
print(endLOOP-startLOOP)

#the candidates array of the submitted job is saved
scipy.io.savemat(pathout + ".mat", {"cands" : final_candidates})

