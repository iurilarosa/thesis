import numpy
import scipy.io

def finalize(cands, PAR_stripeHeight, PAR_beltWidth, spindowns, PAR):
	"""
	Computes the spindown and frequency values given the coordinates in the FrequencyHough map.
	Parameters:
	cands : 4D array
		array with the candidates extracted from the FH map
	PAR_stripeHeight : int
		The size in bins of the spindown portions analyzed by the candidate search function
	PAR_beltWidth : int
		The size in bins of the frequency portions analyzed by the candidate search function
	spindowns : 1D array
		The array of the spindown values
	PAR : dict
		All the parameters necessary for the computation of the Hough map.        
Returns:
	cands : 2D array
		the final candidates array
	"""	    
	cands = numpy.reshape(cands, [cands.shape[0],cands.shape[1], cands.shape[3]]).astype(numpy.float64)
	# NOTE cands = [amplitude, row = spindown, column = freq, CR, coord1, coord2, errcoord1, errcoord2]
    
	# recovering spindown value
	for i in numpy.arange(PAR['numStripes']):
			row = (cands[i,:,1]+(i*PAR_stripeHeight)).astype(numpy.int64)
			cands[i,:,1] = spindowns[row] #PAR["fdotMin"] + row * PAR["stepFdot"]

	# recovering fr value
	for i in numpy.arange(PAR['numCand']):
			column = (cands[:,i,2]+(i*PAR_beltWidth)+1).astype(numpy.int64)
			cands[:,i,2] = PAR['freqInitial'] + column*PAR["refinedStepFreq"]

	cands = numpy.reshape(cands, [cands.shape[0]*cands.shape[1], cands.shape[2]])
	return cands