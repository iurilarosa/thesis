#Passing arguments to main function
import warnings
warnings.filterwarnings("ignore")
import sys, getopt
import os

def usage():
	msg = """\
	-h, --help       display this message
	-j, --job-id     job ID
	-n, --filename   name of the input filex
	-d, --detector   code of the detector (LH, LL, AV)
	-p, --points     number of points per job"""
	print(msg)

# Initialise command line argument variables
if sys.argv[1] == ("--help"):
    usage()
    sys.exit(1)

job_id = -1.0
filename = -1.0
detector = -1.0
pointsPerJob = -1.0
path = "./"

job_id = int(sys.argv[2])
filename = sys.argv[4]
detector = sys.argv[6]

if job_id == -1.0:
    print(sys.stderr, "No job number specified.")
    print(sys.stderr, "Use --job-id to specify it.")
    usage()
    sys.exit(1)

if filename == -1.0:
    print(sys.stderr, "No file name specified.")
    print(sys.stderr, "Use --filename to specify it.")
    usage()
    sys.exit(1)
    
if detector == -1.0:
    print(sys.stderr, "No detector specified.")
    print(sys.stderr, "Use --detector to specify it.")
    usage()
    sys.exit(1)
    
#-----------
# MAIN CODE |
#-----------
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' # tf log errors only
import tensorflow as tf
#import logging
#logging.getLogger('tensorflow').setLevel(logging.ERROR)
import time
import numpy
import scipy.io
from PAR import parDefs
from NoiseAdapt import compute_weights
from Inputs import loadData
from GridGen import make_skygrid
from DoppCorr import doppcorr
from RadPat import compute_radpat
from GPUHough import frequencyHough
from GPUCandSel import select_candidates
from Write_out import finalize

print(tf.__version__)

#----------------
# Job definition | 
# ---------------
#for the moment working on only one file
run = "O3"
band = "8192Hz"

#pathin = "/home/iuri.larosa/master/inputs/ifil/"
#pathout = "/home/iuri.larosa/master/outputs/"

folderin = "/m100_work/INF23_virgo_0/ilarosa0/data/outliers/" + run + "/" + detector[1] + "/"
dataPath = folderin + "in_" + run + detector + filename + ".mat"

#---------------------------
# Data loading and managing |
#---------------------------
struct = scipy.io.loadmat(dataPath)['job_pack_0']
PAR = parDefs(struct)

# manage data
[firstFreq, peakmap, spindowns, velocities, indices] = loadData(struct, PAR)

freqCorr = peakmap[:,1]
# Computation of the minimum frequency of the Hough map
freqIn = numpy.amin(freqCorr)- PAR['stepFreq']/2 - PAR['refinedStepFreq']
freqFin = numpy.amax(freqCorr) + PAR['stepFreq']/2 + PAR['refinedStepFreq']
PAR.update({'freqInitialCorr': freqIn})
PAR.update({'freqFinalCorr': freqFin})
    
nstepFreqs = numpy.ceil((freqFin-freqIn)/PAR['refinedStepFreq'])+PAR['secbelt']
PAR.update({'nstepFreqs': nstepFreqs})
    
# Computation of the corrected frequency array which will be passed to the FrequencyHough function
freqHM = freqCorr-freqIn
freqHM = (freqHM/PAR['refinedStepFreq'])-numpy.round(PAR['enhance']/2+0.001)

#-----------
# Hough Map |
#-----------
# open a tf session
sess = tf.Session()

timesTF = tf.placeholder(tf.float64, name = 'inputT')
freqsTF = tf.placeholder(tf.float64, name = 'inputF')
weightsTF = tf.placeholder(tf.float64, name = 'inputW')
spindownsTF = tf.placeholder(tf.float64, name = 'inputSD')

#FH lateral stripes already removed
# same as cut_gd2 SNAG function
freqNew = peakmap[:,1]
freqStart = numpy.amin(freqNew) - PAR['stepFreq']/2 - PAR['refinedStepFreq']
indexInitial = ((PAR['freqInitial']-freqStart)/PAR['refinedStepFreq']+PAR['secbelt']/2).astype(numpy.int32)
indexFinal = numpy.int(indexInitial + PAR["tFft"]/2*PAR['numCand'])

FHMapCand = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF, PAR)[:,indexInitial:indexFinal]
FHMap = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF, PAR)
totWidth = tf.cast(tf.shape(FHMapCand)[1], dtype= tf.int32)
totHeight = tf.cast(tf.shape(FHMapCand)[0], dtype = tf.int32)

#-----------------------------------------------
# Feeding and running the TF graph defined above |
#-----------------------------------------------
# this is the dictionary with the data to be fed to the graph using placeholders
dataDict = { 
			timesTF : peakmap[:,0],
			freqsTF : freqHM,
			weightsTF : numpy.ones(freqHM.size),
			spindownsTF : spindowns
			}

sess.run(tf.global_variables_initializer())
imagecut = sess.run(FHMapCand, feed_dict = dataDict)
scipy.io.savemat( "/m100_work/INF23_virgo_0/ilarosa0/output/test/hmapcutFULLORIG_" + str(job_id) + ".mat" ,{"hmap" : imagecut})

image = sess.run(FHMap, feed_dict = dataDict)
print(image.shape)
scipy.io.savemat( "/m100_work/INF23_virgo_0/ilarosa0/output/test/hmapFULLORIG_" + str(job_id) + ".mat" ,{"hmap" : image})
sess.close()
tf.reset_default_graph()