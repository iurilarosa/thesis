peaksize = 20
offset = peaksize*10

matrix = randi([0,1],peaksize,peaksize);
[t,f] = find(sparse(matrix));
w = rand(size(t));

save('peakmap.mat','t','f','w')

nbin_d=10
d = 1:1:nbin_d
size(d)
nRows = nbin_d;
nColumns = peaksize+offset;

%%% HERE STARTS THE NEW CODE
frequenciesHM = f; %(peaks(2)-inifr)/ddf-deltaf2;
weights = w; %peaks(5);
timesHM = t; %peaks(1)*Day_inSeconds/ddf;
spindowns = d;

houghMap = zeros(nRows,nColumns);
for i = 1:nbin_d
    houghMap(i,:) = compute_row(i, timesHM, frequenciesHM, weights, spindowns,offset,nColumns);
end

% VECCHIO LOOP
n_of_peaks=length(f);
ii=1:1:n_of_peaks;
nTimeSteps=length(ii); % number of times of the peakmap
I500 = offset;
inifr = 0
ddf = 1;
ii0=1;
enh = 0;
Day_inSeconds = 1;
deltaf2=round(enh/2+0.001);

binh_df0=zeros(nRows,nColumns);  %  HM matrix container
for it = 1:nTimeSteps
    kf=(frequenciesHM(ii0:ii(it))-inifr)/ddf;  % normalized frequencies
    w=weights(ii0:ii(it));               % wiener weights
    t=timesHM(ii0)*Day_inSeconds;; % time conversion days to s
    tddf=t/ddf;
    f0_a=kf-deltaf2; 

    for id = 1:nbin_d   % loop for the creation of half-differential map
        td=d(id)*tddf;

        a=1+round(f0_a-td+I500); 
        binh_df0(id,a)=binh_df0(id,a)+w; % left edge of the strips
    end
    ii0=ii(it)+1;
end

% FINE VECCHIO LOOP

diff =(binh_df0 - houghMap);
diff(2,:)
length(nonzeros(diff))
%diff(nRows,:)

save('oldmap.mat','binh_df0')
save('newmap.mat','houghMap')


function [row] = compute_row(index,t,f,w,d,offset,nCol)
    index
    freqShifts = d(index)*t;
    %WARNING ho tolto il "1+" perché nel codice originale la variabile a (riga 45 qui)
    % è un indice che punta a un elemento della matrice di hough (bin_df0, riga 46)
    % e in Matlab gli array partono da 1 invece che da 0 (causando che devi aggiungere gli "1+" in casi del genere)
    % in questo caso, io *non* sto più puntando a un singolo elemento ma faccio tutta la riga assieme
    % quindi aggiungere 1 non serve più e il codice è equivalente tra matlab e python
    transform = round(f-freqShifts+offset);
    %!WARNING! values = numpy.bincount(positions,weights,minlength = nColumns)
    % possible working function using efficiently accumarray: [histw, histv] = histwv(transform, weights, 1,nbin_f0,nbin_f0)
    % documentation: https://www.mathworks.com/matlabcentral/fileexchange/58450-histwv-v-w-min-max-bins
    % pasted below
    [histw, histv] = histwv(transform, w, 1,max(transform),nCol);
    row = histw;
end




%%% HERE ENDS THE NEW CODE

function [histw, histv] = histwv(v, w, min, max, bins)
    %Inputs: 
    %vv - values
    %ww - weights
    %minV - minimum value
    %maxV - max value
    %bins - number of bins (inclusive)
    
    %Outputs:
    %histw - wieghted histogram
    %histv (optional) - histogram of values    
   
    delta = (max-min)/(bins-1);
    subs = round((v-min)/delta)+1;
    
    histw = accumarray(subs(:),w(:),[bins,1]);
    if nargout == 2
        histv = accumarray(subs(:),1,[bins,1]);
    end
        
end

