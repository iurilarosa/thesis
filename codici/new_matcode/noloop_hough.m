load /home/iuri.larosa/in_O3LH_03_0754_.mat
df = job_pack_0.basic_info.dfr;
tFft = 1/df;

pin = job_pack_0.peaks;
pout=pin;

index=job_pack_0.basic_info.index;
Nt= job_pack_0.basic_info.ntim;

peaks = job_pack_0.peaks;
[n1,n2]=size(peaks);

I1000=4000;  % security belt (even)
I500=I1000/2;
Day_inSeconds=86400;

tObs = tFft*Nt/(2*60*60*24*30) # months
#print(tObs, " mesi")
tObs = round(tObs*30*24*60*60); #seconds

peaks(5,:)=peaks(5,:)./peaks(4,:);
peaks(5,:)=peaks(5,:)/mean(peaks(5,:));
%end

epoch=basic_info.epoch; % Hough epoch (time barycenter) in mjd
peaks(1,:)=peaks(1,:)-epoch;

minf0=min(peaks(2,:));
maxf0=max(peaks(2,:));
df=1/tFft;   % raw frequency resolution
enh=10;  % frequency enhancement factor (typically 10)
df2=df/2;
ddf=df/enh;  % refined frequency step
inifr=minf0-df2-ddf;
finfr=maxf0+df2+ddf;
nbin_f0=ceil((finfr-inifr)/ddf)+I1000;
deltaf2=round(enh/2+0.001); % semi-width of the strip (in ddf)

n_of_peaks=length(peaks);
ii=find(diff(peaks(1,:)));  % find the different times
ii=[ii n_of_peaks]; 
nTimeSteps=length(ii); % number of times of the peakmap

dmin1=-1e-8;
dmax =1e-9;% spin-down initial value
deltad=df/tObs;  % spin-down step
nbin_d=ceil((dmax-dmin1)/deltad)  % spin-down number of steps

d=dmin1+(0:nbin_d-1)*deltad;  
ii0=1; %nbin_d,nbin_f0
binh_df0=zeros(nbin_d,nbin_f0);  %  HM matrix container

%%% HERE STARTS THE NEW CODE
frequenciesHM = (peaks(2)-inifr)/ddf-deltaf2;
weights = peaks(5);
timesHM = peaks(1)*Day_inSeconds/ddf;
spindowns = d;

function [row] = compute_row(index)
    freqShifts = spindowns(i)*timesHM;
    transform = 1+round(frequenciesHM-freqShifts+I500);
    #!WARNING! values = numpy.bincount(positions,weights,minlength = nColumns)
    # possible working function using efficiently accumarray: [histw, histv] = histwv(transform, weights, 1,nbin_f0,nbin_f0)
    # documentation: https://www.mathworks.com/matlabcentral/fileexchange/58450-histwv-v-w-min-max-bins
    [histw, histv] = histwv(transform, weights, 1,nbin_f0,nbin_f0)

houghMap = zeros(nbin_d,nbin_f0)
for i = 1:nbin_d
    houghMap(i) = compute_row(i);
end

bin_df0 = houghMap;

%%% HERE ENDS THE NEW CODE

binh_df0(:,deltaf2*2+1:nbin_f0)=...
    binh_df0(:,deltaf2*2+1:nbin_f0)-binh_df0(:,1:nbin_f0-deltaf2*2); % half to full diff. map - Carl Sabottke idea
binh_df0=cumsum(binh_df0,2);   % creation of the Hough map
    
hfdf=gd2(binh_df0.');
hfdf=edit_gd2(hfdf,'dx',ddf,'ini',inifr-I500*ddf,'dx2',deltad,'ini2',dmin1,'capt','Histogram of spin-f0');
