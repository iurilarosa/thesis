import tensorflow as tf
import numpy
import time

#test comparativo TensorFlow-Numpy
# genera due matrici con valori random, ne fa il prodotto matriciale
for rc in numpy.arange(4000,10000,500):
	print(rc)

	#test Numpy
	#start = time.time()
	#a = numpy.random.rand(rc,rc)
	#b = numpy.random.rand(rc,rc)
	#c = numpy.dot(a*b)
	#stop = time.time()
	
	#x = numpy.array([rc,stop-start]).reshape((1,2))
	#f_handle = open('timestatsNP.txt', 'a')
	#numpy.savetxt(f_handle, x,delimiter=',')
	#f_handle.close()
	
	
	#test TensorFlow
	sess = tf.Session()
	atf = tf.random_uniform(shape=(rc,rc))
	btf = tf.random_uniform(shape=(rc,rc))
	ctf = tf.matmul(atf, btf)

	start = time.time()
	c = sess.run(ctf)
	stop = time.time()
	sess.close()
	
	x = numpy.array([rc,stop-start]).reshape((1,2))
	f_handle = open('timestatsTF.txt', 'a')
	numpy.savetxt(f_handle, x,delimiter=',')
	f_handle.close()

