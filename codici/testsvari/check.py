import tensorflow as tf
import numpy

rc = 1000

sess = tf.Session()

for deviceName in ['/cpu:0','/device:GPU:0', '/device:GPU:1']:
        with tf.device(deviceName):
                matrices = tf.random_uniform([rc,rc,4],minval = 0, maxval = 1, dtype = tf.float32)

                def mult(i):
                        product = tf.matmul(matrices[:,:,i],matrices[:,:,i+1])
                        return product

                mul = tf.zeros([rc,rc,3], dtype = tf.float32)
mul = tf.map_fn(mult, numpy.array([0,1,2]), dtype = tf.float32, parallel_iterations = 10)

m = sess.run(mul)
