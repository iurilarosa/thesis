import tensorflow as tf
import numpy
import time

for esponenti in numpy.arange(1,14):

		rc = numpy.power(2, esponenti)
		print(rc)
		sess = tf.Session()
		atf = tf.random_uniform(shape=(rc,rc))
		btf = tf.random_uniform(shape=(rc,rc))
		ctf = tf.matmul(atf, btf)

		start = time.time()
		c = sess.run(ctf)
		stop = time.time()

		sess.close()

		x = numpy.array([rc,stop-start]).reshape((1,2))
		f_handle = open('timestats.txt', 'a')
		numpy.savetxt(f_handle, x,delimiter=',')
		f_handle.close()


for rc in numpy.arange(8000,11500,500):
		
		
		#rc = numpy.power(2, esponenti)
		print(rc)
		start = time.time()
		a = numpy.random.rand(rc,rc)
		b = numpy.random.rand(rc,rc)

		sess = tf.Session()
		atf = tf.random_uniform(shape=(rc,rc))
		btf = tf.random_uniform(shape=(rc,rc))
		ctf = tf.matmul(atf, btf)

		start = time.time()
		c = sess.run(ctf)
		stop = time.time()

		x = numpy.array([rc,stop-start]).reshape((1,2))
		f_handle = open('timestats.txt', 'a')
		numpy.savetxt(f_handle, x,delimiter=',')
		f_handle.close()

