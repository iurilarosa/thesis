import tensorflow as tf
import numpy
import time

rc = 20000

sess = tf.Session()


#for d in ['/device:GPU:0', '/device:GPU:1']:
	#with tf.device(d):
a = tf.random_uniform([rc,rc],minval = 0, maxval = 1, dtype = tf.float32)
b = tf.random_uniform([rc,rc],minval = 0, maxval = 1, dtype = tf.float32)

c = tf.matmul(a,b)


start = time.time()
m = sess.run(c)
stop = time.time()

print(stop-start)


