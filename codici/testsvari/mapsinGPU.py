import tensorflow as tf
import numpy
import time

rc = 5000

sess = tf.Session()


#for d in ['/device:GPU:0', '/device:GPU:1']:
#        with tf.device(d):
a = tf.random_uniform([rc,rc,4],minval = 0, maxval = 1, dtype = tf.float32)

def mult(i):
	c = tf.matmul(a[:,:,i],a[:,:,i+1])
	return c

mul = tf.zeros([rc,rc,3], dtype = tf.float32)
mul = tf.map_fn(mult, numpy.array([0,1,2]), dtype = tf.float32, parallel_iterations = 10)

start = time.time()
m = sess.run(mul)
stop = time.time()

#print(m.shape,m)
print(stop-start)
