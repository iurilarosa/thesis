import tensorflow as tf
import numpy
import time

rc = 5000

sess = tf.Session()

a = tf.random_uniform([rc,rc],minval = 0, maxval = 1, dtype = tf.float32)
b = tf.random_uniform([rc,rc],minval = 0, maxval = 1, dtype = tf.float32)

c = tf.matmul(a,b)

start = time.time()
m = sess.run(c)
stop = time.time()

print(stop-start)
