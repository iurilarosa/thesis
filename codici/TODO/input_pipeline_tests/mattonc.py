import numpy
import xarray
import scipy.io
import glob

folderin = "/home/protoss/Documenti/TESI/data/"
folderout = "/home/protoss/Documenti/TESI/datanc/"
hertz = numpy.arange(312,327, 5)
print(hertz)
filelist = glob.glob(folderin + "*")
numfile = len(filelist)
print("num files:", numfile)

filesin = numpy.full(numfile, fill_value = 'a', dtype = '<U55')
filesout = numpy.full(numfile, fill_value = 'a', dtype = '<U55')


def leggi_peakmat(nome):
	struct = scipy.io.loadmat(nome)['job_pack_0']
	
	
	#---------------------------------------
	# defining parameters                   |
	#---------------------------------------
	# times
	PAR_tFft = 4096.0
	PAR_Ntimes = struct["basic_info"][0,0]["ntim"][0,0][0,0]
	PAR_tObs = numpy.round(PAR_tFft*PAR_Ntimes/(2*60*60*24*30)) # mesi
	PAR_tObs = PAR_tObs*30*24*60*60

	# frequencies
	PAR_enhance = 10
	PAR_stepFreq = 1/PAR_tFft
	PAR_refinedStepFreq =  PAR_stepFreq/PAR_enhance

	# others
	PAR_secbelt = 4000	
	
	peakmap = struct['peaks'][0,0][0:2,:].astype(numpy.float32)
	
	##---------------------------------------
	## loading and managing data             |
	##---------------------------------------
	# times
	times = peakmap[0,:]
	timeMin = numpy.amin(times)
	timeMax = numpy.amax(times)
	PAR_epoch = (timeMin+timeMax)/2 
	times = times-PAR_epoch
	times = ((times)*60*60*24/PAR_refinedStepFreq)

	# frequencies
	freqs = peakmap[1,:]
	freqMin = numpy.amin(freqs)
	freqMax = numpy.amax(freqs)
	freqStart = freqMin- PAR_stepFreq/2 - PAR_refinedStepFreq
	freqEnd = freqMax + PAR_stepFreq/2 + PAR_refinedStepFreq
	nstepFreqs = numpy.ceil((freqEnd-freqStart)/PAR_refinedStepFreq)+PAR_secbelt
	freqs = freqs-freqStart
	freqs = (freqs/PAR_refinedStepFreq)-round(PAR_enhance/2+0.001)
	
	weights = numpy.random.rand(peakmap.shape[1]).astype(numpy.float32)
	
	dictionary = {
				'tFft': PAR_tFft,
				'nTimes' : PAR_Ntimes,
				'timeMin' : timeMin,
				'tObs' : PAR_tObs,
				'stepFreq' : PAR_stepFreq,
				'nStepsFreq' : nstepFreqs,
				'secbelt' : PAR_secbelt
			}
	
	return times, freqs, weights, dictionary

def tonc(t, f, w, attr, nomeout):
	coordinate_names = ['peak_index']
	coordinate_values = [numpy.arange(len(t))]
	tx = xarray.DataArray(data=t, 
					dims=coordinate_names, 
					#coords=coordinate_values
					)
	fx = xarray.DataArray(data=f, 
					dims=coordinate_names, 
					#coords=coordinate_values
					)
	wx = xarray.DataArray(data=w, 
					dims=coordinate_names, 
					#coords=coordinate_values
					)
	datasette = xarray.Dataset(data_vars={'time':tx, 
								'frequency':fx, 
								'weight':wx}, 
						attrs=attr)
	print(datasette)
	datasette.to_netcdf(path = nomeout, format = 'NETCDF4')
	return(print("scritto", nomeout))




def filini():
	for i in numpy.arange(numfile):
		filesin[i] = folderin + "in_O2LL_02_0" + str(hertz[i]) + "_.mat"
		filesout[i] = folderout + "in_O2LL_02_0" + str(hertz[i]) + "_.nc"
		t, f, w, dic = leggi_peakmat(filesin[i])
		tonc(t, f, w, dic, filesout[i])
	return(print("FINITO"))

def filone():
	for i in numpy.arange(numfile):
		filesin[i] = folderin + "in_O2LL_02_0" + str(hertz[i]) + "_.mat"
	
	fileout = folderout + "peakmappona.nc"
	filesin1 = filesin[0]
	t,f,w, dic = leggi_peakmat(filesin1)
	print(t.shape)
	for i in numpy.arange(1, numfile):
		filesin2 = filesin[i]
		t2,f2,w2, dic2 = leggi_peakmat(filesin2)
		t = numpy.concatenate((t,t2))
		f = numpy.concatenate((f,f2))
		w = numpy.concatenate((w,w2))
		print(t.shape)
	
	print(t.shape,f.shape,w.shape)
	tonc(t,f,w,dic,fileout)
	return(print("FINITO"))

def filone_da_filini():
	for i in numpy.arange(numfile):
		filesin[i] = folderout + "in_O2LL_02_0" + str(hertz[i]) + "_.nc"
		
	fileout = folderout + "peakmappona.nc"
	filesin1 = filesin[0]

	datasette = xarray.open_dataset(filesin1)
	t =  datasette.time
	f =  datasette.frequency
	w =  datasette.weight
	dic = datasette.attrs
	
	for i in numpy.arange(1, numfile):
		filesin2 = filesin[i]
		datasette2 = xarray.open_dataset(filesin2)
		t2 =  datasette2.time
		f2 =  datasette2.frequency
		w2 =  datasette2.weight
		t = numpy.concatenate((t,t2))
		f = numpy.concatenate((f,f2))
		w = numpy.concatenate((w,w2))
		print(t.shape)
	
	print(t.shape,f.shape,w.shape)
	tonc(t,f,w,dic,fileout)
	return(print("FINITO"))
	

#filini()
filone()
#filone_da_filini()
