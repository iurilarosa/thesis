import numpy
import xarray
import scipy.io
import glob
import tensorflow as tf
import time

<<<<<<< HEAD


folder = "/home/protoss/Documenti/TESI/datanc/"
freqMin = 312
freqMax = 322

def multiset(fmin, fmax):
	hertz = numpy.arange(fmin,fmax+5, 5)
	print(hertz)
	filelist = glob.glob(folder + "*")
	numfile = len(filelist)-1
	print("num files:", numfile)

=======


folder = "/home/iurilarosa/datanc/"
freqMin = 312
freqMax = 322

def multiset(fmin, fmax):
	hertz = numpy.arange(fmin,fmax+5, 5)
	print(hertz)
	filelist = glob.glob(folder + "*")
	numfile = len(filelist)-1
	print("num files:", numfile)

>>>>>>> cc7d207487a7b1a6cd3cb563a45526563f9bf363
	filesin = numpy.full(numfile, fill_value = 'a', dtype = '<U55')
	for i in numpy.arange(numfile):
			filesin[i] = folder + "in_O2LL_02_0" + str(hertz[i]) + "_.nc"
			print(filesin[i])
			
	datasette = xarray.open_mfdataset(filesin, engine='netcdf4')
	return datasette

def datasettone():
<<<<<<< HEAD
	filone = "/home/protoss/Documenti/TESI/datanc/peakmappona.nc"
=======
	filone = "/home/iurilarosa/datanc/peakmappona.nc"
>>>>>>> cc7d207487a7b1a6cd3cb563a45526563f9bf363
	datasette = xarray.open_dataset(filone)
	return datasette

#datasette = multiset(freqMin, freqMax)
#datasette = datasettone()


<<<<<<< HEAD
datasette = xarray.open_dataset("/home/protoss/Documenti/TESI/datanc/in_O2LL_02_0312_.nc")
=======
datasette = xarray.open_dataset("/home/iurilarosa/datanc/in_O2LL_02_0312_.nc")
>>>>>>> cc7d207487a7b1a6cd3cb563a45526563f9bf363

print(datasette.attrs['tFft'])

# open a tf session
sess = tf.Session()

#---------------------------------------
# defining parameters                   |
#---------------------------------------

PAR_stepFreq = datasette.attrs['stepFreq']
PAR_tObs = datasette.attrs['tObs']
PAR_nstepsFreq = datasette.attrs['nStepsFreq']

# spindowns
PAR_fdotMin = -1e-9
PAR_fdotMax = 1e-10
PAR_stepFdot = PAR_stepFreq/PAR_tObs
PAR_nstepFdot = numpy.round((PAR_fdotMax-PAR_fdotMin)/PAR_stepFdot).astype(numpy.int32)



##---------------------------------------
## loading and managing data             |
##---------------------------------------
# times
#times = datasette.time

# frequencies
#freqs = datasette.frequency

 
# spindowns
#spindowns = numpy.arange(0, PAR_nstepFdot)
#spindowns = numpy.multiply(spindowns,PAR_stepFdot)
#spindowns = numpy.add(spindowns, PAR_fdotMin)

# others
#weights = datasette.weight

PAR_nRows = numpy.int32(PAR_nstepFdot)
PAR_nColumns = numpy.int32(PAR_nstepsFreq)

##---------------------------------------
## defining TensorFlow graph             |
##---------------------------------------

PAR_secbeltTF = tf.constant(4000,dtype = tf.float32, name = 'secur')
timesTF = tf.placeholder(tf.float32, name = 'inputT')
freqsTF = tf.placeholder(tf.float32, name = 'inputF')
weightsTF = tf.placeholder(tf.float32, name = 'inputW')


spindownsTF = tf.range(PAR_nstepFdot, dtype = tf.float32, name = 'inputSD')
spindownsTF = spindownsTF*PAR_stepFdot+PAR_fdotMin

def frequencyHough(timesHM, freqsHM, weightsHM, spindownsHM):
	def mapnonVar(ithStep):
		sdTimed = tf.multiply(spindownsHM[ithStep], timesHM, name = "Tdotpert")
		transform = tf.round(freqsHM-sdTimed+PAR_secbeltTF/2, name = "trasfFreq")
		transform = tf.cast(transform, dtype=tf.int32)
		values = tf.unsorted_segment_sum(weightsHM, transform, PAR_nColumns, name = "cumsum")
		return values

	houghLeft = tf.Variable(tf.zeros([PAR_nRows,PAR_nColumns]),name = "var")
	houghLeft = houghLeft.assign(tf.map_fn(mapnonVar, tf.range(0, PAR_nRows), dtype=tf.float32, parallel_iterations=8))

	leftStripe = tf.slice(houghLeft, [0,0], [houghLeft.get_shape()[0],10], name = "left_stripe")
	houghRight = tf.subtract(tf.slice(houghLeft, [0,10],[houghLeft.get_shape()[0], houghLeft.get_shape()[1]-10]),
							 tf.slice(houghLeft, [0,0],[houghLeft.get_shape()[0], houghLeft.get_shape()[1]-10]), name = "right_stripe")
	houghDiff = tf.concat([leftStripe,houghRight],1, name = "diff_hough")
	houghMap = tf.cumsum(houghDiff, axis = 1, name = "output")
	return houghMap

FHMap = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF)

dataDict = { 
               timesTF: datasette.time,
               freqsTF: datasette.frequency,
               weightsTF: datasette.weight
             }
# initializing the variable
sess.run(tf.global_variables_initializer())
# running the graph
start = time.time()
image = sess.run(FHMap, feed_dict = dataDict)
stop = time.time()
print("execution time " + str(stop-start) + " s")

#from matplotlib import pyplot
#a = pyplot.imshow(image, aspect = 200)
#pyplot.show()
sess.close()
 




