import tensorflow as tf
import numpy
import time

rc = 1000000
#rc = numpy.power(2, esponenti)
print(rc)
start = time.time()
a = numpy.random.rand(rc).astype(numpy.float32)
b = numpy.random.rand(rc).astype(numpy.float32)

data = numpy.stack((a,b))
print(data)

sess = tf.Session()

dataset = tf.data.Dataset.from_tensor_slices(data)
iterator = dataset.make_one_shot_iterator()

atf = iterator.get_next()
#atf = tf.reshape(atf,(rc,1))
btf = iterator.get_next()
#btf = tf.reshape(btf,(rc,1))

#print(sess.run(atf), sess.run(btf))

ctf = atf*btf

#ctf = tf.matmul(atf, btf)

start = time.time()
c = sess.run(ctf)
stop = time.time()


print(stop-start)
