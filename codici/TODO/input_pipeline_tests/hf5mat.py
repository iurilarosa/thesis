import numpy
import tensorflow as tf
import h5py

# open a tf session
sess = tf.Session()
nHertz = 1




with h5py.File('/home/iurilarosa/peakmap.mat', 'r') as file:
    picchiali = numpy.array(file['peaks']).astype(numpy.float32)

limiti = numpy.loadtxt('/home/iurilarosa/picchialimiti.txt').astype(numpy.int32)

limite = limiti[nHertz-1]
picchiricchi = picchiali[:limite,:]
print(picchiricchi)
datasette = tf.data.Dataset.from_tensor_slices(picchiricchi).batch(3)

iter = datasette.make_one_shot_iterator()
elements = iter.get_next()


print(sess.run(elements))
