# Importing python libraries
import warnings
import sys
import os
import numpy
import scipy
import argparse
import configparser
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' # tf log errors only
warnings.filterwarnings("ignore")

# Importing functions
import Outliers
import InputOutput
import Weighting
import DoppCorr
import TF1search

parser = argparse.ArgumentParser(prog='FrequencyHough_allsky',
                    description='This program selects candidates for an all-sky search for CW, using the FrequencyHough algorithm. The code is optimized for GPU use with a series of tools available (TensorFlow, JAX) but it allows CPU computation via Numpy.\n All the parameters are passed by a parameters file which path can be specified via command line.',
                    epilog='Copyright ???')
#argParser.add_argument("-h", "--help", help="Help info")
parser.add_argument("-f", "--file", help="path of the parameters file. If none, default is ./params.ini")
parser.add_argument("-t", "--tool", help="toolkit used for the FH and candidates computation. Values admitted: tf1 (TensorFlow 1), tf2 (TensorFlow 2), jax (JAX), np (Numpy). If different, default is np")

args = parser.parse_args()

if(args.file == "None"):
    args.file = "./params.ini"
if(args.tool != "tf1" and 
   args.tool != "tf2" and
   args.tool != "jax"):
    args.tool = "np"


### MAIN CODE ###
# input parameters
param_file = args.file
param = configparser.ConfigParser()
param.read(param_file)
print(param.sections())
[job_id, pointsPerJob, sciencePath, outPath, dataPath] = InputOutput.config(param)

#LOADING DATA OF THE INPUT FILE AND SEARCH SETUP
struct = scipy.io.loadmat(dataPath)['job_pack_0']
# the dictionary containing all the parameters is generated with the input file and the input parameters
PAR = InputOutput.parDefs(struct,param)

# all the data extracted or generated according the search parameters
[firstFreq, peakmap, spindowns, velocities, indices] = InputOutput.loadData(struct, PAR)
scienceWeights = Weighting.compute_weights(struct, PAR, sciencePath)

# sky grid generation
[eclCoord, rectCoord, eqCoord] = DoppCorr.make_skygrid(PAR)
# sky point indeces which are iterated in the main loop, according to the job and the number of points per job chosen
points = numpy.arange(job_id*pointsPerJob,job_id*pointsPerJob+pointsPerJob)
print("Computing over " + str(pointsPerJob) +  " points, from point " + str(points[0]) + " to " + str(points[-1]))

#main loop starts: in each loop iteration for each sky point the weighted map is computed, frequencies are Doppler corrected, FH map is computed and candidates are selected and appended to the final output
final_candidates = numpy.zeros((PAR['numCand']*PAR['numStripes']*pointsPerJob, 8))
j = 0
for i in points:
    #DOPPLER CORRECTION AND WEIGHTED PEAKMAP COMPUTATION
    print("Coordinates", eclCoord[i], "\n")
    radiationPattern = Weighting.compute_radpat(struct, PAR, eqCoord[i])
    weights = radiationPattern*scienceWeights
    weights = weights/numpy.mean(weights)
    freqCorr = DoppCorr.doppcorr(rectCoord[i], peakmap[:,1], indices, velocities, PAR)
    peakmap_final = numpy.stack([peakmap[:,0],freqCorr,weights],axis=1)
    
    #Commented lines for debug: function below injects a straight horizontal line
    #peaks_new = Outliers.inject_line(peaks_final, PAR["freqInitial"]) 
    #[PAR_stripeHeight, PAR_beltWidth, cands] = GPUstuff.fh_cand(peaks_new, spindowns, PAR, eclCoord[i])
    
    #Below the core search is ran according the tools chosen (so far available only tf1)
    if(args.tool == "tf1"):
        [PAR_stripeHeight, PAR_beltWidth, cands] = TF1search.fh_cand(peakmap_final[:,0],peakmap_final[:,1],peakmap_final[:,2], spindowns, PAR, eclCoord[i])
    if(args.tool == "tf2"):
        [PAR_stripeHeight, PAR_beltWidth, cands] = TF2search.fh_cand(peakmap_final[:,0],peakmap_final[:,1],peakmap_final[:,2], spindowns, PAR, eclCoord[i])
    if(args.tool == "jax"):
        [PAR_stripeHeight, PAR_beltWidth, cands] = JAXsearch.fh_cand(peakmap_final[:,0],peakmap_final[:,1],peakmap_final[:,2], spindowns, PAR, eclCoord[i])
    if(args.tool == "np"):
        [PAR_stripeHeight, PAR_beltWidth, cands] = NPsearch.fh_cand(peakmap_final[:,0],peakmap_final[:,1],peakmap_final[:,2], spindowns, PAR, eclCoord[i])

    #the candidates array coming from the core search code are rearranged and appended to a single array
    cands = InputOutput.finalize(cands, PAR_stripeHeight, PAR_beltWidth, spindowns, PAR)
    top = PAR['numCand']*PAR['numStripes']*j
    bottom = PAR['numCand']*PAR['numStripes']*(j+1)
    final_candidates[top:bottom,:] = cands
    j = j+1

#the candidates array of the submitted job is saved
print("Saving output in " + outPath)
scipy.io.savemat(outPath, {"cands" : final_candidates})
