import numpy
import astropy.time
import scipy.interpolate

def compute_weights(struct, PAR, sciencePath):
    """
    Vetoes the non-science times and computes the weighted peakmap
    Parameters:
    struct : dict from Matlab structure
        The structure from the input file.
    PAR : dict
        All the parameters necessary for the computation of the Hough map.
    sciencePath : char
        Name of the file with the science times listed.
    Returns:
    weights : 1D array
        The non-zero weighted elements of the peakmap in a sparse format.
    """

    # threshold for science times
    exclusionFactor = 2/3
    minPercentage = 0.99

    # redifining some variable for more clarity
    lenFFT = PAR["tFft"]
    semilenFFT = lenFFT/2
    numTimes = PAR["numTimes"]

    # input data
    # indices
    indices = struct['basic_info'][0,0]['index'][0,0][0]

    # spectra
    spectra  = struct['basic_info'][0,0]['gsp'][0,0][0][0][1]
    firstFreq  = struct['basic_info'][0,0]['gsp'][0,0][0][0][9]
    deltaFreq  = struct['basic_info'][0,0]['gsp'][0,0][0][0][10]

    # peakmap
    peakmap = struct['peaks'][0,0]

    # science times
    scienceTimes = numpy.loadtxt(sciencePath)

    # array of the times of the FFTs (center binning) in gps
    timesFFT_mjd = struct["basic_info"][0,0]["tim"][0,0][0]
    timesFFT_mjd = astropy.time.Time(timesFFT_mjd, format = 'mjd')
    timesFFT = timesFFT_mjd.gps
    # selectidng all the peaks that can be considered science
    start = scienceTimes[:,0]
    end = scienceTimes[:,1]
    goodIndices = numpy.array([])

    for i in numpy.arange(start.size):
        selectedInd = numpy.where(timesFFT >= start[i])[0]
        if(selectedInd.size != 0):
            firstInd = selectedInd[0]
            tempiSelected = timesFFT[selectedInd]
            selectedInd = numpy.where(tempiSelected <= end[i])[0]
            tempiSelected = tempiSelected[selectedInd]

            #condition 1
            A = (tempiSelected-start[i])/semilenFFT
            B = (-tempiSelected+end[i])/semilenFFT
            over50A=numpy.where(A > 0.5)
            A[over50A] = 0.5
            over50B=numpy.where(B > 0.5)
            B[over50B] = 0.5
            condition = numpy.where(A+B >= minPercentage)        
            goodIndices = numpy.append(goodIndices, selectedInd[condition]+firstInd)

    timesFFT = timesFFT-semilenFFT
    for i in numpy.arange(start.size):
        selectedInd = numpy.where(timesFFT < start[i])[0]
        if(selectedInd.size != 0):
            firstInd = selectedInd[0]
            tempiSelected = timesFFT[selectedInd]
            selectedInd = numpy.where(tempiSelected+lenFFT <= end[i])[0]
            tempiSelected = tempiSelected[selectedInd]

            #condition 2.a
            A = (tempiSelected+lenFFT-start[i])/lenFFT
            condition = numpy.where(A >= minPercentage)
            goodIndices = numpy.append(goodIndices,selectedInd[condition]+firstInd)

    for i in numpy.arange(start.size):
        selectedInd = numpy.where(timesFFT < start[i])[0]
        if(selectedInd.size != 0):
            firstInd = selectedInd[0]
            tempiSelected = timesFFT[selectedInd]
            selectedInd = numpy.where(tempiSelected+lenFFT > end[i])[0]

            #condition 2.b
            A = (start[i]-end[i])/lenFFT
            condition = numpy.where(A >= minPercentage)
            goodIndices = numpy.append(goodIndices,selectedInd[condition]+firstInd)

    goodIndices = numpy.unique(goodIndices).astype(numpy.int64)

    # initialization of arrays for weights computation
    goodSpots = numpy.zeros(numTimes)
    goodSpots[goodIndices] = 1
    wien = numpy.zeros(spectra.shape)

    # wiener coefficients computation
    newSpectra = numpy.zeros(spectra.shape)
    newSpectra[goodIndices,:] = spectra[goodIndices,:]
    goodSpectra = newSpectra[goodIndices]
    medianPower_f = numpy.median(goodSpectra, axis=0)
    badPeaks = numpy.where(newSpectra < exclusionFactor*medianPower_f)
    newSpectra[badPeaks] = 0
    badPeaks = numpy.where(newSpectra > 10*medianPower_f)
    newSpectra[badPeaks] = 0


    for i in numpy.arange(spectra.shape[1]):
        newSpectrum= newSpectra[:,i]
        goodPeaks = numpy.nonzero(newSpectrum)
        wienPeaks = 1/newSpectrum[goodPeaks]
        wien[goodPeaks,i] = wienPeaks/numpy.mean(wienPeaks, axis = 0)

    #print(wien.shape)
    #print(wien[:3,:])
    #wiener weighted spectrum
    #spectra = newSpectra*wien
    #num = numpy.sum(spectra, axis = 0)
    #den = numpy.sum(wien, axis = 0)
    #wienerSpectrum = num/den

    # wiener weights for peakmap computation
    for i in numpy.arange(numTimes):
        start = indices[i]-1
        end = indices[i+1]-1
        frequencies = peakmap[1, start:end]
        freqIndices = numpy.round((frequencies - firstFreq)/deltaFreq).astype(numpy.int64)
        peakmap[3, start:end]= numpy.median(wien[i,freqIndices])

    # weights = peakmap[3]
    return peakmap[3]

# porting of SNAG function pss_radpat
def compute_radpat(struct, PAR, point):
    """
    Computes the radiation pattern for the weighted peakmap computation
    Parameters:
    struct : dict from Matlab structure
        The structure from the input file.
    PAR : dict
        All the parameters necessary for the computation of the Hough map.
    point : 1D array
        Sky coordinates of the analyzed point in equatorial coordinates
    Returns:
    radiationPattern : 1D array
        The values of the radiation pattern for each FFT time
    """

    def convert_to_GMST(times):
        times[numpy.where(times<1000000)] = times[numpy.where(times<1000000)]+2400000.5
        t0 = numpy.floor(times-0.5)+0.5
        h = (times-t0)*24
        d = times - 2451545
        d0 = t0 - 2451545
        T = d/36525
        siderealTime = numpy.mod(6.697374558+0.06570982441908*d0+1.00273790935*h+0.000026*T**2,24)
        return siderealTime

    def compute_siderealPattern(det_coordinates, alpha, delta, eta, psi, numpoints_Day):
        lat = det_coordinates['lat'][0,0]*numpy.pi/180
        lon = det_coordinates['long'][0,0]*numpy.pi/180
        azimuth = det_coordinates['azim'][0,0]*numpy.pi/180

        alphalon = numpy.exp(-1j*(alpha-lon))

        a0=-(3/16) * ( 1 + numpy.cos(2*delta)) * (1 + numpy.cos(2*lat)) * numpy.cos(2*azimuth)
        a1c=-(1/4) * numpy.sin(2*delta) * numpy.sin(2*lat) * numpy.cos(2*azimuth)
        a1s=-(1/2) * numpy.sin(2*delta) * numpy.cos(lat) * numpy.sin(2*azimuth)
        a2c=(-1/16) * (3 - numpy.cos(2*delta)) * (3 - numpy.cos(2*lat)) * numpy.cos(2*azimuth)
        a2s=-(1/4) * (3 - numpy.cos(2*delta)) * numpy.sin(lat) * numpy.sin(2*azimuth)

        b1c=-numpy.cos(delta) * numpy.cos(lat) * numpy.sin(2*azimuth)
        b1s=(1/2) * numpy.cos(delta) * numpy.sin(2*lat) * numpy.cos(2*azimuth)
        b2c=-numpy.sin(delta) * numpy.sin(lat) * numpy.sin(2*azimuth)
        b2s=(1/4) * numpy.sin(delta) * (3 - numpy.cos(2*lat)) * numpy.cos(2*azimuth)

        L0 = numpy.zeros(5).astype(numpy.complex64)
        L45 = numpy.zeros(5).astype(numpy.complex64)

        L0[0]=(alphalon**(-2))*(a2c+1j*a2s)/2;
        L0[1]=(alphalon**-1)*(a1c+1j*a1s)/2;
        L0[2]=a0;
        L0[3]=(alphalon)*(a1c-1j*a1s)/2;
        L0[4]=(alphalon**2)*(a2c-1j*a2s)/2;

        L45[0]=(alphalon**-2)*(b2c+1j*b2s)/2;
        L45[1]=(alphalon**-1)*(b1c+1j*b1s)/2;
        L45[2]=0;
        L45[3]=(alphalon)*(b1c-1j*b1s)/2;
        L45[4]=(alphalon**2)*(b2c-1j*b2s)/2;

        Hp=numpy.sqrt(1/(1+eta**2)) * (numpy.cos(2*psi)-1j*eta*numpy.sin(2*psi))
        Hc=numpy.sqrt(1/(1+eta**2)) * (numpy.sin(2*psi)+1j*eta*numpy.cos(2*psi))
        v=Hp*L0+Hc*L45

        step = numpy.arange(numpoints_Day)*2*numpy.pi/numpoints_Day

        lf = 0
        for i in numpy.arange(5):
            lf = lf + v[i]*numpy.exp(1j*(i+1-3)*step)

        siderealPattern = numpy.absolute(lf)**2
        return siderealPattern

    peakmap = struct["peaks"][0,0]
    times = peakmap[0]
    standardTime = convert_to_GMST(times)

    alpha = point[0]*numpy.pi/180
    delta = point[1]*numpy.pi/180
    eta = 1
    psi = 0
    det_coord = struct['basic_info'][0,0]['run'][0,0]['ant'][0,0]
    numpoints_Day = 120
    widthStep = 24/numpoints_Day
    siderealPattern = compute_siderealPattern(det_coord, alpha, delta, eta, psi, numpoints_Day)

    steps = numpy.arange(0, numpoints_Day)*widthStep

    minStep = numpy.amin(steps)-widthStep/2
    maxStep = numpy.amax(steps)+widthStep/2

    indices = numpy.where(standardTime < minStep)
    standardTime[indices] = minStep
    indices = numpy.where(standardTime > maxStep)
    standardTime[indices] = maxStep

    interpolator = scipy.interpolate.splrep(steps, siderealPattern)
    radiationPattern = scipy.interpolate.splev(standardTime, interpolator, der = 0)

    radiationPattern = radiationPattern/numpy.mean(radiationPattern)
    return radiationPattern
