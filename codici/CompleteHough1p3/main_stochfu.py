# Importing python libraries
import warnings
import sys
import os
import numpy
import scipy
import argparse
import configparser
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' # tf log errors only
warnings.filterwarnings("ignore")

# Importing functions
import Outliers
import InputOutput
import Weighting
import DoppCorr
import GPUstuff

parser = argparse.ArgumentParser(prog='FrequencyHough_stochasticFollowup',
                    description='This program follows up outliers from SGWB radiometer searches and selects candidates to be furtherly followed-up, using the FrequencyHough algorithm. The code is optimized for GPU use with a series of tools available (TensorFlow, JAX) but it allows CPU computation via Numpy.\n All the parameters are passed by a parameters file which path can be specified via command line.',
                    epilog='Copyright ???')
#argParser.add_argument("-h", "--help", help="Help info")
parser.add_argument("-f", "--file", help="path of the parameters file. If none, default is ./params.ini")
parser.add_argument("-t", "--tool", help="toolkit used for the FH and candidates computation. Values admitted: tf1 (TensorFlow 1), tf2 (TensorFlow 2), jax (JAX), np (Numpy). If different, default is np")

args = parser.parse_args()

if(args.file == "None"):
    args.file = "./params.ini"
if(args.tool != "tf1" and 
   args.tool != "tf2" and
   args.tool != "jax"):
    args.tool = "np"


### MAIN CODE ###
# input parameters
#param_file = "/m100/home/userexternal/ilarosa0/thesis/codici/Archiviati/CompleteHough1p3/paramsFU.ini"
param_file = args.file
param = configparser.ConfigParser()
param.read(param_file)
print(param.sections())
[job_id, pointsPerJob, sciencePath, outPath, dataPath] = InputOutput.config(param)

outlierList = param["job"]["outliers"]
interval = param["search"]["interval"] #Hz
detector = dataPath[-14]
datafolder = dataPath[:,-17]
print(dtafolder)

#DEFINING FREQUENCY OF THE OUTLIER AND RESPECTIVE INPUT FILE
job_par_list = Outliers.compute_list(datafolder,outlierList,["H","L"])
print(numpy.stack([numpy.arange(job_par_list[:,1].size),job_par_list[:,0]],axis=1))
if detector == "H":
    dataPath = job_par_list[job_id,1]
    dataList = numpy.sort(job_par_list[:,1])
if detector == "L":
    dataPath = job_par_list[job_id,2]
    dataList = numpy.sort(job_par_list[:,2])

freqOutlier = numpy.float(job_par_list[job_id,0])
print("Computing outlier at " + str(freqOutlier) + " Hz")
print(dataPath)

# sky grid generation
gridfolder = "./datas/"
[eclCoord, rectCoord, eqCoord] = Outliers.load_skygrid(job_id, gridfolder + "gridtest")
#print(eclCoord)
numPoints = eclCoord.shape[0]

print("Computing over " + str(numPoints) + " points")
#points = numpy.arange(numPoints)
points = numpy.arange(2)
[normal_loop, newPath] = Outliers.check_edges(dataPath, freqOutlier, interval)

#LOADING DATA OF THE MAIN INPUT FILE (THE ONE WITH THE OUTLIER)
struct = scipy.io.loadmat(dataPath)['job_pack_0']
PAR = InputOutput.parDefs(struct)
[firstFreq, peakmap, spindowns, velocities, indices] = InputOutput.loadData(struct, PAR)
scienceWeights = Weighting.compute_weights(struct, PAR, sciencePath)

if(normal_loop == False):
    struct_2 = scipy.io.loadmat(newPath)['job_pack_0']
    PAR_2 = InputOutput.parDefs(struct_2)
    [firstFreq_2, peakmap_2, spindowns, velocities, indices_2] = InputOutput.loadData(struct_2, PAR_2)
    scienceWeights_2 = Weighting.compute_weights(struct_2, PAR_2, sciencePath)

final_candidates = numpy.zeros((PAR['numCand']*PAR['numStripes']*numPoints, 8))
j = 0
for i in points:
    #DOPPLER CORRECTION AND WEIGHTED PEAKMAP COMPUTATION
    print("Coordinates", eclCoord[i], "\n")
    radiationPattern = Weighting.compute_radpat(struct, PAR, eqCoord[i])
    weights = radiationPattern*scienceWeights
    #weights = weights/numpy.mean(weights)
    #freqCorr = DoppCorr.doppcorr(rectCoord[i], peakmap[:,1], indices, velocities, PAR)
    freqCorr = peakmap[:,1]
    weights = numpy.ones(freqCorr.size)

    
    if(normal_loop == False):
        radiationPattern = Weighting.compute_radpat(struct_2, PAR_2, eqCoord[i])
        weights = radiationPattern*scienceWeights_2
        weights_2 = weights/numpy.mean(weights)
        freqCorr_2 = DoppCorr.doppcorr(rectCoord[i], peakmap_2[:,1], indices_2, velocities, PAR_2)

        #WARNING, WHEN JOINING PEAKMAP THERE ARE SEVERAL DOZENS OF FFTS WHICH ARE OVERLAPPED BETWEEN TO NEIGHBOR PEAKMAPS, SO IT MIGHT PRODUCE A COUNT EXCESS IN THE OVERLAPPING AREA!
        #WARNING OVERLAPPING AREA LOOKS TO HAVE EXACTLY OVERLAPPING PEAKS (F,T)
        #USING UNIQUE TO REMOVE THEM TODO CHECK WEIGHTS
        freqCorr = peakmap[:,1]
        freqCorr_2 = peakmap_2[:,1]
        [bot_f,bot_t,bot_w] = Outliers.cut_peakmap(freqCorr, peakmap[:,0], weights, freqOutlier-interval, numpy.amax(freqCorr))
        [top_f,top_t,top_w] = Outliers.cut_peakmap(freqCorr_2, peakmap_2[:,0], weights_2, numpy.amax(freqCorr), freqOutlier+interval)
        top_portion = numpy.stack([top_t,top_f,top_w]).T
        bot_portion = numpy.stack([bot_t,bot_f,bot_w]).T
        
        #top_portion = numpy.stack([peakmap[:,0],peakmap[:,1],numpy.ones(peakmap[:,0].size)]).T
        #bot_portion = numpy.stack([peakmap_2[:,0],peakmap_2[:,1],numpy.ones(peakmap_2[:,0].size)]).T
        #scipy.io.savemat(folderout+"top_peak_" + str(job_id) + ".mat", {"top_peak" : top_portion})
        #scipy.io.savemat(folderout+"bot_peak_" + str(job_id) + ".mat", {"bot_peak" : bot_portion})
        
        peakmap_final = numpy.concatenate([top_portion,bot_portion])
    else:
        [f,t,w] = Outliers.cut_peakmap(freqCorr, peakmap[:,0], weights, freqOutlier-interval, freqOutlier+interval)
        peakmap_final = numpy.stack([t,f,w]).T
        peakmap_final = numpy.stack([peakmap[:,0],peakmap[:,1],numpy.ones(peakmap[:,0].size)]).T

    scipy.io.savemat("/m100_work/INF23_virgo_0/ilarosa0/output/test/"+"cut_peakJOIN_" + str(job_id) + ".mat", {"tot_peak" : peakmap_final})
    PAR.update({'freqInitial': freqOutlier-interval})
    PAR.update({'freqFinal': freqOutlier+interval})
        
    #unic_t = numpy.unique(peakmap_final[:,0])
    #freq_inj_bottom = numpy.zeros(unic_t.size) + numpy.amin(peakmap_final[:,1])
    #freq_inj_top = numpy.zeros(unic_t.size) + numpy.amax(peakmap_final[:,1])
    #weigh_inj = numpy.ones(unic_t.size)
    #peaks_inj_bottom = numpy.stack([unic_t,freq_inj_bottom,weigh_inj],axis = 1)
    #peaks_inj_top = numpy.stack([unic_t,freq_inj_top,weigh_inj],axis = 1)
    #peaks_new = numpy.concatenate([peakmap_final,peaks_inj_bottom])
    #peaks_new = numpy.concatenate([peaks_new,peaks_inj_top])
    #[PAR_stripeHeight, PAR_beltWidth, cands] = GPUstuff.fh_cand(peaks_new, spindowns, PAR, eclCoord[i])
    
    
    [imagecut, image] = GPUstuff.fh_cand(peakmap_final, spindowns, PAR, eclCoord[i])
    scipy.io.savemat( "/m100_work/INF23_virgo_0/ilarosa0/output/test/hmapcutCUTJOIN_"+str(job_id)+".mat" ,{"hmap" : imagecut})
    scipy.io.savemat( "/m100_work/INF23_virgo_0/ilarosa0/output/test/hmapCUTJOIN_"+str(job_id)+".mat" ,{"hmap" : image})

    
#    [PAR_stripeHeight, PAR_beltWidth, cands] = GPUstuff.fh_cand(peakmap_final, spindowns, PAR, eclCoord[i])
#    #the candidates array coming from the TensorFlow code are rearranged and appended to a unique array
#    cands = InputOutput.finalize(cands, PAR_stripeHeight, PAR_beltWidth, spindowns, PAR)
#    top = PAR['numCand']*PAR['numStripes']*j
#    bottom = PAR['numCand']*PAR['numStripes']*(j+1)
#    final_candidates[top:bottom,:] = cands
#    j = j+1

#the candidates array of the submitted job is saved
#print("Saving output in " + outPath)
#scipy.io.savemat(outPath, {"cands" : final_candidates})






