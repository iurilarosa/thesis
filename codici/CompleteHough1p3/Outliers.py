import numpy
import astropy.units 
from astropy.coordinates import SkyCoord
import scipy.io
import glob
#import healpy
import os

def compute_list(datafolder,outlist, pair):
    #filepath = "/m100/home/userexternal/ilarosa0/folloup/outliers_example.csv"
    filepath = outlist
    outliers = numpy.genfromtxt(filepath, delimiter=',')
    freqOutlier = outliers[:,0]

    folder = datafolder + pair[0] + "/"
    fileslistH = numpy.array(glob.glob(folder + "*"))
    fileslistH = numpy.sort(fileslistH)

    folder = datafolder + pair[1] + "/"
    fileslistL = numpy.array(glob.glob(folder + "*"))
    fileslistL = numpy.sort(fileslistL)

    freqFile = numpy.zeros(fileslistH.size)
    for i in numpy.arange(fileslistH.size):
        freqFile[i] = fileslistH[i][-9:-5]

    indexFile = numpy.zeros(freqOutlier.size)
    for i in numpy.arange(freqOutlier.size):
        indexFile[i] = numpy.where(freqFile < freqOutlier[i])[0][-1].astype(int)

    indexFile = indexFile.astype(int)
    outlist = numpy.stack([freqOutlier, fileslistH[indexFile], fileslistL[indexFile]], axis = 1)
    return outlist


def load_skygrid(job_id, filename):
    eclGrid = scipy.io.loadmat(filename + str(job_id+1) + ".mat")["skymap"]
    # Conversion from ecliptic to equatorial
    # NB there is a slight difference (~1%) between the equatorial coordinates system used in the SNAG function and the icrs system used in astropy.coordinates
    coordToConv = SkyCoord(lon = eclGrid[:,0]*astropy.units.degree, lat = eclGrid[:,1]*astropy.units.degree, frame = 'barycentrictrueecliptic') 
    eqCoord = coordToConv.transform_to('icrs')
    eqRa = numpy.array(eqCoord.ra)
    eqDec = numpy.array(eqCoord.dec)
    eqGrid = numpy.stack([eqRa, eqDec], axis = 1)
    eqRad = eqGrid[:,0:2]*numpy.pi/180

    # Porting of the SNAG function astro2rect
    rectGrid = numpy.zeros((eclGrid.shape[0], 3))
    rectGrid[:,0] = numpy.cos(eqRad[:,0])*numpy.cos(eqRad[:,1])
    rectGrid[:,1] = numpy.sin(eqRad[:,0])*numpy.cos(eqRad[:,1])
    rectGrid[:,2] = numpy.sin(eqRad[:,1])

    return eclGrid, rectGrid, eqGrid


#def compute_radius(outlist):
#    # parameters
#    filepath = outlist
#    NSIDE = 32
#
#   # input
#    outliers = numpy.genfromtxt(filepath, delimiter=',')
#    pixels = outliers[:,1].astype(int)
#    coords = numpy.radians(outliers[:,2:])
#
#
#   NPIX = healpy.nside2npix(NSIDE)
#    m = numpy.arange(healpy.nside2npix(NSIDE))
#
#    neighbors = healpy.get_all_neighbours(NSIDE, pixels)
#
#    thetaNeigh, phiNeigh = healpy.pix2ang(NSIDE, neighbors)
#    thetaCenters, phiCenters = healpy.pix2ang(NSIDE,pixels)
#
#   raNeigh = phiNeigh-numpy.pi
#    decNeigh = numpy.pi/2-thetaNeigh
#    raCenters = phiCenters-numpy.pi
#    decCenters = numpy.pi/2-thetaCenters
#
#    raNeigh = raNeigh.T
#    decNeigh = decNeigh.T
#    distsHeal = numpy.zeros_like(raNeigh)
#    for i in numpy.arange(raCenters.size):
#        distsHeal[i] = healpy.rotator.angdist([raNeigh[i,:],decNeigh[i,:]], [raCenters[i],decCenters[i]])
#
#    distmax = numpy.amax(distsHeal.T, axis = 0)
#    distmax = numpy.degrees(distmax)
#
#    centersEq = SkyCoord(ra=raCenters*astropy.units.rad, dec=decCenters*astropy.units.rad)
#    centersEcl = centersEq.transform_to('barycentrictrueecliptic')
#    latCenters = centersEcl.lat.deg
#    lonCenters = centersEcl.lon.deg
#
#    outdict = {"radius" : distmax,
#              "lon" : lonCenters,
#              "lat" : latCenters}
#
#    scipy.io.savemat("/home/iuri.larosa/PhD/phdproject/codes/folloup/outliers_example_radiusGrid.mat", outdict)

def cut_peakmap(frequencies, times, weights, freqMin, freqMax):
    print("Cutting peakmap of range " + str(numpy.amin(frequencies)) + "-" + str(numpy.amax(frequencies)) + "Hz")
    print("in range " + str(freqMin) + "-" + str(freqMax) + "Hz")
    
    top_cond = numpy.where(frequencies < freqMax)
    sel_frequencies = frequencies[top_cond]
    bottom_cond = numpy.where(sel_frequencies > freqMin)
    fin_frequencies = sel_frequencies[bottom_cond]
    
    sel_times = times[top_cond]
    fin_times = sel_times[bottom_cond]
    
    sel_weights = weights[top_cond]
    fin_weights = weights[bottom_cond]
    
    return fin_frequencies, fin_times, fin_weights

#def join_inputs(path1,path2):

def check_edges(dataPath, freqOutlier, interval):
    #PREPARING FILENAME FOR WHEN OUTLIER IS CLOSE TO A PEAKMAP EDGE
    freqFile = numpy.int(dataPath[-9:-5])
    newPath = dataPath[:-9] + f'{freqFile:04}' + dataPath[-5:]
    freqFile_down = numpy.int(dataPath[-9:-5])-5
    freqFile_up = numpy.int(dataPath[-9:-5])+5

    if freqOutlier < 128:
        freqFile_up = numpy.int(dataPath[-9:-5])+1
        freqFile_down = numpy.int(dataPath[-9:-5])-1

    newPath_down = dataPath[:-9] + f'{freqFile_down:04}' + dataPath[-5:]
    newPath_up = dataPath[:-9] + f'{freqFile_up:04}' + dataPath[-5:]

    print(newPath_down,newPath_up)

    #CHECKING IF OUTLIER IS TOO CLOSE TO THE EDGE FOR THE FREQ INTERVAL CHOSEN
    #LOADING PREVIOUS/NEXT INPUT FILE IN CASE
    normal_loop = True
    newPath = dataPath
    print("CONDITIONS")
    searchInterval_bottom = (freqOutlier-interval < 10)
    searchInterval_top = (freqOutlier+interval > 2048)
    FFTband_top = ((freqOutlier < 128 and freqOutlier+interval > 128) or
          (freqOutlier < 512 and freqOutlier+interval > 512) or
          (freqOutlier < 1024 and freqOutlier+interval > 1024))
    FFTband_bottom =  ((freqOutlier > 128 and freqOutlier-interval < 128) or
          (freqOutlier > 512 and freqOutlier-interval < 512) or
          (freqOutlier > 1024 and freqOutlier-interval < 1024))
    peakmapEdge_bottom = (freqOutlier-interval < freqFile)
    peakmapEdge_top = (freqOutlier+interval > freqFile_up)

    #print(freqOutlier, interval, freqFile_up, freqOutlier-interval, freqFile, (freqOutlier-interval < freqFile), (freqOutlier+interval > freqFile_up))
    if searchInterval_bottom:
        print("WARNING, bottom limit reached, analyzing less than " + str(interval) + "Hz before the outlier")
    elif searchInterval_top:
        print("WARNING, top limit reched,  analyzing less than " + str(interval) + "Hz after the outlier")
    elif FFTband_top:
        print("WARNING, top limit of the band reached, analyzing less than " + str(interval) + "Hz after the outlier to avoid to mix different T_FFT")
    elif FFTband_bottom:
        print("WARNING, top limit of the band reached, analyzing less than " + str(interval) + "Hz after the outlier to avoid to mix different T_FFT")
    elif peakmapEdge_bottom:
        print("WARNING, outlier interval exceding the bottom edge, computing the previous input file:")
        newPath = newPath_down
        normal_loop = False
    elif peakmapEdge_top:
        print("WARNING, outlier interval exceding the top edge, computing the next input file")
        newPath = newPath_up
        normal_loop = False
    else:
        print("Outlier interval fully in loaded peakmap at file:")

    print(newPath)
    return normal_loop, newPath


def inject_line(peakmap,freq_inj,sd_inj=0):
    unic_t = numpy.unique(peakmap[:,0])
    freq_inj = (numpy.zeros(unic_t.size) + freq_inj)+unic_t*sd_inj
    weigh_inj = numpy.ones(unic_t.size)

    peaks_inj = numpy.stack([unic_t,freq_inj,weigh_inj],axis = 1)
    peaks_new = numpy.concatenate([peakmap,peaks_inj_bottom])
    
    return peaks_new
