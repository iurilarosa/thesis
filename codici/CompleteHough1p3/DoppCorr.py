import scipy.io
import numpy
import math
import astropy
from astropy.coordinates import SkyCoord
from astropy.coordinates import BarycentricTrueEcliptic

def make_skygrid(PAR):
    """
    Computes the sky grid for the input file analyzed.
    Parameters:
    PAR : dict
        All the parameters necessary for the computation of the Hough map
    Returns:
    eclGrid, eqGrid, rectGrid : 5D arrays
        Arrays of the positions and sizes of the elements of the sky grid, in ecliptic (for candidate listing), equatorial (for radiation pattern) and rectangular (for Doppler correction) coordinates. Dimensions are: lon, lat, deltalon, semideltalat up, semideltalat down
    """


    freqMax = PAR['freqFinal']#749
    tFft = PAR['tFft']#2048
    pi = math.pi
    # Porting of the SNAG function pss_optmatp with some vectorization
    def ecl_skygrid():
        dopplerNum = 0.000106*tFft*freqMax

        indBet = 1 + 0.01/dopplerNum
        betaErr = 1
        beta = -0.999

        i = 0
        while betaErr > -beta:
            i = i + 1
            betaerr = -beta
            beta = pi/2

            mid = 1
            indBet = indBet - 0.01/dopplerNum
            while beta > 0:
                mid = mid+1
                beta = beta - indBet/(dopplerNum*math.sin(beta))

        mid = mid - 1
        nLats = mid*2-1

        eclLat = numpy.zeros(nLats);
        eclLat[0] = pi/2
        for j in numpy.arange(1,mid-1):
            eclLat[j] = eclLat[j-1] - indBet/(dopplerNum*math.sin(eclLat[j-1]))

        secondHalf = -eclLat[:mid-1]
        secondHalf = secondHalf[::-1]
        eclLat[mid:] = secondHalf

        numLon = numpy.ceil( 2*pi * dopplerNum * numpy.cos(eclLat) )
        deltaLon = 360.0 / numLon
        nPoints = numpy.sum(numLon).astype(numpy.int32)

        eclCoord = numpy.zeros((nPoints,5))

        js = numpy.arange(1,nLats-1)
        fase = (js+1 - 2*numpy.floor((js+1)/2))*deltaLon[1:-1]/2

        start = 0
        for j in numpy.arange(1,nLats-1):
            start = numpy.int(start + numLon[j-1])
            end = numpy.int(start + numLon[j])
            k = numpy.arange(0,numLon[j])

            eclCoord[start:end,0] = fase[j-1] + k*deltaLon[j]
            eclCoord[start:end,1] = eclLat[j]*180/pi
            eclCoord[start:end,2] = deltaLon[j]
            eclCoord[start:end,3] = eclLat[j-1]*180/pi
            eclCoord[start:end,4] = eclLat[j+1]*180/pi


        eclCoord[0,0] = 180
        eclCoord[0,1] = 90
        eclCoord[0,2] = 360
        eclCoord[0,3] = 90
        eclCoord[0,4] = eclCoord[1,1]

        eclCoord[-1,0] = 180
        eclCoord[-1,1] = -90
        eclCoord[-1,2] = 360
        eclCoord[-1,3] = eclCoord[-2,1]
        eclCoord[-1,4] = -90

        return eclCoord

    eclGrid = ecl_skygrid()

    # Conversion from ecliptic to equatorial
    # NB there is a slight difference (~1%) between the equatorial coordinates system used in the SNAG function and the icrs system used in astropy.coordinates
    coordToConv = SkyCoord(lon = eclGrid[:,0]*astropy.units.degree, lat = eclGrid[:,1]*astropy.units.degree, frame = 'barycentrictrueecliptic') 
    eqCoord = coordToConv.transform_to('icrs')
    eqRa = numpy.array(eqCoord.ra)
    eqDec = numpy.array(eqCoord.dec)
    eqGrid = numpy.stack([eqRa, eqDec], axis = 1)
    eqRad = eqGrid[:,0:2]*pi/180

    # Porting of the SNAG function astro2rect
    rectGrid = numpy.zeros((eclGrid.shape[0], 3))
    rectGrid[:,0] = numpy.cos(eqRad[:,0])*numpy.cos(eqRad[:,1])
    rectGrid[:,1] = numpy.sin(eqRad[:,0])*numpy.cos(eqRad[:,1])
    rectGrid[:,2] = numpy.sin(eqRad[:,1])

    return eclGrid, rectGrid, eqGrid

#WARNING TODO
#def make_spotgrid(PAR, center, radius):
#    pi = math.pi
#
#    freqMax = PAR['freqFinal']#749
#    tFft = PAR['tFft']#2048
#
#    def ecl_spotgrid():
#        dopplerNum = 0.000106*tFft*freqMax
#
#        if (beta + radius > 90):
#            poles = 1
#        elif (beta - radius < -90):
#            poles = -1
#        else:
#            poles = 0
#
#        radius_rad = radius * pi/180
#
#        beta_rad[0] = beta * pi/180
#        deltabeta = 1/numpy.abs(dopplerNum*numpy.sin(beta_rad[0]))
#        if deltabeta > radius_rad:
#            deltabeta = radius_rad
#
#        i = 0
#
#       if  poles = 0:
#            while((beta_rad[i] < beta_rad[0] + radius_rad) and
#                    (beta_rad[i] < pi/2 - deltabeta)):
#                i = i +1
#                beta_rad[i] = beta_rad[i-1]+deltabeta
#                deltabeta = 1/numpy.abs(dopplerNum*numpy.sin(beta_rad[i]))
#
#            deltabeta = 1/numpy.abs(dopplerNum*numpy.sin(beta_rad[0]))
#            if deltabeta > radius_rad:
#                deltabeta = radius_rad
#
#            beta_rad2 = 0
#
#    return 0


# Simple porting of the SNAG function hfdf_patch
def doppcorr(point, frequencies, indices,  velocities, PAR):
    """
    Computes the Doppler correction of the frequencies, given the sky point.
    Parameters:
    point : 1D array
        The coordinates of the sky point
    frequency :  1D array
        The array of frequencies of the peakmap analyzed
    indices : 1D array
        The time indices of "science" FFTs.
    velocities : 3D array
        The array of the Earth velocities at every FFT time
    PAR : dict
        All the parameters necessary for the computation of the Hough map
    Returns:
    freqCorr : 1D array
        The array of the Doppler corrected frequencies
    freqIn : float
        The Hough transform matrix
    freqHM : 1D array
        Doppler corrected frequencies suited for the Hough Map computation
    """
    # Defining the arrays used for array slicing in the for loop
    starts = indices[:-1]-1
    ends = indices[1:]-1
    #print(point)
    # Selection of the velocities at the "science" times
    velForCorr = numpy.zeros((3,frequencies.size))

    for i in numpy.arange(0, PAR['numTimes']):
        velForCorr[:,starts[i]:ends[i]+1] = velocities[:,i:i+1]

    # Doppler correction
    velXpos = numpy.dot(point,velForCorr)
    freqCorr = frequencies / (1+velXpos)
    return freqCorr
 
