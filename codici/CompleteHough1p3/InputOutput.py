import numpy
import __main__
import configparser
import os

def config(param):
    """
    Passing arguments to main function.
    Parameters:
    param: configparser struc
        The structure from the parameters file
    Returns:
    job_id: int
        The id of the search job
    pointsPerJob: int
        The number of sky points analyzed by each job
    sciencePath: string
        The path to the file containing the science FFTs
    outPath: string
        The path of the output file containing the candidates list
    dataPath: string
        The path of the input file containing peakmap and other search parameters
    """
    print(param.sections())
    job_id = numpy.int32(param['job']['id'])
    dataPath = param['job']['inputfile']
    pointsPerJob = numpy.int32(param['job']['numpoints'])
    folderout = param['job']['outfolder']
    sciencePath = param['job']['sciencefile']

    filename = dataPath[-16:]

    os.system('mkdir ' + folderout)
    outPath = folderout + "out_" + str(job_id) + ".mat"
    points = numpy.arange(job_id*pointsPerJob, job_id*pointsPerJob+pointsPerJob)
    print("Computing input file in " + dataPath)
    return job_id, pointsPerJob, sciencePath, outPath, dataPath

def loadData(struct, PAR):
    """
    Defines al the data arrays for Doppler correction, Frequency Hough and Candidate selection functions.
    Parameters:
    struct : dict from Matlab structure
        The structure from the input file.
    PAR : dict
        All the parameters necessary for the computation of the Hough map
    Returns:
    freqStart : float
        The lowest frequency of the Frequency Hough map
    nstepFreqs : int
        The number of frequency steps in the input array
    peakmap : 3D array
        The non-zero elements of the weighted peakmap in a sparse format: time, frequency, weight
    spindowns : 1D array
        The array of spindown values
    velocities : 3D array
        The array of the Earth velocities at every FFT time.
    indices : 1D array
        The time indices of "science" FFTs.
    """	

    # Times
    times = struct['peaks'][0,0][0]
    times = times-PAR['epoch']

#   timesec = times*60*60*24
#   min = numpy.amin(timesec)
#   print(min, numpy.where(timesec > min + 60*60*24*30*6))

    times = ((times)*60*60*24/PAR['refinedStepFreq'])

    # Frequencies
    freqs = struct['peaks'][0,0][1]

    freqMin = numpy.amin(freqs)
    freqMax = numpy.amax(freqs)
    freqStart = freqMin- PAR['stepFreq']/2 - PAR['refinedStepFreq']
    freqEnd = freqMax + PAR['stepFreq']/2 + PAR['refinedStepFreq']
    #nstepFreqs = numpy.ceil((freqEnd-freqStart)/PAR['refinedStepFreq'])+PAR['secbelt']

    #freqs = freqs-freqStart
    #freqs = (freqs/PAR['refinedStepFreq'])-round(PAR['enhance']/2+0.001)

    # Spindowns
    spindowns = numpy.arange(0, PAR['nstepFdot'])
    spindowns = numpy.multiply(spindowns,PAR['stepFdot'])
    spindowns = numpy.add(spindowns, PAR['fdotMin'])

    # Doppler correction
    velocities = struct['basic_info'][0,0]['velpos'][0,0][0:3,:].astype(numpy.float64)
    indices = struct['basic_info'][0,0]['index'][0,0][0]

    # Once the data of the peakmap are rearranged as above, it's better to rejoin them in an unique array
    peakmap = numpy.stack((times,freqs),1)#.astype(numpy.float32)

    return freqStart, peakmap, spindowns, velocities, indices



def parDefs(struct,param):
    """
    Defines all the parameters needed by the pipeline.
    Parameters:
    struct : dict from Matlab structure
        The structure from the input file.
    Returns:
    parameters : dict
        One dictionary with all the parameters included
    """

    # Frequencies
    enhance = 10
    stepFreq = struct["basic_info"][0,0]["dfr"][0,0][0,0]
    refinedStepFreq =  stepFreq/enhance

    # Times
    tFft = numpy.int32(1/stepFreq)
    numTimes = struct["basic_info"][0,0]["ntim"][0,0][0,0]
    tObs = tFft*numTimes/(2*60*60*24*30) # mesi
    #print(tObs, " mesi")
    tObs = numpy.round(tObs*30*24*60*60)
    epoch = struct["basic_info"][0,0]["epoch"][0,0][0,0]
    
    numStripes = numpy.int(param["search"]["cands_stripes"])
    
    # Spindowns
    fdotMin = numpy.float(param["search"]["min_sd"])
    fdotMax = numpy.float(param["search"]["max_sd"])
    stepFdot = stepFreq/tObs
    nstepFdot = numpy.round((fdotMax-fdotMin)/stepFdot).astype(numpy.int32)
    extraBins = numpy.mod(nstepFdot,numStripes)
    nstepFdot = numpy.round(nstepFdot + extraBins).astype(numpy.int32)
    fdotMax = fdotMax + extraBins*stepFdot

    # Others WARNING hardcoded
    secbelt = 2000
    if(tFft == 4096):
        secbelt = 1000#*2

    freqInitial = struct['basic_info'][0,0]['frin'][0,0][0,0]
    freqFinal = struct['basic_info'][0,0]['frfi'][0,0][0,0]

    # Cands
    #WARNING TODO SEE HOUGH CUT
    numCand = numpy.int(param["search"]["cands_per_hz"])*numpy.round(freqFinal-freqInitial).astype(int)
   
    # Defining a dictionary with all the parameters needed
    parameters = {
            'enhance' : enhance,
            'stepFreq' : stepFreq,
            'refinedStepFreq' : refinedStepFreq,

            'tFft' : tFft,
            'numTimes': numTimes,
            'tObs' : tObs,
            'epoch' : epoch,

            'fdotMin' : fdotMin,
            'fdotMax' : fdotMax,
            'stepFdot' : stepFdot,
            'nstepFdot' : nstepFdot,

            'secbelt' : secbelt,

            'numCand' : numCand,
            'numStripes' : numStripes,
            'freqInitial' : freqInitial,
            'freqFinal' : freqFinal
        }

    return parameters

def finalize(cands, PAR_stripeHeight, PAR_beltWidth, spindowns, PAR):
    """
    Computes the spindown and frequency values given the coordinates in the FrequencyHough map.
    Parameters:
    cands : 4D array
        array with the candidates extracted from the FH map
    PAR_stripeHeight : int
        The size in bins of the spindown portions analyzed by the candidate search function
    PAR_beltWidth : int
        The size in bins of the frequency portions analyzed by the candidate search function
    spindowns : 1D array
        The array of the spindown values
    PAR : dict
        All the parameters necessary for the computation of the Hough map.
Returns:
    cands : 2D array
        the final candidates array
    """
    cands = numpy.reshape(cands, [cands.shape[0],cands.shape[1], cands.shape[3]]).astype(numpy.float64)
    # NOTE cands = [amplitude, row = spindown, column = freq, CR, coord1, coord2, errcoord1, errcoord2]

    # recovering spindown value
    for i in numpy.arange(PAR['numStripes']):
            row = (cands[i,:,1]+(i*PAR_stripeHeight)).astype(numpy.int64)
            cands[i,:,1] = spindowns[row] #PAR["fdotMin"] + row * PAR["stepFdot"]

    # recovering fr value
    for i in numpy.arange(PAR['numCand']):
            column = (cands[:,i,2]+(i*PAR_beltWidth)+1).astype(numpy.int64)
            cands[:,i,2] = PAR['freqInitial'] + column*PAR["refinedStepFreq"]

    cands = numpy.reshape(cands, [cands.shape[0]*cands.shape[1], cands.shape[2]])
    return cands
