import tensorflow as tf
import numpy
import scipy 

def frequencyHough(times, frequencies, weights, spindowns, PAR):
    """
        Computes the frequency-Hough transform of a sparse peakmap.
        Parameters:
        times, frequencies : 1D tensors
            The coordinates of the peakmap in sparse format
        weights :  1D tensor
            The weighted values of the peaks in the sparse peakmap
        spindowns : 1D tensor
            The spindowns values over which calculate the Hough transform
        PAR : dict
            All the parameters necessary for the computation of the Hough map
        Returns:
        houghMap : 2D tensor
            The Hough transform matrix
    """

    # these variables are only redifinition of old variable to clarify better the following code
    nRows = numpy.int32(PAR['nstepFdot'])
    nColumns = numpy.int32(PAR['nstepFreqs'])
    enhance = numpy.int32(PAR['enhance'])

    #WARNING use the following line for 64bit data
    PAR_secbeltTF = tf.constant(PAR['secbelt'],dtype = tf.float64, name = 'secur')
    #WARNING use following line for 32bit data
    #PAR_secbeltTF = tf.constant(PAR['secbelt'],dtype = tf.float32, name = 'secur')

    # this function computes the Hough transform histogram for a given spindown
    def rowTransform(ithStep):
        freqShift = tf.multiply(spindowns[ithStep], times, name = "Tdotpert")
        #WARNING use the following line only for 64bit data (perhaps unnecessary cast)
        freqShift = tf.cast(freqShift, dtype = tf.float64)

        transform = tf.round(frequencies-freqShift+PAR_secbeltTF/2, name = "trasfFreq")
        #WARNING use the following line only for 64bit data (perhaps unnecessary cast)
        transform = tf.cast(transform, dtype=tf.int32)

        values = tf.unsorted_segment_sum(weights, transform, nColumns, name = "cumsum")
        #WARNING use the following line only for 64bit data
        values = tf.cast(values, dtype=tf.float32)
        return values

    # definition of the TensorFlow variable and values assignment with tf.map_fn
    houghLeft = tf.Variable(tf.zeros([nRows, nColumns]), name = "var")
    houghLeft = houghLeft.assign(tf.map_fn(rowTransform, tf.range(0, nRows), dtype=tf.float32, parallel_iterations=8))

    # let's superimpose the right edge on the image
    leftStripe = tf.slice(houghLeft, [0,0], [houghLeft.get_shape()[0],enhance], name = "left_stripe")
    houghRight = tf.subtract(tf.slice(houghLeft, [0,enhance],
                              [houghLeft.get_shape()[0],houghLeft.get_shape()[1]-enhance]),
                             tf.slice(houghLeft, [0,0],
                             [houghLeft.get_shape()[0], houghLeft.get_shape()[1]-enhance]),name = "right_stripe")

    # now we have the so called differential hough map
    houghDiff = tf.concat([leftStripe,houghRight],1, name = "diff_hough")
    # and at last we can cumulative sum along the rows to have the integral hough map
    houghMap = tf.cumsum(houghDiff, axis = 1, name = "output")
    return houghMap

def select_candidates(FHMap, totWidth, totHeight, pointCoords, PAR):
    """
        Selects the candidates in the Frequency Hough map
        Parameters:
        FHMap : 2D tensor
            The FrequencyHough map matrix
        totWidth, totHeight :  int
            The shape of the Frequency Hough matrix
        coords : 1D array
            The coordinates of the sky point
        PAR : dict
            All the parameters necessary for the computation of the Frequency Hough map
        Returns:
        stripeHeight, beltWidth : int scalar
            The size of the horizontal-vertical portions of the Frequency Hough map
        candidates : 8D tensor
            The array of the candidates with values: HM matrix row (spindown), column (frequency), amplitude, CR, lon, lat, deltalon, semideltalat up, semideltalat down
    """

    # These two variables are only redifinition of old variable to clarify better the code
    numCand = numpy.int32(PAR['numCand'])
    numStripes = numpy.int32(PAR['numStripes'])

    NPcoords = numpy.array([pointCoords[0], pointCoords[1], pointCoords[2]/2, numpy.abs(pointCoords[3]-pointCoords[4])/4])
    coords = tf.constant(numpy.zeros((numCand,4)) + NPcoords, dtype = tf.float32)

    # Defining the horizontal-vertical portions of the Frequency Hough map
    beltWidth = tf.floor(totWidth/numCand)
    beltWidth = tf.cast(beltWidth, dtype = tf.int32)
    belts = tf.range(0, totWidth, beltWidth)
    #print(sess.run([__PAR__totWidth, __PAR__beltWidth], feed_dict = dataDict))

    stripeHeight = tf.floor(totHeight/numStripes)
    stripeHeight = tf.cast(stripeHeight, dtype = tf.int32)
    # Splitting the map in vertical belts
    houghBelts = tf.zeros([numCand, totHeight,beltWidth])
    houghBelts = tf.split(FHMap, num_or_size_splits=numCand, axis=1)
    houghBelts = tf.cast(houghBelts, dtype = tf.int32)
    # Computing the median of the amplitude values over the whole map
    semimedianTOT = tf.contrib.distributions.percentile(FHMap, 50)/2

  # Function that selects the candidates both in horizontal and in vertical portions of the map
    def cands(i):
        # Spitting the ith vertical belt in horizontal stripes
        houghSquares = houghBelts[:,i*stripeHeight:(i+1)*stripeHeight,:]
        # Finding the local maximum values in each belt of the ith stripe
        def find_maxs(tensor, numColumns):
            local_top = tf.reduce_max(tensor,axis = [1,2])

            x = tf.reshape(tensor, [tf.shape(tensor)[0], -1])
            x = tf.cast(x, tf.int32)
            indices = tf.argmax(x, axis=1)
            indices = tf.cast(indices, tf.int32)
            rows = tf.cast(indices / numColumns, tf.float32)
            cols = tf.cast(indices % numColumns, tf.float32)
            rows = tf.cast(rows, tf.int32)
            cols = tf.cast(cols, tf.int32)

            local_maxs = tf.transpose(tf.stack([local_top, rows, cols]))
            return local_maxs

        # Executing the function vectorially in each belt of the ith stripe
        primaryMaxs = find_maxs(houghSquares, beltWidth)
        #NB structure of the array: [max value, row => spindown, column => freq]

        # Finding the local median and dev std for the median, in each belt of the ith stripe
        localMedians = tf.contrib.distributions.percentile(houghSquares, 50, axis = [1,2])
        localSigmans = tf.contrib.distributions.percentile(tf.abs(houghSquares-tf.reshape(localMedians,[numCand,1,1])), 50, axis = [1,2])
        localSigmans = tf.cast(localSigmans, tf.float32)/0.6745

        # Data conversion for tensorflow
        # TODO perhaps unnecessary TODO remove it to save memory usage
        primaryMaxs = tf.cast(primaryMaxs, tf.float32)
        localMedians = tf.cast(localMedians, tf.float32)
        localSigmans = tf.cast(localSigmans, tf.float32)
        #semimedianTOT = tf.cast(semimedianTOT, tf.float32)

        # Function that checks which of the local maxima is a candidate
        def find_cands(medians, sigmans, maxs, semimedian):
            # Critical Ratio CR_i = (amplitude_i - median)/sigman
            CR = tf.reshape((maxs[:,0]-medians)/sigmans, [numCand, 1])
            preliminaryCands = tf.concat([maxs, CR, coords], axis = 1)

            # Conditions to select the candidates
            primaryCond0 = (medians > 0)
            primaryCond1 = tf.logical_and(maxs[:,0] > medians, maxs[:,0] > semimedian)
            primaryCond = tf.equal(primaryCond0,primaryCond1)

            # Selection
            cands = tf.gather(preliminaryCands,tf.where(primaryCond))
            shapesel = tf.shape(cands)[0]
            void = tf.cast((shapesel < 100), tf.bool)
            primaryCands = tf.cond(void, lambda: tf.concat([cands,tf.zeros([numCand-shapesel,1,8])],axis = 0), lambda: (tf.identity(cands)))
            #primaryCands = tf.gather(preliminaryCands,tf.where(primaryCond))

            return primaryCands
        return find_cands(localMedians, localSigmans, primaryMaxs, semimedianTOT)

    # With tf.map it's possible to search the candidates over all the stripes in parallel
    candidates = tf.map_fn(cands, tf.range(0, numStripes), dtype=tf.float32, parallel_iterations=1)
    return [stripeHeight, beltWidth, candidates]


def fh_cand(times, frequencies, weights, spindowns, PAR, pointECL):
    """
        Wrapper function that takes data and parameters from CPU memory, runs the core functions on GPU (Hough map creation and candidates selection) and returns the final output on CPU memory. All the GPU computation is confined in this function.
        Parameters:
        times, frequencies : 1D arrays
            The coordinates of the peakmap in sparse format
        weights :  1D array
            The weighted values of the peaks in the sparse peakmap
        spindowns : 1D array
            The spindowns values over which calculate the Hough transform
        pointECL : 1D array
            The coordinates of the sky point, in ecliptic coordinates
        PAR : dict
            All the parameters necessary for the computation of the Frequency Hough map
        Returns:
        PAR_stripeHeight, PAR_beltWidth : int
            The size of the horizontal-vertical portions of the Frequency Hough map
        candidates : 8D array
            The array of the candidates with values: HM matrix row (spindown), column (frequency), amplitude, CR, lon, lat, deltalon, semideltalat up, semideltalat down
    """
    #-----------------------
    # Hough Map computation |
    #-----------------------
    # Computation of the minimum frequency of the Hough map matrix
    freqMin = numpy.amin(frequencies)
    freqStart = freqMin- PAR['stepFreq']/2 - PAR['refinedStepFreq']
    freqMax = numpy.amax(frequencies)
    freqEnd = freqMax + PAR['stepFreq']/2 + PAR['refinedStepFreq']
    PAR.update({'freqInitialCorr': freqStart})
    PAR.update({'freqFinalCorr': freqEnd})

    nstepFreqs = numpy.ceil((freqEnd-freqStart)/PAR['refinedStepFreq'])+PAR['secbelt']
    PAR.update({'nstepFreqs': nstepFreqs})

    #Effective start and end of Hough freq dimension (removing security sidebands)
    indexInitial = ((PAR['freqInitial']-freqStart)/PAR['refinedStepFreq']+PAR['secbelt']/2).astype(numpy.int32)
    indexFinal = ((PAR['freqFinal']-freqStart)/PAR['refinedStepFreq']+PAR['secbelt']/2).astype(numpy.int32)
    #Debug, check size of the HM
    print(PAR['freqInitial'],PAR['freqFinal'])
    print(PAR["freqInitialCorr"], PAR["freqFinalCorr"])
    print(indexInitial, indexFinal)
        
    # "Normalization" of the frequency array which will be passed to the FrequencyHough function
    freqHM = frequencies-freqStart
    freqHM = (freqHM/PAR['refinedStepFreq'])-numpy.round(PAR['enhance']/2+0.001)

    # open a tf session: here the TF graph is generated
    sess = tf.Session()

    # standard way to input data with TF1: create placeholder that will be fed during execution of the graph
    timesTF = tf.placeholder(tf.float64, name = 'inputT')
    freqsTF = tf.placeholder(tf.float64, name = 'inputF')
    weightsTF = tf.placeholder(tf.float64, name = 'inputW')
    spindownsTF = tf.placeholder(tf.float64, name = 'inputSD')
    # si far opted for 64bit data, considering to move evertything to 32bit data with newer versions
    #timesTF = tf.placeholder(tf.float32, name = 'inputT')
    #freqsTF = tf.placeholder(tf.float32, name = 'inputF')
    #weightsTF = tf.placeholder(tf.float32, name = 'inputW')
    #spindownsTF = tf.placeholder(tf.float32, name = 'inputSD')

    # FH lateral stripes already removed
    # as in cut_gd2 SNAG function
    FHMapCand = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF, PAR)[:,indexInitial:indexFinal]
    # Uncomment for debug
    #FHMap = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF, PAR)

    totWidth = tf.cast(tf.shape(FHMapCand)[1], dtype= tf.int32)
    totHeight = tf.cast(tf.shape(FHMapCand)[0], dtype = tf.int32)

    #---------------------
    # Candidate selection |
    #---------------------
    # Note: so far we didn't execute the tf graph, the candidate selection function runs on GPUs working on the same graph
    [stripeHeight, beltWidth, candidates] = select_candidates(FHMapCand, totWidth, totHeight, pointECL, PAR)

    #-----------------------------------------------
    # Feeding and running the TF graph defined above |
    #-----------------------------------------------
    # this is the dictionary with the data to be fed to the graph using placeholders
    dataDict = {
        timesTF : times,
        freqsTF : freqHM,
        weightsTF : weights,
        spindownsTF : spindowns
    }

    # variables initialization and graph execution; the only output will be the candidates array
    sess.run(tf.global_variables_initializer())

    # debug lines: compute and save cut and uncut FH maps
    #imagecut = sess.run(FHMapCand, feed_dict = dataDict)
    #scipy.io.savemat( "/m100_work/INF23_virgo_0/ilarosa0/output/tests/H/hmapNEW.mat" ,{"hmap" : imagecut})
    #image = sess.run(FHMap, feed_dict = dataDict)
    #scipy.io.savemat( "/m100_work/INF23_virgo_0/ilarosa0/output/test/hmapFULL.mat" ,{"hmap" : image})
    
    [PAR_stripeHeight, PAR_beltWidth, cands] = sess.run([stripeHeight, beltWidth, candidates], feed_dict = dataDict)
    # flaw of tf1: at every iteration the graph needs to be reset to unload the memory
    sess.close()
    tf.reset_default_graph()

    return PAR_stripeHeight, PAR_beltWidth, cands




