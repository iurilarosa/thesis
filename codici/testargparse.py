import argparse
parser = argparse.ArgumentParser(prog='FrequencyHough_allsky',
                    description='This program selects candidates for an all-sky search for CW, using the FrequencyHough algorithm. The code is optimized for GPU use with a series of tools available (TensorFlow, JAX) but it allows CPU computation via Numpy.\n All the parameters are passed by a parameters file which path can be specified via command line.',
                    epilog='Text at the bottom of help')
#argParser.add_argument("-h", "--help", help="Help info")
parser.add_argument("-f", "--file", help="path of the parameters file")
parser.add_argument("-t", "--tool", help="toolkit used for the FH and candidates computation. Values admitted: tf1 (TensorFlow 1), tf2 (TensorFlow 2), jax (JAX), np (Numpy). If different, default is np")

args = parser.parse_args()
print(args)

if(args.file == None):
    args.file = "./params.ini"
if(args.tool != "tf1" and 
   args.tool != "tf2" and
   args.tool != "jax"):
    args.tool = "np"
    
    
print(args)