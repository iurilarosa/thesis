addpath(genpath("/home/iuri.larosa/programmi/matlabLibs"))
load("outliers_example_radiusGrid.mat")

outliers = readtable('outliers_example.csv');
freq = outliers.Var1;
healpixel = outliers.Var2;

size(freq)

for i = 1:size(freq)
    freqOutlier = freq(i)

    if  freqOutlier >1024 
        TFFT = 1024;
    end

    if freqOutlier < 1024
        TFFT = 2048;
    end

    if freqOutlier < 512
        TFFT = 4096;
    end
    if freqOutlier < 128
        TFFT = 8192;
    end

    TFFT

    ND=0.000106*TFFT*freqOutlier;
    res=1;
    ddd=radius(i); %vedi, gradi attorno +-

    lam0=lon(i); %SLong; average of the two (stiamo usando pOAV)
    bet0=lat(i); % SLat;
    [skymap,lam,bet]=pss_optmap_spot(ND,res,lam0,bet0,ddd);
    %figure
    %plot(skymap(:,1),skymap(:,2),'* ')
    %grid
    NN=size(skymap);
    Npoint=NN(1);
    filename = "gridtest" + i + ".mat"
    save(filename, 'skymap')
end