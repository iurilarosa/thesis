import numpy
import astropy.time

def compute_weights(struct, PAR, sciencePath):
	"""
	Vetoes the non-science times and computes the weighted peakmap
	Parameters:
	struct : dict from Matlab structure
		The structure from the input file.
	PAR : dict
		All the parameters necessary for the computation of the Hough map.
	sciencePath : char
		Name of the file with the science times listed.
	Returns:
	weights : 1D array
		The non-zero weighted elements of the peakmap in a sparse format.
	"""

	# threshold for science times
	exclusionFactor = 2/3
	minPercentage = 0.99

	# redifining some variable for more clarity
	lenFFT = PAR["tFft"]
	semilenFFT = lenFFT/2
	numTimes = PAR["numTimes"]

	# input data
	# indices
	indices = struct['basic_info'][0,0]['index'][0,0][0]

	# spectra
	spectra  = struct['basic_info'][0,0]['gsp'][0,0][0][0][1]
	firstFreq  = struct['basic_info'][0,0]['gsp'][0,0][0][0][9]
	deltaFreq  = struct['basic_info'][0,0]['gsp'][0,0][0][0][10]

	# peakmap
	peakmap = struct['peaks'][0,0]

	# science times
	scienceTimes = numpy.loadtxt(sciencePath)

	# array of the times of the FFTs (center binning) in gps
	timesFFT_mjd = struct["basic_info"][0,0]["tim"][0,0][0]
	timesFFT_mjd = astropy.time.Time(timesFFT_mjd, format = 'mjd')
	timesFFT = timesFFT_mjd.gps
	# selectidng all the peaks that can be considered science
	start = scienceTimes[:,0]
	end = scienceTimes[:,1]
	goodIndices = numpy.array([])

	for i in numpy.arange(start.size):
		selectedInd = numpy.where(timesFFT >= start[i])[0]
		if(selectedInd.size != 0):
			firstInd = selectedInd[0]
			tempiSelected = timesFFT[selectedInd]
			selectedInd = numpy.where(tempiSelected <= end[i])[0]
			tempiSelected = tempiSelected[selectedInd]

			#condition 1
			A = (tempiSelected-start[i])/semilenFFT
			B = (-tempiSelected+end[i])/semilenFFT
			over50A=numpy.where(A > 0.5)
			A[over50A] = 0.5
			over50B=numpy.where(B > 0.5)
			B[over50B] = 0.5
			condition = numpy.where(A+B >= minPercentage)        
			goodIndices = numpy.append(goodIndices, selectedInd[condition]+firstInd)

	timesFFT = timesFFT-semilenFFT
	for i in numpy.arange(start.size):
		selectedInd = numpy.where(timesFFT < start[i])[0]
		if(selectedInd.size != 0):
			firstInd = selectedInd[0]
			tempiSelected = timesFFT[selectedInd]
			selectedInd = numpy.where(tempiSelected+lenFFT <= end[i])[0]
			tempiSelected = tempiSelected[selectedInd]
            
			#condition 2.a
			A = (tempiSelected+lenFFT-start[i])/lenFFT
			condition = numpy.where(A >= minPercentage)
			goodIndices = numpy.append(goodIndices,selectedInd[condition]+firstInd)
                
	for i in numpy.arange(start.size):
		selectedInd = numpy.where(timesFFT < start[i])[0]
		if(selectedInd.size != 0):
			firstInd = selectedInd[0]
			tempiSelected = timesFFT[selectedInd]
			selectedInd = numpy.where(tempiSelected+lenFFT > end[i])[0]
			
			#condition 2.b
			A = (start[i]-end[i])/lenFFT
			condition = numpy.where(A >= minPercentage)
			goodIndices = numpy.append(goodIndices,selectedInd[condition]+firstInd)
    
	goodIndices = numpy.unique(goodIndices).astype(numpy.int64)

	# initialization of arrays for weights computation
	goodSpots = numpy.zeros(numTimes)
	goodSpots[goodIndices] = 1
	wien = numpy.zeros(spectra.shape)

	# wiener coefficients computation
	newSpectra = numpy.zeros(spectra.shape)
	newSpectra[goodIndices,:] = spectra[goodIndices,:]
	goodSpectra = newSpectra[goodIndices]
	medianPower_f = numpy.median(goodSpectra, axis=0)
	badPeaks = numpy.where(newSpectra < exclusionFactor*medianPower_f)
	newSpectra[badPeaks] = 0
	badPeaks = numpy.where(newSpectra > 10*medianPower_f)
	newSpectra[badPeaks] = 0

    
	for i in numpy.arange(spectra.shape[1]):
		newSpectrum= newSpectra[:,i]
		goodPeaks = numpy.nonzero(newSpectrum)
		wienPeaks = 1/newSpectrum[goodPeaks]
		wien[goodPeaks,i] = wienPeaks/numpy.mean(wienPeaks, axis = 0)
    
#	print(wien.shape)
#	print(wien[:3,:])
#	wiener weighted spectrum
#	spectra = newSpectra*wien
#	num = numpy.sum(spectra, axis = 0)
#	den = numpy.sum(wien, axis = 0)
#	wienerSpectrum = num/den

	# wiener weights for peakmap computation
	for i in numpy.arange(numTimes):
		start = indices[i]-1
		end = indices[i+1]-1
		frequencies = peakmap[1, start:end]
		freqIndices = numpy.round((frequencies - firstFreq)/deltaFreq).astype(numpy.int64)
		peakmap[3, start:end]= numpy.median(wien[i,freqIndices])

	# weights = peakmap[3]
	return peakmap[3]