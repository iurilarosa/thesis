import numpy
import scipy.io

# Simple porting of the SNAG function hfdf_patch
def doppcorr(point, frequencies, indices,  velocities, PAR):
	"""
	Computes the Doppler correction of the frequencies, given the sky point.
	Parameters:
	point : 1D array
		The coordinates of the sky point.
	frequency :  1D array
		The array of frequencies of the peakmap analyzed.
	indices : 1D array
		The time indices of "science" FFTs.
	velocities : 3D array
		The array of the Earth velocities at every FFT time.
	PAR : dict
		All the parameters necessary for the computation of the Hough map
	Returns:
	freqCorr : 1D array
		The array of the Doppler corrected frequencies
	freqIn : float
		The Hough transform matrix
	freqHM : 1D array
		Doppler corrected frequencies suited for the Hough Map computation
	"""
	# Defining the arrays used for array slicing in the for loop
	starts = indices[:-1]-1
	ends = indices[1:]-1
	# Selection of the velocities at the "science" times
	velForCorr = numpy.zeros((3,frequencies.size))
    
	for i in numpy.arange(0, PAR['numTimes']):
		velForCorr[:,starts[i]:ends[i]+1] = velocities[:,i:i+1]
	
	# Doppler correction
	velXpos = numpy.dot(point,velForCorr)
	freqCorr = frequencies / (1+velXpos)
    
	# Computation of the minimum frequency of the Hough map
	freqMin = numpy.amin(freqCorr)
	freqIn = freqMin- PAR['stepFreq']/2 - PAR['refinedStepFreq']
	freqMax = numpy.amax(freqCorr)
	freqFin = freqMax + PAR['stepFreq']/2 + PAR['refinedStepFreq']
	PAR.update({'freqInitialCorr': freqIn})
	PAR.update({'freqFinalCorr': freqFin})
#	PAR.update({'freqInitialbis': PAR["freqInitial"]})
    
	nstepFreqs = numpy.ceil((freqFin-freqIn)/PAR['refinedStepFreq'])+PAR['secbelt']
	PAR.update({'nstepFreqs': nstepFreqs})
    
    
	# Computation of the corrected frequency array which will be passed to the FrequencyHough function
	freqHM = freqCorr-freqIn
	freqHM = (freqHM/PAR['refinedStepFreq'])-numpy.round(PAR['enhance']/2+0.001)
	pathout = "/home/iuri.larosa/master/out/"
	return freqCorr, freqHM
 
