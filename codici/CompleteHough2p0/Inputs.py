import numpy

def loadData(struct, PAR):
	"""
	Defines al the data arrays for Doppler correction, Frequency Hough and Candidate selection functions.
	Parameters:
	struct : dict from Matlab structure
		The structure from the input file.
	PAR : dict
		All the parameters necessary for the computation of the Hough map
	Returns:
	freqStart : float
		The lowest frequency of the Frequency Hough map
	nstepFreqs : int
		The number of frequency steps in the input array
	peakmap : 3D array
		The non-zero elements of the weighted peakmap in a sparse format: time, frequency, weight
	spindowns : 1D array
		The array of spindown values
	velocities : 3D array
		The array of the Earth velocities at every FFT time.
	indices : 1D array
		The time indices of "science" FFTs.
	"""	
	
	# Times
	times = struct['peaks'][0,0][0]
	times = times-PAR['epoch']
	
#	timesec = times*60*60*24
#	min = numpy.amin(timesec)
#	print("AAAAAAAAAAAAAAAAAAAA \n AAAAAAAAAAAAAAAAA", min, numpy.where(timesec > min + 60*60*24*30*6))

	times = ((times)*60*60*24/PAR['refinedStepFreq'])

	# Frequencies
	freqs = struct['peaks'][0,0][1]
	
	freqMin = numpy.amin(freqs)
	freqMax = numpy.amax(freqs)
	freqStart = freqMin- PAR['stepFreq']/2 - PAR['refinedStepFreq']
	freqEnd = freqMax + PAR['stepFreq']/2 + PAR['refinedStepFreq']
	#nstepFreqs = numpy.ceil((freqEnd-freqStart)/PAR['refinedStepFreq'])+PAR['secbelt']
	
	#freqs = freqs-freqStart
	#freqs = (freqs/PAR['refinedStepFreq'])-round(PAR['enhance']/2+0.001)

	# Spindowns
	spindowns = numpy.arange(0, PAR['nstepFdot'])
	spindowns = numpy.multiply(spindowns,PAR['stepFdot'])
	spindowns = numpy.add(spindowns, PAR['fdotMin'])

	# Doppler correction
	velocities = struct['basic_info'][0,0]['velpos'][0,0][0:3,:].astype(numpy.float64)
	indices = struct['basic_info'][0,0]['index'][0,0][0]

	# Once the data of the peakmap are rearranged as above, it's better to rejoin them in an unique array
	peakmap = numpy.stack((times,freqs),1)#.astype(numpy.float32)
	
	return freqStart, peakmap, spindowns, velocities, indices
