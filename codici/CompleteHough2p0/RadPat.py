import numpy
import scipy.interpolate

def compute_radpat(struct, PAR, point):
	"""
	Computes the radiation pattern for the weighted peakmap computation
	Parameters:
	struct : dict from Matlab structure
		The structure from the input file.
	PAR : dict
		All the parameters necessary for the computation of the Hough map.
	point : 1D array
		Sky coordinates of the analyzed point in equatorial coordinates
	Returns:
	radiationPattern : 1D array
		The values of the radiation pattern for each FFT time
	"""
    
	def convert_to_GMST(times):
		times[numpy.where(times<1000000)] = times[numpy.where(times<1000000)]+2400000.5
		t0 = numpy.floor(times-0.5)+0.5
		h = (times-t0)*24
		d = times - 2451545
		d0 = t0 - 2451545
		T = d/36525
		siderealTime = numpy.mod(6.697374558+0.06570982441908*d0+1.00273790935*h+0.000026*T**2,24)
		return siderealTime

	def compute_siderealPattern(det_coordinates, alpha, delta, eta, psi, numpoints_Day):
		lat = det_coordinates['lat'][0,0]*numpy.pi/180
		lon = det_coordinates['long'][0,0]*numpy.pi/180
		azimuth = det_coordinates['azim'][0,0]*numpy.pi/180

		alphalon = numpy.exp(-1j*(alpha-lon))

		a0=-(3/16) * ( 1 + numpy.cos(2*delta)) * (1 + numpy.cos(2*lat)) * numpy.cos(2*azimuth)
		a1c=-(1/4) * numpy.sin(2*delta) * numpy.sin(2*lat) * numpy.cos(2*azimuth)
		a1s=-(1/2) * numpy.sin(2*delta) * numpy.cos(lat) * numpy.sin(2*azimuth)
		a2c=(-1/16) * (3 - numpy.cos(2*delta)) * (3 - numpy.cos(2*lat)) * numpy.cos(2*azimuth)
		a2s=-(1/4) * (3 - numpy.cos(2*delta)) * numpy.sin(lat) * numpy.sin(2*azimuth)

		b1c=-numpy.cos(delta) * numpy.cos(lat) * numpy.sin(2*azimuth)
		b1s=(1/2) * numpy.cos(delta) * numpy.sin(2*lat) * numpy.cos(2*azimuth)
		b2c=-numpy.sin(delta) * numpy.sin(lat) * numpy.sin(2*azimuth)
		b2s=(1/4) * numpy.sin(delta) * (3 - numpy.cos(2*lat)) * numpy.cos(2*azimuth)

		L0 = numpy.zeros(5).astype(numpy.complex64)
		L45 = numpy.zeros(5).astype(numpy.complex64)

		L0[0]=(alphalon**(-2))*(a2c+1j*a2s)/2;
		L0[1]=(alphalon**-1)*(a1c+1j*a1s)/2;
		L0[2]=a0;
		L0[3]=(alphalon)*(a1c-1j*a1s)/2;
		L0[4]=(alphalon**2)*(a2c-1j*a2s)/2;

		L45[0]=(alphalon**-2)*(b2c+1j*b2s)/2;
		L45[1]=(alphalon**-1)*(b1c+1j*b1s)/2;
		L45[2]=0;
		L45[3]=(alphalon)*(b1c-1j*b1s)/2;
		L45[4]=(alphalon**2)*(b2c-1j*b2s)/2;

		Hp=numpy.sqrt(1/(1+eta**2)) * (numpy.cos(2*psi)-1j*eta*numpy.sin(2*psi))
		Hc=numpy.sqrt(1/(1+eta**2)) * (numpy.sin(2*psi)+1j*eta*numpy.cos(2*psi))
		v=Hp*L0+Hc*L45

		step = numpy.arange(numpoints_Day)*2*numpy.pi/numpoints_Day

		lf = 0
		for i in numpy.arange(5):
			lf = lf + v[i]*numpy.exp(1j*(i+1-3)*step)

		siderealPattern = numpy.absolute(lf)**2
		return siderealPattern

	peakmap = struct["peaks"][0,0]
	times = peakmap[0]
	standardTime = convert_to_GMST(times)
    
	alpha = point[0]*numpy.pi/180
	delta = point[1]*numpy.pi/180
	eta = 1
	psi = 0
	det_coord = struct['basic_info'][0,0]['run'][0,0]['ant'][0,0]
	numpoints_Day = 120
	widthStep = 24/numpoints_Day
	siderealPattern = compute_siderealPattern(det_coord, alpha, delta, eta, psi, numpoints_Day)
    
	steps = numpy.arange(0, numpoints_Day)*widthStep

	minStep = numpy.amin(steps)-widthStep/2
	maxStep = numpy.amax(steps)+widthStep/2

	indices = numpy.where(standardTime < minStep)
	standardTime[indices] = minStep
	indices = numpy.where(standardTime > maxStep)
	standardTime[indices] = maxStep

	interpolator = scipy.interpolate.splrep(steps, siderealPattern)
	radiationPattern = scipy.interpolate.splev(standardTime, interpolator, der = 0)
   
	radiationPattern = radiationPattern/numpy.mean(radiationPattern)
	return radiationPattern