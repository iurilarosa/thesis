import tensorflow as tf
import numpy

def frequencyHough(times, frequencies, weights, spindowns, PAR):
    """
    Computes the frequency-Hough transform of a sparse peakmap.
    Parameters:
        times, frequencies : 1D tensors
        The coordinates of the peakmap in sparse format.
        weights :  1D tensor
        The values of the peaks in the sparse peakmap.
        spindowns : 1D tensor
        The spindowns values over which calculate the Hough transform.
        PAR : dict
        All the parameters necessary for the computation of the Hough map
    Returns:
        houghMap : 2D tensor
        The Hough transform matrix
    """
    # these variables are only redifinition of old variable to clarify better the following code
    nRows = numpy.int32(PAR['nstepFdot'])
    nColumns = numpy.int32(PAR['nstepFreqs'])
    enhance = numpy.int32(PAR['enhance'])

    #WARNING uncomment the following line for 64bit data
    PAR_secbeltTF = tf.constant(PAR['secbelt'],dtype = tf.float64, name = 'secur')
    #WARNING uncomment the following line for 32bit data
    #PAR_secbeltTF = tf.constant(PAR['secbelt'],dtype = tf.float32, name = 'secur')	

    # this function computes the Hough transform histogram for a given spindown
    times = tf.reshape(times,[1,times.size])
    weights = tf.reshape(weights,[1,weights.size])

    freqShift = tf.matmul(spindowns,times)
    matrix_w = tf.matmul(tf.ones([nRows,1]),weigths)

    shifted = -1*(freqShift-frequencies)
    offsetted = shifted+PAR_secbeltTF
    shiftedFreqs = tf.round(offsetted)
    shiftedFreqs = tf.cast(shiftedFreqs, dtype=tf.int32)
    houghLeft = tf.math.bincount(shiftedFreqs,weights=matrix_w, minlength = nColumns,axis = -1)

    # let's superimpose the right edge on the image

    leftStripe = tf.slice(houghLeft, [0,0], [houghLeft.get_shape()[0],enhance], name = "left_stripe")
    houghRight = tf.subtract(tf.slice(houghLeft, 
    [0,enhance],
    [houghLeft.get_shape()[0],houghLeft.get_shape()[1]-enhance]),
    tf.slice(houghLeft,
    [0,0],
    [houghLeft.get_shape()[0], houghLeft.get_shape()[1]-enhance]),
    name = "right_stripe")

    # now we have the so called differential hough map
    houghDiff = tf.concat([leftStripe,houghRight],1, name = "diff_hough")
    # and at last we can cumulative sum along the rows to have the integral hough map
    houghMap = tf.cumsum(houghDiff, axis = 1, name = "output")
    return houghMap
