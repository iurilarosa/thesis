import __main__
import numpy

def parDefs(struct):
	"""
	Defines all the parameters needed by the pipeline.
	Parameters:
	struct : dict from Matlab structure
		The structure from the input file.
	Returns:
	parameters : dict
		One dictionary with all the parameters included
	"""
    
	# Frequencies
	enhance = 10
	stepFreq = struct["basic_info"][0,0]["dfr"][0,0][0,0]
	refinedStepFreq =  stepFreq/enhance

	# Times
	tFft = numpy.int32(1/stepFreq)
	numTimes = struct["basic_info"][0,0]["ntim"][0,0][0,0]
	tObs = tFft*numTimes/(2*60*60*24*30) # mesi
	#print(tObs, " mesi")
	tObs = numpy.round(tObs*30*24*60*60)
	epoch = struct["basic_info"][0,0]["epoch"][0,0][0,0]

	# Cands
	numCand =100
	if(tFft == 8192):
		numCand = 20
	numStripes = 1
    
	# Spindowns
	fdotMin = -1e-8
	fdotMax = 1e-9
	stepFdot = stepFreq/tObs
	nstepFdot = numpy.round((fdotMax-fdotMin)/stepFdot).astype(numpy.int32)
	extraBins = numpy.mod(nstepFdot,numStripes)
	nstepFdot = numpy.round(nstepFdot + extraBins).astype(numpy.int32)
	fdotMax = fdotMax + extraBins*stepFdot
    
	# Others
	secbelt = 4000
	if(tFft == 4096):
		secbelt = 1000#*2
        
    
	freqInitial = struct['basic_info'][0,0]['frin'][0,0][0,0]
	freqFinal = struct['basic_info'][0,0]['frfi'][0,0][0,0]

	# Defining a dictionary with all the parameters needed
	parameters = {
			'enhance' : enhance,
			'stepFreq' : stepFreq,
			'refinedStepFreq' : refinedStepFreq,
			
			'tFft' : tFft,
			'numTimes': numTimes,
			'tObs' : tObs,
			'epoch' : epoch,
			
			'fdotMin' : fdotMin,
			'fdotMax' : fdotMax,
			'stepFdot' : stepFdot,
			'nstepFdot' : nstepFdot,
			
			'secbelt' : secbelt,
			
			'numCand' : numCand,
			'numStripes' : numStripes,
			'freqInitial' : freqInitial,
			'freqFinal' : freqFinal
		}
	
	return parameters
