#Passing arguments to main function
import warnings
warnings.filterwarnings("ignore")
import sys, getopt
import os

def usage():
	msg = """\
	-h, --help       display this message
	-j, --job-id     job ID
	-n, --filename   name of the input filex
	-d, --detector   code of the detector (LH, LL, AV)
	-p, --points     number of points per job"""
	print(msg)

# Initialise command line argument variables
if sys.argv[1] == ("--help"):
    usage()
    sys.exit(1)

job_id = -1.0
filename = -1.0
detector = -1.0
pointsPerJob = -1.0
path = "./"

job_id = int(sys.argv[2])
filename = sys.argv[4]
detector = sys.argv[6]
pointsPerJob = sys.argv[8]

if job_id == -1.0:
    print(sys.stderr, "No job number specified.")
    print(sys.stderr, "Use --job-id to specify it.")
    usage()
    sys.exit(1)

if filename == -1.0:
    print(sys.stderr, "No file name specified.")
    print(sys.stderr, "Use --filename to specify it.")
    usage()
    sys.exit(1)
    
if detector == -1.0:
    print(sys.stderr, "No detector specified.")
    print(sys.stderr, "Use --detector to specify it.")
    usage()
    sys.exit(1)
    
if pointsPerJob == -1.0:
    print(sys.stderr, "No detector specified.")
    print(sys.stderr, "Use --detector to specify it.")
    usage()
    sys.exit(1)

#-----------
# MAIN CODE |
#-----------
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' # tf log errors only
import tensorflow as tf
import logging
#logging.getLogger('tensorflow').setLevel(logging.ERROR)

import numpy
import scipy.io
from PAR import parDefs
from NoiseAdapt import compute_weights
from Inputs import loadData
from GridGen import make_skygrid
from DoppCorr import doppcorr
from RadPat import compute_radpat
from GPUHough import frequencyHough
from GPUCandSel import select_candidates
from Write_out import finalize
import time
print(tf.__version__)
#tf.compat.v1.disable_eager_execution()
#----------------
# Job definition | 
# ---------------
#for the moment working on only one file
run = "O3"
band = "2048Hz"

#pathin = "/home/iuri.larosa/master/inputs/ifil/"
#pathout = "/home/iuri.larosa/master/outputs/"

folderin = "/home/iuri.larosa/master/data/"
folderscience = "/home/iuri.larosa/master/inputs/"


folderout = "/home/iuri.larosa/master/outputs/in_" + run + detector + filename + "/"
logdir = folderout+"log/"
os.system('mkdir ' + folderout)
os.system('mkdir ' + logdir)
pathout = folderout + "out_" + str(job_id)

dataPath = folderin + "in_" + run + detector + filename + ".mat"
sciencePath = folderscience + run + "_" + detector + "_C01_segments_science_withCAT1veto.txt"

pointsPerJob = numpy.int32(pointsPerJob)
points = numpy.arange(job_id*pointsPerJob, job_id*pointsPerJob+pointsPerJob)
print("Computing input file" + folderin + "in_" + run + detector + filename + ".mat")
print("Processing points from " + str(points[0]) + " to " + str(points[-1]))

#---------------------------
# Data loading and managing |
#---------------------------
struct = scipy.io.loadmat(dataPath)['job_pack_0']
PAR = parDefs(struct)

# manage data
[firstFreq, peakmap, spindowns, velocities, indices] = loadData(struct, PAR)

# sky grid generation
[eclCoord, rectCoord, eqCoord] = make_skygrid(PAR)

#--------------------------------------------------
#radiation pattern and weighted peakmap computation|
#--------------------------------------------------
scienceWeights = compute_weights(struct, PAR, sciencePath)

final_candidates = numpy.zeros((PAR['numCand']*PAR['numStripes']*pointsPerJob, 8))

#main loop starts: for each sky point the weighted map is computed, frequencies are Doppler corrected,
#                  FH map is computed and candidates are selected and appended to the final output
j = 0
start = time.time()
for i in points:
	print(j/pointsPerJob)    
	if i > eclCoord.shape[0]:
		print("Input File " + filename + "  analyzed")
		sys.exit()
	
	radiationPattern = compute_radpat(struct, PAR, eqCoord[i])
	weights = radiationPattern*scienceWeights
	weights = weights/numpy.mean(weights)

	#--------------------
	# Doppler correction |
	#--------------------
	pointECL = eclCoord[i]
	pointRECT = rectCoord[i]

	# dopp corr
	[freqNew, freqHM] = doppcorr(pointRECT, peakmap[:,1], indices, velocities, PAR)

	#-----------
	# Hough Map |
	#-----------
	#tf.profiler.experimental.start(logdir)
	timesTF = tf.constant(peakmap[:,0])
	freqsTF = tf.constant(freqHM)
	weightsTF = tf.constant(weights)
	spindownsTF = tf.constant(spindowns)

	#FH lateral stripes already removed
	# same as cut_gd2 SNAG function
	freqStart = numpy.amin(freqNew) - PAR['stepFreq']/2 - PAR['refinedStepFreq']
	indexInitial = ((PAR['freqInitial']-freqStart)/PAR['refinedStepFreq']+PAR['secbelt']/2).astype(numpy.int32)
	indexFinal = numpy.int(indexInitial + PAR["tFft"]/2*PAR['numCand'])
	startHough = time.time()
	FHMapCand = frequencyHough(timesTF, freqsTF, weightsTF, spindownsTF, PAR)[:,indexInitial:indexFinal]
	print(time.time()-startHough)
	totWidth = tf.cast(tf.shape(FHMapCand)[1], dtype= tf.int32)
	totHeight = tf.cast(tf.shape(FHMapCand)[0], dtype = tf.int32)

	#---------------------
	# Candidate selection |
	#---------------------
	startCand = time.time()
	[PAR_stripeHeight, PAR_beltWidth, TFcands] = select_candidates(FHMapCand, totWidth, totHeight, pointECL, PAR)
	print(time.time()-startCand)    
	cands = TFcands.numpy()
	PAR_stripeHeight = PAR_stripeHeight.numpy()
	PAR_beltWidth = PAR_beltWidth.numpy()
	#tf.keras.backend.clear_session()
	#tf.profiler.experimental.stop()
	#the candidates array coming from the TensorFlow code are rearranged and appended to a unique array    
	cands = finalize(cands, PAR_stripeHeight, PAR_beltWidth, spindowns, PAR)
	top = PAR['numCand']*PAR['numStripes']*j
	bottom = PAR['numCand']*PAR['numStripes']*(j+1)
	j = j + 1
	final_candidates[top:bottom,:] = cands
print("AAAAAAAA\n\n", time.time()-start, "\n\n")
#the candidates array of the submitted job is saved
scipy.io.savemat(pathout + ".mat", {"cands" : final_candidates})

