import glob
import os
import numpy
import scipy.io
from PAR import parDefs
from GridGen import make_skygrid

pathslist = glob.glob("/home/iuri.larosa/master/data/4096Hz/LH/*")
pathslist = numpy.sort(numpy.array(pathslist))
fileslist = pathslist

time_per_iter = 5.5
npoints = numpy.zeros(fileslist.size)

for i in numpy.arange(fileslist.size):
    print(i)
    dataPath = fileslist[i]
    struct = scipy.io.loadmat(dataPath)['job_pack_0']
    PAR = parDefs(struct)
    # sky grid generation
    [eclCoord, rectCoord, eqCoord] = make_skygrid(PAR)
    npoints[i] = eclCoord.shape[0]
    print(npoints[i])
npoints = npoints.astype(int)
numpy.savetxt('gridsizes.txt',npoints, fmt = '%d')
    
time_per_file = time_per_iter*npoints

ndetectors = 2
jobtime = 12 #ore per detector (2 detectors)
totjobtimeH = jobtime*ndetectors
jobtime = jobtime*3600

njobs = numpy.ceil(time_per_file/jobtime).astype(int)

pointsPerJob = numpy.ceil(npoints/njobs).astype(int)


print(npoints, njobs, pointsPerJob)
